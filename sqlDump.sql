--
-- PostgreSQL database dump
--

-- Dumped from database version 13.6 (Debian 13.6-1.pgdg110+1)
-- Dumped by pg_dump version 13.6 (Debian 13.6-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application (
    tool_name character varying(100) NOT NULL,
    domain character varying(100),
    rowid bigint NOT NULL
);


ALTER TABLE public.application OWNER TO postgres;

--
-- Name: TABLE application; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.application IS 'Specialized domain for a tool (environmental metabolomics, model organism, etc)';


--
-- Name: approach; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.approach (
    tool_name character varying(100) NOT NULL,
    approach character varying(100),
    rowid bigint NOT NULL
);


ALTER TABLE public.approach OWNER TO postgres;

--
-- Name: TABLE approach; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.approach IS 'Metabolomics Approach (untargeted, targeted, isotopic labeling analysis, imaging)';


--
-- Name: approach_domain_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.approach_domain_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.approach_domain_rowid_seq OWNER TO postgres;

--
-- Name: approach_domain_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.approach_domain_rowid_seq OWNED BY public.application.rowid;


--
-- Name: molecule_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.molecule_type (
    tool_name character varying(100) NOT NULL,
    moltype character varying(100),
    rowid bigint NOT NULL
);


ALTER TABLE public.molecule_type OWNER TO postgres;

--
-- Name: TABLE molecule_type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.molecule_type IS 'Omics type applicable to tool (genomics, proteomics, transcriptomics, metabolomics, glycomics, lipidomics)';


--
-- Name: approach_moltype_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.approach_moltype_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.approach_moltype_rowid_seq OWNER TO postgres;

--
-- Name: approach_moltype_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.approach_moltype_rowid_seq OWNED BY public.molecule_type.rowid;


--
-- Name: approach_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.approach_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.approach_rowid_seq OWNER TO postgres;

--
-- Name: approach_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.approach_rowid_seq OWNED BY public.approach.rowid;


--
-- Name: citations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.citations (
    tool_name character varying NOT NULL,
    citations integer NOT NULL,
    rowid integer NOT NULL
);


ALTER TABLE public.citations OWNER TO postgres;

--
-- Name: citations_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.citations ALTER COLUMN rowid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.citations_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: containers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.containers (
    tool_name character varying(100) NOT NULL,
    container_url character varying(255) NOT NULL,
    rowid bigint NOT NULL
);


ALTER TABLE public.containers OWNER TO postgres;

--
-- Name: TABLE containers; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.containers IS 'URLs for containerized versions of tool';


--
-- Name: containers_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.containers_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.containers_rowid_seq OWNER TO postgres;

--
-- Name: containers_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.containers_rowid_seq OWNED BY public.containers.rowid;


--
-- Name: file_formats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.file_formats (
    tool_name character varying(100) NOT NULL,
    format character varying(100),
    rowid bigint NOT NULL
);


ALTER TABLE public.file_formats OWNER TO postgres;

--
-- Name: TABLE file_formats; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.file_formats IS 'File Formats that the tool reads or writes';


--
-- Name: file_formats_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.file_formats_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.file_formats_rowid_seq OWNER TO postgres;

--
-- Name: file_formats_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.file_formats_rowid_seq OWNED BY public.file_formats.rowid;


--
-- Name: output_variables; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.output_variables (
    tool_name character varying(100) NOT NULL,
    output character varying(32) NOT NULL,
    rowid bigint NOT NULL
);


ALTER TABLE public.output_variables OWNER TO postgres;

--
-- Name: TABLE output_variables; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.output_variables IS 'Output Variables and Data Structure';


--
-- Name: formats_output_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.formats_output_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.formats_output_rowid_seq OWNER TO postgres;

--
-- Name: formats_output_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.formats_output_rowid_seq OWNED BY public.output_variables.rowid;


--
-- Name: func_annotation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.func_annotation (
    tool_name character varying(100) NOT NULL,
    annotation character varying(255),
    rowid bigint NOT NULL
);


ALTER TABLE public.func_annotation OWNER TO postgres;

--
-- Name: TABLE func_annotation; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.func_annotation IS 'annotation approaches (MS2a, NMR, etc) , Compound Identificaiton';


--
-- Name: func_annotation_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.func_annotation_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.func_annotation_rowid_seq OWNER TO postgres;

--
-- Name: func_annotation_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.func_annotation_rowid_seq OWNED BY public.func_annotation.rowid;


--
-- Name: func_interpretation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.func_interpretation (
    tool_name character varying(100) NOT NULL,
    interpret character varying(255),
    rowid bigint NOT NULL
);


ALTER TABLE public.func_interpretation OWNER TO postgres;

--
-- Name: TABLE func_interpretation; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.func_interpretation IS 'biological interpretation uses (pathway analysis, disease annotation, etc)';


--
-- Name: func_interpretation_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.func_interpretation_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.func_interpretation_rowid_seq OWNER TO postgres;

--
-- Name: func_interpretation_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.func_interpretation_rowid_seq OWNED BY public.func_interpretation.rowid;


--
-- Name: func_postprocessing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.func_postprocessing (
    tool_name character varying(100) NOT NULL,
    postprocessing character varying(255),
    rowid bigint NOT NULL
);


ALTER TABLE public.func_postprocessing OWNER TO postgres;

--
-- Name: TABLE func_postprocessing; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.func_postprocessing IS 'postprocessing method (centering, imputation, normalization, scaling, transformation, filtering)';


--
-- Name: func_postprocessing_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.func_postprocessing_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.func_postprocessing_rowid_seq OWNER TO postgres;

--
-- Name: func_postprocessing_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.func_postprocessing_rowid_seq OWNED BY public.func_postprocessing.rowid;


--
-- Name: func_preprocessing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.func_preprocessing (
    tool_name character varying(100) NOT NULL,
    preprocessing character varying(255),
    rowid bigint NOT NULL
);


ALTER TABLE public.func_preprocessing OWNER TO postgres;

--
-- Name: func_preprocessing_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.func_preprocessing_rowid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.func_preprocessing_rowid_seq OWNER TO postgres;

--
-- Name: func_preprocessing_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.func_preprocessing_rowid_seq OWNED BY public.func_preprocessing.rowid;


--
-- Name: func_processing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.func_processing (
    tool_name character varying(100) NOT NULL,
    processing character varying(255),
    rowid bigint NOT NULL
);


ALTER TABLE public.func_processing OWNER TO postgres;

--
-- Name: func_processing_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.func_processing_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.func_processing_rowid_seq OWNER TO postgres;

--
-- Name: func_processing_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.func_processing_rowid_seq OWNED BY public.func_processing.rowid;


--
-- Name: func_statistics; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.func_statistics (
    tool_name character varying(100) NOT NULL,
    stats_method character varying(255),
    rowid bigint NOT NULL
);


ALTER TABLE public.func_statistics OWNER TO postgres;

--
-- Name: TABLE func_statistics; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.func_statistics IS 'Statistics Methods used by metabolomics tools not linked to data processing';


--
-- Name: func_statistics_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.func_statistics_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.func_statistics_rowid_seq OWNER TO postgres;

--
-- Name: func_statistics_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.func_statistics_rowid_seq OWNED BY public.func_statistics.rowid;


--
-- Name: functionality; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.functionality (
    tool_name character varying(100) NOT NULL,
    function character varying(100),
    rowid bigint NOT NULL
);


ALTER TABLE public.functionality OWNER TO postgres;

--
-- Name: TABLE functionality; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.functionality IS 'pipeline stage of tool (Experimental Design, PreProcessing, Annotation, Quality Assessment, PostProcessing, Statistical Analysis, biological interpretation, workflow)';


--
-- Name: functionality_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.functionality_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.functionality_rowid_seq OWNER TO postgres;

--
-- Name: functionality_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.functionality_rowid_seq OWNED BY public.functionality.rowid;


--
-- Name: input_variables; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.input_variables (
    tool_name character varying(100) NOT NULL,
    input character varying(32),
    rowid bigint NOT NULL
);


ALTER TABLE public.input_variables OWNER TO postgres;

--
-- Name: TABLE input_variables; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.input_variables IS 'Input Variables and Data Structure';


--
-- Name: input_variables_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.input_variables_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.input_variables_rowid_seq OWNER TO postgres;

--
-- Name: input_variables_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.input_variables_rowid_seq OWNED BY public.input_variables.rowid;


--
-- Name: instrument_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.instrument_data (
    tool_name character varying(100) NOT NULL,
    instrument character varying(100),
    rowid bigint NOT NULL
);


ALTER TABLE public.instrument_data OWNER TO postgres;

--
-- Name: TABLE instrument_data; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.instrument_data IS 'instrument data type (for now single table)';


--
-- Name: instrument_data_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.instrument_data_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instrument_data_rowid_seq OWNER TO postgres;

--
-- Name: instrument_data_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.instrument_data_rowid_seq OWNED BY public.instrument_data.rowid;


--
-- Name: main; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.main (
    tool_name character varying(100) NOT NULL,
    year_released integer,
    website character varying(255),
    date_modified date NOT NULL,
    last_updated date,
    notes text
);


ALTER TABLE public.main OWNER TO postgres;

--
-- Name: TABLE main; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.main IS 'Main Table';


--
-- Name: network_visualization_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.network_visualization_data (
    index bigint,
    tool_name text,
    count bigint,
    "PMID" text,
    start_position bigint
);


ALTER TABLE public.network_visualization_data OWNER TO postgres;

--
-- Name: os_options; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.os_options (
    tool_name character varying(100) NOT NULL,
    os character varying(32),
    rowid bigint NOT NULL
);


ALTER TABLE public.os_options OWNER TO postgres;

--
-- Name: TABLE os_options; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.os_options IS 'Operating System Options';


--
-- Name: os_options_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.os_options_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.os_options_rowid_seq OWNER TO postgres;

--
-- Name: os_options_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.os_options_rowid_seq OWNED BY public.os_options.rowid;


--
-- Name: programming_language; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.programming_language (
    tool_name character varying(100) NOT NULL,
    language character varying(100) NOT NULL,
    rowid bigint NOT NULL
);


ALTER TABLE public.programming_language OWNER TO postgres;

--
-- Name: TABLE programming_language; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.programming_language IS 'Tool programming languages';


--
-- Name: programming_language_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.programming_language_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.programming_language_rowid_seq OWNER TO postgres;

--
-- Name: programming_language_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.programming_language_rowid_seq OWNED BY public.programming_language.rowid;


--
-- Name: publications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.publications (
    rowid integer NOT NULL,
    tool_name character varying(100) NOT NULL,
    pub_doi character varying(100)
);


ALTER TABLE public.publications OWNER TO postgres;

--
-- Name: publications_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.publications ALTER COLUMN rowid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.publications_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: software_license; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.software_license (
    tool_name character varying(100) NOT NULL,
    license character varying(100) NOT NULL,
    rowid bigint NOT NULL
);


ALTER TABLE public.software_license OWNER TO postgres;

--
-- Name: TABLE software_license; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.software_license IS 'Licenses of software tool (GPL, MIT, etc)';


--
-- Name: software_license_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.software_license_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.software_license_rowid_seq OWNER TO postgres;

--
-- Name: software_license_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.software_license_rowid_seq OWNED BY public.software_license.rowid;


--
-- Name: ui_options; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ui_options (
    tool_name character varying(100) NOT NULL,
    ui character varying(10) NOT NULL,
    rowid bigint NOT NULL
);


ALTER TABLE public.ui_options OWNER TO postgres;

--
-- Name: TABLE ui_options; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.ui_options IS 'User Interface Options (CLI, GUI, Web)';


--
-- Name: ui_options_rowid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ui_options_rowid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ui_options_rowid_seq OWNER TO postgres;

--
-- Name: ui_options_rowid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ui_options_rowid_seq OWNED BY public.ui_options.rowid;


--
-- Name: application rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application ALTER COLUMN rowid SET DEFAULT nextval('public.approach_domain_rowid_seq'::regclass);


--
-- Name: approach rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.approach ALTER COLUMN rowid SET DEFAULT nextval('public.approach_rowid_seq'::regclass);


--
-- Name: containers rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.containers ALTER COLUMN rowid SET DEFAULT nextval('public.containers_rowid_seq'::regclass);


--
-- Name: file_formats rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_formats ALTER COLUMN rowid SET DEFAULT nextval('public.file_formats_rowid_seq'::regclass);


--
-- Name: func_annotation rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_annotation ALTER COLUMN rowid SET DEFAULT nextval('public.func_annotation_rowid_seq'::regclass);


--
-- Name: func_interpretation rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_interpretation ALTER COLUMN rowid SET DEFAULT nextval('public.func_interpretation_rowid_seq'::regclass);


--
-- Name: func_postprocessing rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_postprocessing ALTER COLUMN rowid SET DEFAULT nextval('public.func_postprocessing_rowid_seq'::regclass);


--
-- Name: func_preprocessing rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_preprocessing ALTER COLUMN rowid SET DEFAULT nextval('public.func_preprocessing_rowid_seq'::regclass);


--
-- Name: func_processing rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_processing ALTER COLUMN rowid SET DEFAULT nextval('public.func_processing_rowid_seq'::regclass);


--
-- Name: func_statistics rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_statistics ALTER COLUMN rowid SET DEFAULT nextval('public.func_statistics_rowid_seq'::regclass);


--
-- Name: functionality rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.functionality ALTER COLUMN rowid SET DEFAULT nextval('public.functionality_rowid_seq'::regclass);


--
-- Name: input_variables rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.input_variables ALTER COLUMN rowid SET DEFAULT nextval('public.input_variables_rowid_seq'::regclass);


--
-- Name: instrument_data rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_data ALTER COLUMN rowid SET DEFAULT nextval('public.instrument_data_rowid_seq'::regclass);


--
-- Name: molecule_type rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.molecule_type ALTER COLUMN rowid SET DEFAULT nextval('public.approach_moltype_rowid_seq'::regclass);


--
-- Name: os_options rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.os_options ALTER COLUMN rowid SET DEFAULT nextval('public.os_options_rowid_seq'::regclass);


--
-- Name: output_variables rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.output_variables ALTER COLUMN rowid SET DEFAULT nextval('public.formats_output_rowid_seq'::regclass);


--
-- Name: programming_language rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programming_language ALTER COLUMN rowid SET DEFAULT nextval('public.programming_language_rowid_seq'::regclass);


--
-- Name: software_license rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.software_license ALTER COLUMN rowid SET DEFAULT nextval('public.software_license_rowid_seq'::regclass);


--
-- Name: ui_options rowid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ui_options ALTER COLUMN rowid SET DEFAULT nextval('public.ui_options_rowid_seq'::regclass);


--
-- Data for Name: application; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.application (tool_name, domain, rowid) FROM stdin;
mQTL.NMR	human	1
mQTL.NMR	rodent	2
NP-StructurePredictor	plant	3
PAMBD	Pseudomonas aeruginosa	4
LiverWiki	Liver	5
SoyKB	soy bean	6
YMDB	yeast	7
ConsensusPathDB	human	8
ConsensusPathDB	mouse	9
ConsensusPathDB	yeast	10
MESSI	yeast	11
MeRy-B	plant	12
GabiPD	plant	13
ZEAMAP	Maize	14
MAIMS	Metabolic tracer analysis	15
MAIMS	Stable isotope resolved metabolomics	16
SigMa	urine	17
fobitools	food biomarker ontology	18
fobitoolsGUI	food biomarker ontology	19
MelonnPan	microbiology	20
\.


--
-- Data for Name: approach; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.approach (tool_name, approach, rowid) FROM stdin;
Binner	untargeted	1
XCMS	untargeted	2
GSimp	targeted	3
GSimp	untargeted	4
TargetSearch	targeted	5
MRMPROBS	targeted	6
MS-FINDER	untargeted	7
MetTailor	untargeted	8
metaMS	untargeted	9
MetaboliteDetector	targeted	10
MetaboliteDetector	untargeted	11
metabomxtr	untargeted	12
MAGMa	untargeted	13
credential	untargeted	14
MSPrep	untargeted	15
decoMS2	untargeted	16
SIMAT	targeted	17
CSI:FingerrID	untargeted	18
Lipid-Pro	untargeted	19
MetFamily	untargeted	20
XCMS online	untargeted	21
XCMS online	targeted	22
Mummichog	untargeted	23
X13CMS	untargeted	24
METLIN	untargeted	25
CliqueMS	untargeted	26
MetaboClust	untargeted	27
nPYc-Toolbox	targeted	28
Skyline	targeted	29
MetMatch	untargeted	30
xMSannotator	targeted	31
MINEs	untargeted	32
UC2	untargeted	33
SmileMS	targeted	34
SmileMS	untargeted	35
MetaboGroup S	untargeted	36
TOXcms	untargeted	37
Ideom	targeted	38
FragPred	targeted	39
MSClust	untargeted	40
MetShot	untargeted	41
MetaDB	untargeted	42
CluMSID	untargeted	43
ADAP/MZmine	untargeted	44
Warpgroup	untargeted	45
HiCoNet	untargeted	46
KIMBLE	targeted	47
KIMBLE	untargeted	48
DeepRiPP	untargeted	49
msPurity	targeted	50
MZmine 2	targeted	51
MZmine 2	untargeted	52
mz.unity	targeted	53
mz.unity	untargeted	54
MetabNet	targeted	55
ropls	targeted	56
MetFusion	untargeted	57
eRah	targeted	58
batchCorr	untargeted	59
AMDORAP	untargeted	60
CROP	untargeted	61
Qemistree	untargeted	62
Notame	untargeted	63
MetaboShiny	untargeted	64
MetNormalizer	untargeted	65
MetaboloDerivatizer	untargeted	66
AlpsNMR	untargeted	67
ADAP-GC	untargeted	68
MS-FLO	untargeted	69
MetaboQC	untargeted	70
AntDAS	untargeted	71
TidyMS	untargeted	72
Autotuner	untargeted	73
MetumpX	untargeted	74
VOCCluster	untargeted	75
HastaLaVista	untargeted	76
DecoMetDIA	untargeted	77
misaR	untargeted	78
SLAW	untargeted	79
MS2Query	untargeted	80
MET-COFEA	untargeted	81
MetaboLyzer	untargeted	82
TracMass2	untargeted	83
PARADISe	untargeted	84
TNO-DECO	targeted	85
Metabolomics-Filtering	untargeted	86
xMSanalyzer	untargeted	87
swathTUNER	targeted	88
\.


--
-- Data for Name: citations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.citations (tool_name, citations, rowid) FROM stdin;
CluMSID	5	1
Lilikoi	17	2
MetaboClust	6	3
VMH	110	4
METLIN	1443	5
apLCMS	197	6
TracMass2	30	7
X13CMS	105	8
ProbMetab	44	9
Mummichog	388	10
DeepRiPP	34	11
AMDORAP	14	12
iMet-Q	10	13
intCor	42	14
Ionwinze	1	15
specmine	28	16
msPurity	26	17
Normalyzer	132	18
MVAPACK	69	19
mQTL.NMR	21	20
MetFlow	15	21
Zero-fill	53	22
XCMS online	721	23
BRAIN	25	24
MetFamily	54	25
COVAIN	52	26
MetMSLine	21	27
CCPN Metabolomics	2	28
Lipid-Pro	23	29
iPath	74	30
metabnorm	37	31
COLMAR	29	32
iontree	14	33
ALEX123	109	34
Pathview	122	35
LDA	131	36
KIMBLE	11	37
BioCyc	20	38
jQMM	12	39
RankProd	54	40
GNPS	1327	41
MetaboLab	79	42
Galaxy-M	61	43
ICT	16	44
FALCON	16	45
IsoCor	187	46
IPO	161	47
MESSI	0	48
LIMSA	175	49
LipidXplorer	193	50
MAGMa	80	51
Maui-VIA	11	52
MAVEN	350	53
MeltDB	57	54
MET-COFEA	44	55
PathVisio	245	56
MET-IDEA	199	57
MET-XAlign	23	58
Metab	85	59
MetaboHunter	81	60
MetabolAnalyze	77	61
MetaboliteDetector	261	62
MetaboLyzer	71	63
metabomxtr	14	64
CGBayesNets	42	65
MetaboSearch	57	66
MetaDB	18	67
MetAlign	126	68
metaMS	55	69
MetaQuant	49	70
MetExtract	56	71
MetFrag	382	72
MetFusion	129	73
MetiTree	19	74
MetNorm	95	75
MetShot	23	76
MetTailor	1	77
MI-PACK	72	78
MMSAT	18	79
MRMPROBS	60	80
MZedDB	114	81
mzMatch	212	82
MS-LAMP	19	83
MS2Analyzer	59	84
MSClust	127	85
MS-FINDER	235	86
muma	59	87
MyCompoundID	137	88
mzR	578	89
nmrglue	141	90
QCScreen	13	91
Escher	183	92
RMassBank	67	93
ropls	364	94
SIMAT	5	95
QPMASS	7	96
SpectConnect	188	97
TargetSearch	136	98
vaLID	10	99
Workflow4Metabolomics	216	100
xMSanalyzer	182	101
MetaBox	7	102
MetaMapR	78	103
MetSign	48	104
MIDAS	73	105
ChromA	27	106
GAVIN	77	107
HAMMER	32	108
icoshift	168	109
InCroMAP	33	110
IIS	36	111
LICRE	7	112
MathDAMP	122	113
MetabNet	34	114
MetaboQuant	15	115
PAPi	75	116
TNO-DECO	18	117
BiNCHE	26	118
swathTUNER	102	119
IsoMS	53	120
polyPK	8	121
nPYc-Toolbox	12	122
MetaboNetworks	52	123
MultiAlign	21	124
OPTIMAS-DW	35	125
DiffCorr	97	126
MassTRIX	162	127
MeRy-B	14	128
DrugBank	2047	129
CliqueMS	29	130
SMPDB	182	131
KeggArray	80	132
MetaboDiff	17	133
Pachyderm	17	134
MarVis-Suite	30	135
NOMAD	13	136
MetaboAnalystR	236	137
MetaboAnalyst	1043	138
Automics	58	139
BATMAN	111	140
CAMERA	603	141
HMDB	1846	142
SAMMI	2	143
credential	55	144
decoMS2	50	145
FragPred	14	146
MetaboMiner	141	147
XCMS	2758	148
batchCorr	72	149
crmn	116	150
CSI:FingerrID	356	151
eMZed	34	152
eRah	61	153
FingerID	106	154
MetaBridge	2	155
VSClust	11	156
MINEs	130	157
CMM	47	158
IntLIM	14	159
GSimp	34	160
SistematX	23	161
MINMA	7	162
Curatr	9	163
BinBase	117	164
WikiPathways	16	165
NP-StructurePredictor	4	166
PAMBD	26	167
mixOmics	902	168
UC2	6	169
proFIA	6	170
MetaNetter	44	171
LiverWiki	3	172
SysteMHC	76	173
MetaComp	8	174
MetScape	251	175
Binner	13	176
RAMClustR	136	177
MSPrep	34	178
rNMR	136	179
ALLocater	21	180
AMDIS	20	181
Metabolomics-Filtering	32	182
missForest	1229	183
MetImp	139	184
ChemDistiller	25	185
MetExplore	54	186
Pathos	41	187
PaintOmics	109	188
VANTED	13	189
Ideom	210	190
mz.unity	42	191
Warpgroup	16	192
KMDA	23	193
CFM-ID	255	194
TriDAMP	8	195
Meta P-server	42	196
MZmine 2	1782	197
Bayesil	140	198
MAIT	52	199
TOXcms	6	200
MetDisease	15	201
Rhea	22	202
UniProtKB	14	203
MetaboLights	99	204
MetaboGroup S	18	205
E-Dragon	1002	206
PhenoMeNal	30	207
MoDentify	8	208
HappyTools	7	209
PiMP	24	210
MWASTools	13	211
RDFIO	1	212
pyQms	14	213
KPIC	12	214
mzAccess	8	215
SmileMS	63	216
SDAMS	1	217
MetCirc	16	218
PathBank	24	219
IsoMS-Quant	53	220
MetCCS Predictor	41	221
compMS2Miner	18	222
PathWhiz	0	223
SpectraClassifier	3	224
SoyKB	56	225
xMSannotator	136	226
ChemSpider	548	227
LIPID MAPS	1	228
PlantMAT	43	229
YMDB	80	230
MetMatch	4	231
PathoLogic	78	232
ConsensusPathDB	506	233
InterpretMSSpectrum	14	234
BatMass	27	235
SMART	18	236
Greazy	40	237
NMRPro	9	238
PARADISe	103	239
GAM	50	240
Skyline	42	241
ORCA	13	242
libChEBI	350	243
El-MAVEN	17	244
MaConDa	21	245
GNPS-MassIVE	1327	246
BiGG Models	419	247
mineXpert	3	248
MetMask	33	249
PeroxisomeDB	73	250
GabiPD	25	251
BioNetApp	3	252
RaMP	15	253
CROP	5	254
Qemistree	16	255
Notame	19	256
MetaboShiny	3	257
Viime	2	258
NOREVA	30	259
JS-MS	6	260
JS-MS	6	261
pyMolNetEnhancer	76	262
RMolNetEnhancer	76	263
ZEAMAP	16	264
MassComp	2	265
iMet	30	266
MetNormalizer	74	267
tmod	20	268
PYQUAN	9	269
MAIMS	7	270
\.


--
-- Data for Name: containers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.containers (tool_name, container_url, rowid) FROM stdin;
MetaboShiny	https://hub.docker.com/r/jcwolthuis/metaboshiny	1
Viime	https://hub.docker.com/r/viime/opencpu	2
POMAShiny	https://hub.docker.com/repository/docker/pcastellanoescuder/pomashiny	3
NMRProcFlow	https://hub.docker.com/r/nmrprocflow/nmrprocflow	4
SLAW	https://hub.docker.com/r/zambonilab/slaw	5
\.


--
-- Data for Name: file_formats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.file_formats (tool_name, format, rowid) FROM stdin;
COVAIN	.csv	1
XCMS online	.d	2
LDA	.chrom	3
ALEX123	.raw	4
ALEX123	.txt	5
XCMS online	.mzData	6
XCMS online	.cdf	7
XCMS online	.wiff	8
XCMS online	.wiff.scan	9
XCMS online	mz.Data.XML	10
MS2Analyzer	.MSP	11
ALLocater	.raw	12
MetaboliteDetector	.netCDF	13
AMDIS	.ISL	14
CAMERA	.csv	15
CAMERA	.txt	16
RAMClustR	.csv	17
DiffCorr	.csv	18
MetaQuant	.csv	19
MetaQuant	.XML	20
iMet-Q	.mzXML	21
AMDORAP	.mzXML	22
AMDORAP	.netCDF	23
MetaboMiner	.png	24
Ionwinze	.netCDF	25
iMet-Q	.netCDF	26
MetaMapR	.txt	27
XCMS	.AIA/.ANDI	28
mineXpert	.mzXML	29
polyPK	.csv	30
QPMASS	.netCDF	31
QPMASS	.mzXML	32
MSClust	.txt	33
ChromA	.netCDF	34
MetTailor	.mzML	35
Binner	.tsv	36
rNMR	.xls	37
MetAlign	.mzData	38
MetAlign	.mzXML	39
MS-FINDER	.txt	40
BioNetApp	.csv	41
MultiAlign	.mzXML	42
Automics	XWIN-NMR	43
MS2Analyzer	.MGF	44
mzR	.netCDF	45
MetShot	.mzXML	46
AMDIS	.MSL	47
AMDIS	.CAL	48
MetaQuant	.xlsx	49
VSClust	.csv	50
CliqueMS	.mzXML	51
ChemoSpec	.csv	52
BatMass	.mzXML	53
MVAPACK	.txt	54
MetaboGroup S	.txt	55
mzR	.mzData	56
rNMR	.txt	57
MetScape	.csv	58
MIDAS	.FT2	59
GAVIN	.netCDF	60
InCroMAP	.csv	61
MathDAMP	.txt	62
SpectConnect	.ELU	63
xMSanalyzer	.html	64
xMSanalyzer	.txt	65
MetSign	.mzML	66
Mummichog	.JSON	67
Mummichog	.txt	68
InCroMAP	.xlsx	69
msPurity	.mzML	70
msPurity	.mzXML	71
MetaboQuant	.xls	72
mzMatch	.mzXML	73
mzMatch	.mzML	74
intCor	.netCDF	75
MultiAlign	.csv	76
PlantMAT	.XLS	77
alsace	.csv	78
HiCoNet	.JSON	79
nPYc-Toolbox	Bruker	80
MetScape	.xls	81
MetaboAnalystR	.mzML	82
MetAlign	.raw	83
msPurity	.raw	84
nmrglue	sparky	85
nmrglue	nmrpipe	86
nmrglue	Bruker	87
apLCMS	.netCDF	88
MVAPACK	Bruker	89
Automics	Bruker	90
rNMR	sparky	91
CFM-ID	.csv	92
MetAlign	Masslynx	93
MetAlign	.d	94
MVAPACK	.d	95
MZmine 2	.raw	96
LDA	.mzXML	97
XCMS online	.netCDF	98
XCMS online	.mzXML	99
mQTL.NMR	.txt	100
TOXcms	.csv	101
MZmine 2	.mzML	102
TOXcms	.mzXML	103
MZmine 2	.mzData	104
swathTUNER	.txt	105
BATMAN	.csv	106
MetExtract	.mzXML	107
mzR	.mzML	108
mzR	.mzIdentML	109
MetiTree	.mzXML	110
SIMAT	.netCDF	111
SIMAT	.MSL	112
PAPi	.csv	113
XCMS	.mzML	114
AMDIS	.CSL	115
AMDIS	.FIN	116
DeepRiPP	.mzML	117
Maui-VIA	.csv	118
Lilikoi	.csv	119
XCMS	.mxzData	120
KeggArray	.tsv	121
Bayesil	.fid	122
nmrglue	Varian	123
MetaboLab	Bruker	124
MetaboLab	Varian	125
Maui-VIA	.mzML	126
credential	.csv	127
CROP	.mzTab	128
CROP	.csv	129
Qemistree	MGF	130
Qemistree	.csv	131
Notame	.xls	132
MetaboShiny	.csv	133
MetShot	.csv	134
MetShot	.EXP	135
CFM-ID	.msp	136
Normalyzer	.txt	137
Normalyzer	.csv	138
MetabNet	.raw	139
MZmine 2	.mzXML	140
iMet-Q	.mzML	141
MetScape	.tsv	142
BATMAN	.txt	143
X13CMS	.mzXML	144
specmine	.netCDF	145
MetabNet	.cdf	146
apLCMS	.mzML	147
apLCMS	.mzXML	148
apLCMS	.mzData	149
MetaMapR	.csv	150
LICRE	.csv	151
TargetSearch	.netCDF	152
NMRfilter	.csv	153
NMRfilter	.tsv	154
NMRfilter	SMILES	155
MetaboDiff	.csv	156
CluMSID	.mzXML	157
mzMatch	.mzData	158
iontree	.mzXML	159
iontree	.mzML	160
iontree	.raw	161
MSHub	hdf5	162
MSHub	.mgf	163
MSHub	.mzML	164
RGCxGC	netCDF	165
TidyMS	mzML	166
Autotuner	raw	167
Autotuner	mzML	168
hRUV	.csv	169
fobitools	owl	170
fobitools	obo	171
fobitoolsGUI	obo	172
fobitoolsGUI	owl	173
BioDendro	.mgf	174
BioDendro	.txt	175
QSSR Automator	.csv	176
MetIDfyR	mzML	177
MetIDfyR	.tsv	178
CANOPUS	.mgf	179
CANOPUS	.ms	180
CANOPUS	.txt	181
marr	.csv	182
Plasmodesma	Bruker	183
Plasmodesma	.csv	184
VOCCluster	.csv	185
WiPP	mzML	186
WiPP	mzData	187
WiPP	NetCDF	188
Peakonly	.mzML	189
MelonnPan	.txt	190
rDolphin	.csv	191
rDolphin	Bruker	192
RawTools	raw	193
RawTools	Thermo	194
misaR	.tsv	195
statTarget	.csv	196
mwTab	mwTab	197
Risa	ISA	198
SLAW	mzML	199
MS2Query	mzML	200
MS2Query	json	201
MS2Query	mgf	202
MS2Query	msp	203
MS2Query	mzxml	204
MS2Query	usi	205
Maui-VIA	.tsv	206
MetaQuant	.SBML	207
MetaboliteDetector	FastFlight2	208
MetaboSearch	.csv	209
batchCorr	.txt	210
batchCorr	.csv	211
MetaboQuant	.txt	212
MAVEN	.mzXML	213
MAVEN	.mzData	214
MetaboLyzer	.csv	215
MAGMa	.mzXML	216
MRMPROBS	.ABF	217
MetAlign	.netCDF	218
XCMS	.netCDF	219
XCMS	.mzXML	220
nPYc-Toolbox	.csv	221
PARADISe	.netCDF	222
xMSannotator	.txt	223
Maui-VIA	.cdf	224
TracMass2	.netCDF	225
TracMass2	.mzData	226
TracMass2	.mzXML	227
TracMass2	.mzML	228
MET-COFEA	.cdf	229
MS-LAMP	.xls	230
MetTailor	.mzXML	231
GSimp	.csv	232
pyMolNetEnhancer	.csv	233
pyMolNetEnhancer	.mzML	234
RMolNetEnhancer	.csv	235
RMolNetEnhancer	.mzML	236
MassComp	.mzXML	237
iMet	.csv	238
MetNormalizer	.mzXML	239
PYQUAN	.CDF	240
MAIMS	.XML	241
MAIMS	.mdl	242
MAIMS	.txt	243
POMAShiny	.csv	244
MetaboloDerivatizer	SMILES	245
MetaboloDerivatizer	.txt	246
R2DGC	.tsv	247
AlpsNMR	.txt	248
AlpsNMR	Bruker	249
AlpsNMR	JCAMP-DX	250
ADAP-GC	raw	251
ADAP-GC	.csv	252
ADAP-GC	.MSP	253
MIDcor	raw	254
MS-FLO	.csv	255
MetaboQC	.csv	256
LIQUID	.mzML	257
LIQUID	raw	258
LIQUID	Thermo	259
AntDAS	.xml	260
AntDAS	mzData	261
pymass	.mzML	262
pymass	.mzXML	263
pySM	imzmL	264
pySM	.csv	265
massPix	imzML	266
massPix	.csv	267
massPix	.ibd	268
MS-FINDER	.MSP	269
ChromA	.mzXML	270
MZmine 2	.NetCDF	271
TargetSearch	.txt	272
HiCoNet	.txt	273
MetaboClust	.csv	274
MetaboAnalyst	.csv	275
MetaboAnalyst	.mzData	276
MetaboAnalyst	.mzXML	277
MetaboAnalyst	.netCDF	278
MarVis-Suite	.csv	279
BatMass	.mzML	280
SMART	.Raw	281
SMART	.mzXML	282
MetaboShiny	.tsv	283
Viime	.xsl	284
Viime	.csv	285
NOREVA	.csv	286
NOREVA	.tsv	287
JS-MS	.mzML	288
JS-MS	MzTree	289
NMRProcFlow	nmrML	290
NMRProcFlow	Bruker	291
NMRProcFlow	Varian	292
NMRProcFlow	Jeol	293
SigMa	Bruker	294
SigMa	matlab	295
MSHub	netCDF	296
\.


--
-- Data for Name: func_annotation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.func_annotation (tool_name, annotation, rowid) FROM stdin;
CliqueMS	fragmentation adducts annotation	1
mz.unity	Mer annotation	2
GNPS	MS-MS spectra	3
GNPS	peptidic natural products LC-MS-MS	4
MyCompoundID	metabolite identification	5
MetDisease	Annotates metabolic networks with MeSH disease terms and links to references	6
xMSannotator	spectral data annotation	7
LipidXplorer	peak matching	8
El-MAVEN	peak editor	9
MetFrag	metabolite identification via fragmentation	10
compMS2Miner	metabolite identification	11
mz.unity	simple annotations	12
MZmine 2	peak identification	13
Meta P-server	mapping metabolites to KEGG	14
MetaboAnalystR	metabolomics pathway analysis	15
MetaboAnalystR	biomarker analysis	16
CMM	lipid annotation service	17
Maui-VIA	metabolite identification	18
PlantMAT	metabolite identification	19
CCPN Metabolomics	Identify biochemical structures	20
MetFamily	metabolite family annotation	21
MAIT	peak annotation	22
MetExplore	metabolic network annotation	23
BinBase	peak annotation	24
ALEX123	isotope compiler	25
MS-FINDER	structure elucidation	26
Rdisop	metabolite identification	27
ProbMetab	Bayesian probabilistic annotation	28
MetFusion	compound identification	29
X13CMS	isotopic labeling	30
PlantMAT	batch metabolite annotation	31
Greazy	phospholipid identification	32
pyQms	spectrum annotation	33
MetaboHunter	metabolite identification	34
MS-LAMP	lipid metabolite identification	35
specmine	metabolite identification	36
MultiAlign	peak matching	37
mz.unity	distal fragment annotations	38
CFM-ID	peak assignment	39
MetaboAnalystR	spectral processing	40
MetaboAnalystR	statistical analysis	41
MetaboAnalystR	metabolite set enrichment analysis	42
MetaboAnalystR	pathway activity prediction	43
El-MAVEN	fragmentation spectra matching	44
mQTL	Quantitative trait locus mapping	45
CSI:FingerrID	fragmentation tree	46
InterpretMSSpectrum	annotated mass spectrum	47
metaMS	metabolite identification	48
xMSanalyzer	small molecule metabolite matching	49
MIDAS	metabolite identification	50
MRMPROBS	metabolite identification	51
PARADISe	peak identification	52
MetiTree	metabolite identification	53
UC2	unique connectivity of atoms of uncharged compounds annotatin	54
AMDIS	target match	55
RAMClustR	spectral matching based annotation	56
Binner	feature annotation	57
MZedDB	metabolite signal annotation	58
MS2Analyzer	small molecule substructure annotation	59
mzOS	feature annotation	60
MI-PACK	metabolite identification	61
VANTED	mapping of multi-dimensional data sets onto relevant biological networks	62
MWASTools	metabolite detection via STOCSY	63
SmileMS	analyte identification of fragmented molecules	64
mzMatch	metabolite identification	65
MS-FINDER	fragment annotation	66
Workflow4Metabolomics	HMDS MS search	67
MetaboAnalyst	statistical analysis	68
MetaboAnalyst	metabolite set enrichment analysis	69
MetaboAnalyst	biomarker analysis	70
MetaboAnalyst	metabolpathway activity prediction	71
GNPS	network annotation propagation LC-MS-MS	72
Workflow4Metabolomics	LC-MS matching	73
MetaBox	metabolite identification	74
iontree	MS-2a Library Spectrum Match	75
Lipid-Pro	peak matching	76
MetMSLine	isotope identification	77
MetaboliteDetector	compound identification	78
GSim	2H MAS spectrum fitting	79
rDolphin	automatic profiling	80
CANOPUS	class assignment	81
CANOPUS	ontology prediction	82
AntDAS	compound identification	83
AntDAS	adaptive network searching	84
pySM	false discovery rate	85
massPix	mass matching	86
fobitools	compound id conversion	87
pyMolNetEnhancer	ClassyFire	88
RMolNetEnhancer	molecular networking	89
RMolNetEnhancer	MS2LDA	90
RMolNetEnhancer	Network Annotation Propagation	91
RMolNetEnhancer	DEREPLICATOR	92
RMolNetEnhancer	ClassyFire	93
iMet	structural annotation	94
MAGMa	chemical structure annotation	95
MetScape	annotate molecular pathways	96
CAMERA	isotope annotation	97
CAMERA	adduct annotation	98
XCMS	peak matching	99
Workflow4Metabolomics	KEGG compounds	100
Workflow4Metabolomics	Chemspider	101
Workflow4Metabolomics	Lipidmaps	102
MassTRIX	annotate high precision mass spectrometry data	103
CliqueMS	isotope annotation	104
Qemistree	hierarchical orderings	105
MetaboShiny	formula-based	106
MetaboShiny	database matching	107
JS-MS	MS1 annotation	108
NMRfilter	compound identification	109
NMRfilter	chemical shift prediction	110
MetaboMiner	spectra annotation	111
MeltDB	peak matching	112
MetaboDiff	metabolite identification	113
MetaQuant	compound classification	114
CliqueMS	ion adducts annotation	115
MetaboloDerivatizer	in silico derivatization	116
R2DGC	metabolite identification	117
ADAP-GC	compound identification	118
ADAP-GC	compound quantitation	119
AntDAS	fragment ion clustering	120
pySM	FDR estimation	121
fobitoolsGUI	free-text annotation	122
BioDendro	hierarchical clustering	123
QSSR Automator	compound identification	124
QSSR Automator	retention time prediction	125
MetIDfyR	metabolite identification	126
MetIDfyR	biotransformation prediction	127
eRah	metabolite identification	128
XCMS online	peak matching	129
MET-COFEA	metabolite mass compound annotation	130
RMassBank	formula assignment	131
ORCA	metabolic pathways identification with futile loop elimination 	132
CluMSID	consensus spectra	133
SMART	peak annotation	134
fobitools	food-biomarker ontology	135
fobitools	enrichment analysis	136
fobitools	free-text annotation	137
fobitoolsGUI	enrichment analysis	138
fobitoolsGUI	compound id conversion	139
vaLID	phospholipid identification	140
ChemDistiller	metabolite identification	141
Pathos	map metabolites to metabolic pathways	142
Workflow4Metabolomics	MassBank	143
MET-COFEI	peak matching	144
LIMSA	peak matching	145
CANOPUS	DNN	146
GISSMO	spin system matrix calculation	147
DecoMetDIA	deconvolution	148
DecoMetDIA	identification	149
DecoMetDIA	data independent acquisition	150
rDolphin	identification	151
misaR	in-source fragment	152
misaR	spectral matching	153
MS2Query	analogue search	154
MS2Query	library search	155
MAVEN	isotope labeling	156
MetCirc	feature annotation	157
CMM	metabolite detection	158
MetaboClust	peak annotation	159
Ideom	metabolite identification	160
MetabNet	metabolic pathway detection	161
Galaxy-M	annotation of high-resolution mass spectra	162
JS-MS	isotope trace annotation	163
JS-MS	isotopic envelope annotation	164
pyMolNetEnhancer	molecular networking	165
pyMolNetEnhancer	MS2LDA	166
pyMolNetEnhancer	Network Annotation Propagation	167
pyMolNetEnhancer	DEREPLICATOR	168
\.


--
-- Data for Name: func_interpretation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.func_interpretation (tool_name, interpret, rowid) FROM stdin;
\.


--
-- Data for Name: func_postprocessing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.func_postprocessing (tool_name, postprocessing, rowid) FROM stdin;
eMZed	alignment	6
MET-COFEA	peak clustering	1
MET-IDEA	ion-based data extraction	2
MET-XAlign	cross-align metabolite compounds	3
MetaboHunter	drift correction	4
MetaboSearch	metabolite identification	5
msPurity	spectral matching	7
COLMAR	Deconvolution	8
COLMAR	NMR quantification	9
MVAPACK	normalization	10
METLIN	metabolite identification	11
Metabolomics-Filtering	data adaptive filtering	12
IsoCor	isotope correction	13
Automics	DC offset correction	14
SMART	peak filtering	15
SMART	sample filtering	16
FingerID	identification	17
MetFlow	scaling	18
MetMatch	chromatogram alignment	19
MetMatch	m/z shift correction	20
MetMatch	Rt shift correction	21
MetMatch		22
SIMAT	retention index calibration	23
MRMPROBS	noise filtering	24
MRMPROBS	isomeric metabolite separation	25
MZmine 2	normalization	26
MSPrep	summarization/averaging to remove/reduce erroneous data	27
mzMatch	peak extraction	28
mzMatch	peak matching	29
MetShot	feature selection	30
MetaQuant	peak analysis correction	31
MetaQuant	peak quantification	32
Meta P-server	estimation of reproducibility and batch effects	33
Ideom	filtering	34
Ideom	gap filling	35
GSim	deconvolution	36
nmrglue	baseline smoothing	37
MetaboDiff	normalization	38
MSPrep	normalization	39
Zero-fill	zero-filling	40
CROP	multiple feature reduction	41
Notame	clustering	42
MetaboShiny	normalization	43
Viime	imputation	44
Viime	normalization	45
Viime	scaling	46
NOREVA	normalization	47
NOREVA	transformation	48
MS-FLO	Pearson correlation	49
MS-FLO	peak height similarity	50
MS-FLO	duplicate peak removal	51
MS-FLO	isotopic peak filtering	52
MetaboQC	polynomial correction	53
MetaboQC	LOESS correction	54
MetaboQC	instrumental variability correction	55
LIQUID	identification	56
AntDAS	peak extraction	57
AntDAS	gaussian smoothing	58
AntDAS	time-shift correction	59
AntDAS	baseline correction	60
AntDAS	peak filling	61
ncGTW	graphical time warping	62
ncGTW	alignment	63
TidyMS	data matrix curation	64
TidyMS	normalization	65
TidyMS	imputation	66
TidyMS	scaling	67
hRUV	normalization	68
hRUV	batch correction	69
hRUV	signal drift correction	70
hRUV	hierarchical normalization	71
hRUV	replicate design	72
statTarget	signal correction	73
MVAPACK	scaling	74
KIMBLE	PQN normalization	75
MVAPACK	binning and alignment	76
MVAPACK	denoising	77
polyPK	imputation	78
polyPK	outlier removal	79
jQMM	13C metabolic flux analysis	80
NMRPro	zero-filling	81
NMRPro	apodization	82
polyPK	zeros removal	83
QPMASS	missing value filtering	84
QPMASS	backfilling	85
MeltDB	peak alignment	86
GSim	1D processing	87
KIMBLE		160
GSim	2D processing	88
ICT	isotope correction	89
MZmine 2	peak alignment	90
GSimp	left censored imputation	91
MetImp	imputation	92
crmn	normalization	93
eRah	deconvolution	94
MeltDB	normalization	95
XCMS online	retention time correction	96
MetFlow	outlier removal	97
MetFlow	imputation	98
IsoMS-Quant	recalculate peak pair ratio	99
InterpretMSSpectrum	locate molecular ion fragments	100
InterpretMSSpectrum	adduct peaks	101
InterpretMSSpectrum	calculate sum formula combination	102
KPIC	detect pure ions	103
SIMAT	peak detection	104
batchCorr	drift correction	105
MetaboQuant	outlier detection	106
pyQms	molecule quantification	107
MetaboGroup S	normalization	108
Automics	phase correction	109
KPIC	align pure ion chromatogram	110
Warpgroup	chromatogram subregion detection	111
Galaxy-M	alignment	112
intCor	intensity drift correction	113
Maui-VIA	peak alignment	114
Maui-VIA	correction	115
MSPrep	imputation	116
LDA	normalization	117
decoMS2	deconvolution	118
ORCA	multi-objective optimization	119
PARADISe	deconvolution	120
Newton	spectral deconvolution	121
swathTUNER	window width estimation	122
Automics	baseline correction	123
LIMSA	isotope correction	124
MET-COFEI	peak clustering	125
metaMS	pseudospectra filtering	126
MINMA	imputation	127
MetaboDiff	imputation	128
MetaboDiff	outlier removal	129
iMet-Q	alignment	130
iMet-Q	noise removal	131
MetFlow	normalization	132
MZmine 2	peak deconvolution	133
eRah	alignment	134
MetaboAnalyst	batch effect correction	135
Rdisop	decomposition of isotopic patterns	136
metabnorm	normalization	137
XCMS	retention time correction	138
RMassBank	spectrum recalibration	139
KPIC	group pure ion chromatogram	140
KPIC	fill missing peaks	141
HappyTools	retention time calibration	142
HappyTools	data extraction	143
Warpgroup	accurate missing value integration	144
mQTL.NMR	restore spectral dependencies	145
mQTL.NMR	metabotype quantitative trait mapping	146
MetFamily	filtering	147
polyPK	transformation	148
MetaboAnalystR	batch effect correction	149
MetaboliteDetector	peak alignment	150
MSPrep	filtering	151
DeepRiPP	deconvolution	152
MetNorm	normalization	153
Bayesil	baseline correction	154
LipidXplorer	isotope correction	155
LipidXplorer	peak alignment	156
MetaboMiner	compound identification	157
mzOS	deisotoping	158
MetaboClust	noise removal	159
nPYc-Toolbox	feature filtering	161
nPYc-Toolbox	spectral line-width calculation	162
MZmine 2	peak remodeling	163
TNO-DECO	deconvolution	164
missForest	imputation	165
SpectConnect	peak picking	166
El-MAVEN	multi-file chromatographic aligner	167
El-MAVEN	peak-feature detector	168
El-MAVEN	isotope calculator	169
El-MAVEN	adduct calculator	170
Notame	QC	171
MetaboClust	control correction	172
RMassBank	spectrum cleanup	173
flagme	normalization	174
Meta P-server	data quality check	175
Ideom	alignment	176
FingerID	matching	177
HCOR	drift correction	178
SMART	normalization	179
flagme	peak alignment	180
Warpgroup	consensus integration bound determination	181
Galaxy-M	normalization	182
Galaxy-M	scaling	183
mzMatch	filtering	184
Metab	normalization	185
MetaboliteDetector	deconvolution	186
NMRPro	phase correction	187
NMRPro	baseline correction	188
proFIA	imputation	189
MET-COFEI	mass trace extraction	190
MWASTools	quality control	191
SMART	batch detection	192
MetTailor	normalization	193
nmrglue	flattening	194
mzMatch	normalization	195
mzMatch	derivative detection	196
MSClust	data reduction	197
MAVEN	chromatographic alignment	198
PYQUAN	deconvolution	199
PYQUAN	peak quantification	200
MAIMS	profile deconvolution	201
R2DGC	peak alignment	202
MS-FLO	retention time alignment	203
MS-FLO	adduct joining	204
\.


--
-- Data for Name: func_preprocessing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.func_preprocessing (tool_name, preprocessing, rowid) FROM stdin;
rNMR	region of interest(ROI)	1
ALLocater	spectra deconvolution	2
HappyTools	quality control	3
MetaboliteDetector	filtering	4
MetaboliteDetector	baseline analysis	5
KIMBLE	area normalization	6
KIMBLE	transfer normalization	7
KIMBLE	cs calibration	8
KIMBLE	spectra normalization	9
AMDORAP	ion chromatogram extraction	10
AMDORAP	m/z estimation	11
Workflow4Metabolomics	quality control	12
Workflow4Metabolomics	batch correction	13
icoshift	spectral alignment	14
LIMSA	peak detection	15
ADAP/MZmine	peak picking	16
mineXpert	deconvolution	17
muma	scaling	18
MetSign	peak list alignment	19
xMSanalyzer	feature detection	20
IsoMS	peak-pair intensity ratio calculation	21
IsoMS	aligning peak-pairs	22
nPYc-Toolbox	drift correction	23
ChemoSpec	peak alignment	24
QPMASS	peak alignment	25
metaMS	peak picking	26
MetaQuant	peak integration	27
MetaQuant	regression based calibration	28
CFM-ID	compound identification	29
XCMS	peak detection	30
Notame	drift correction	31
Notame	imputation	32
NOREVA	filtering	33
NOREVA	imputation	34
MetNormalizer	support vector regression	35
MetNormalizer	normalization	36
POMAShiny	missing value imputation	37
POMAShiny	normalization	38
POMAShiny	outlier detection	39
POMAShiny	exploratory data analysis 	40
AlpsNMR	outlier detection	41
AlpsNMR	spectra alignment	42
AlpsNMR	peak-picking	43
Plasmodesma	Fourier	44
Plasmodesma	calibration	45
WiPP	peak picking	46
Peakonly	peak detection	47
RawTools	scan metrics parsing	48
RawTools	abundance quantification	49
RawTools	performance analysis	50
Risa	metadata parsing	51
Risa	metadata conversion	52
SLAW	peak-picking	53
SLAW	sample alignment	54
SLAW	gap filling	55
SLAW	parameter optimization	56
SLAW	MS2 extraction	57
SLAW	isotopic pattern extraction	58
Bayesil	phasing	59
MetaboQuant	peak calibration	60
MetaboClust	scaling	61
Normalyzer	normalization	62
proFIA	noise estimation	63
TracMass2	peak alignment	64
TracMass2	peak grouping	65
nmrglue	apodization	66
TargetSearch	retention time indices correction	67
MetaboMiner	peak processing	68
rNMR	peak picking	69
GAVIN	peak integration	70
MetaboAnalyst	missing value imputation	71
MetaboAnalyst	normalization	72
MarVis-Suite	normalization	73
IsoMS	peak-pair filtering	74
IsoMS	peak-pair grouping	75
MetSign	metabolite putative assignment	76
flagme	peak detection	77
proFIA	peak detection	78
MetExtract	in-vivo peak detection	108
Ideom	peak detection	109
LICRE	unsupervised feature correlation reduction	110
MeltDB	peak detection	111
cosmiq	peak detection	112
HappyTools	peak detection	113
MetaboliteDetector	noise detection	114
Automics	zero-filling	115
Automics	apodization	116
Automics	Fourier transformation	117
Automics	peak alignment	118
Automics	binning	119
MetaboliteDetector	peak detection	120
proFIA	peak grouping	121
nmrglue	peak picking	122
SIMAT	selection of optimal fragments	123
MetSign	normalization	124
eMZed	peak detection	125
Galaxy-M	peak detection	126
Bayesil	Fourier transformation	127
Bayesil	solvent filtering	128
Bayesil	chemical shift referencing	129
Bayesil	deconvolution	130
Bayesil	reference line shape convolution	131
MetaboClust	batch correction	132
MET-COFEI	peak detection	133
MVAPACK	Fourier transformation	134
MetaboLab	Fourier transformation	135
MetaboLab	linear prediction	136
MetaboLab	apodization	137
TargetSearch	baseline correction	138
ALEX123	target list generator	139
ALEX123	converter	140
nPYc-Toolbox	batch correction	141
Workflow4Metabolomics	normalization	142
xMSanalyzer	sample quality evaluation	143
xMSanalyzer	feature consistency	144
xMSanalyzer	feature overlap	145
AlpsNMR	integration	146
AlpsNMR	normalization	147
AlpsNMR	spectrum interpolation	148
ADAP-GC	deconvolution	149
ADAP-GC	baseline correction	150
ADAP-GC	peak detection	151
ADAP-GC	alignment	152
MIDcor	isotope correction	153
MIDcor	peak overlap correction	154
MIDcor	isopotomer distribution	155
pymass	PIC extraction	156
NMRProcFlow	ppm calibrarion	157
MET-COFEA	peak detection	158
MetSign	peak detection	159
MAIT	peak detection	160
MetSign	peak list alignment	161
CluMSID	merge redundant spectra	162
CluMSID	multidimensional scaling	163
nmrglue	peak integration	164
IPO	XCMS isotopologue parameter optimization	165
MMSAT	peak alignment	166
AMDIS	spectral deconvolution	167
specmine	spectral corrections	168
specmine	filtering	169
specmine	imputation	170
specmine	normalization	171
specmine	scaling	172
apLCMS	peak detection	173
MetExtract	stable isotopic labeling	174
iMet-Q	peak detection	175
PepsNMR	1H NMR preprocessing	176
muma	normalization	177
nmrglue	Fourier transformation	178
TargetSearch	peak identification	179
NOMAD	normalization	180
mQTL	normalization	181
mQTL	peak alignment	182
mQTL	dimensionality reduction	183
SMART	peak alignment	184
MetAlign	baseline correction	185
MetAlign	peak picking	186
MetAlign	artifact filtering	187
MetaboLab	baseline correction	188
MetaboLab	de-noising spectra	189
Plasmodesma	denoising	190
MetaboAnalystR	normalization	79
MAVEN	peak detection	80
MetAlign	peak alignment	81
ADAP/MZmine	alignment	82
ADAP/MZmine	spectral deconvolution	83
TracMass2	peak detection	84
Skyline	chromatographic alignment	85
IPO	XCMS retention time optimization	86
MetMSLine	multiparametric processing	87
msPurity	assess fragmentation spectra quality	88
Skyline	peak picking	89
nmrglue	peak fitting	90
MVAPACK	phase correction	91
MultiAlign	alignment	92
IsoMS	peak pairing	93
MetFlow	batch alignment	94
XCMS online	peak detection	95
ChromA	retention time alignment	96
Skyline	transition selection	97
MVAPACK	apodization	98
MVAPACK	zero-filling	99
MetSign	deconvolution	100
BATMAN	peak deconvolution	101
NMRPro	peak picking	102
batchCorr	batch normalisation	103
MetaQuant	peak recognition	104
MetaboAnalystR	missing value imputation	105
LDA	peak detection	106
batchCorr	batch alignment	107
NMRProcFlow	baseline correction	191
NMRProcFlow	spectra alignment	192
NMRProcFlow	normalization	193
NMRProcFlow	zeroing	194
speaq	peak alignment	195
speaq	peak detection	196
speaq	noise reduction	197
speaq	spectrum alignment	198
speaq	peak grouping	199
speaq	peak filling	200
SigMa	peak picking	201
SigMa	normalization	202
SigMa	interval recognition	203
SigMa	spectral alignment	204
SigMa	quantitation	205
MSHub	deconvolution	206
MSHub	peak matching	207
MSHub	peak alignment	208
MSHub	peak detection	209
RGCxGC	baseline correction	210
RGCxGC	smoothing	211
RGCxGC	peak alignment	212
TidyMS	peak picking	213
TidyMS	feature detection	214
TidyMS	batch correction	215
Autotuner	parameter optimization	216
Autotuner	TIC peak detection	217
Plasmodesma	peak picking	218
Plasmodesma	bucketing	219
Plasmodesma	apodization	220
Plasmodesma	baseline correction	221
Plasmodesma	phasing	222
\.


--
-- Data for Name: func_processing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.func_processing (tool_name, processing, rowid) FROM stdin;
KIMBLE		88
eMZed	alignment	1
MET-COFEA	peak clustering	2
MET-IDEA	ion-based data extraction	3
MET-XAlign	cross-align metabolite compounds	4
MetaboHunter	drift correction	5
MetaboSearch	metabolite identification	6
msPurity	spectral matching	7
COLMAR	Deconvolution	8
COLMAR	NMR quantification	9
MVAPACK	normalization	10
METLIN	metabolite identification	11
Metabolomics-Filtering	data adaptive filtering	12
IsoCor	isotope correction	13
Automics	DC offset correction	14
SMART	peak filtering	15
SMART	sample filtering	16
FingerID	identification	17
MetFlow	scaling	18
MetMatch	chromatogram alignment	19
MetMatch	m/z shift correction	20
MetMatch	Rt shift correction	21
MetMatch		22
SIMAT	retention index calibration	23
MRMPROBS	noise filtering	24
MRMPROBS	isomeric metabolite separation	25
MZmine 2	normalization	26
MSPrep	summarization/averaging to remove/reduce erroneous data	27
mzMatch	peak extraction	28
mzMatch	peak matching	29
MetShot	feature selection	30
MetaQuant	peak analysis correction	31
MetaQuant	peak quantification	32
Meta P-server	estimation of reproducibility and batch effects	33
Ideom	filtering	34
Ideom	gap filling	35
GSim	deconvolution	36
nmrglue	baseline smoothing	37
MetaboDiff	normalization	38
MSPrep	normalization	39
Zero-fill	zero-filling	40
CROP	multiple feature reduction	41
Notame	clustering	42
MetaboShiny	normalization	43
Viime	imputation	44
Viime	normalization	45
Viime	scaling	46
NOREVA	normalization	47
NOREVA	transformation	48
MS-FLO	Pearson correlation	49
MS-FLO	peak height similarity	50
MS-FLO	duplicate peak removal	51
MS-FLO	isotopic peak filtering	52
MetaboQC	polynomial correction	53
MetaboQC	LOESS correction	54
MetaboQC	instrumental variability correction	55
LIQUID	identification	56
AntDAS	peak extraction	57
AntDAS	gaussian smoothing	58
AntDAS	time-shift correction	59
AntDAS	baseline correction	60
AntDAS	peak filling	61
ncGTW	graphical time warping	62
ncGTW	alignment	63
TidyMS	data matrix curation	64
TidyMS	normalization	65
TidyMS	imputation	66
TidyMS	scaling	67
hRUV	normalization	68
hRUV	batch correction	69
hRUV	signal drift correction	70
hRUV	hierarchical normalization	71
hRUV	replicate design	72
statTarget	signal correction	73
MVAPACK	scaling	74
KIMBLE	PQN normalization	75
MVAPACK	binning and alignment	76
MVAPACK	denoising	77
polyPK	imputation	78
polyPK	outlier removal	79
jQMM	13C metabolic flux analysis	80
NMRPro	zero-filling	81
NMRPro	apodization	82
polyPK	zeros removal	83
QPMASS	missing value filtering	84
QPMASS	backfilling	85
MeltDB	peak alignment	86
GSim	1D processing	87
GSim	2D processing	89
ICT	isotope correction	90
MZmine 2	peak alignment	91
GSimp	left censored imputation	92
MetImp	imputation	93
crmn	normalization	94
eRah	deconvolution	95
MeltDB	normalization	96
XCMS online	retention time correction	97
MetFlow	outlier removal	98
MetFlow	imputation	99
IsoMS-Quant	recalculate peak pair ratio	100
InterpretMSSpectrum	locate molecular ion fragments	101
InterpretMSSpectrum	adduct peaks	102
InterpretMSSpectrum	calculate sum formula combination	103
KPIC	detect pure ions	104
SIMAT	peak detection	105
batchCorr	drift correction	106
MetaboQuant	outlier detection	107
pyQms	molecule quantification	108
MetaboGroup S	normalization	109
Automics	phase correction	110
KPIC	align pure ion chromatogram	111
Warpgroup	chromatogram subregion detection	112
Galaxy-M	alignment	113
intCor	intensity drift correction	114
Maui-VIA	peak alignment	115
Maui-VIA	correction	116
MSPrep	imputation	117
LDA	normalization	118
decoMS2	deconvolution	119
ORCA	multi-objective optimization	120
PARADISe	deconvolution	121
Newton	spectral deconvolution	122
swathTUNER	window width estimation	123
Automics	baseline correction	124
LIMSA	isotope correction	125
MET-COFEI	peak clustering	126
metaMS	pseudospectra filtering	127
MINMA	imputation	128
MetaboDiff	imputation	129
MetaboDiff	outlier removal	130
iMet-Q	alignment	131
iMet-Q	noise removal	132
MetFlow	normalization	133
MZmine 2	peak deconvolution	134
eRah	alignment	135
MetaboAnalyst	batch effect correction	136
Rdisop	decomposition of isotopic patterns	137
metabnorm	normalization	138
XCMS	retention time correction	139
RMassBank	spectrum recalibration	140
KPIC	group pure ion chromatogram	141
KPIC	fill missing peaks	142
HappyTools	retention time calibration	143
HappyTools	data extraction	144
Warpgroup	accurate missing value integration	145
mQTL.NMR	restore spectral dependencies	146
mQTL.NMR	metabotype quantitative trait mapping	147
MetFamily	filtering	148
polyPK	transformation	149
MetaboAnalystR	batch effect correction	150
MetaboliteDetector	peak alignment	151
MSPrep	filtering	152
DeepRiPP	deconvolution	153
MetNorm	normalization	154
Bayesil	baseline correction	155
LipidXplorer	isotope correction	156
LipidXplorer	peak alignment	157
MetaboMiner	compound identification	158
mzOS	deisotoping	159
MetaboClust	noise removal	160
nPYc-Toolbox	feature filtering	161
nPYc-Toolbox	spectral line-width calculation	162
MZmine 2	peak remodeling	163
TNO-DECO	deconvolution	164
missForest	imputation	165
SpectConnect	peak picking	166
El-MAVEN	multi-file chromatographic aligner	167
El-MAVEN	peak-feature detector	168
El-MAVEN	isotope calculator	169
El-MAVEN	adduct calculator	170
Notame	QC	171
MetaboClust	control correction	172
RMassBank	spectrum cleanup	173
flagme	normalization	174
Meta P-server	data quality check	175
Ideom	alignment	176
FingerID	matching	177
HCOR	drift correction	178
SMART	normalization	179
flagme	peak alignment	180
Warpgroup	consensus integration bound determination	181
Galaxy-M	normalization	182
Galaxy-M	scaling	183
mzMatch	filtering	184
Metab	normalization	185
MetaboliteDetector	deconvolution	186
NMRPro	phase correction	187
NMRPro	baseline correction	188
proFIA	imputation	189
MET-COFEI	mass trace extraction	190
MWASTools	quality control	191
SMART	batch detection	192
MetTailor	normalization	193
nmrglue	flattening	194
mzMatch	normalization	195
mzMatch	derivative detection	196
MSClust	data reduction	197
MAVEN	chromatographic alignment	198
PYQUAN	deconvolution	199
PYQUAN	peak quantification	200
MAIMS	profile deconvolution	201
R2DGC	peak alignment	202
MS-FLO	retention time alignment	203
MS-FLO	adduct joining	204
rNMR	region of interest(ROI)	205
ALLocater	spectra deconvolution	206
HappyTools	quality control	207
MetaboliteDetector	filtering	208
MetaboliteDetector	baseline analysis	209
KIMBLE	area normalization	210
KIMBLE	transfer normalization	211
KIMBLE	cs calibration	212
KIMBLE	spectra normalization	213
AMDORAP	ion chromatogram extraction	214
AMDORAP	m/z estimation	215
Workflow4Metabolomics	quality control	216
Workflow4Metabolomics	batch correction	217
icoshift	spectral alignment	218
LIMSA	peak detection	219
ADAP/MZmine	peak picking	220
mineXpert	deconvolution	221
muma	scaling	222
MetSign	peak list alignment	223
xMSanalyzer	feature detection	224
IsoMS	peak-pair intensity ratio calculation	225
IsoMS	aligning peak-pairs	226
nPYc-Toolbox	drift correction	227
ChemoSpec	peak alignment	228
QPMASS	peak alignment	229
metaMS	peak picking	230
MetaQuant	peak integration	231
MetaQuant	regression based calibration	232
CFM-ID	compound identification	233
XCMS	peak detection	234
Notame	drift correction	235
Notame	imputation	236
NOREVA	filtering	237
NOREVA	imputation	238
MetNormalizer	support vector regression	239
MetNormalizer	normalization	240
POMAShiny	missing value imputation	241
POMAShiny	normalization	242
POMAShiny	outlier detection	243
POMAShiny	exploratory data analysis 	244
AlpsNMR	outlier detection	245
AlpsNMR	spectra alignment	246
AlpsNMR	peak-picking	247
Plasmodesma	Fourier	248
Plasmodesma	calibration	249
WiPP	peak picking	250
Peakonly	peak detection	251
RawTools	scan metrics parsing	252
RawTools	abundance quantification	253
RawTools	performance analysis	254
Risa	metadata parsing	255
Risa	metadata conversion	256
SLAW	peak-picking	257
SLAW	sample alignment	258
SLAW	gap filling	259
SLAW	parameter optimization	260
SLAW	MS2 extraction	261
SLAW	isotopic pattern extraction	262
Bayesil	phasing	263
MetaboQuant	peak calibration	264
MetaboClust	scaling	265
Normalyzer	normalization	266
proFIA	noise estimation	267
TracMass2	peak alignment	268
TracMass2	peak grouping	269
nmrglue	apodization	270
TargetSearch	retention time indices correction	271
MetaboMiner	peak processing	272
rNMR	peak picking	273
GAVIN	peak integration	274
MetaboAnalyst	missing value imputation	275
MetaboAnalyst	normalization	276
MarVis-Suite	normalization	277
IsoMS	peak-pair filtering	278
IsoMS	peak-pair grouping	279
MetSign	metabolite putative assignment	280
flagme	peak detection	281
proFIA	peak detection	282
MetExtract	in-vivo peak detection	283
Ideom	peak detection	284
LICRE	unsupervised feature correlation reduction	285
MeltDB	peak detection	286
cosmiq	peak detection	287
HappyTools	peak detection	288
MetaboliteDetector	noise detection	289
Automics	zero-filling	290
Automics	apodization	291
Automics	Fourier transformation	292
Automics	peak alignment	293
Automics	binning	294
MetaboliteDetector	peak detection	295
proFIA	peak grouping	296
nmrglue	peak picking	297
SIMAT	selection of optimal fragments	298
MetSign	normalization	299
eMZed	peak detection	300
Galaxy-M	peak detection	301
Bayesil	Fourier transformation	302
Bayesil	solvent filtering	303
Bayesil	chemical shift referencing	304
Bayesil	deconvolution	305
Bayesil	reference line shape convolution	306
MetaboClust	batch correction	307
MET-COFEI	peak detection	308
MVAPACK	Fourier transformation	309
MetaboLab	Fourier transformation	310
MetaboLab	linear prediction	311
MetaboLab	apodization	312
TargetSearch	baseline correction	313
ALEX123	target list generator	314
ALEX123	converter	315
nPYc-Toolbox	batch correction	316
Workflow4Metabolomics	normalization	317
xMSanalyzer	sample quality evaluation	318
xMSanalyzer	feature consistency	319
xMSanalyzer	feature overlap	320
AlpsNMR	integration	321
AlpsNMR	normalization	322
AlpsNMR	spectrum interpolation	323
ADAP-GC	deconvolution	324
ADAP-GC	baseline correction	325
ADAP-GC	peak detection	326
ADAP-GC	alignment	327
MIDcor	isotope correction	328
MIDcor	peak overlap correction	329
MIDcor	isopotomer distribution	330
pymass	PIC extraction	331
NMRProcFlow	ppm calibrarion	332
MET-COFEA	peak detection	333
MetSign	peak detection	334
MAIT	peak detection	335
MetSign	peak list alignment	336
CluMSID	merge redundant spectra	337
CluMSID	multidimensional scaling	338
nmrglue	peak integration	339
IPO	XCMS isotopologue parameter optimization	340
MMSAT	peak alignment	341
AMDIS	spectral deconvolution	342
specmine	spectral corrections	343
specmine	filtering	344
specmine	imputation	345
specmine	normalization	346
specmine	scaling	347
apLCMS	peak detection	348
MetExtract	stable isotopic labeling	349
iMet-Q	peak detection	350
PepsNMR	1H NMR preprocessing	351
muma	normalization	352
nmrglue	Fourier transformation	353
TargetSearch	peak identification	354
NOMAD	normalization	355
mQTL	normalization	356
mQTL	peak alignment	357
mQTL	dimensionality reduction	358
SMART	peak alignment	359
MetAlign	baseline correction	360
MetAlign	peak picking	361
MetAlign	artifact filtering	362
MetaboLab	baseline correction	363
MetaboLab	de-noising spectra	364
Plasmodesma	denoising	365
MetaboAnalystR	normalization	366
MAVEN	peak detection	367
MetAlign	peak alignment	368
ADAP/MZmine	alignment	369
ADAP/MZmine	spectral deconvolution	370
TracMass2	peak detection	371
Skyline	chromatographic alignment	372
IPO	XCMS retention time optimization	373
MetMSLine	multiparametric processing	374
msPurity	assess fragmentation spectra quality	375
Skyline	peak picking	376
nmrglue	peak fitting	377
MVAPACK	phase correction	378
MultiAlign	alignment	379
IsoMS	peak pairing	380
MetFlow	batch alignment	381
XCMS online	peak detection	382
ChromA	retention time alignment	383
Skyline	transition selection	384
MVAPACK	apodization	385
MVAPACK	zero-filling	386
MetSign	deconvolution	387
BATMAN	peak deconvolution	388
NMRPro	peak picking	389
batchCorr	batch normalisation	390
MetaQuant	peak recognition	391
MetaboAnalystR	missing value imputation	392
LDA	peak detection	393
batchCorr	batch alignment	394
NMRProcFlow	baseline correction	395
NMRProcFlow	spectra alignment	396
NMRProcFlow	normalization	397
NMRProcFlow	zeroing	398
speaq	peak alignment	399
speaq	peak detection	400
speaq	noise reduction	401
speaq	spectrum alignment	402
speaq	peak grouping	403
speaq	peak filling	404
SigMa	peak picking	405
SigMa	normalization	406
SigMa	interval recognition	407
SigMa	spectral alignment	408
SigMa	quantitation	409
MSHub	deconvolution	410
MSHub	peak matching	411
MSHub	peak alignment	412
MSHub	peak detection	413
RGCxGC	baseline correction	414
RGCxGC	smoothing	415
RGCxGC	peak alignment	416
TidyMS	peak picking	417
TidyMS	feature detection	418
TidyMS	batch correction	419
Autotuner	parameter optimization	420
Autotuner	TIC peak detection	421
Plasmodesma	peak picking	422
Plasmodesma	bucketing	423
Plasmodesma	apodization	424
Plasmodesma	baseline correction	425
Plasmodesma	phasing	426
\.


--
-- Data for Name: func_statistics; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.func_statistics (tool_name, stats_method, rowid) FROM stdin;
MetaboAnalyst	SOM	1
MetaboAnalyst	dendogram	2
MetaboAnalyst	SAM	3
MetaboAnalyst	ANOVA	4
MetaboAnalyst	EBAM	5
MetaboAnalyst	PLS-DA	6
Galaxy-M	imputation	7
mixOmics	multilevel decomposition	8
KIMBLE	PCA	9
MZmine 2	PCA	10
MetaboClust	clustering	11
SMART	ANCOVA	12
MetaboLyzer	t-test	13
BiNCHE	chemical enrichment analysis	14
MetaboliteDetector	ANOVA	15
Galaxy-M	univariate statistics	16
mixOmics	cluster map	17
COVAIN	Granger causality	18
PathoLogic	machine learning	19
MarVis-Suite	unsupervised clustering	20
MetSign	significance tests	21
BioNetApp	correlation analysis	22
rNMR	ROI summaries	23
specmine	machine learning and feature selection	24
MetaComp	paired t-test	25
COVAIN	permutation entropy analysis	26
MetaboDiff	ANOVA	27
MetaComp	regression analysis	28
MeltDB	hierarchical clustering analysis	29
COVAIN	PCA	30
VANTED	U-test	31
XCMS online	Kruskal-Wallis	32
XCMS online	PCA	33
VSClust	fuzzy clustering	34
muma	PCA	35
ChemoSpec	hierarchical clustering analysis	36
ORCA	robustness analysis	37
Metab	ANOVA	38
MESSI	best strain identification	39
MetaboLyzer	Mann-Whitney U test	40
mixOmics	16S multivariate analysis	41
mixOmics	vertical multiple integration	42
Metab	t-test	43
MetFlow	differential metabolite discovery	44
flagme	imputation	45
MWASTools	partial correlation	46
MetaComp	Fishers exact test	47
X13CMS	differential labeling analysis	48
nmrglue	extrapolation	49
KPIC	multivariate pattern recognition	50
VANTED	correlation	51
Galaxy-M	PCA	52
XCMS	univariate t-test	53
credential	two-fold filtering of artifactual signals 	54
MetaComp	t-test	55
KIMBLE	clustering	56
Workflow4Metabolomics	OLS	57
RankProd	rank sum	58
SDAMS	semi-parametric differential abundance analysis	59
IntLIM	linear model	60
ORCA	fractional benefit analysis	61
mixOmics	sparse IPCA	62
polyPK	correlation plot	63
polyPK	scatter plot	64
polyPK	heatmap	65
MESSI	metabolics target selection	66
BioNetApp	Self Organizing Maps	67
MetFamily	PCA	68
MetFamily	hierarchical clustering	69
jQMM	comprehensive genome-scale models	70
MetaboLyzer	multidimensional scaling	71
CGBayesNets	network predictions via conditional Gaussian Bayesian networks	72
MAIT	discriminant analysis	73
MZmine 2	categorical data analysis	74
MetaboliteDetector	retention index calculation	75
MetaboAnalyst	PCA	76
MetabolAnalyze	mixtures probabilistic PCA	77
MAIT	t-test	78
MultiAlign	clustering	79
Ionwinze	z-test	80
MetMSLine	univariate statistical analysis	81
BioNetApp	K-Medoids	82
nmrglue	linear prediction modeling	83
MetaboClust	univariate statistics	84
ORCA	dynamic flux balance analysis framework incorporating kinetic constraints	85
MeltDB	PCA	86
eMZed	isotope abundance analyses	87
DeepRiPP	predict peptide class and cleaved sequence	88
MetaboDiff	tSNE	89
NP-StructurePredictor	prediction of unknown natural products in plant mixtures	90
ADAP/MZmine	ANOVA	91
VANTED	clustering	92
Ionwinze	Z-factor	93
Ionwinze	t-test	94
nPYc-Toolbox	PCA	95
VANTED	Welch-Satterthwaite t-test	96
specmine	univariate analysis	97
specmine	unsupervised multivariate analysis	98
MetabolAnalyze	probabilistic covariates	99
metabomxtr	mixture model analysis	100
MetaComp	Mann-Whitney U test	101
MetaComp	Wilcoxon signed-rank test	102
MetaboAnalystR	fold change analysis	103
MetaboAnalystR	correlation analysis	104
MetaboAnalystR	random forest	105
MetaboAnalystR	ANOVA	106
MetaboAnalystR	orthogonal PLS-DA	107
MetaboAnalystR	EBAM	108
MetaboAnalystR	PCA	109
MetaboAnalystR	dendogram	110
MetaboAnalystR	heatmap	111
MetaboAnalystR	k-means	112
MetaboAnalystR	t-test	113
MetaboAnalystR	PLS-DA	114
MetaboAnalystR	SOM	115
MetaboAnalystR	SVM	116
MetaboAnalystR	volcano plot	117
MetaboAnalystR	SAM	118
MetCirc	normalized dot product similarity calculation	119
polyPK	similarity analysis	120
MetaboAnalyst	t-test	121
GAM	FDR	122
MetMSLine	biomarker discovery	123
MetMSLine	hierarchical clustering analysis	124
MetMSLine	multiparametric automatic regression	125
CluMSID		126
BRAIN	compute occurrence probabilities of consecutive aggregated isotopic peaks	127
FALCON	enzyme complex abundance estimation	128
MetaboDiff	PCA	129
MetaboliteDetector	t-test	130
E-Dragon	calculation of molecule indices	131
TOXcms	model dose response curves	132
MetaboDiff	correlation network analysis	133
VANTED	students t-test	134
COVAIN	ANOVA	135
SpectraClassifier	automated pattern recognition anlaysis	136
alsace	alternating least squares	137
mixOmics	regularized canonical correlation	138
MetCCS Predictor	CCS value prediction model	139
MVAPACK	PLS	140
MVAPACK	PCA	141
MVAPACK	LDA	142
FragPred	selection of ensembles of fragmentation reactions	143
ropls	OPLS	144
mixOmics	loading plot	145
RankProd	permutation p-value	146
Automics	uncorrelated LDA	147
Automics	PLS	148
Automics	KNN	149
Automics	PCA	150
Automics	Fishers criterion	151
Automics	SVM	152
Automics	LDA	153
Automics	SICMA	154
Workflow4Metabolomics	univariate	155
Workflow4Metabolomics	multivariate	156
jQMM	flux balance analysis	157
MultiAlign	spectral clustering	158
mixOmics	sparse PLS	159
mixOmics	sparse PLS-DA	160
COVAIN	independent component analysis	161
COVAIN	differential Jacobian matrix reconstruction	162
RankProd	rank product	163
MetaboLyzer	Kolmogorov-Smirnov test	164
Meta P-server	PCA	165
Meta P-server	correlation tests	166
CFM-ID	spectra prediction	167
MetaboAnalyst	orthogonal PLS-DA	168
MetaboAnalyst	correlation analysis	169
COVAIN	clustering and correlation coefficient analysis	170
Meta P-server	hypothesis tests for multiple categorical phenotypes	171
mixOmics	sparse PCA	172
E-Dragon	calculation of molecular descriptors	173
mineXpert	isotopic cluster calculations	174
XCMS online	t-test	175
XCMS online	ANOVA	176
MathDAMP	arithmetic operations to signal intensities	177
BioNetApp	farthest first algorithms	178
Workflow4Metabolomics	OPLS	179
BRAIN	calculate the center-masses of the aggregated isotopic variants	180
MetSign	unsupervised pattern recognition	181
MetaboLyzer	PCA	182
MetaboLyzer	log fold change	183
MetaboLyzer	independent component analysis	184
MetaComp	PCA	185
MetaboAnalyst	random forest	186
MetaboAnalyst	SVM	187
MetaboAnalyst	fold change analysis	188
CluMSID	density based clustering	189
CluMSID	correlation network	190
COLMAR	covariance NMR spectrum	191
MetabolAnalyze	probabilistic PCA	192
MetaComp	barplot	193
Notame	multivariate models	194
Notame	t-test	195
Notame	ANOVA	196
Notame	Mann-Whitney	197
Notame	Kruskal-Wallis	198
Notame	Friedman	199
Notame	PCA	200
Notame	t-SNE	201
Notame	PLS	202
MetaComp	heatmap	203
ropls	univariate hypothesis testing	204
MAIT	ANOVA	205
KMDA	kernel based score test	206
Ideom	Fisher ratio	207
POMAShiny	multidimensional scaling	208
POMAShiny	Pearson correlation	209
POMAShiny	Spearman correlation	210
POMAShiny	Kendall correlation	211
POMAShiny	Gaussian Graphical Models	212
POMAShiny	LASSO	213
POMAShiny	Ridge regression	214
POMAShiny	Elasticnet regression	215
POMAShiny	Random forest	216
POMAShiny	Odds ratio	217
POMAShiny	Rank product permutation	218
POMAShiny	PLS-DA	219
AntDAS	ANOVA	220
AntDAS	hierarchical cluster analysis	221
AntDAS	PCA	222
massPix	PCA	223
massPix	k-means	224
RGCxGC	PCA	225
QSSR Automator	linear regression	226
QSSR Automator	random forest	227
QSSR Automator	support vector machines	228
marr	maximal rank	229
marr	reproducibility analysis	230
marr	maximum rank reproducibility	231
sampleDrift	spline regression	232
sampleDrift	cluster-based modeling	233
VOCCluster	hierarchical clustering	234
VOCCluster	OPTICS	235
VOCCluster	DBSCAN	236
VOCCluster	Retention Index Variance Calculation	237
MetaboAnalyst	k-means	238
Mummichog	pathway activity	239
Mummichog	module activity	240
DiffCorr	calculates differential correlations	241
DiffCorr	biomarker detection	242
FragPred	likelihood estimations	243
CluMSID	hierarchical clustering	244
mixOmics	horizontal multiple integration	245
mixOmics	correlation plot	246
Ideom	t-test	247
muma	OPLS-DA	248
muma	STOCSY	249
muma	PLS-DA	250
HiCoNet	community detection	251
HiCoNet	PLS	252
MetaboLyzer	database filtering	253
polyPK	calculate pharmacokinetic parameters	254
MetaboClust	multivariate statistics	255
ChemoSpec	PCA	256
MetaComp	clustering	257
MVAPACK	OPLS	258
MWASTools	GLM	259
mixOmics	network plot	260
MAIT	KNN	261
MAIT	PLS	262
mQTL.NMR	enhance structural assignment of genetically determined metabolites	263
FALCON	metabolic flux estimation	264
MetaboLyzer	Anderson-Darling test	265
Lilikoi	machine learning classification & prediction	266
Galaxy-M	multivariate statistics	267
MetaComp	dendogram	268
MetaboDiff	t-test	269
BioNetApp	K-Means	270
Mummichog	permutation	271
MetMSLine	PCA	272
mixOmics	arrow plot	273
mixOmics	circos plot	274
MAIT	SVM	275
MetaboAnalyst	volcano plot	276
MetaboAnalyst	heatmap	277
VOCCluster	similarity matrix	278
MelonnPan	predictive metabolomic profiling	279
statTarget	PCA	280
statTarget	PLS-DA	281
statTarget	VIP	282
statTarget	S-plot	283
statTarget	Random forest	284
statTarget	permutation importance	285
Notame	Random forest	286
MetaboShiny	t-test	287
MetaboShiny	PCA	288
MetaboShiny	PLS-DA	289
MetaboShiny	t-SNE	290
MetaboShiny	Random forest	291
Viime	PCA	292
Viime	Wilcoxon	293
Viime	ANOVA	294
tmod	feature set enrichment	295
POMAShiny	t-test	296
POMAShiny	ANOVA	297
POMAShiny	ANCOVA	298
POMAShiny	Limma	299
POMAShiny	Mann-Whitney	300
POMAShiny	Kruskal-Wallis	301
POMAShiny	PCA	302
POMAShiny	k-means	303
\.


--
-- Data for Name: functionality; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.functionality (tool_name, function, rowid) FROM stdin;
ORCA	annotation	143
TracMass2	postprocessing	1
MAIT	postprocessing	2
KPIC	postprocessing	3
Automics	statistical analysis	4
BiNCHE	statistical analysis	5
BATMAN	preprocessing	6
credential	statistical analysis	7
HMDB	database	8
PaintOmics	network/visualization	9
Escher	network/visualization	10
KeggArray	network/visualization	11
SMPDB	database	12
SAMMI	network/visualization	13
MetaboNetworks	network/visualization	14
MetaNetter	network/visualization	15
matNMR	network/visualization	16
MetaboClust	network/visualization	17
WikiPathways	network/visualization	18
BiGG Models	network/visualization	19
mzR	data management/workflow	20
iontree	data management/workflow	21
MetaDB	data management/workflow	22
PhenoMeNal	data management/workflow	23
UC2	data management/workflow	24
SistematX	data management/workflow	25
Workflow4Metabolomics	annotation	26
GAM	statistical analysis	27
IsoMS	preprocessing	28
ProbMetab	annotation	29
mzMatch	postprocessing	30
Newton	postprocessing	31
E-Dragon	statistical analysis	32
ADAP/MZmine	statistical analysis	33
Ionwinze	statistical analysis	34
MWASTools	statistical analysis	35
proFIA	preprocessing	36
proFIA	postprocessing	37
MetaboAnalystR	statistical analysis	38
MINMA	postprocessing	39
MWASTools	annotation	40
apLCMS	preprocessing	41
MetaboSearch	postprocessing	42
mQTL.NMR	postprocessing	43
mQTL.NMR	statistical analysis	44
Ideom	annotation	45
TNO-DECO	postprocessing	46
MS-FINDER	annotation	47
xMSannotator	annotation	48
SDAMS	statistical analysis	49
MetaboHunter	annotation	50
LIMSA	postprocessing	51
Zero-fill	postprocessing	52
Bayesil	preprocessing	53
Galaxy-M	statistical analysis	54
compMS2Miner	annotation	55
mzOS	annotation	56
CFM-ID	statistical analysis	57
MetaboHunter	postprocessing	58
MMSAT	preprocessing	59
MSClust	postprocessing	60
MathDAMP	statistical analysis	61
TargetSearch	preprocessing	62
SIMAT	postprocessing	63
Rdisop	annotation	64
El-MAVEN	annotation	65
mzOS	postprocessing	66
muma	statistical analysis	67
MetaboliteDetector	preprocessing	68
MS-LAMP	annotation	69
ORCA	statistical analysis	70
ORCA	postprocessing	71
CluMSID	preprocessing	72
TOXcms	statistical analysis	73
NMRPro	preprocessing	74
MS2Analyzer	annotation	75
CluMSID	statistical analysis	76
Greazy	annotation	77
ChemDistiller	annotation	78
MI-PACK	annotation	79
MetAlign	preprocessing	80
metaMS	postprocessing	81
MeltDB	annotation	82
Ideom	preprocessing	83
Ideom	postprocessing	84
Ideom	statistical analysis	85
MetabNet	annotation	86
xMSanalyzer	preprocessing	87
GSimp	postprocessing	88
VSClust	statistical analysis	89
MET-COFEA	postprocessing	90
HiCoNet	statistical analysis	91
PlantMAT	annotation	92
eMZed	preprocessing	93
eMZed	postprocessing	94
muma	preprocessing	95
ICT	postprocessing	96
MetabolAnalyze	statistical analysis	97
FragPred	statistical analysis	98
alsace	statistical analysis	99
LDA	preprocessing	100
SMART	statistical analysis	101
SMART	preprocessing	102
Normalyzer	preprocessing	103
MVAPACK	postprocessing	104
MVAPACK	statistical analysis	105
El-MAVEN	postprocessing	106
eRah	postprocessing	107
HCOR	postprocessing	108
SMART	annotation	109
MAIT	annotation	110
rNMR	preprocessing	111
rNMR	statistical analysis	112
mineXpert	preprocessing	113
mineXpert	statistical analysis	114
MetaboDiff	postprocessing	115
MetaboDiff	statistical analysis	116
CCPN Metabolomics	annotation	117
PathoLogic	statistical analysis	118
MathDAMP	network/visualization	119
MetFlow	network/visualization	120
SMART	network/visualization	121
mineXpert	network/visualization	122
BioNetApp	network/visualization	123
compMS2Miner	network/visualization	124
rDolphin	annotation	251
CGBayesNets	network/visualization	125
iPath	network/visualization	126
MetaboAnalystR	network/visualization	127
Mummichog	network/visualization	128
MetaNetter	network/visualization	129
PathoLogic	statistical analysis	130
MetaBridge	network/visualization	131
Pathview	network/visualization	132
PAPi	network/visualization	133
KeggArray	network/visualization	134
Pachyderm	data management/workflow	135
compMS2Miner	data management/workflow	136
Galaxy-M	postprocessing	137
MetImp	postprocessing	138
Maui-VIA	postprocessing	139
InterpretMSSpectrum	annotation	140
InterpretMSSpectrum	postprocessing	141
X13CMS	annotation	142
MetCirc	annotation	144
RMassBank	postprocessing	145
Automics	postprocessing	146
MVAPACK	preprocessing	147
MZedDB	annotation	148
MIDAS	annotation	149
vaLID	annotation	150
MetaboAnalyst	postprocessing	151
MetaboAnalyst	statistical analysis	152
nmrglue	preprocessing	153
jQMM	statistical analysis	154
jQMM	postprocessing	155
XCMS	statistical analysis	156
Meta P-server	postprocessing	157
MultiAlign	statistical analysis	158
KMDA	statistical analysis	159
AMDORAP	preprocessing	160
DeepRiPP	statistical analysis	161
DeepRiPP	postprocessing	162
missForest	postprocessing	163
MET-COFEI	preprocessing	164
MET-COFEI	postprocessing	165
msPurity	preprocessing	166
msPurity	postprocessing	167
SpectraClassifier	statistical analysis	168
MetMatch	postprocessing	169
MetExtract	preprocessing	170
LDA	postprocessing	171
eRah	annotation	172
Meta P-server	statistical analysis	173
MetFlow	preprocessing	174
MetFlow	postprocessing	175
MetFlow	statistical analysis	176
METLIN	postprocessing	177
nPYc-Toolbox	postprocessing	178
MET-IDEA	postprocessing	179
icoshift	preprocessing	180
MultiAlign	preprocessing	181
MassTRIX	annotation	182
nPYc-Toolbox	preprocessing	183
MetExplore	annotation	184
cosmiq	preprocessing	185
xMSanalyzer	annotation	186
MetaboLab	preprocessing	187
NP-StructurePredictor	statistical analysis	188
IsoCor	postprocessing	189
MoNA	database	190
VMH	database	191
ConsensusPathDB	database	192
Rhea	database	193
YMDB	database	194
SysteMHC	database	195
PAMBD	database	196
GabiPD	database	197
MetMask	database	198
LIPID MAPS	database	199
GAM	network/visualization	200
BatMass	network/visualization	201
TriDAMP	network/visualization	202
nPYc-Toolbox	network/visualization	203
Skyline	network/visualization	204
BiGG Models	network/visualization	205
CluMSID	network/visualization	206
MetaboAnalyst	network/visualization	207
MetaMapR	network/visualization	208
HAMMER	data management/workflow	209
MetMask	data management/workflow	210
RDFIO	data management/workflow	211
CROP	postprocessing	212
Qemistree	annotation	213
Notame	preprocessing	214
Notame	postprocessing	215
Notame	statistical analysis	216
Notame	network/visualization	217
Notame	data management/workflow 	218
MetaboShiny	postprocessing	219
NMRProcFlow	preprocessing	220
SigMa	preprocessing	221
MSHub	preprocessing	222
RGCxGC	preprocessing	223
RGCxGC	statistical analysis	224
ncGTW	postprocessing	225
TidyMS	preprocessing	226
TidyMS	postprocessing	227
Autotuner	preprocessing	228
hRUV	postprocessing	229
MetumpX	data management/workflow	230
fobitools	annotation	231
fobitools	network/visualization	232
fobitoolsGUI	annotation	233
fobitoolsGUI	network/visualization	234
BioDendro	annotation	235
QSSR Automator	annotation	236
QSSR Automator	statistical analysis	237
MetIDfyR	annotation	238
CANOPUS	annotation	239
marr	statistical analysis	240
Plasmodesma	preprocessing	241
Plasmodesma	postprocessing	242
GISSMO	annotation	243
sampleDrift	statistical analysis	244
VOCCluster	statistical analysis	245
WiPP	preprocessing	246
Peakonly	preprocessing	247
HastaLaVista	network/visualization	248
MelonnPan	statistical analysis	249
DecoMetDIA	annotation	250
RawTools	preprocessing	252
misaR	annotation	253
statTarget	postprocessing	254
statTarget	statistical analysis	255
statTarget	network/visualization	256
mwTab	data management/workflow	257
MetaboSignal	network/visualization	258
Risa	processing	259
Risa	annotation	260
SLAW	processing	261
MS2Query	annotation	262
MWASTools	postprocessing	263
MetSign	statistical analysis	264
nmrglue	postprocessing	265
ChromA	preprocessing	266
Galaxy-M	annotation	267
Warpgroup	postprocessing	268
HappyTools	preprocessing	269
HappyTools	postprocessing	270
Rdisop	postprocessing	271
intCor	postprocessing	272
KPIC	statistical analysis	273
MetFamily	postprocessing	274
MetFamily	annotation	275
specmine	statistical analysis	276
specmine	annotation	277
Bayesil	postprocessing	278
MetaboMiner	preprocessing	279
MetaboMiner	postprocessing	280
Skyline	preprocessing	281
swathTUNER	postprocessing	282
MetaboliteDetector	postprocessing	283
MZmine 2	annotation	284
MZmine 2	postprocessing	285
MetTailor	postprocessing	286
metaMS	preprocessing	287
metaMS	annotation	288
GNPS	annotation	289
MetSign	preprocessing	290
MetaboliteDetector	annotation	291
LipidXplorer	annotation	292
LipidXplorer	postprocessing	293
MetShot	postprocessing	294
MetaboClust	preprocessing	295
IIS	annotation	296
MetaboQuant	postprocessing	297
ChemoSpec	statistical analysis	298
RankProd	statistical analysis	299
SpectConnect	postprocessing	300
GAVIN	preprocessing	301
ropls	statistical analysis	302
batchCorr	preprocessing	303
CluMSID	annotation	304
mixOmics	statistical analysis	305
MarVis-Suite	statistical analysis	306
MarVis-Suite	preprocessing	307
batchCorr	postprocessing	308
LIMSA	annotation	309
FALCON	statistical analysis	310
CSI:FingerrID	annotation	311
MetaboAnalystR	preprocessing	312
MetaboAnalystR	postprocessing	313
MetaboAnalystR	annotation	314
KIMBLE	postprocessing	315
KIMBLE	statistical analysis	316
KIMBLE	preprocessing	317
UC2	annotation	318
MaConDa	database	319
GNPS-MassIVE	database	320
SistematX	database	321
RMassBank	database	322
MINEs	database	323
LiverWiki	database	324
MetaboLights	database	325
BiGG Models	database	326
GNPS	database	327
RaMP	database	328
PeroxisomeDB	database	329
BinBase	database	330
libChEBI	database	331
ChemSpider	database	332
Metab	postprocessing	333
crmn	postprocessing	334
flagme	postprocessing	335
MetaboShiny	statistical analysis	336
MetaboShiny	annotation	337
MetaboShiny	network/visualization	338
Viime	postprocessing	339
Viime	statistical analysis	340
Viime	network/visualization	341
NOREVA	preprocessing	342
NOREVA	postprocessing	343
JS-MS	annotation	344
JS-MS	network/visualization	345
pyMolNetEnhancer	annotation	346
pyMolNetEnhancer	network/visualization	347
RMolNetEnhancer	annotation	348
RMolNetEnhancer	network/visualization	349
ZEAMAP	database	350
MassComp	data management/workflow	351
iMet	annotation	352
MetNormalizer	preprocessing	353
tmod	statistical analysis	354
PYQUAN	postprocessing	355
MAIMS	postprocessing	356
POMAShiny	preprocessing	357
POMAShiny	statistical analysis	358
POMAShiny	network/visualization	359
MetaboloDerivatizer	annotation	360
R2DGC	postprocessing	361
R2DGC	annotation	362
AlpsNMR	preprocessing	363
ADAP-GC	preprocessing	364
ADAP-GC	annotation	365
MIDcor	preprocessing	366
MS-FLO	postprocessing	367
MetaboQC	postprocessing	368
LIQUID	postprocessing	369
AntDAS	annotation	370
AntDAS	postprocessing	371
AntDAS	statistical analysis	372
pymass	preprocessing	373
pySM	annotation	374
massPix	annotation	375
massPix	statistical analysis	376
speaq	preprocessing	377
mQTL	annotation	378
mQTL	preprocessing	379
NMRfilter	annotation	380
Metabolomics-Filtering	postprocessing	381
SMART	postprocessing	382
Mummichog	statistical analysis	383
X13CMS	statistical analysis	384
CliqueMS	annotation	385
MetaboQuant	preprocessing	386
QPMASS	preprocessing	387
Pathos	annotation	388
XCMS	annotation	389
GSim	annotation	390
COVAIN	statistical analysis	391
decoMS2	postprocessing	392
MetaboMiner	annotation	393
metabomxtr	statistical analysis	394
AMDIS	preprocessing	395
polyPK	statistical analysis	396
polyPK	postprocessing	397
MESSI	statistical analysis	398
FingerID	postprocessing	399
MetaboGroup S	postprocessing	400
MRMPROBS	annotation	401
MRMPROBS	postprocessing	402
Workflow4Metabolomics	statistical analysis	403
Workflow4Metabolomics	preprocessing	404
IntLIM	statistical analysis	405
MeltDB	preprocessing	406
ADAP/MZmine	preprocessing	407
LIMSA	preprocessing	408
SIMAT	preprocessing	409
Galaxy-M	preprocessing	410
mzMatch	annotation	411
metabnorm	postprocessing	412
PARADISe	postprocessing	413
PARADISe	annotation	414
MetCCS Predictor	statistical analysis	415
MetaboDiff	annotation	416
IsoMS-Quant	postprocessing	417
MyCompoundID	annotation	418
MAVEN	preprocessing	419
MAVEN	postprocessing	420
MET-XAlign	postprocessing	421
MultiAlign	annotation	422
COLMAR	statistical analysis	423
COLMAR	postprocessing	424
flagme	preprocessing	425
IPO	preprocessing	426
MAIT	statistical analysis	427
MAIT	preprocessing	428
Lilikoi	statistical analysis	429
BioNetApp	statistical analysis	430
CMM	annotation	431
BRAIN	statistical analysis	432
DiffCorr	statistical analysis	433
MAGMa	annotation	434
MSPrep	postprocessing	435
Maui-VIA	annotation	436
MetNorm	postprocessing	437
MetScape	annotation	438
nPYc-Toolbox	statistical analysis	439
pyQms	annotation	440
Automics	preprocessing	441
MetaBox	annotation	442
MAVEN	annotation	443
MetaboClust	statistical analysis	444
MetaboClust	annotation	445
InCroMAP	network/visualization	446
MetaboClust	network/visualization	447
MetabNet	network/visualization	448
MoDentify	network/visualization	449
GNPS	network/visualization	450
MetaboAnalyst	statistical analysis	451
MetaboAnalystR	statistical analysis	452
PathBank	database	453
Lilikoi	network/visualization	454
PiMP	data management/workflow	455
KIMBLE	data management/workflow	456
QCScreen	data management/workflow	457
HiCoNet	data management/workflow	458
GNPS	data management/workflow	459
Curatr	data management/workflow	460
BinBase	annotation	461
MetaComp	statistical analysis	462
SmileMS	annotation	463
MetFrag	annotation	464
MZmine 2	statistical analysis	465
MetFusion	annotation	466
MET-COFEA	annotation	467
MET-COFEA	preprocessing	468
LICRE	preprocessing	469
XCMS	postprocessing	470
ChemoSpec	preprocessing	471
NMRPro	postprocessing	472
specmine	preprocessing	473
Meta P-server	annotation	474
nmrglue	statistical analysis	475
MetiTree	annotation	476
iontree	annotation	477
MetaboLyzer	statistical analysis	478
PepsNMR	preprocessing	479
GSim	postprocessing	480
RAMClustR	annotation	481
CGBayesNets	statistical analysis	482
CFM-ID	annotation	483
CFM-ID	preprocessing	484
MetaboliteDetector	statistical analysis	485
ALLocater	preprocessing	486
MET-COFEI	annotation	487
Binner	annotation	488
VANTED	statistical analysis	489
MetCirc	statistical analysis	490
pyQms	postprocessing	491
eMZed	statistical analysis	492
MetDisease	annotation	493
CAMERA	annotation	494
DrugBank	database	495
MeRy-B	database	496
MultiAlign	network/visualization	497
CGBayesNets	network/visualization	498
PathWhiz	network/visualization	499
ALEX123	data management/workflow	500
IPO	data management/workflow	501
mzAccess	data management/workflow	502
UniProtKB	database	503
OPTIMAS-DW	database	504
BioCyc	database	505
SoyKB	database	506
VMH	network/visualization	507
swathTUNER	network/visualization	508
KIMBLE	network/visualization	509
PathVisio	network/visualization	510
MetaboAnalyst	annotation	511
MetaboAnalyst	preprocessing	512
VANTED	annotation	513
Lipid-Pro	annotation	514
XCMS online	annotation	515
XCMS online	preprocessing	516
XCMS online	statistical analysis	517
MetaboClust	postprocessing	518
MetaQuant	preprocessing	519
MetaQuant	annotation	520
iMet-Q	preprocessing	521
iMet-Q	postprocessing	522
NOMAD	preprocessing	523
XCMS online	postprocessing	524
MetFamily	statistical analysis	525
MetMSLine	annotation	526
RMassBank	annotation	527
MetMSLine	statistical analysis	528
MetMSLine	preprocessing	529
QPMASS	postprocessing	530
XCMS	preprocessing	531
mz.unity	annotation	532
MeltDB	postprocessing	533
MeltDB	statistical analysis	534
Metab	statistical analysis	535
\.


--
-- Data for Name: input_variables; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.input_variables (tool_name, input, rowid) FROM stdin;
MetaboAnalyst	MS peak list	1
MetaboAnalyst	NMR peak list	2
MetaboAnalyst	MS spectra	3
ALEX123	spectra	4
AMDORAP	spectra	5
apLCMS	spectra	6
ChemoSpec	spectra	7
Bayesil	NMR spectra	8
CFM-ID	peak list	9
CFM-ID	SMILES	10
CFM-ID	InChl	11
iontree	MS2	12
iontree	MS3	13
MetaboAnalystR	MS peak list	14
MetaboAnalystR	MS spectra	15
MetaboAnalystR	NMR peak list	16
MetaboAnalystR	concentration table	17
rNMR	spectra	18
ALLocater	raw LC-ESI-MS	19
CAMERA	peak table	20
alsace	spectral data	21
BATMAN	NMR spectra	22
batchCorr	peak table	23
Automics	spectra	24
\.


--
-- Data for Name: instrument_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.instrument_data (tool_name, instrument, rowid) FROM stdin;
Workflow4Metabolomics	LC-MS	1
MINMA	LC-MS	2
PiMP	LC-MS	3
SIMAT	GC-MS	4
HCOR	LC-MS	5
Ionwinze	LC-MS	6
swathTUNER	LC-MS	7
batchCorr	LC-MS	8
XCMS	LC-MS	9
RAMClustR	MS	10
metaMS	GC-MS	11
LDA	LC-MS	12
MetFlow	MS	13
MultiAlign	LC-MS	14
MetaDB	GC-MS	15
MetaDB	LC-MS	16
MAGMa	LC-MS	17
Metab	GC-MS	18
VSClust	LC-MS	19
metabomxtr	GC-MS	20
MSPrep	LC-MS	21
MWASTools	NMR	22
MWASTools	MS	23
MeltDB	GC-MS	24
Warpgroup	LC-MS	25
Warpgroup	GC-MS	26
BatMass	LC-MS	27
AMDORAP	LC-MS	28
Binner	LC-MS	29
Ideom	LC-MS	30
GNPS	GC-MS	31
MVAPACK	NMR	32
MAVEN	LC-MS	33
MET-IDEA	GC-MS	34
MET-IDEA	LC-MS	35
eRah	GC-MS	36
X13CMS	LC-MS	37
MeRy-B	NMR	38
MeRy-B	GC-MS	39
CliqueMS	LC-MS	40
MetaBox	GC-MS	41
CAMERA	LC-MS	42
crmn	GC-MS	43
FingerID	MS	44
MetShot	LC-MS	45
El-MAVEN	LC-MS	46
MMSAT	LC-MS	47
MMSAT	GC-MS	48
SmileMS	LC-MS	49
MetSign	MS	50
HAMMER	MS	51
MS2Analyzer	MS	52
Galaxy-M	MS	53
ALEX123	MS	54
MIDAS	MS	55
CSI:FingerrID	MS	56
MI-PACK	MS	57
msPurity	MS	58
IsoMS-Quant	LC-MS	59
Meta P-server	LC-MS	60
CFM-ID	MS	61
RMassBank	MS	62
ChemDistiller	MS	63
MyCompoundID	MS	64
Lipid-Pro	LC-MS	65
PlantMAT	MS	66
Skyline	LC-MS	67
Greazy	LC-MS	68
iontree	LC-MS	69
MS-FINDER	MS	70
MS-LAMP	MS	71
MetFamily	MS	72
BRAIN	MS	73
Zero-fill	LC-MS	74
mz.unity	LC-MS	75
xMSannotator	MS	76
MultiAlign	MS	77
IsoMS	LC-MS	78
iontree	MS	79
MS-FINDER	MS	80
nPYc-Toolbox	NMR	81
ChemoSpec	NMR	82
MetTailor	MS	83
MetCirc	MS	84
LipidXplorer	MS	85
FragPred	MS	86
QPMASS	GC-MS	87
SpectraClassifier	MRS	88
icoshift	NMR	89
nPYc-Toolbox	LC-MS	90
MetMatch	LC-MS	91
Warpgroup	MS	92
BinBase	GC-MS	93
ALLocater	LC-MS	94
CluMSID	LC-MS	95
METLIN	LC-MS	96
MetiTree	MS	97
RAMClustR	MS	98
MRMPROBS	MS	99
MS-LAMP	MS	100
alsace	HPLC	101
CROP	LC-MS	102
Qemistree	LC-MS	103
Notame	LC-MS	104
MetaboShiny	MS	105
Viime	MS	106
Viime	NMR	107
JS-MS	MS	108
pyMolNetEnhancer	MS	109
RMolNetEnhancer	MS	110
MassComp	MS	111
MetNormalizer	LC-MS	112
PYQUAN	GC-MS	113
MAIMS	LC-MS	114
MetaboloDerivatizer	GC-MS	115
R2DGC	GC-MS	116
AlpsNMR	NMR	117
ADAP-GC	GC-MS	118
MIDcor	GC-MS	119
MS-FLO	LC-MS	120
MetaboQC	LC-MS	121
LIQUID	LC-MS	122
AntDAS	LC-MS	123
pymass	LC-MS	124
NMRProcFlow	NMR	125
speaq	NMR	126
SigMa	NMR	127
MSHub	GC-MS	128
RGCxGC	GC-MS	129
ncGTW	LC-MS	130
TidyMS	LC-MS	131
Autotuner	MS	132
hRUV	LC-MS	133
MetumpX	MS	134
NMRfilter	NMR	135
XCMS online	LC-MS	136
TNO-DECO	GC-MS	137
IIS	MS	138
mzOS	LC-MS	139
MetShot	LC-MS	140
decoMS2	LC-MS	141
xMSanalyzer	MS-MS	142
MET-COFEI	GC-MS	143
mQTL.NMR	NMR	144
BioDendro	LC-MS	145
QSSR Automator	LC-MS	146
MetIDfyR	LC-MS	147
CANOPUS	MS	148
marr	MS	149
Plasmodesma	NMR	150
GISSMO	NMR	151
sampleDrift	NMR	152
VOCCluster	GC-MS	153
WiPP	GC-MS	154
Peakonly	LC-MS	155
HastaLaVista	NMR	156
DecoMetDIA	MS	157
rDolphin	NMR	158
MetaboMiner	NMR	159
iontree	LC-MS	160
pySM	MS	161
pyQms	LC-MS	162
MeltDB	LC-MS	163
MET-XAlign	LC-MS	164
MetaQuant	GC-MS	165
HappyTools	HPLC	166
Maui-VIA	GC-MS	167
BATMAN	NMR	168
MetaboHunter	NMR	169
AMDIS	GC-MS	170
MetaboClust	LC-MS	171
KIMBLE	NMR	172
eMZed	LC-MS	173
intCor	LC-MS	174
iMet-Q	LC-MS	175
TOXcms	LC-MS	176
MAIT	LC-MS	177
apLCMS	LC-MS	178
credential	LC-MS	179
Workflow4Metabolomics	NMR	180
TNO-DECO	LC-MS	181
GNPS	MS	182
IsoCor	GC-MS	183
ChromA	GC-MS	184
GAVIN	GC-MS	185
MetSign	LC-MS	186
MetNorm	GC-MS	187
Automics	NMR	188
KPIC	LC-MS	189
MetaboGroup S	LC-MS	190
MetaboGroup S	NMR	191
MetaboClust	NMR	192
MetaboClust	GC-MS	193
Newton	NMR	194
NMRPro	NMR	195
GSim	NMR	196
MZmine 2	LC-MS	197
mz.unity	LC-MS	198
ChemoSpec	Raman	199
El-MAVEN	GC-MS	200
matNMR	NMR	201
icoshift	MS	202
PepsNMR	NMR	203
mzMatch	LC-MS	204
ADAP/MZmine	GC-MS	205
ADAP/MZmine	LC-MS	206
MSClust	LC-MS	207
MSClust	GC-MS	208
ChemoSpec	XRF	209
QCScreen	LC-MS	210
ropls	LC-MS	211
MZedDB	MS	212
MetFusion	MS	213
proFIA	MS	214
MetFrag	MS	215
iMet	MS	216
MetAlign	LC-MS	217
MetAlign	GC-MS	218
MetaboLyzer	LC-MS	219
DeepRiPP	LC-MS	220
MetFamily	LC-MS	221
compMS2Miner	LC-MS	222
CMM	LC-MS	223
specmine	NMR	224
specmine	MS	225
specmine	UV-vis	226
specmine	IR	227
ChemoSpec	IR	228
metabnorm	NMR	229
COLMAR	NMR	230
InterpretMSSpectrum	GC-MS	231
MetExtract	LC-MS	232
MetaboLab	NMR	233
MetaboAnalystR	LC-MS	234
MetaboAnalystR	GC-MS	235
MetaboAnalystR	NMR	236
MET-COFEA	LC-MS	237
MetabNet	LC-MS	238
SpectConnect	GC-MS	239
ChromA	LC-MS	240
MetaboQuant	NMR	241
Metabolomics-Filtering	LC-MS	242
TargetSearch	GC-MS	243
Workflow4Metabolomics	GC-MS	244
Mummichog	LC-MS	245
ProbMetab	LC-MS	246
LIMSA	LC-MS	247
Bayesil	NMR	248
SMART	LC-MS	249
SMART	GC-MS	250
NP-StructurePredictor	LC-MS	251
RawTools	MS	252
misaR	MS	253
statTarget	MS	254
mwTab	MS	255
mwTab	NMR	256
Risa	NMR	257
Risa	MS	258
SLAW	LC-MS	259
MS2Query	MS	260
PARADISe	GC-MS	261
MetaboliteDetector	GC-MS	262
MS-FINDER	GC-MS	263
CMM	MS-MS	264
MetaNetter	LC-MS	265
mzAccess	LC-MS	266
El-MAVEN	LC-MS	267
MyCompoundID	LC-MS	268
TracMass2	LC-MS	269
GNPS	LC-MS	270
MetaboAnalyst	GC-MS	271
MetaboAnalyst	NMR	272
MetaboAnalyst	LC-MS	273
nmrglue	NMR	274
Galaxy-M	LC-MS	275
MetMSLine	LC-MS	276
MINEs	LC-MS	277
flagme	GC-MS	278
rNMR	NMR	279
\.


--
-- Data for Name: main; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.main (tool_name, year_released, website, date_modified, last_updated, notes) FROM stdin;
BiNCHE	2015	https://github.com/pcm32/BiNCheWeb	2020-09-10	2015-08-19	
swathTUNER	2015	https://sourceforge.net/projects/swathtuner/	2020-09-10	2019-01-30	
IsoMS	2015	http://www.mycompoundid.org/mycompoundid_IsoMS/IsoMS.jsp	2020-09-15	2015-03-08	
polyPK	2018	https://cran.r-project.org/web/packages/polyPK/index.html	2020-09-18	2018-07-11	
nPYc-Toolbox	2019	https://github.com/phenomecentre/nPYc-Toolbox	2020-10-08	2020-10-07	
MetaboNetworks	2014	MetaboNetworks	2020-10-09	2017-09-05	
MultiAlign	2013	https://omics.pnl.gov/software/multialign	2020-10-13	2014-04-01	
OPTIMAS-DW	2012	https://apex.ipk-gatersleben.de/apex/f?p=270:1::::::	2020-10-13	\N	
DiffCorr	2013	https://sourceforge.net/projects/diffcorr/	2020-10-13	2017-02-13	
MassTRIX	2008	http://masstrix3.helmholtz-muenchen.de/masstrix3/	2020-10-15	2008-05-20	
MeRy-B	2014	http://services.cbib.u-bordeaux.fr/MERYB/	2020-10-20	2018-02-09	
DrugBank	2006	https://go.drugbank.com	2020-10-20	2020-07-02	
CliqueMS	2019	https://github.com/osenan/cliqueMS	2020-10-22	2020-03-26	
SMPDB	2010	https://smpdb.ca	2020-10-22	\N	
KeggArray	2009	https://www.kegg.jp/kegg/download/kegtools.html	2020-10-27	2018-06-08	
MetScape	2012	http://metscape.med.umich.edu	2020-06-08	2017-05-03	
Binner	2019	https://binner.med.umich.edu	2020-06-08	\N	
RAMClustR	2014	https://github.com/cbroeckl/RAMClustR	2020-06-08	2020-04-14	
MSPrep	2013	https://github.com/KechrisLab/MSPrep	2020-06-08	2019-03-21	
rNMR	2009	http://rnmr.nmrfam.wisc.edu	2020-06-08	2015-02-09	
ALLocater	2014	https://allocator.cebitec.uni-bielefeld.de/landing.htm	2020-06-08	2014-11-26	
AMDIS	1996	https://chemdata.nist.gov/dokuwiki/doku.php?id=chemdata:amdis	2020-06-08	2019-11-25	
BATMAN	2012	http://batman.r-forge.r-project.org	2020-06-08	2016-06-19	
CAMERA	2012	http://bioconductor.org/packages/release/bioc/html/CAMERA.html	2020-06-08	2019-10-29	
HMDB	2007	https://hmdb.ca	2020-06-08	2018-01-01	
SAMMI	2019	https://bioinformatics.mdanderson.org/Software/SAMMI/	2020-06-08	2019-03-25	Not sure if this tool falls under annotation?
credential	2014	http://pattilab.wustl.edu/software/credential	2020-06-08	2019-06-27	
decoMS2	2013	http://pattilab.wustl.edu/software/decoms2	2020-06-08	\N	
FragPred	2015	http://pattilab.wustl.edu/software/FragPred	2020-06-08	\N	
MetaboMiner	2008	http://wishart.biology.ualberta.ca/metabominer/	2020-06-08	2008-05-25	
XCMS	2006	https://www.bioconductor.org/packages/release/bioc/html/xcms.html	2020-06-08	2020-03-04	
alsace	2014	https://www.bioconductor.org/packages/release/bioc/html/alsace.html	2020-06-08	2019-05-24	
batchCorr	2016	https://gitlab.com/CarlBrunius/batchCorr	2020-06-08	2020-01-09	
ADAP/MZmine	2019	https://github.com/du-lab/mzmine3	2020-06-25	2020-04-24	
cosmiq	\N	http://bioconductor.org/packages/release/bioc/html/cosmiq.html	2020-07-01	2020-04-27	
crmn	2009	https://cran.r-project.org/web/packages/crmn/index.html	2020-07-01	2020-02-10	
CSI:FingerrID	2015	https://www.csi-fingerid.uni-jena.de	2020-07-01	2016-02-02	
eMZed	2013	https://emzed.ethz.ch/index.html	2020-07-01	2018-04-26	
eRah	2016	https://cran.r-project.org/web/packages/erah/index.html	2020-07-01	2018-07-11	
FingerID	2012	https://github.com/icdishb/fingerid	2020-07-01	2016-10-12	
flagme	2014	https://bioconductor.org/packages/release/bioc/html/flagme.html	2020-07-01	2020-04-27	
Galaxy-M	2015	https://github.com/Viant-Metabolomics/Galaxy-M	2020-07-01	2016-11-22	
GSim	2006	https://sourceforge.net/projects/gsim/	2020-07-01	2013-04-30	
HCOR	2014	http://b2slab.upc.edu/software-and-tutorials/retention-time-drift-correction/	2020-07-01	\N	
ICT	2015	https://github.com/jungreuc/isotope_correction_toolbox/	2020-07-01	2017-08-27	
FALCON	2015	https://github.com/bbarker/FALCON	2020-09-10	2016-07-06	
IsoCor	2011	https://github.com/MetaSys-LISBP/IsoCor	2020-07-01	2020-02-25	
IPO	2015	https://www.bioconductor.org/packages/release/bioc/html/IPO.html	2020-07-01	2020-04-27	
MESSI	2015	http://sbb.hku.hk/MESSI/	2020-09-10	\N	
LIMSA	2006	http://www.helsinki.fi/science/lipids/software.html	2020-07-03	2006-07-31	
LipidXplorer	2012	https://lifs.isas.de/wiki/index.php/Main_Page	2020-07-03	2020-02-10	
MAGMa	2013	http://www.emetabolomics.org/magma	2020-07-03	\N	
Maui-VIA	2014	https://bimsbstatic.mdc-berlin.de/kempa/software/kempaSoftware.html	2020-07-03	2014-12-17	
MAVEN	2010	http://genomics-pubs.princeton.edu/mzroll/index.php	2020-07-03	2020-06-11	
MeltDB	2008	https://meltdb.cebitec.uni-bielefeld.de/cgi-bin/login.cgi	2020-07-03	2011-10-11	
MET-COFEA	2013	http://bioinfo.noble.org/manuscript-support/met-cofea/	2020-07-03	2016-01-12	
PathVisio	2015	https://pathvisio.github.io	2020-09-22	2019-05-01	
MET-IDEA	2006	http://bioinfo.noble.org/download/	2020-07-03	2012-05-03	
MET-XAlign	\N	http://bioinfo.noble.org/manuscript-support/met-xalign/	2020-07-03	2015-03-30	
Metab	2011	https://www.bioconductor.org/packages/release/bioc/html/Metab.html	2020-07-03	2020-04-27	
MetaboHunter	2011	https://www.nrcbioinformatics.ca/metabohunter/	2020-07-03	2018-01-24	
MetabolAnalyze	2010	https://cran.r-project.org/web/packages/MetabolAnalyze/index.html	2020-07-03	2019-08-31	
MetaboliteDetector	2009	https://md.tu-bs.de	2020-07-03	2018-03-29	
MetaboLyzer	2013	https://sites.google.com/a/georgetown.edu/fornace-lab-informatics/home/metabolyzer	2020-07-06	2013-06-13	
metabomxtr	2014	https://www.bioconductor.org/packages/release/bioc/html/metabomxtr.html	2020-07-06	2020-04-27	
CGBayesNets	2014	http://www.cgbayesnets.com	2020-10-09	2020-03-23	
MetaboSearch	2012	http://omics.georgetown.edu/metabosearch.html	2020-07-06	\N	
MetaDB	2013	https://github.com/rmylonas/MetaDB	2020-07-06	2014-04-09	
MetAlign	2010	https://www.wur.nl/en/show/MetAlign-1.htm	2020-07-06	2012-04-10	
metaMS	2014	https://www.bioconductor.org/packages/release/bioc/html/metaMS.html	2020-07-06	2020-04-29	
MetaQuant	2006	http://bioinformatics.org/metaquant/	2020-07-06	2008-09-04	
MetExtract	2011	https://code.google.com/archive/p/metextract/	2020-07-06	2011-12-07	
MetFrag	2010	https://msbi.ipb-halle.de/MetFragBeta/	2020-07-06	\N	
MetFusion	2013	http://mgerlich.github.io/MetFusion/	2020-07-07	2014-03-07	
MetiTree	2012	http://www.metitree.nl	2020-07-07	2012-06-14	
MetNorm	2015	https://cran.r-project.org/web/packages/MetNorm/index.html	2020-07-07	2015-02-08	
MetShot	2012	https://github.com/sneumann/MetShot	2020-07-07	2016-05-23	
MetTailor	2015	https://sourceforge.net/projects/mettailor/	2020-07-07	2015-08-20	
MetaBridge	2018	https://metabridge.org	2020-08-17	2018-08-30	
MI-PACK	2010	http://www.biosciences-labs.bham.ac.uk/viant/mipack/	2020-07-07	\N	
MMSAT	2011	https://powcs.med.unsw.edu.au/research/adult-cancer-program/services-resources/mmsat	2020-07-07	2011-11-18	
MRMPROBS	2013	http://prime.psc.riken.jp/compms/mrmprobs/main.html	2020-07-13	2019-01-15	
MZedDB	2008	http://maltese.dbs.aber.ac.uk:8888/hrmet/index.html	2020-07-13	2009-12-14	
mzMatch	2011	http://mzmatch.sourceforge.net/index.php	2020-07-13	2017-11-11	
MS-LAMP	2012	http://ms-lamp.igib.res.in	2020-07-13	\N	
MS2Analyzer	2013	https://fiehnlab.ucdavis.edu/projects/MS2Analyzer/	2020-07-13	2016-11-18	
MSClust	2013	https://www.wur.nl/nl/Onderzoek-Resultaten/Onderzoeksinstituten/food-safety-research/show-rikilt/MetAlign.htm	2020-07-13	\N	
MS-FINDER	2016	http://prime.psc.riken.jp/compms/msfinder/main.html	2020-07-13	2020-06-13	
muma	2012	https://cran.r-project.org/web/packages/muma/index.html	2020-07-14	2012-12-03	
MyCompoundID	2013	http://www.mycompoundid.org/mycompoundid_IsoMS/	2020-07-14	2013-12-19	
mzOS	2015	https://github.com/jerkos/mzOS	2020-07-15	2016-09-12	
mzR	2011	http://bioconductor.org/packages/release/bioc/html/mzR.html	2020-07-15	2020-04-27	
Newton	2013	http://newton.nmrfam.wisc.edu	2020-07-15	\N	
nmrglue	2013	https://www.nmrglue.com	2020-07-15	2020-06-24	
QCScreen	2015	https://sourceforge.net/projects/qcscreen/	2020-07-15	2018-02-21	
Rdisop	2008	http://bioconductor.org/packages/release/bioc/html/Rdisop.html	2020-07-15	2020-04-27	
Escher	2015	https://escher.github.io/#/	2020-09-10	2019-09-05	
RMassBank	2013	https://bioconductor.org/packages/release/bioc/html/RMassBank.html	2020-07-15	2020-04-27	
ropls	2015	http://bioconductor.org/packages/release/bioc/html/ropls.html	2020-07-16	2020-04-27	
SIMAT	2015	http://bioconductor.org/packages/3.1/bioc/html/SIMAT.html	2020-07-16	2015-04-16	
PepsNMR	2015	https://github.com/ManonMartin/PepsNMR	2020-07-16	2019-08-02	
QPMASS	2020	http://ftp//download.big.ac.cn/QPmass/QPmass_V1.0.zip	2020-09-15	\N	
SpectConnect	2007	http://spectconnect.mit.edu	2020-07-16	\N	not sure about this tool
TargetSearch	2009	http://bioconductor.org/packages/2.5/bioc/html/TargetSearch.html	2020-07-17	\N	
vaLID	2013	http://neurolipidomics.ca	2020-07-17	2017-01-11	
Workflow4Metabolomics	2014	https://workflow4metabolomics.org	2020-07-17	2020-05-13	
xMSanalyzer	2013	https://sourceforge.net/projects/xmsanalyzer/	2020-07-17	2019-07-08	
MetaBox	2014	https://github.com/kwanjeeraw/metabox	2020-07-17	2017-02-28	
MetaMapR	2014	http://dgrapov.github.io/MetaMapR/	2020-07-20	2014-05-21	
MetSign	2011	http://metaopen.sourceforge.net/metsign.html	2020-07-20	2016-05-10	
MIDAS	2014	https://facultyweb.mga.edu/yingfeng.wang/Assets/midas/midas.html	2020-07-20	2014-03-13	
ChromA	2009	https://bibiserv.cebitec.uni-bielefeld.de/chroma/	2020-07-20	\N	
GAVIN	2011	https://ars.els-cdn.com/content/image/1-s2.0-S000326971100234X-mmc1.zip	2020-07-20	2011-01-06	
HAMMER	2014	http://www.biosciences-labs.bham.ac.uk/viant/hammer/	2020-07-20	\N	not really a tool 
HiCoNet	2019	https://github.com/shuzhao-li/hiconet	2021-02-15	2019-06-02	
icoshift	2011	http://www.models.life.ku.dk/icoshift	2020-07-20	2017-03-28	
InCroMAP	2011	http://www.ra.cs.uni-tuebingen.de/software/InCroMAP/index.htm	2020-07-20	2013-05-28	
IIS	2014	http://bioinfo03.ibi.unicamp.br/lnbio/IIS2/	2020-07-20	\N	INCOMPLETE, not sure how to classify this tool
LICRE	2014	https://sites.google.com/site/licrerepository/	2020-07-20	2014-09-25	
MathDAMP	2006	http://mathdamp.iab.keio.ac.jp	2020-07-20	2006-05-05	
matNMR	1999	http://matnmr.sourceforge.net	2020-07-20	2015-11-21	
MetabNet	2015	https://sourceforge.net/projects/metabnet/	2020-07-21	2017-07-17	
MetaboQuant	2013	https://www.uni-regensburg.de/medicine/statistical-bioinformatics/software/software-from-gronwald-group/metaboquant/index.html	2020-07-21	2013-02-05	
PAPi	2013	https://www.bioconductor.org/packages/release/bioc/html/PAPi.html	2020-07-22	2020-04-27	
TNO-DECO	2010	https://github.com/NetherlandsMetabolomicsCentre/TNO-DECO	2020-07-22	2013-12-24	
Metabolomics-Filtering	2019	https://github.com/courtneyschiffman/Metabolomics-Filtering	2020-07-23	2019-03-21	
missForest	2011	https://cran.r-project.org/web/packages/missForest/index.html	2020-07-23	2013-12-31	
MetImp	2017	https://metabolomics.cc.hawaii.edu/software/MetImp/	2020-07-23	2018-05-21	
ChemDistiller	2018	https://bitbucket.org/iAnalytica/chemdistillerpython/src/master/	2020-07-23	2017-10-23	
MetExplore	2018	https://metexplore.toulouse.inra.fr/index.html/	2020-07-23	\N	
Pathos	2011	http://motif.gla.ac.uk/Pathos/index.html	2020-07-23	2019-01-23	
PaintOmics	2011	http://www.paintomics.org	2020-07-23	\N	
VANTED	2006	https://www.cls.uni-konstanz.de/software/vanted/	2020-07-23	\N	
Ideom	2012	http://mzmatch.sourceforge.net/ideom.php	2020-07-25	2017-11-11	
mz.unity	2016	http://pattilab.wustl.edu/software/mzunity	2020-07-25	2016-04-09	
Warpgroup	2015	http://pattilab.wustl.edu/software/warpgroup	2020-07-25	2017-01-24	
KMDA	2015	https://cran.r-project.org/web/packages/KMDA/	2020-07-25	2015-04-01	
CFM-ID	2014	http://cfmid.wishartlab.com	2020-07-25	2016-11-16	not sure where these methods should be categorized
MET-COFEI	2014	http://bioinfo.noble.org/manuscript-support/met-cofei/index.php?page=Introduction	2020-07-25	2014-04-15	
TriDAMP	2007	http://mathdamp.iab.keio.ac.jp/tridamp/	2020-07-26	\N	
Meta P-server	2010	http://metap.helmholtz-muenchen.de/metap2/	2020-07-26	2010-11-23	
MZmine 2	2010	http://mzmine.github.io	2020-07-29	2020-03-27	
Bayesil	2015	http://bayesil.ca	2020-09-18	\N	
MAIT	2014	https://www.bioconductor.org/packages/release/bioc/html/MAIT.html	2020-08-07	2020-06-15	
TOXcms	2019	http://pattilab.wustl.edu/software/toxcms	2020-08-07	\N	
MetDisease	2014	http://metdisease.ncibi.org	2020-08-10	2014-03-04	
ChemoSpec	2009	https://cran.r-project.org/web/packages/ChemoSpec/index.html	2020-09-18	2020-01-24	
Rhea	2015	https://www.rhea-db.org	2020-08-11	2020-06-17	
UniProtKB	2005	https://www.uniprot.org	2020-08-11	2018-04-10	
MetaboLights	2012	https://www.ebi.ac.uk/metabolights/	2020-08-11	\N	
MetaboGroup S	2018	https://omicstools.shinyapps.io/MetaboGroupSapp/	2020-08-11	\N	
E-Dragon	2005	http://www.vcclab.org/lab/edragon/	2020-10-09	\N	
PhenoMeNal	2018	https://phenomenal-h2020.eu/home/	2020-08-11	2019-06-27	
MoDentify	2018	https://github.com/krumsieklab/MoDentify	2020-08-12	2019-04-07	
HappyTools	2018	https://github.com/Tarskin/HappyTools	2020-08-12	2020-03-17	
MetaboDiff	2018	https://github.com/andreasmock/MetaboDiff/	2020-08-17	2019-07-08	
VSClust	2018	https://bitbucket.org/veitveit/vsclust/src/master/	2020-08-17	2020-08-12	
MINEs	2015	https://minedatabase.mcs.anl.gov/#/home	2020-08-18	\N	
CMM	2018	http://ceumass.eps.uspceu.es	2020-08-18	2020-06-01	
IntLIM	2018	https://github.com/mathelab/IntLIM	2020-08-18	2019-04-09	
GSimp	2018	https://github.com/WandeRum/GSimp	2020-08-18	2020-01-29	
SistematX	2018	https://sistematx.ufpb.br	2020-08-18	\N	
MINMA	2017	http://web1.sph.emory.edu/users/tyu8/MINMA	2020-08-19	2017-04-06	
Curatr	2018	https://curatr.mcf.embl.de	2020-08-19	2019-11-03	
BinBase	2011	http://binbase.sourceforge.net/	2020-08-19	2013-04-13	
WikiPathways	2018	https://www.wikipathways.org/index.php/WikiPathways	2020-08-19	2017-12-10	
NP-StructurePredictor	2017	http://npstructurepredictor.cmdm.tw/NPSP.rar	2020-08-19	\N	
PAMBD	2018	http://pseudomonas.umaryland.edu/	2020-08-19	\N	
mixOmics	2017	http://mixomics.org	2020-08-19	2020-08-09	
UC2	2018	http://webs2.kazusa.or.jp/mfsearcher/	2020-08-20	\N	
proFIA	2017	http://bioconductor.org/packages/release/bioc/html/proFIA.html	2020-08-20	2020-04-27	
MetaNetter	2018	http://apps.cytoscape.org/apps/metanetter2	2020-08-20	2018-01-01	
LiverWiki	2017	http://liverwiki.hupo.org.cn/mediawiki/index.php/Main_Page	2020-08-20	2017-09-19	
SysteMHC	2018	https://systemhcatlas.org	2020-08-20	\N	
MetaComp	2017	https://github.com/pzhaipku/MetaComp	2020-08-20	2017-11-13	
PiMP	2017		2020-08-20	2017-06-17	
MWASTools	2018	https://bioconductor.org/packages/release/bioc/html/MWASTools.html	2020-08-20	2020-04-27	
RDFIO	2017	https://pharmb.io/project/rdfio/	2020-08-21	2017-08-17	
pyQms	2017	https://github.com/pyQms/pyqms	2020-08-21	2020-06-18	
KPIC	2017	https://github.com/hcji/KPIC2	2020-08-21	2019-03-24	
mzAccess	2017	http://www.mzaccess.org	2020-08-21	2018-06-29	
MoNA	\N	https://mona.fiehnlab.ucdavis.edu	2020-08-24	2020-06-09	
SmileMS	2009	http://genebio.com/products/smilems/	2020-08-24	\N	
SDAMS	2019	https://www.bioconductor.org/packages/release/bioc/html/SDAMS.html	2020-08-24	2020-04-27	
MetCirc	2017	https://github.com/PlantDefenseMetabolism/MetCirc	2020-08-24	2017-06-17	
PathBank	2020	www.pathbank.org	2020-08-24	2020-02-19	
IsoMS-Quant	2015	http://www.mycompoundid.org/mycompoundid_IsoMS/quant.jsp	2020-09-15	2015-04-09	
MetCCS Predictor	2016	http://www.metabolomics-shanghai.org/MetCCS/	2020-08-24	2017-03-30	
compMS2Miner	2017	https://wmbedmands.shinyapps.io/compMS2Example/	2020-08-24	2017-04-18	
PathWhiz	2017	https://smpdb.ca/pathwhiz	2020-08-25	\N	
SpectraClassifier	2015	http://gabrmn.uab.es/?q=sc	2020-08-25	2015-07-08	
SoyKB	2014	http://soykb.org	2020-08-25	2015-08-01	
xMSannotator	2017	https://sourceforge.net/projects/xmsannotator/	2020-08-25	2019-07-01	
ChemSpider	2010	https://www.chemspider.com	2020-08-25	\N	
LIPID MAPS	2015	https://lipidmaps.org	2020-08-25	2020-06-10	
PlantMAT	2016	https://sourceforge.net/projects/plantmat/	2020-09-01	2016-11-30	
YMDB	2017	http://www.ymdb.ca/	2020-09-01	\N	
MetMatch	2016	https://metabolomics-ifa.boku.ac.at/MetMatch/index.html	2020-09-01	2016-06-21	
PathoLogic	2011	https://metacyc.org	2020-09-03	\N	
ConsensusPathDB	2013	http://consensuspathdb.org	2020-09-03	2019-01-15	
InterpretMSSpectrum	2016	https://cran.r-project.org/web/packages/InterpretMSSpectrum/index.html	2020-09-03	2018-05-03	
BatMass	2016	https://github.com/chhh/batmass	2020-09-03	2020-01-24	
SMART	2016	http://www.stat.sinica.edu.tw/hsinchou/metabolomics/SMART.htm	2020-09-03	2019-03-27	
Greazy	2017	http://tiny.cc/bumbershoot-vc12-bin64	2020-09-08	\N	
NMRPro	2016	https://github.com/ahmohamed/nmrpro	2020-09-08	2016-10-17	
PARADISe	2017	http://www.models.life.ku.dk/GCparadise	2020-09-22	2019-03-03	
GAM	2016	https://artyomovlab.wustl.edu/shiny/gam/	2020-09-08	2018-05-16	
Skyline	2020	https://github.com/ProteoWizard/pwiz/tree/master/pwiz_tools/Skyline	2020-09-08	2020-08-06	
ORCA	\N	https://sourceforge.net/projects/exorca/	2020-10-09	2015-11-06	
libChEBI	2016		2020-09-10	2020-08-07	
El-MAVEN	2019	https://github.com/ElucidataInc/ElMaven	2020-10-13	2020-09-28	
MaConDa	2012	https://maconda.bham.ac.uk	2020-10-15	2012-09-01	
GNPS-MassIVE	2016	https://gnps.ucsd.edu/ProteoSAFe/datasets.jsp#%7B%22query%22%3A%7B%7D%2C%22table_sort_history%22%3A%22createdMillis_dsc%22%2C%22title_input%22%3A%22GNPS%22%7D	2020-10-15	2020-10-12	
BiGG Models	2016	http://bigg.ucsd.edu	2020-10-20	2019-11-01	
mineXpert	2019	http://msxpertsuite.org/wiki/pmwiki.php/Main/Minexpert2	2020-10-20	2020-10-12	
MetMask	2010	https://github.com/hredestig/metmask	2020-10-22	2017-04-09	
PeroxisomeDB	2010	http://www.peroxisomedb.org	2020-10-22	\N	
GabiPD	\N	http://www.gabipd.org	2020-10-27	\N	
BioNetApp	2019	https://zenodo.org/record/2563129#.X5hAZC1h2-w	2020-10-27	2018-03-08	
RaMP	2018	https://github.com/Mathelab/RaMP-DB	2020-10-27	2019-03-19	
CluMSID	2019	https://bioconductor.org/packages/release/bioc/html/CluMSID.html	2020-10-27	2020-10-16	
Lilikoi	2018	https://lilikoi.shinyapps.io/lilikoi/	2020-10-27	2018-08-21	
MetaboClust	2018	https://bitbucket.org/mjr129/metaboclust/src/master/	2020-10-27	2018-08-22	
VMH	2019	https://vmh.life	2020-10-27	2019-05-01	
METLIN	2005	http://metlin.scripps.edu	2021-02-15	2020-04-23	
apLCMS	2009	http://web1.sph.emory.edu/apLCMS/	2021-02-15	2019-11-20	
TracMass2	2010	https://pubs.acs.org/doi/suppl/10.1021/ac403905h	2021-02-15	2013-11-18	
X13CMS	2014	http://pattilab.wustl.edu/software/x13cms	2021-02-15	\N	
ProbMetab	2013	http://labpib.fmrp.usp.br/methods/probmetab/	2021-02-15	2013-11-06	
Mummichog	2013	http://mummichog.org	2021-02-15	2020-04-23	
DeepRiPP	2019	http://deepripp.magarveylab.ca/	2021-02-15	2019-06-24	
AMDORAP	2011	http://amdorap.sourceforge.net	2021-02-15	2012-12-13	
iMet-Q	2016	http://ms.iis.sinica.edu.tw/comics/Software_iMet-Q.html	2021-02-15	2015-10-02	
intCor	2014	http://b2slab.upc.edu/software-and-tutorials/intensity-drift-correction/	2021-02-15	\N	
Ionwinze	2013	https://sourceforge.net/projects/ionwinze/	2021-02-15	2016-06-24	
specmine	2015	https://cran.r-project.org/src/contrib/Archive/specmine/	2021-02-15	2020-07-10	
msPurity	2015	https://bioconductor.org/packages/devel/bioc/html/msPurity.html	2021-02-15	2020-04-27	
Normalyzer	2014	http://normalyzer.immunoprot.lth.se/normalize.php	2021-02-15	2014-12-14	
MVAPACK	2014	http://bionmr.unl.edu/mvapack.php	2021-02-15	2015-07-14	
mQTL.NMR	2015	https://bioconductor.riken.jp/packages/3.1/bioc/html/mQTL.NMR.html	2021-02-15	2015-04-16	
MetFlow	2019	http://metflow.zhulab.cn	2021-02-15	2018-12-13	
Zero-fill	2015	http://www.mycompoundid.org/mycompoundid_IsoMS/zero.jsp	2021-02-15	2014-03-19	
XCMS online	2013	https://xcmsonline.scripps.edu	2021-02-15	2020-04-23	
BRAIN	2013	http://www.bioconductor.org/packages/release/bioc/html/BRAIN.html	2021-02-15	2020-04-27	
MetFamily	2016	https://msbi.ipb-halle.de/MetFamily/	2021-02-15	2020-03-17	
COVAIN	2012	https://mosys.univie.ac.at/resources/software/	2021-02-15	2017-05-23	
MetMSLine	2015	http://wmbedmands.github.io/MetMSLine/	2021-02-15	2017-03-16	
CCPN Metabolomics	2015	https://www.ccpn.ac.uk/acl_users/credentials_cookie_auth/require_login?came_from=https%3A//www.ccpn.ac.uk/collaborations/metabolomics	2021-02-15	2020-04-23	
Lipid-Pro	2015	https://www.biozentrum.uni-wuerzburg.de/en/neurogenetics/services/lipidpro/	2021-02-15	2015-04-01	
iPath	2018	http://pathways.embl.de/	2021-02-15	\N	
metabnorm	2014	https://sourceforge.net/projects/metabnorm/	2021-02-15	2014-11-12	
COLMAR	2011	http://spin.ccic.ohio-state.edu/index.php/colmar	2021-02-15	2018-04-01	
iontree	2013	https://www.bioconductor.org/packages/3.6/bioc/html/iontree.html	2021-02-15	2018-04-12	deprecated in bioconductor
ALEX123	2013	www.msLipidomics.info	2021-02-15	2018-03-23	
Pathview	2017	https://pathview.uncc.edu	2021-02-15	\N	
LDA	2010	http://genome.tugraz.at/lda/lda_description.shtml	2021-02-15	2013-11-28	
KIMBLE	2018	http://cpm.lumc.nl/kimble/	2021-02-15	\N	
BioCyc	2015	https://www.biocyc.org	2021-02-15	2020-05-24	
jQMM	2017	https://github.com/JBEI/jqmm	2021-02-15	2017-02-17	
RankProd	2017	https://www.bioconductor.org/packages/devel/bioc/html/RankProd.html	2021-02-15	2020-06-11	
GNPS	2016	https://gnps.ucsd.edu/ProteoSAFe/static/gnps-splash.jsp	2021-02-15	2020-05-22	
MetaboLab	2000	https://www.ludwiglab.org/software-development/metabolab/index.html	2021-02-15	2019-12-08	
Pachyderm	2018	https://github.com/pachyderm/pachyderm	2021-02-15	2020-08-05	
MarVis-Suite	2009	http://marvis.gobics.de	2021-02-15	\N	
NOMAD	2017	https://github.com/carlmurie/NOMAD	2021-02-15	2017-11-16	
MetaboAnalystR	2018	https://github.com/xia-lab/MetaboAnalystR	2021-02-15	2020-04-10	
MetaboAnalyst	2009	https://www.metaboanalyst.ca/	2021-02-15	2020-04-08	when a tool is a workflow/suite there are too many input and output variables making it difficult to tell what inputs correspond to what outputs, input and output variables may not be feasible for such tools
mQTL	2013	https://cran.r-project.org/web/packages/mQTL/index.html	2021-02-15	2013-10-21	
Automics	2009	https://www.softpedia.com/get/Science-CAD/Automics.shtml	2021-02-15	2009-10-10	This tool does multiple things and each will return a set of outputs, not sure about the feasibility of including such tools in the Sankey constructor.
CROP	2020	https://github.com/rendju/CROP	2021-08-30	2020-07-09	
Qemistree	2020	https://github.com/biocore/q2-qemistree	2021-08-30	2021-06-08	\N
Notame	2020	https://github.com/antonvsdata/notame	2021-08-30	2021-07-04	used with MS-DIAL
MetaboShiny	2020	https://github.com/joannawolthuis/Metaboshiny	2021-08-30	2021-01-13	integrates MetaboAnalystR and caret
Viime	2020	https://viime.org/#/	2021-08-30	2021-03-11	
NOREVA	2020	http://idrblab.cn/noreva/	2021-08-30	\N	not open source but putting in because well documented
JS-MS	2017	https://github.com/optimusmoose/jsms	2021-08-30	2020-10-01	entering 1.0 and 2.0 as one tool
pyMolNetEnhancer	2019	https://github.com/madeleineernst/pymolnetenhancer	2021-08-30	2019-11-28	python version of molnetenhancer
RMolNetEnhancer	2019	https://github.com/madeleineernst/RMolNetEnhancer	2021-08-30	2019-07-10	R version of MolNetEnhancer
ZEAMAP	2020	http://zeamap.com/	2021-08-30	2020-05-20	maize multiomics datbase
MassComp	2019	https://github.com/iochoa/MassComp	2021-08-30	2019-04-03	lossless compressor for mass spectrometry data 
iMet	2017	http://imet.seeslab.net/	2021-09-12	\N	
MetNormalizer	2016	https://github.com/jaspershen/MetNormalizer/	2021-09-12	2021-03-03	
tmod	2016	https://github.com/cran/tmod	2021-09-12	2020-10-02	
PYQUAN	2016	https://github.com/lycopoda/Pyquan	2021-09-12	2018-02-24	
MAIMS	2017	https://github.com/savantas/MAIMS	2021-09-12	2017-10-13	
POMAShiny	2021	https://github.com/nutrimetabolomics/POMAShiny	2021-09-13	2021-04-19	based on the POMA R/Bioconductor package
MetaboloDerivatizer	2017	http://prime.psc.riken.jp/compms/others/main.html#Derivatizer	2021-09-13	2017-03-30	
R2DGC	2018	https://github.com/hudsonalpha/r2dgc	2021-09-13	2017-08-22	
AlpsNMR	2020	https://github.com/sipss/AlpsNMR	2021-09-13	2021-08-05	
ADAP-GC	2010	https://github.com/du-lab/ADAP-GC-3.0	2021-09-13	2017-03-31	later versions incorporated to mzmine
MIDcor	2017	https://github.com/seliv55/mid_correct	2021-09-13	2019-02-11	isotopic labelling analysis (this should be approach)
MS-FLO	2017	https://bitbucket.org/fiehnlab/ms-flo/src/master/	2021-09-13	2020-05-28	
MetaboQC	2017	https://github.com/cran/MetaboQC	2021-09-13	2016-09-15	
LIQUID	2017	https://github.com/PNNL-Comp-Mass-Spec/LIQUID	2021-09-13	2021-07-09	
AntDAS	2017	software.tobaccodb.org/software/antdas	2021-09-13	\N	
pymass	2017	https://github.com/zmzhang/pymass	2021-09-13	2018-01-11	
pySM	2016	https://github.com/alexandrovteam/pySM	2021-09-14	2016-11-15	imaging MS (should be approach), spatial metabolomics
massPix	2017	https://github.com/hallz/massPix	2021-09-14	2017-08-08	imaging (approach)
NMRProcFlow	2017	https://nmrprocflow.org/	2021-09-14	2019-09-03	
speaq	2011	https://github.com/beirnaert/speaq	2021-09-14	2019-12-10	
SigMa	2020	https://github.com/BEKZODKHAKIMOV/SigMa_Ver1	2021-09-14	2020-05-26	
MSHub	2020	https://ccms-ucsd.github.io/GNPSDocumentation/gcanalysis/	2021-09-14	2020-11-13	autodeconvolution , meant to be used with GNPS
RGCxGC	2020	https://github.com/danielquiroz97/rgcxgc	2021-09-14	2020-04-22	2D GC data
ncGTW	2020	https://github.com/chiungtingwu/ncgtw	2021-09-14	2019-12-29	meant to be used with xcms
TidyMS	2020	https://github.com/griquelme/tidyms	2021-09-14	2021-08-24	
Autotuner	2020	https://github.com/crmclean/autotuner	2021-09-14	2021-01-21	
hRUV	2021	https://sydneybiox.github.io/hRUV/	2021-09-14	2021-05-19	
MetumpX	2020	https://github.com/hasaniqbal777/metumpx-bin	2021-09-14	2020-01-11	MetumpX is a Ubuntu based software package that facilitate easy download and installation of about 89 tools related to standard Untargeted Metabolomics Mass Spectrometry pipeline.
fobitools	2020	https://github.com/pcastellanoescuder/fobitools	2021-09-14	2021-07-23	
fobitoolsGUI	2020	https://github.com/pcastellanoescuder/fobitoolsGUI	2021-09-14	2021-07-24	shiny version of fobitools
BioDendro	2020	https://github.com/ccdmb/BioDendro	2021-09-14	2021-02-25	
QSSR Automator	2020	https://github.com/UofUMetabolomicsCore/QSRR_Automator	2021-09-14	2020-05-21	
MetIDfyR	2020	https://github.com/agnesblch/MetIDfyR	2021-09-14	2021-01-18	in silico drug phase I/II biotransformation prediction and mass-spectrometric data mining
CANOPUS	2020	https://bio.informatik.uni-jena.de/software/canopus/	2021-09-15	2020-08-05	part of sirius
marr	2021	https://github.com/Ghoshlab/marr	2021-09-15	2021-05-19	
Plasmodesma	2017	https://github.com/delsuc/plasmodesma	2021-09-15	2021-08-26	to be use with SPIKE library
GISSMO	2017	http://gissmo.nmrfam.wisc.edu/	2021-09-15	\N	
sampleDrift	2017	https://gitlab.com/CarlBrunius/sampleDrift	2021-09-15	2017-03-22	Prediction and modeling of pre-analytical sampling errors 
VOCCluster	2020	https://github.com/Yaser218/Untargeted-Metabolomics-Clustering	2021-09-15	2019-02-03	
WiPP	2019	https://github.com/bihealth/wipp	2021-09-15	2021-02-19	
Peakonly	2020	https://github.com/arseha/peakonly	2021-09-15	2021-07-17	
HastaLaVista	2019	https://github.com/jwist/hastalavista	2021-09-15	2020-09-16	
MelonnPan	2019		2021-09-15	2021-05-14	
DecoMetDIA	2019	https://github.com/zhumslab/decometdia	2021-09-15	2019-09-24	
rDolphin	2018	https://github.com/danielcanueto/rdolphin	2021-09-15	2019-03-17	
RawTools	2019	https://github.com/kevinkovalchik/rawtools	2021-09-15	2021-05-03	for raw orbitrap files
misaR	2019	https://github.com/xdomingoal/misar	2021-09-15	2019-03-26	also available in xcms online
statTarget	2018	https://stattarget.github.io/	2021-09-15	2021-05-19	
mwTab	2018	https://pypi.org/project/mwtab/	2021-09-15	2021-05-04	
MetaboSignal	2018	https://github.com/andrearmicl/metabosignal	2021-09-15	2016-10-17	t is a network-based approach that allows overlaying metabolic and signaling pathways and exploring the topological relationship between genes (signaling or metabolic genes) and metabolites.
Risa	2014	https://github.com/ISA-tools/Risa	2022-06-03	2016-10-12	
SLAW	2021	https://github.com/zamboni-lab/SLAW	2022-06-03	2022-05-20	
MS2Query	2022	https://github.com/iomega/ms2query	2022-07-28	2022-07-07	
NMRfilter	2019	https://github.com/stefhk3/nmrfilter	2022-07-30	2021-04-20	
\.


--
-- Data for Name: molecule_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.molecule_type (tool_name, moltype, rowid) FROM stdin;
MetShot	metabolomics	1
ICT	metabolomics	2
GNPS	proteomics	3
GNPS	metabolomics	4
FragPred	metabolomics	5
SDAMS	proteomics	6
MetaComp	genomics	7
RDFIO	genomics	8
MET-COFEA	metabolomics	9
MET-XAlign	metabolomics	10
MetabolAnalyze	metabolomics	11
HAMMER	metabolomics	12
InCroMAP	metabolomics	13
InCroMAP	proteomics	14
VANTED	transcriptomics	15
GAVIN	metabolomics	16
SpectraClassifier	metabolomics	17
ropls	metabolomics	18
VSClust	transcriptomics	19
MetNorm	metabolomics	20
BiGG Models	genomics	21
LIPID MAPS	proteomics	22
VMH	genomics	23
ConsensusPathDB	genomics	24
ConsensusPathDB	proteomics	25
NMRPro	metabolomics	26
MultiAlign	metabolomics	27
alsace	metabolomics	28
LiverWiki	transcriptomics	29
decoMS2	metabolomics	30
MetDisease	metabolomics	31
MetExplore	metabolomics	32
BioCyc	genomics	33
Bayesil	metabolomics	34
missForest	transcriptomics	35
missForest	metabolomics	36
pyQms	genomics	37
GabiPD	genomics	38
CMM	metabolomics	39
XCMS online	metabolomics	40
BiGG Models	metabolomics	41
ChemDistiller	metabolomics	42
pyQms	transcriptomics	43
TracMass2	metabolomics	44
BioNetApp	genomics	45
BioNetApp	transcriptomics	46
RaMP	metabolomics	47
CluMSID	metabolomics	48
LIMSA	lipidomics	49
GNPS-MassIVE	metabolomics	50
BRAIN	metabolomics	51
PhenoMeNal	metabolomics	52
mzR	glycomics	53
cosmiq	lipidomics	54
PathBank	proteomics	55
SMART	metabolomics	56
GAM	metabolomics	57
MetaboAnalyst	metabolomics	58
MetaboMiner	metabolomics	59
MS-FINDER	metabolomics	60
KeggArray	transcriptomics	61
KeggArray	metabolomics	62
VMH	metabolomics	63
MathDAMP	metabolomics	64
MSClust	metabolomics	65
muma	metabolomics	66
BinBase	metabolomics	67
libChEBI	metabolomics	68
IsoMS	metabolomics	69
TriDAMP	genomics	70
ADAP/MZmine	metabolomics	71
MetExplore	genomics	72
PiMP	metabolomics	73
iMet-Q	metabolomics	74
Curatr	metabolomics	75
icoshift	metabolomics	76
MAIT	metabolomics	77
Mummichog	metabolomics	78
MSPrep	metabolomics	79
PeroxisomeDB	metabolomics	80
LiverWiki	proteomics	81
pyQms	metabolomics	82
PARADISe	metabolomics	83
PAPi	metabolomics	84
Metabolomics-Filtering	metabolomics	85
jQMM	proteomics	86
SistematX	metabolomics	87
UniProtKB	proteomics	88
PAMBD	metabolomics	89
SysteMHC	immunopeptidomics	90
HappyTools	metabolomics	91
PathWhiz	metabolomics	92
MetaBridge	genomics	93
FALCON	genomics	94
metaMS	metabolomics	95
MetFusion	metabolomics	96
mixOmics	genomics	97
PathBank	metabolomics	98
PathoLogic	metabolomics	99
PathoLogic	genomics	100
Greazy	metabolomics	101
Greazy	lipidomics	102
jQMM	transcriptomics	103
jQMM	metabolomics	104
Pathview	metabolomics	105
CliqueMS	metabolomics	106
MetaNetter	metabolomics	107
RDFIO	transcriptomics	108
PeroxisomeDB	proteomics	109
MetaboClust	metabolomics	110
E-Dragon	metabolomics	111
missForest	lipidomics	112
InCroMAP	transcriptomics	113
MetaboQuant	metabolomics	114
InterpretMSSpectrum	metabolomics	115
DrugBank	metabolomics	116
polyPK	metabolomics	117
iontree	metabolomics	118
RDFIO	metabolomics	119
Galaxy-M	metabolomics	120
IsoCor	metabolomics	121
FALCON	metabolomics	122
FALCON	transcriptomics	123
GabiPD	transcriptomics	124
BioNetApp	metabolomics	125
MIDAS	metabolomics	126
CMM	lipidomics	127
TriDAMP	metabolomics	128
HiCoNet	metabolomics	129
HiCoNet	transcriptomics	130
CFM-ID	lipidomics	131
TriDAMP	glycomics	132
TOXcms	metabolomics	133
UC2	metabolomics	134
BatMass	proteomics	135
SoyKB	metabolomics	136
SoyKB	proteomics	137
SoyKB	transcriptomics	138
compMS2Miner	metabolomics	139
Pathview	genomics	140
Pathview	proteomics	141
PaintOmics	metabolomics	142
MS2Analyzer	metabolomics	143
MyCompoundID	metabolomics	144
Normalyzer	metabolomics	145
Normalyzer	proteomics	146
SpectConnect	metabolomics	147
MZmine 2	proteomics	148
msPurity	metabolomics	149
credential	metabolomics	150
AMDIS	metabolomics	151
X13CMS	metabolomics	152
mineXpert	metabolomics	153
mineXpert	proteomics	154
SIMAT	metabolomics	155
apLCMS	metabolomics	156
DiffCorr	metabolomics	157
DiffCorr	genomics	158
DiffCorr	transcriptomics	159
missForest	glycomics	160
MetCCS Predictor	metabolomics	161
MeltDB	metabolomics	162
PlantMAT	metabolomics	163
mzR	metabolomics	164
Rdisop	metabolomics	165
Skyline	metabolomics	166
Skyline	proteomics	167
Rhea	transcriptomics	168
MoDentify	metabolomics	169
MS-LAMP	lipidomics	170
mzR	proteomics	171
Workflow4Metabolomics	metabolomics	172
COVAIN	metabolomics	173
SDAMS	metabolomics	174
iPath	metabolomics	175
VSClust	genomics	176
PaintOmics	transcriptomics	177
PaintOmics	genomics	178
PaintOmics	proteomics	179
IIS	metabolomics	180
ALLocater	metabolomics	181
Pachyderm	metabolomics	182
El-MAVEN	metabolomics	183
DrugBank	genomics	184
Metab	metabolomics	185
IntLIM	transcriptomics	186
IntLIM	genomics	187
HappyTools	proteomics	188
Workflow4Metabolomics	lipidomics	189
MetaMapR	metabolomics	190
swathTUNER	metabolomics	191
WikiPathways	metabolomics	192
MetTailor	metabolomics	193
MetaboNetworks	metabolomics	194
pyQms	lipidomics	195
GAM	transcriptomics	196
LDA	lipidomics	197
MetaComp	transcriptomics	198
MaConDa	metabolomics	199
MetaboLights	metabolomics	200
Warpgroup	metabolomics	201
LIPID MAPS	metabolomics	202
LIPID MAPS	lipidomics	203
LIPID MAPS	genomics	204
MetImp	metabolomics	205
PathWhiz	genomics	206
IIS	proteomics	207
mzOS	metabolomics	208
Ionwinze	metabolomics	209
Normalyzer	lipidomics	210
Normalyzer	transcriptomics	211
Normalyzer	genomics	212
libChEBI	proteomics	213
MetaboSearch	metabolomics	214
KMDA	metabolomics	215
mz.unity	metabolomics	216
BioNetApp	proteomics	217
MetaboLyzer	metabolomics	218
MetaBridge	transcriptomics	219
MetaBridge	proteomics	220
MassTRIX	proteomics	221
MassTRIX	genomics	222
Ideom	metabolomics	223
MetabNet	metabolomics	224
MetExtract	metabolomics	225
MAVEN	metabolomics	226
KIMBLE	metabolomics	227
BioCyc	metabolomics	228
VSClust	metabolomics	229
metabnorm	metabolomics	230
MetaboLab	metabolomics	231
Pathos	metabolomics	232
MetFlow	metabolomics	233
Rhea	proteomics	234
METLIN	metabolomics	235
mQTL.NMR	metabolomics	236
MetiTree	metabolomics	237
ChromA	metabolomics	238
ChromA	proteomics	239
IntLIM	metabolomics	240
pyQms	proteomics	241
RAMClustR	metabolomics	242
Binner	metabolomics	243
WikiPathways	transcriptomics	244
WikiPathways	proteomics	245
TargetSearch	metabolomics	246
xMSanalyzer	metabolomics	247
RawTools	proteomics	248
misaR	metabolomics	249
statTarget	metabolomics	250
statTarget	proteomics	251
mwTab	metabolomics	252
MetaboSignal	genomics	253
MetaboSignal	metabolomics	254
Risa	metabolomics	255
Risa	transcriptomics	256
SLAW	metabolomics	257
SLAW	lipidomics	258
MS2Query	metabolomics	259
TNO-DECO	metabolomics	260
QPMASS	metabolomics	261
MetMask	metabolomics	262
Lilikoi	metabolomics	263
CGBayesNets	metabolomics	264
CGBayesNets	genomics	265
swathTUNER	proteomics	266
cosmiq	metabolomics	267
NMRfilter	metabolomics	268
nPYc-Toolbox	metabolomics	269
OPTIMAS-DW	metabolomics	270
OPTIMAS-DW	transcriptomics	271
OPTIMAS-DW	proteomics	272
PathVisio	metabolomics	273
intCor	metabolomics	274
Automics	metabolomics	275
NOMAD	metabolomics	276
SoyKB	genomics	277
NOMAD	proteomics	278
Zero-fill	metabolomics	279
AMDORAP	metabolomics	280
ChemSpider	metabolomics	281
mzMatch	metabolomics	282
MetaComp	metabolomics	283
MetMatch	metabolomics	284
MetFamily	metabolomics	285
MZedDB	metabolomics	286
pyQms	glycomics	287
vaLID	lipidomics	288
MetaBox	metabolomics	289
HappyTools	glycomics	290
MZmine 2	metabolomics	291
PathWhiz	proteomics	292
PathWhiz	transcriptomics	293
MetaComp	proteomics	294
ALEX123	lipidomics	295
VSClust	proteomics	296
MET-COFEI	metabolomics	297
MMSAT	metabolomics	298
PathVisio	genomics	299
PathVisio	proteomics	300
Escher	metabolomics	301
Escher	transcriptomics	302
mixOmics	metabolomics	303
VANTED	metabolomics	304
BRAIN	proteomics	305
BRAIN	lipidomics	306
MI-PACK	metabolomics	307
MVAPACK	metabolomics	308
LiverWiki	genomics	309
LiverWiki	metabolomics	310
metabomxtr	metabolomics	311
proFIA	metabolomics	312
MWASTools	metabolomics	313
mzAccess	metabolomics	314
ORCA	metabolomics	315
crmn	metabolomics	316
eRah	metabolomics	317
Meta P-server	metabolomics	318
CSI:FingerrID	metabolomics	319
FingerID	metabolomics	320
TriDAMP	lipidomics	321
missForest	proteomics	322
SMPDB	proteomics	323
SMPDB	transcriptomics	324
MeRy-B	metabolomics	325
CFM-ID	metabolomics	326
BATMAN	metabolomics	327
HMDB	metabolomics	328
LICRE	lipidomics	329
GSimp	metabolomics	330
MarVis-Suite	metabolomics	331
MarVis-Suite	genomics	332
MetCirc	metabolomics	333
MRMPROBS	metabolomics	334
MetScape	metabolomics	335
COLMAR	metabolomics	336
MINEs	metabolomics	337
MINMA	metabolomics	338
MetaboGroup S	metabolomics	339
MetaboDiff	metabolomics	340
MetAlign	metabolomics	341
BatMass	metabolomics	342
flagme	metabolomics	343
MAGMa	metabolomics	344
MetaboHunter	metabolomics	345
xMSannotator	metabolomics	346
rNMR	metabolomics	347
MassTRIX	metabolomics	348
VANTED	proteomics	349
LipidXplorer	lipidomics	350
RankProd	transcriptomics	351
RankProd	metabolomics	352
RankProd	proteomics	353
QCScreen	metabolomics	354
Lipid-Pro	lipidomics	355
IsoMS-Quant	metabolomics	356
MET-IDEA	metabolomics	357
Pathview	transcriptomics	358
DrugBank	proteomics	359
MetMSLine	metabolomics	360
SmileMS	metabolomics	361
MetaboAnalystR	metabolomics	362
Risa	proteomics	363
MESSI	metabolomics	364
YMDB	metabolomics	365
MetaBridge	metabolomics	366
GabiPD	metabolomics	367
GabiPD	proteomics	368
MoNA	metabolomics	369
DeepRiPP	metabolomics	370
DeepRiPP	proteomics	371
DeepRiPP	genomics	372
MetSign	metabolomics	373
MetaDB	metabolomics	374
MetaQuant	metabolomics	375
MetFrag	metabolomics	376
eMZed	metabolomics	377
Maui-VIA	metabolomics	378
CAMERA	metabolomics	379
SAMMI	metabolomics	380
Escher	proteomics	381
ConsensusPathDB	metabolomics	382
IPO	metabolomics	383
SMPDB	metabolomics	384
mixOmics	transcriptomics	385
InCroMAP	genomics	386
MetaboliteDetector	metabolomics	387
Rhea	genomics	388
IIS	genomics	389
mixOmics	proteomics	390
specmine	metabolomics	391
CROP	metabolomics	392
Qemistree	metabolomics	393
Notame	metabolomics	394
MetaboShiny	metabolomics	395
Viime	metabolomics	396
NOREVA	metabolomics	397
JS-MS	metabolomics	398
pyMolNetEnhancer	metabolomics	399
RMolNetEnhancer	metabolomics	400
ZEAMAP	genomics	401
ZEAMAP	transcriptomics	402
ZEAMAP	metabolomics	403
MassComp	metabolomics	404
MassComp	proteomics	405
iMet	metabolomics	406
MetNormalizer	metabolomics	407
tmod	metabolomics	408
tmod	transcriptomics	409
PYQUAN	metabolomics	410
MAIMS	metabolomics	411
KPIC	metabolomics	412
ProbMetab	metabolomics	413
batchCorr	metabolomics	414
XCMS	metabolomics	415
POMAShiny	metabolomics	416
POMAShiny	proteomics	417
MetaboloDerivatizer	metabolomics	418
R2DGC	metabolomics	419
AlpsNMR	metabolomics	420
PeroxisomeDB	genomics	421
BRAIN	glycomics	422
TriDAMP	transcriptomics	423
CCPN Metabolomics	metabolomics	424
Rhea	metabolomics	425
missForest	genomics	426
ADAP-GC	metabolomics	427
MIDcor	metabolomics	428
MS-FLO	metabolomics	429
MetaboQC	metabolomics	430
LIQUID	lipidomics	431
AntDAS	metabolomics	432
pymass	metabolomics	433
pySM	metabolomics	434
massPix	lipidomics	435
NMRProcFlow	metabolomics	436
speaq	metabolomics	437
SigMa	metabolomics	438
MSHub	metabolomics	439
RGCxGC	metabolomics	440
ncGTW	metabolomics	441
TidyMS	metabolomics	442
Autotuner	metabolomics	443
hRUV	metabolomics	444
MetumpX	metabolomics	445
fobitools	metabolomics	446
fobitoolsGUI	metabolomics	447
BioDendro	metabolomics	448
QSSR Automator	metabolomics	449
QSSR Automator	lipidomics	450
MetIDfyR	metabolomics	451
CANOPUS	metabolomics	452
marr	metabolomics	453
Plasmodesma	metabolomics	454
GISSMO	metabolomics	455
sampleDrift	metabolomics	456
VOCCluster	metabolomics	457
WiPP	metabolomics	458
Peakonly	metabolomics	459
HastaLaVista	metabolomics	460
DecoMetDIA	metabolomics	461
rDolphin	metabolomics	462
\.


--
-- Data for Name: network_visualization_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.network_visualization_data (index, tool_name, count, "PMID", start_position) FROM stdin;
0	SMART	1	12625841	4602
1	BRAIN	1	15473914	2116
2	PathoLogic	10	15642094	969
3	BioCyc	6	16246909	22
4	PathoLogic	7	16246909	2684
5	SMART	1	16401340	3247
6	AMDIS	2	16762068	1252
7	BioCyc	8	16893953	64
8	AMDIS	1	17016516	6989
9	XCMS	1	17355638	2324
10	Newton	1	17367203	231
11	BioCyc	1	17521419	1872
12	LDA	3	17605789	11249
13	RankProd	2	17678549	9393
14	ORCA	1	17696614	5370
15	AMDIS	1	18034876	11690
16	AMDIS	1	18226195	2632
17	SpectConnect	2	18226195	2684
18	MIDAS	1	18253491	6019
19	mQTL	2	18369453	6271
20	BinBase	5	18442406	2645
21	BioCyc	1	18500999	4332
22	LIPID MAPS	1	18523432	2442
23	LDA	4	18523432	6882
24	XCMS	5	18793413	12398
25	BRAIN	6	18958272	1441
26	XCMS	1	19040729	11092
27	Skyline	1	19158949	1558
28	AMDIS	1	19183481	4944
29	LDA	3	19208232	3837
30	LDA	4	19344524	4949
31	LDA	1	19390697	6125
32	Newton	1	19409084	4085
33	BinBase	1	19415114	2159
34	UniProtKB	1	19523227	776
35	BioCyc	1	19646286	198
36	SMART	1	19788757	4228
37	AMDIS	1	19798534	7093
38	MIDAS	2	19893610	6281
39	DrugBank	1	19958509	242
40	MetAlign	2	20008451	4313
41	XCMS	5	20019811	4368
42	SMART	1	20019811	13330
43	GAM	1	20021657	3869
44	AMDIS	1	20040118	2552
45	PathVisio	1	20098690	7818
46	WikiPathways	1	20098690	8042
47	AMDIS	1	20122158	10662
48	MeltDB	1	20181048	13569
49	BioCyc	3	20186266	14774
50	XCMS	1	20300169	3654
51	HMDB	1	20300169	6504
52	BinBase	2	20352092	6877
53	MetAlign	2	20376177	9468
54	SMART	2	20416102	670
55	SMART	2	20423525	1749
56	SMART	1	20504312	3343
57	BioCyc	2	20526351	4274
58	MassTRIX	2	20526857	9999
59	AMDIS	1	20565801	6079
60	MET-IDEA	1	20565801	6217
61	SMART	1	20624727	1733
62	MassTRIX	1	20637123	3564
63	BinBase	5	20676379	8489
64	METLIN	1	20676379	11209
65	HMDB	1	20676379	11415
66	AMDIS	1	20676398	7842
67	AMDIS	1	20706589	8576
68	SpectConnect	1	20706589	8734
69	SMART	1	20732878	88
70	BioCyc	4	20807400	385
71	UniProtKB	1	20823849	445
72	ChemSpider	1	20830202	7649
73	SMART	2	20854695	1404
74	Skyline	1	20886892	2061
75	XCMS	3	20957145	4168
76	METLIN	1	20957145	5822
77	MassTRIX	3	20976215	4124
78	HMDB	1	20976215	4334
79	SMART	2	21068208	1957
80	AMDIS	5	21079799	5735
81	BioCyc	1	21083885	509
82	SMART	2	21092138	6125
83	MetabolAnalyze	2	21092268	4535
84	TargetSearch	1	21124901	4119
85	MetAlign	2	21151979	10129
86	mzMatch	2	21152055	5665
87	RankProd	1	21152055	8411
88	MetAlign	1	21167023	11652
89	RankProd	1	21194489	910
90	SMART	3	21247460	7589
91	BioCyc	1	21251315	1008
92	AMDIS	1	21283710	6863
93	MIDAS	1	21283831	3541
94	BioCyc	2	21285434	1957
95	LDA	2	21298004	9928
96	VANTED	1	21303545	384
97	MZmine 2	1	21306563	3102
98	ORCA	1	21321100	5332
99	HMDB	4	21359215	8297
100	MetMask	1	21359231	3889
101	ChemSpider	1	21359231	4218
102	MZmine 2	1	21429189	10975
103	MetAlign	1	21461040	1308
104	AMDIS	1	21513553	5455
105	Newton	1	21533132	3968
106	MetAlign	2	21542920	16853
107	LDA	3	21696593	11205
108	SMART	1	21737679	5740
109	RankProd	1	21738454	9470
110	DrugBank	1	21749716	6736
111	MAVEN	1	21756354	11783
112	SMART	1	21772259	7499
113	SMART	1	21778184	718
114	DrugBank	2	21824432	325
115	HMDB	2	21824432	831
116	HMDB	1	21833635	2155
117	UniProtKB	1	21849072	0
118	XCMS	1	21853132	7057
119	METLIN	1	21853132	9751
120	icoshift	1	21859467	3227
121	LIPID MAPS	2	21909445	3863
122	MassTRIX	2	21909445	3914
123	LDA	1	21917861	5010
124	mQTL	4	21931564	4293
125	GAM	1	21988831	8752
126	SMART	3	22022265	508
127	XCMS	1	22022469	7567
128	CAMERA	2	22022469	7854
129	MZmine 2	1	22025777	5196
130	XCMS	3	22028833	2781
131	CAMERA	3	22028833	3234
132	MetMask	1	22034874	474
133	MZedDB	1	22039364	9358
134	LDA	4	22039364	10648
135	XCMS	2	22110719	5039
136	SMART	1	22111916	1740
137	Metab	1	22118224	11802
138	VANTED	1	22174812	3627
139	MetaboAnalyst	1	22174909	6551
140	HMDB	9	22194963	30
141	DrugBank	2	22194963	6194
142	HMDB	1	22194988	4864
143	XCMS	2	22194988	5617
144	Newton	1	22200740	2847
145	SMART	2	22238585	1297
146	MeRy-B	2	22241996	5763
147	HMDB	1	22241996	5776
148	UniProtKB	1	22253706	12023
149	mQTL	13	22330898	3
150	MSClust	1	22330898	2552
151	HMDB	1	22355651	5504
152	MetaboAnalyst	3	22363586	16015
153	HMDB	1	22363586	18051
154	MetaboMiner	1	22384111	5939
155	METLIN	2	22384120	4059
156	HMDB	2	22384120	4087
157	DrugBank	1	22389732	4981
158	AMDIS	1	22393443	10053
159	SpectConnect	1	22393443	10080
160	apLCMS	1	22412977	2353
161	METLIN	1	22415876	4443
162	LIPID MAPS	1	22415876	4712
163	MetaboliteDetector	1	22433058	12953
164	ORCA	1	22442685	6734
165	MultiAlign	2	22457619	2166
166	LIPID MAPS	1	22458914	4586
167	XCMS	2	22493681	8146
168	ORCA	1	22514600	2394
169	MIDAS	2	22539984	4015
170	XCMS	1	22546713	3967
171	AMDIS	1	22546835	1915
172	mzMatch	1	22563508	3997
173	Ideom	4	22563508	4014
174	XCMS	1	22563508	4147
175	HMDB	2	22583555	964
176	DrugBank	2	22587596	1724
177	BinBase	2	22590580	3705
178	BinBase	2	22591066	2410
179	HMDB	1	22591066	3868
180	XCMS	1	22593722	1139
181	mzMatch	1	22593722	1282
182	BioCyc	1	22593722	2870
183	MetAlign	2	22593725	5999
184	ChemSpider	1	22629264	3341
185	MetAlign	1	22645535	1790
186	SMART	1	22655051	404
187	XCMS	1	22662204	4825
188	XCMS	2	22662221	3259
189	CAMERA	1	22662221	4011
190	AMDIS	1	22676814	8974
191	XCMS	2	22690245	4214
192	Rhea	1	22693442	1891
193	PathoLogic	1	22703714	1870
194	VANTED	1	22704468	3212
195	MIDAS	1	22723948	1603
196	CMM	5	22724011	6908
197	ChemSpider	1	22745732	7812
198	LIPID MAPS	1	22747852	7538
199	XCMS	1	22759585	373
200	CAMERA	2	22759585	3692
201	MetAlign	2	22761719	4550
202	AMDIS	1	22761719	5684
203	MetAlign	2	22792336	5394
204	HMDB	1	22792382	6370
205	XCMS	1	22792382	6797
206	AMDIS	1	22792382	7201
207	XCMS	2	22799605	7822
208	UniProtKB	1	22807670	1311
209	BinBase	2	22808006	4597
210	XCMS	1	22808288	3855
211	BinBase	1	22832350	1892
212	MetaboAnalyst	1	22832350	4854
213	XCMS	1	22849329	9345
214	MetaboAnalyst	1	22849329	9682
215	XCMS	1	22859960	3090
216	XCMS	2	22866179	3737
217	CAMERA	1	22866179	4273
218	METLIN	1	22866179	6353
219	XCMS	5	22911836	5848
220	AMDIS	1	22916280	5528
221	mQTL	4	22947126	2338
222	XCMS	2	22952947	12751
223	Newton	1	22957002	3679
224	METLIN	2	22957002	12077
225	SMART	2	22963618	9149
226	ConsensusPathDB	3	22969728	4441
227	HMDB	1	22970281	6299
228	GAM	1	22984471	2519
229	HMDB	1	22984493	9210
230	HMDB	1	22985496	10517
231	HMDB	3	23010998	6679
232	VANTED	1	23015778	2526
233	HMDB	1	23024759	2561
234	AMDIS	1	23025749	6794
235	VANTED	1	23029411	11982
236	XCMS	1	23029590	4440
237	Newton	1	23041950	4108
238	XCMS	3	23042035	4974
239	HMDB	1	23042035	5959
240	AMDIS	1	23046838	3309
241	XCMS	1	23056382	3016
242	MIDAS	1	23070401	502
243	BinBase	1	23077522	2225
244	METLIN	1	23077522	2919
245	XCMS	2	23077591	8530
246	METLIN	1	23077591	9957
247	BioCyc	1	23103756	6927
248	TargetSearch	1	23112854	1724
249	mQTL	10	23121691	198
250	SMART	1	23166508	562
251	UniProtKB	2	23166508	2059
252	MZmine 2	1	23232715	12350
253	UniProtKB	1	23285211	116
254	MetAlign	2	23335867	7685
255	MSClust	2	23335867	7896
256	AMDIS	1	23335867	9164
257	MeltDB	1	23335869	4325
258	XCMS	1	23335869	4523
259	MetaboAnalyst	1	23349779	7251
260	BinBase	3	23383346	3488
261	MetaboAnalyst	1	23383346	4286
262	XCMS	1	23384616	5773
263	XCMS	1	23401718	2752
264	AMDIS	1	23404899	4115
265	LIPID MAPS	1	23405143	4834
266	MetaboAnalyst	1	23408980	7593
267	XCMS	2	23409005	7635
268	XCMS	1	23411940	4176
269	Newton	1	23425943	2305
270	UniProtKB	1	23452435	4291
271	XCMS	3	23455543	8579
272	MetAlign	1	23456655	4946
273	XCMS	5	23469044	565
274	CAMERA	1	23469044	1052
275	MetaboAnalyst	1	23497222	5123
276	AMDIS	1	23497242	5296
277	MET-IDEA	1	23497242	5949
278	SMART	1	23497397	2111
279	METLIN	1	23533484	5974
280	HMDB	1	23533484	6148
281	BinBase	2	23536766	2887
282	ChemSpider	1	23537001	8108
283	HMDB	1	23537001	8179
284	HMDB	1	23555690	15583
285	CMM	3	23555809	9263
286	ChemSpider	1	23573132	3143
287	UniProtKB	1	23575632	1462
288	HMDB	1	23577129	2582
289	GAM	2	23587327	1298
290	MZmine 2	1	23593032	2821
291	XCMS	2	23593245	1679
292	HMDB	2	23593245	4762
293	AMDIS	1	23593322	5370
294	METLIN	5	23593322	6410
295	HMDB	1	23593322	10718
296	LIPID MAPS	1	23593322	10727
297	BioCyc	2	23593322	11602
298	HMDB	1	23606874	8180
299	HMDB	1	23615477	4540
300	METLIN	1	23615477	4560
301	UniProtKB	1	23620802	17236
302	XCMS	2	23634896	6102
303	CAMERA	1	23634896	6112
304	SMART	1	23638167	3232
305	SMPDB	1	23641933	4526
306	HMDB	1	23658609	7338
307	METLIN	1	23658609	7402
308	MIDAS	1	23658706	7084
309	COVAIN	1	23678342	7888
310	AMDIS	1	23678344	4008
311	COVAIN	3	23678344	4143
312	XCMS	2	23690837	3112
313	METLIN	1	23700429	5978
314	LIPID MAPS	1	23700429	6249
315	XCMS	3	23704878	7619
316	CAMERA	3	23704878	8239
317	MetaboAnalyst	1	23704878	12559
318	XCMS	2	23710235	4848
319	HMDB	1	23724060	2629
320	METLIN	2	23724060	2641
321	UniProtKB	1	23724367	9831
322	XCMS	1	23725349	3211
323	XCMS	1	23737844	4220
324	MassTRIX	1	23741374	8529
325	ChemSpider	1	23762151	338
326	DrugBank	1	23762151	710
327	MetAlign	2	23776651	7824
328	TargetSearch	1	23799147	5051
329	LIPID MAPS	9	23817071	144
330	HMDB	1	23823578	6415
331	Skyline	1	23835326	4262
332	IsoCor	1	23840455	6545
333	Newton	1	23840608	12390
334	MultiAlign	4	23840608	16010
335	mixOmics	1	23844124	6442
336	apLCMS	1	23861661	1378
337	MetaboLights	1	23861661	3084
338	MAVEN	3	23861661	3764
339	BioCyc	1	23861661	4485
340	AMDIS	2	23861785	6663
341	ChemSpider	1	23862058	4174
342	AMDIS	1	23883549	5313
343	VANTED	1	23886069	11648
344	BioCyc	1	23894306	5522
345	BioCyc	1	23917584	7300
346	MZmine 2	1	23922748	11489
347	icoshift	1	23922748	12580
348	XCMS	1	23940645	6508
349	AMDIS	1	23940645	6777
350	MIDAS	1	23940684	5597
351	GAM	1	23940749	4806
352	MassTRIX	2	23950718	14907
353	MZmine 2	1	23950718	16879
354	XCMS	1	23951074	8005
355	CAMERA	1	23951074	8481
356	MZedDB	1	23951074	9007
357	AMDIS	1	23951145	3631
358	PAPi	1	23951145	4416
359	HMDB	1	23967205	3851
360	METLIN	1	23967205	3878
361	AMDIS	1	23967252	9604
362	LipidXplorer	1	23967253	4587
363	MZmine 2	2	23967362	9335
364	HMDB	1	23967362	11668
365	METLIN	1	23967362	11696
366	MetaboAnalyst	1	23967362	12274
367	MassTRIX	1	23967366	5107
368	HMDB	1	23984345	6291
369	MetaboAnalyst	5	23984345	7411
370	LDA	1	23984415	5725
371	UniProtKB	1	23985341	13447
372	AMDIS	1	23990511	2191
373	apLCMS	1	24015273	2292
374	AMDIS	1	24019856	646
375	MetaboAnalyst	1	24039617	6291
376	XCMS	2	24039618	5524
377	MetaboAnalyst	1	24039618	6349
378	CAMERA	1	24039618	8325
379	HMDB	1	24039868	3899
380	SMART	1	24043856	1269
381	AMDIS	1	24058459	3580
382	SpectConnect	1	24058459	3880
383	MathDAMP	1	24058493	3969
384	MassTRIX	2	24058591	5418
385	HMDB	1	24058591	5575
386	AMDIS	1	24062529	7431
387	SpectConnect	1	24062529	7550
388	BioCyc	1	24065962	10459
389	SMART	1	24066073	3342
390	ORCA	1	24066102	11584
391	AMDIS	1	24070026	4318
392	SMART	2	24086109	5896
393	MetaboAnalyst	1	24098417	3862
394	MAIT	14	24101382	1800
395	ChemSpider	1	24124589	4377
396	XCMS	2	24130822	11170
397	MetAlign	2	24146875	6867
398	HMDB	1	24146875	8831
399	MeRy-B	2	24151307	4435
400	mixOmics	1	24151307	10002
401	HMDB	1	24160444	11198
402	ChemSpider	1	24160444	11449
403	MZmine 2	1	24167631	5353
404	AMDIS	1	24170342	3585
405	IIS	1	24172336	630
406	PathVisio	2	24172336	3069
407	ChemSpider	1	24188386	3645
408	HMDB	2	24204787	4486
409	BinBase	2	24252434	4031
410	AMDIS	3	24260553	4000
411	XCMS	1	24273473	8522
412	CAMERA	1	24273473	8550
413	MZedDB	1	24273473	9066
414	HMDB	1	24273473	9945
415	MetaboLights	1	24274116	3415
416	XCMS	2	24278445	3143
417	SMART	1	24280345	183
418	PathoLogic	1	24280345	1816
419	ChemSpider	1	24288569	11154
420	MetFrag	2	24288569	11280
421	METLIN	1	24288569	11689
422	MetaboAnalyst	1	24288569	11800
423	SMART	2	24302568	274
424	ICT	1	24307992	3408
425	VANTED	2	24312553	1807
426	AMDIS	1	24324663	2351
427	MetaboAnalyst	1	24334864	2492
428	MetaboAnalyst	2	24358183	8874
429	MetaboAnalyst	2	24358308	3739
430	XCMS	1	24371466	3212
431	HMDB	1	24376829	3747
432	icoshift	1	24376829	4183
433	METLIN	1	24383425	4387
434	HMDB	1	24383425	4471
435	XCMS	3	24386157	12526
436	CAMERA	4	24386157	12535
437	AMDIS	1	24386418	4679
438	MetaboAnalyst	3	24386418	8807
439	MAVEN	1	24391526	5331
440	COVAIN	1	24400018	2527
441	AMDIS	1	24409310	1961
442	XCMS	2	24413286	6466
443	HMDB	1	24413286	7126
444	METLIN	1	24413286	7154
445	BinBase	2	24416208	11211
446	XCMS	1	24453970	7065
447	mzMatch	2	24453970	7205
448	mQTL	1	24457419	4258
449	LipidXplorer	1	24457528	5234
450	VANTED	1	24458129	12466
451	XCMS	2	24465634	5434
452	HMDB	1	24465634	5749
453	METLIN	1	24465634	5777
454	MetaboAnalyst	1	24466087	9599
455	MetMask	2	24468196	0
456	HMDB	4	24468196	188
457	AMDIS	1	24475113	2071
458	XCMS	1	24475228	5162
459	HMDB	1	24475228	6378
460	SMART	1	24483277	905
461	HMDB	1	24484441	5203
462	XCMS	2	24495308	2793
463	MassTRIX	1	24495308	6069
464	HMDB	1	24495308	6390
465	LIPID MAPS	1	24495308	6395
466	ChemSpider	1	24498209	13783
467	METLIN	1	24498209	15755
468	MetaboliteDetector	1	24504095	4558
469	XCMS	1	24505462	6307
470	SMART	1	24507483	1585
471	MetAlign	2	24532977	2973
472	MSClust	1	24532977	3208
473	mQTL	2	24532977	3456
474	HMDB	3	24586186	6492
475	XCMS	3	24586655	6084
476	METLIN	1	24586655	6553
477	LDA	6	24586655	6943
478	HMDB	1	24587236	8554
479	TargetSearch	1	24587323	2511
480	HMDB	1	24592282	6247
481	MAVEN	2	24595068	6027
482	XCMS	1	24595068	7186
483	XCMS online	1	24595068	7186
484	MAVEN	1	24603560	2284
485	AMDIS	2	24608751	5723
486	SpectConnect	1	24608751	6233
487	HMDB	1	24618672	3888
488	METLIN	1	24618672	3916
489	ChemSpider	1	24618672	3953
490	LIPID MAPS	1	24618672	4027
491	XCMS	2	24618989	6386
492	HMDB	1	24633423	5420
493	METLIN	1	24633423	5438
494	XCMS	1	24659489	1402
495	CAMERA	1	24659489	1510
496	HMDB	1	24671089	2406
497	METLIN	1	24671089	2467
498	XCMS	1	24672603	1949
499	MetaboAnalyst	1	24672603	3077
500	AMDIS	1	24675792	4980
501	BioCyc	4	24688832	273
502	ICT	10	24690942	6472
503	HMDB	1	24707821	627
504	XCMS	1	24713823	3723
505	HMDB	1	24713823	6393
506	MathDAMP	1	24713860	5343
507	Newton	1	24716544	2119
508	LDA	7	24716544	6817
509	METLIN	1	24717228	4628
510	HMDB	1	24717228	4636
511	LIPID MAPS	1	24717228	4646
512	AMDIS	1	24723740	4784
513	SpectConnect	1	24723740	5091
514	MetAlign	1	24736747	4418
515	MSClust	1	24736747	4664
516	UniProtKB	1	24739900	2511
517	ChemSpider	1	24757578	4489
518	MetaboAnalyst	1	24757578	4610
519	HMDB	3	24772056	6177
520	VMH	1	24781306	13568
521	BioCyc	1	24789011	9699
522	MetaboAnalyst	2	24795766	2655
523	apLCMS	1	24799415	4335
524	HMDB	1	24799415	4591
525	LIPID MAPS	1	24799415	4610
526	MSPrep	2	24799415	4756
527	mixOmics	1	24799415	6507
528	icoshift	1	24800239	2648
529	ChemSpider	1	24800806	4290
530	HMDB	1	24810058	18491
531	METLIN	2	24810058	18503
532	MetaboliteDetector	1	24810058	24339
533	XCMS	1	24824572	5808
534	MetaboAnalyst	1	24824572	6934
535	HMDB	1	24824572	7676
536	MZmine 2	1	24828577	4848
537	MetaboAnalyst	1	24843015	5085
538	SMART	1	24846378	5652
539	UniProtKB	1	24847885	6448
540	SMART	1	24847885	6663
541	ORCA	1	24847885	11465
542	mzMatch	1	24853684	2363
543	Ideom	3	24853684	2380
544	XCMS	1	24853684	2518
545	UniProtKB	1	24853684	5280
546	Skyline	1	24860088	2424
547	HMDB	3	24860088	10310
548	HMDB	2	24866127	10454
549	AMDIS	1	24870353	4260
550	WikiPathways	1	24875638	1341
551	BioCyc	1	24875638	1355
552	SMART	1	24885189	2570
553	XCMS	1	24885189	11508
554	CAMERA	1	24885189	11636
555	HMDB	1	24885189	11840
556	TargetSearch	1	24885763	3625
557	BinBase	1	24887281	4235
558	UniProtKB	1	24888481	3185
559	UniProtKB	1	24895011	5347
560	UniProtKB	1	24904625	342
561	HMDB	1	24918908	5349
562	XCMS	2	24922509	14461
563	MetaboAnalyst	1	24937094	12009
564	MVAPACK	3	24937102	3980
565	LDA	1	24937102	4457
566	HMDB	2	24937102	5672
567	METLIN	1	24937102	6696
568	MetaboAnalyst	1	24937646	13970
569	MetaboLights	1	24937646	16166
570	HMDB	1	24940599	4840
571	METLIN	1	24940599	4903
572	XCMS	3	24947160	3098
573	METLIN	1	24952473	4374
574	HMDB	1	24952473	4448
575	MetaboAnalyst	1	24952473	5076
576	AMDIS	1	24954394	2194
577	SpectConnect	2	24954394	2547
578	MetScape	2	24954394	20828
579	MetDisease	3	24954394	21022
580	FingerID	1	24958002	327
581	METLIN	3	24958268	3035
582	BinBase	1	24964784	10857
583	AMDIS	1	24966854	6101
584	MIDAS	1	24968217	1779
585	HMDB	1	25007263	4091
586	XCMS	1	25009546	2094
587	XCMS	1	25010005	3978
588	Rhea	2	25012562	3
589	HMDB	1	25012562	6440
590	METLIN	2	25016594	3811
591	mixOmics	2	25023612	9943
592	METLIN	1	25027794	5810
593	XCMS	1	25032816	4397
594	MetaboSearch	2	25032816	6880
595	HMDB	1	25032816	7074
596	LIPID MAPS	1	25032816	7142
597	PathoLogic	1	25048541	455
598	BioCyc	2	25048541	2278
599	MAIT	20	25049336	121
600	XCMS	3	25057268	5841
601	MetExtract	2	25057268	9644
602	AMDIS	1	25057319	3124
603	Metab	1	25057319	3809
604	MetaboAnalyst	1	25057319	4064
605	AMDIS	1	25058345	2628
606	SpectConnect	2	25058345	2650
607	XCMS	1	25058345	3776
608	XCMS online	1	25058345	3776
609	MIDAS	1	25058581	6728
610	Metab	1	25058581	8937
611	HMDB	1	25059459	4318
612	AMDIS	1	25061513	7936
613	DrugBank	1	25062064	4074
614	ChemSpider	1	25068885	126
615	DrugBank	1	25068885	762
616	UniProtKB	1	25068885	1097
617	BinBase	2	25069065	2717
618	MetaboAnalyst	1	25072247	1819
619	METLIN	1	25085339	8672
620	MetAlign	1	25109402	6265
621	MSClust	1	25109402	6289
622	MetaboAnalyst	2	25109402	7775
623	MetAlign	2	25121768	1826
624	HMDB	1	25126707	9163
625	MetaboLyzer	1	25126707	9868
626	MetaboAnalyst	1	25126707	9887
627	AMDIS	1	25127240	9835
628	AMDIS	1	25128291	4022
629	AMDIS	2	25133804	4212
630	Skyline	1	25136337	5089
631	MetaboLights	1	25136337	8441
632	mixOmics	1	25157567	4534
633	HMDB	2	25159688	10055
634	METLIN	2	25159688	10064
635	UniProtKB	2	25180703	6053
636	Skyline	2	25181601	5045
637	MetaboAnalyst	2	25211248	6947
638	MSClust	1	25222144	3642
639	mQTL	1	25222144	6631
640	HMDB	1	25225614	4157
641	MetaboAnalyst	1	25225614	4357
642	MZmine 2	2	25227127	4462
643	MVAPACK	2	25228990	9913
644	MetAlign	1	25238064	2988
645	MAVEN	1	25261994	32990
646	MetaboAnalyst	1	25272163	10624
647	HMDB	1	25275468	10613
648	ORCA	1	25283467	4179
649	apLCMS	3	25329995	6724
650	xMSanalyzer	2	25329995	6736
651	HMDB	1	25329995	9718
652	ChemSpider	1	25329995	9768
653	METLIN	2	25333951	1170
654	MetaboLyzer	3	25333951	7541
655	HMDB	1	25333951	8557
656	AMDIS	2	25338676	3414
657	MetAlign	1	25339964	3145
658	MSClust	1	25339964	3494
659	HMDB	1	25341677	2841
660	METLIN	1	25341677	2902
661	MetaboAnalyst	1	25341677	3412
662	MetaboAnalyst	1	25353990	2210
663	UniProtKB	1	25360753	6125
664	ORCA	1	25365260	1864
665	AMDIS	1	25366096	6643
666	DrugBank	1	25366653	3352
667	XCMS	3	25368622	11415
668	CAMERA	1	25368622	12341
669	XCMS	3	25368974	2348
670	METLIN	1	25368974	2402
671	HMDB	1	25368974	6659
672	MyCompoundID	1	25368974	8723
673	BioCyc	1	25369450	9146
674	XCMS	1	25370494	5198
675	XCMS	1	25372853	3039
676	mixOmics	1	25372853	4165
677	Newton	1	25374324	2720
678	XCMS	2	25374324	7663
679	METLIN	1	25374324	8547
680	LIPID MAPS	1	25374324	8600
681	Newton	1	25374486	3966
682	muma	1	25374774	5229
683	METLIN	1	25380056	5238
684	XCMS	2	25380056	5881
685	MetaboAnalyst	2	25386958	6588
686	XCMS	2	25390340	5548
687	METLIN	1	25390340	6223
688	MZmine 2	1	25390735	2779
689	IIS	1	25405334	1939
690	AMDIS	1	25410248	6974
691	MetAlign	1	25411961	2870
692	MZmine 2	1	25419661	4092
693	HMDB	1	25419661	5805
694	METLIN	1	25419661	5836
695	ORCA	1	25419710	13125
696	MetShot	1	25431760	1342
697	MetFusion	1	25431760	1552
698	AMDIS	1	25436455	8803
699	HMDB	1	25437454	3780
700	xMSanalyzer	1	25453034	6410
701	apLCMS	1	25453034	6455
702	mixOmics	1	25453034	9484
703	GAM	9	25463381	4993
704	apLCMS	1	25468946	5063
705	xMSanalyzer	1	25468946	5326
706	LIPID MAPS	1	25468946	5603
707	MSPrep	2	25468946	5743
708	mixOmics	1	25468946	8775
709	MZmine 2	1	25469718	5884
710	Newton	1	25471357	14890
711	XCMS	2	25482491	1196
712	METLIN	1	25486521	7765
713	HMDB	2	25486521	7811
714	MetaboAnalyst	2	25486521	9607
715	MetaboAnalyst	1	25490710	3546
716	MetaboAnalyst	1	25491116	2632
717	SMART	1	25502441	5209
718	XCMS	1	25502724	2401
719	MetaboLab	1	25502759	1210
720	VANTED	2	25506350	8594
721	MZmine 2	3	25506824	3340
722	METLIN	1	25506922	3250
723	HMDB	1	25506922	3294
724	HMDB	1	25511422	1547
725	MetaboAnalyst	1	25517133	10196
726	MIDAS	6	25517503	618
727	MetAlign	1	25526885	4923
728	MSClust	1	25526885	5074
729	LDA	1	25531678	5643
730	MetaboAnalyst	1	25531678	6285
731	GAM	1	25534747	9339
732	HMDB	1	25535749	3229
733	PathoLogic	1	25538713	2341
734	BioCyc	2	25540032	3928
735	AMDIS	2	25545686	3652
736	MET-IDEA	1	25545686	3869
737	MetaboAnalyst	2	25545719	7167
738	UniProtKB	1	25548518	1537
739	XCMS	1	25553245	2445
740	ConsensusPathDB	1	25569235	3889
741	UniProtKB	1	25569235	3911
742	METLIN	1	25585609	5539
743	MZmine 2	1	25591556	2629
744	MetaboAnalyst	1	25591556	2947
745	UniProtKB	1	25617470	1094
746	VANTED	1	25617470	3026
747	AMDIS	1	25628629	8124
748	TargetSearch	1	25653655	8710
749	SMART	1	25658744	1704
750	MetaboAnalyst	2	25658945	3357
751	mixOmics	1	25663847	6263
752	COVAIN	2	25663847	7330
753	XCMS	2	25667596	4095
754	Skyline	4	25678338	14446
755	XCMS	1	25685448	334
756	XCMS	1	25690418	2836
757	AMDIS	1	25690418	3234
758	XCMS	1	25705145	1791
759	MetaboAnalyst	1	25705365	12374
760	MetaboAnalyst	2	25711705	6066
761	XCMS	1	25713290	8001
762	HMDB	1	25714999	11280
763	SMART	1	25716828	8899
764	VANTED	1	25721093	5131
765	XCMS	1	25749400	4659
766	XCMS online	1	25749400	4659
767	DrugBank	1	25750602	65
768	AMDIS	1	25750603	4644
769	MZmine 2	2	25750603	7923
770	HMDB	1	25750603	8929
771	MetaboAnalyst	1	25750603	9155
772	AMDIS	3	25751150	4542
773	MetaboAnalyst	2	25758533	2436
774	METLIN	1	25766252	2321
775	IPO	1	25767467	3308
776	CMM	3	25775470	58
777	XCMS	1	25775470	3435
778	mzMatch	1	25775470	3480
779	Ideom	1	25775470	4558
780	muma	1	25781638	5095
781	ChemSpider	1	25781947	6643
782	LDA	4	25784292	5150
783	ORCA	1	25784700	3136
784	HMDB	2	25786031	8269
785	VANTED	1	25793974	7250
786	MetaboAnalyst	2	25793974	7512
787	MetaboAnalyst	2	25804930	4146
788	MAGMa	6	25806366	5377
789	HMDB	4	25806366	5679
790	METLIN	1	25806366	8679
791	METLIN	1	25824377	4752
792	SMART	1	25826089	3575
793	METLIN	1	25830305	3196
794	ChemSpider	1	25834622	3805
795	mixOmics	2	25848535	4856
796	HMDB	1	25849323	2194
797	GAM	1	25850106	7291
798	MetaboliteDetector	1	25852471	12216
799	ChemSpider	1	25856314	5154
800	XCMS	1	25861152	4517
801	MeRy-B	1	25873655	4276
802	XCMS	1	25873669	4811
803	AMDIS	1	25874568	8652
804	SpectConnect	1	25874568	8847
805	MetaboAnalyst	1	25880539	2466
806	SMART	1	25887758	2643
807	MetaboAnalyst	1	25894462	2175
808	METLIN	1	25896951	5271
809	MetaboliteDetector	1	25905710	4545
810	MultiAlign	1	25905710	8384
811	XCMS	2	25919591	2715
812	Skyline	1	25926824	4073
813	METLIN	1	25927228	6224
814	LIPID MAPS	1	25927228	8004
815	HMDB	1	25927228	8022
816	mixOmics	1	25927228	8559
817	MetaboAnalyst	1	25934505	3509
818	XCMS	2	25938477	9211
819	MetaboAnalyst	1	25943561	4515
820	XCMS	1	25945117	3519
821	XCMS	1	25946070	12498
822	mzMatch	1	25946070	12560
823	MetaboAnalyst	1	25946120	3640
824	METLIN	1	25961003	1993
825	Newton	1	25961713	6716
826	METLIN	1	25967672	7305
827	HMDB	1	25967672	7327
828	GAM	1	25972363	3565
829	HMDB	1	25972769	1806
830	METLIN	1	25972771	7360
831	MarVis-Suite	14	25972773	29
832	BioCyc	3	25972773	7080
833	MZmine 2	2	25972848	4060
834	METLIN	1	25972848	6653
835	XCMS	1	25974350	2316
836	HMDB	1	25974350	3531
837	METLIN	1	25974350	3594
838	UniProtKB	1	25975821	10744
839	MetaboAnalyst	1	25977701	3838
840	HMDB	1	25977701	4101
841	METLIN	1	25977701	4164
842	LIPID MAPS	1	25977701	4201
843	ChemSpider	1	25977701	4245
844	MetaboLights	2	25977770	6078
845	ChemSpider	1	25978103	2150
846	PathVisio	1	25978957	1869
847	LDA	2	26000959	10845
848	XCMS	1	26000959	11109
849	MetaboAnalyst	1	26000959	12141
850	HMDB	1	26008980	8162
851	METLIN	1	26008980	8182
852	Bayesil	1	26017271	10156
853	ICT	1	26022255	5429
854	AMDIS	1	26030804	4302
855	MetaboliteDetector	3	26030804	4553
856	XCMS	4	26038762	4145
857	MetaboAnalyst	2	26038762	4842
858	CAMERA	1	26038762	6704
859	METLIN	1	26038762	7140
860	MetaboAnalyst	1	26046375	7724
861	AMDIS	1	26052317	9081
862	Newton	1	26053618	2600
863	MetabolAnalyze	1	26054242	5290
864	mixOmics	1	26054242	7169
865	METLIN	1	26067663	6791
866	Pathos	2	26067663	7318
867	MAVEN	1	26070680	3573
868	ORCA	1	26076475	5529
869	LIPID MAPS	2	26076478	12637
870	XCMS	1	26078715	2169
871	MetaboAnalyst	1	26078715	2718
872	HMDB	1	26078715	2776
873	BioCyc	2	26078715	2798
874	METLIN	1	26080063	5955
875	HMDB	1	26086077	8177
876	MetaboAnalyst	3	26090681	8035
877	METLIN	1	26090713	8906
878	HMDB	1	26090713	8979
879	XCMS	4	26097677	3332
880	MetaboAnalyst	2	26097677	4039
881	CAMERA	1	26097677	5819
882	METLIN	1	26097677	6226
883	HMDB	1	26097677	6304
884	MetaboLights	1	26099471	5534
885	HMDB	1	26099471	6708
886	XCMS	2	26099471	7042
887	apLCMS	1	26102199	3674
888	MetaboAnalyst	2	26102199	3804
889	HMDB	1	26102199	4688
890	IPO	1	26107498	5122
891	BinBase	4	26113856	785
892	XCMS	1	26118551	2164
893	HMDB	1	26122266	14436
894	xMSanalyzer	1	26125020	2299
895	apLCMS	1	26125020	2346
896	SMART	2	26136670	1516
897	HMDB	1	26147971	8631
898	MetaboAnalyst	2	26149577	3949
899	MetaboAnalyst	1	26149720	6477
900	SMART	1	26150806	10430
901	METLIN	1	26154191	4616
902	HMDB	1	26154191	4652
903	MetaboAnalyst	1	26154191	5067
904	XCMS	2	26157423	5927
905	UniProtKB	2	26157443	3192
906	METLIN	3	26161866	8765
907	AMDIS	1	26161866	8975
908	AMDIS	1	26163699	7065
909	MetAlign	1	26163699	7520
910	HMDB	1	26170882	3961
911	XCMS	1	26170891	4091
912	METLIN	1	26170891	5164
913	LDA	1	26208165	9020
914	SMART	1	26219257	2479
915	HMDB	1	26229545	5051
916	XCMS	1	26236101	1592
917	ConsensusPathDB	1	26238291	2209
918	XCMS	1	26241678	6012
919	METLIN	1	26241678	6912
920	MetaboAnalyst	1	26241678	7452
921	XCMS	2	26244428	4576
922	XCMS online	1	26244428	4576
923	METLIN	3	26244428	5495
924	HMDB	2	26244428	5503
925	HMDB	1	26246736	4224
926	METLIN	1	26246736	4247
927	GAM	1	26248853	1713
928	VANTED	1	26262626	2818
929	MZmine 2	1	26267065	2767
930	BinBase	1	26270538	2701
931	XCMS	3	26273324	3825
932	CAMERA	1	26273324	4485
933	MetaboLights	1	26273324	4674
934	MetaboAnalyst	1	26284788	4577
935	LDA	1	26284788	5902
936	MetaboAnalyst	3	26293811	5358
937	MAIT	8	26295709	767
938	MeltDB	1	26300898	4412
939	LDA	1	26300900	3077
940	MetaQuant	1	26300900	8590
941	MIDAS	1	26304458	15743
942	MetaboAnalyst	1	26310325	3662
943	DrugBank	1	26312166	3422
944	GAM	2	26315217	269
945	Automics	1	26315217	2719
946	rNMR	1	26315217	3077
947	MetScape	2	26315396	5918
948	MetaboAnalyst	2	26317866	1373
949	HMDB	1	26317866	1631
950	SMPDB	1	26317866	1671
951	LDA	1	26317866	2151
952	HMDB	3	26317986	3336
953	BioCyc	1	26317986	4400
954	MetaboAnalyst	1	26329643	6020
955	MetExtract	1	26335000	9491
956	AMDIS	2	26337225	8290
957	HMDB	1	26352407	10299
958	Newton	1	26364041	2769
959	MetaboLights	1	26364855	7150
960	MetAlign	1	26365159	3899
961	XCMS	3	26365159	4011
962	CAMERA	1	26365159	4302
963	XCMS	4	26366133	3281
964	MetaboAnalyst	3	26366134	3624
965	MetAlign	1	26366137	2172
966	HMDB	1	26366138	6773
967	MetaboAnalyst	1	26366580	11580
968	XCMS	1	26368322	6304
969	mzMatch	1	26368322	6444
970	METLIN	1	26368322	7928
971	XCMS	1	26369413	7565
972	MetaboAnalyst	2	26369413	10193
973	HMDB	1	26372698	6642
974	MAVEN	2	26377487	6178
975	MetaboAnalyst	2	26379774	6143
976	MetAlign	2	26379776	5643
977	MSClust	2	26379776	5658
978	MetAlign	1	26381754	7421
979	MassTRIX	1	26383269	7185
980	AMDIS	1	26387596	1615
981	MZmine 2	1	26387596	3837
982	SMART	1	26388881	4315
983	MathDAMP	1	26392107	4620
984	HMDB	1	26399231	4459
985	mQTL	4	26401656	2006
986	LIPID MAPS	1	26404114	8456
987	HMDB	1	26404114	8532
988	METLIN	1	26404114	8560
989	IIS	2	26413039	902
990	Skyline	1	26413039	11168
991	MetaboAnalyst	1	26413039	13097
992	MetaboliteDetector	1	26413876	15756
993	XCMS	1	26417019	3076
994	METLIN	1	26417019	4120
995	MetaboliteDetector	1	26438291	3238
996	MetaboLights	2	26438291	4187
997	TargetSearch	1	26440112	6606
998	HMDB	1	26442281	434
999	DrugBank	1	26442281	1067
1000	UniProtKB	1	26442528	909
1001	SMART	1	26442528	2300
1002	XCMS	2	26459926	4395
1003	XCMS	2	26483760	7392
1004	mzMatch	2	26483760	7455
1005	Pathos	2	26483760	9157
1006	Newton	1	26489015	10020
1007	CMM	3	26491423	2152
1008	XCMS	1	26491423	5936
1009	mzMatch	3	26491423	5998
1010	MetaboLights	1	26491424	9389
1011	HMDB	1	26491426	3034
1012	GAM	1	26495013	1116
1013	Skyline	1	26496078	6996
1014	MetAlign	1	26500626	4823
1015	MSClust	1	26500626	4924
1016	AMDIS	1	26504583	4307
1017	XCMS	1	26505639	5859
1018	METLIN	1	26505639	8534
1019	MetaboAnalyst	1	26510913	7477
1020	AMDIS	1	26514086	4580
1021	MetExplore	2	26517871	8783
1022	MetaboAnalyst	1	26526930	4531
1023	XCMS	1	26528248	6746
1024	MetAlign	1	26528299	2643
1025	mQTL	4	26540294	1813
1026	AMDIS	2	26549189	5422
1027	VANTED	1	26549189	8806
1028	ChemSpider	1	26553294	10027
1029	BinBase	1	26571212	6984
1030	MZmine 2	1	26571212	7759
1031	HMDB	1	26573008	6770
1032	LIPID MAPS	1	26573008	6841
1033	MAVEN	1	26578677	8394
1034	MetaboAnalyst	2	26579171	6570
1035	SMART	1	26579184	2151
1036	XCMS	2	26580805	2339
1037	CAMERA	1	26580805	2662
1038	icoshift	1	26585232	5576
1039	MetaboAnalyst	1	26592771	3908
1040	XCMS	1	26594136	4502
1041	AMDIS	4	26599280	3788
1042	Metab	2	26599280	3905
1043	MetaboAnalyst	1	26600152	8720
1044	Skyline	1	26610700	16659
1045	LDA	1	26613342	5902
1046	MetabolAnalyze	1	26617479	14312
1047	XCMS	1	26618079	1480
1048	METLIN	1	26621314	1026
1049	MetaboAnalyst	1	26621314	1162
1050	GNPS	1	26623545	6872
1051	METLIN	1	26626856	8106
1052	HMDB	2	26626856	8141
1053	COVAIN	1	26628055	7539
1054	XCMS	2	26629338	6576
1055	MetaboAnalyst	1	26629816	6019
1056	MetaboAnalyst	1	26633561	10043
1057	MeltDB	1	26635840	4450
1058	LipidXplorer	1	26639035	7524
1059	AMDIS	3	26641455	8099
1060	HMDB	1	26641455	12381
1061	HMDB	1	26647645	4478
1062	BinBase	1	26648521	8411
1063	XCMS	2	26673153	3861
1064	Skyline	1	26674602	3000
1065	MetaboliteDetector	1	26677845	11778
1066	MassTRIX	1	26677845	12317
1067	XCMS	1	26685189	4314
1068	LDA	3	26688490	3233
1069	DrugBank	3	26695483	1119
1070	XCMS	1	26716870	2270
1071	BinBase	1	26716989	4658
1072	MetaboAnalyst	1	26716989	5615
1073	ORCA	1	26727517	3103
1074	HMDB	1	26732447	4040
1075	METLIN	1	26733692	4071
1076	LDA	2	26735340	6831
1077	mixOmics	2	26735340	9040
1078	Newton	1	26742069	1970
1079	MAIT	3	26743076	1043
1080	XCMS	3	26752292	2195
1081	SMART	1	26754912	1260
1082	HMDB	1	26755737	9089
1083	MassTRIX	1	26755737	9175
1084	MetaboAnalyst	1	26781291	4846
1085	icoshift	1	26784246	5491
1086	HMDB	1	26784246	6976
1087	XCMS	2	26785939	5196
1088	HMDB	1	26785939	7323
1089	METLIN	1	26785939	7351
1090	XCMS	1	26786712	2048
1091	ChemSpider	1	26788114	7708
1092	apLCMS	1	26792212	3070
1093	xMSanalyzer	1	26792212	3083
1094	mixOmics	1	26792212	4833
1095	XCMS	1	26795831	4629
1096	MetaboAnalyst	1	26805550	2590
1097	MassTRIX	1	26805550	2618
1098	SMART	1	26820138	1085
1099	HMDB	1	26824323	2217
1100	SMART	1	26828478	510
1101	ICT	7	26828485	327
1102	GNPS	4	26839597	127404
1103	XCMS	3	26842393	6519
1104	Newton	1	26846427	5393
1105	PathoLogic	1	26847793	7957
1106	MetAlign	1	26848289	2732
1107	MSClust	1	26848289	3091
1108	mixOmics	1	26848290	4704
1109	METLIN	2	26848530	2158
1110	MetaboAnalyst	2	26848530	2282
1111	MetAlign	1	26848749	7978
1112	HMDB	2	26848749	9816
1113	MetAlign	1	26863302	4595
1114	PAPi	2	26865323	10503
1115	METLIN	1	26867941	9830
1116	HMDB	1	26867941	9924
1117	METLIN	1	26870025	13679
1118	HMDB	1	26870025	13740
1119	SoyKB	4	26872939	2179
1120	XCMS	2	26878908	815
1121	MassTRIX	1	26878908	3815
1122	XCMS	1	26882264	2346
1123	CAMERA	1	26882264	2404
1124	METLIN	2	26882264	2996
1125	MetFrag	1	26882264	3220
1126	AMDIS	1	26883691	3068
1127	HMDB	1	26883691	3702
1128	UniProtKB	1	26887230	10128
1129	METLIN	1	26888486	6000
1130	HMDB	1	26888486	6049
1131	HMDB	1	26890003	2670
1132	MetaboAnalyst	2	26898711	4948
1133	HMDB	1	26903149	7489
1134	TargetSearch	1	26904035	10777
1135	HMDB	1	26904134	3617
1136	icoshift	1	26907339	4544
1137	MetExplore	7	26909353	7407
1138	BioCyc	1	26909353	10937
1139	MetaboAnalyst	1	26909364	3296
1140	XCMS	2	26911805	5882
1141	HMDB	2	26913918	10523
1142	MetaboAnalyst	2	26914934	3321
1143	HMDB	1	26919205	4305
1144	METLIN	1	26919205	4325
1145	Pathos	2	26924392	9023
1146	BioCyc	1	26924392	9066
1147	XCMS	1	26927094	5910
1148	MetaboAnalyst	2	26927094	6151
1149	METLIN	1	26927094	8826
1150	HMDB	1	26927094	8875
1151	XCMS	1	26932197	1891
1152	CAMERA	1	26932197	1900
1153	HMDB	1	26932197	4982
1154	XCMS	2	26932318	2080
1155	CAMERA	1	26932318	2911
1156	METLIN	1	26932318	3800
1157	HMDB	1	26932318	3838
1158	MetaboAnalyst	1	26932318	4427
1159	MetaboAnalyst	1	26934749	2994
1160	XCMS	2	26940428	3003
1161	METLIN	1	26940428	6905
1162	HMDB	1	26940428	6944
1163	XCMS	1	26941722	12132
1164	MetaboLights	1	26943791	2509
1165	MetaboAnalyst	2	26946124	5356
1166	SMART	1	26950110	200
1167	HMDB	1	26959121	10689
1168	MetaboAnalyst	1	26967252	1519
1169	HMDB	1	26967252	2410
1170	ChemSpider	1	26967897	4560
1171	HMDB	1	26967897	4744
1172	SMART	1	26968518	552
1173	XCMS	2	26973858	1626
1174	METLIN	1	26973858	1890
1175	XCMS	1	26974972	4935
1176	HMDB	1	26974972	5044
1177	LIPID MAPS	1	26974972	5056
1178	XCMS	3	26978774	1642
1179	XCMS online	2	26978774	1642
1180	ChemSpider	1	26978774	5303
1181	HMDB	1	26982073	11452
1182	METLIN	1	26988915	3826
1183	HMDB	1	26988915	3854
1184	PathoLogic	2	26992093	2596
1185	BioCyc	1	26992093	4504
1186	XCMS	2	26996990	2540
1187	CAMERA	1	26996990	3028
1188	ORCA	1	26998764	10237
1189	Skyline	2	27001617	6179
1190	LDA	3	27013803	611
1191	XCMS	1	27013931	4091
1192	ICT	1	27020289	5923
1193	LDA	4	27029742	3942
1194	MAIT	13	27034405	2355
1195	HMDB	2	27036109	1025
1196	SMPDB	2	27036109	1311
1197	MetaboAnalyst	1	27036109	4426
1198	MetaboAnalyst	1	27042044	3022
1199	muma	1	27042190	7552
1200	HMDB	1	27043516	5346
1201	METLIN	1	27043516	5375
1202	METLIN	1	27048914	4309
1203	XCMS	2	27048914	5772
1204	CAMERA	1	27048914	5939
1205	MAIT	2	27049062	5355
1206	METLIN	1	27053227	2216
1207	MetaboAnalyst	1	27054608	3390
1208	MetaboLab	1	27055152	6128
1209	HMDB	1	27055163	11810
1210	METLIN	1	27055163	11838
1211	MetaboAnalyst	1	27057080	5468
1212	MetaboAnalyst	1	27057740	6270
1213	HMDB	2	27058522	7575
1214	MetaboAnalyst	3	27065293	4640
1215	METLIN	2	27065293	5035
1216	MZmine 2	1	27069508	9929
1217	icoshift	2	27070784	1805
1218	MSClust	1	27073351	9977
1219	XCMS	1	27073845	5122
1220	HMDB	1	27073845	6284
1221	HMDB	1	27075394	8916
1222	HMDB	1	27076285	6001
1223	MetaboAnalyst	1	27076285	6360
1224	XCMS	1	27082433	4509
1225	CAMERA	1	27082433	5092
1226	HMDB	3	27082433	8331
1227	ConsensusPathDB	2	27082433	9387
1228	METLIN	1	27092153	12978
1229	DrugBank	2	27095146	4603
1230	UniProtKB	1	27095146	4788
1231	MetaboAnalyst	1	27095146	6834
1232	HMDB	1	27097220	6627
1233	MZmine 2	1	27098794	2140
1234	Newton	1	27102823	4796
1235	HMDB	1	27102823	10606
1236	MZmine 2	1	27102866	11961
1237	HMDB	1	27103079	5130
1238	MetaboAnalyst	3	27103079	5266
1239	MetaboliteDetector	3	27110360	1518
1240	MetaboLights	1	27113113	5234
1241	MZmine 2	1	27116459	8072
1242	MetFusion	2	27116459	8149
1243	MetaboAnalyst	1	27119083	5073
1244	BioCyc	1	27121083	4910
1245	XCMS	2	27123000	1612
1246	missForest	2	27123000	3716
1247	Galaxy-M	2	27123000	9965
1248	mixOmics	1	27123000	10792
1249	BinBase	2	27141305	4600
1250	MZmine 2	1	27141305	7092
1251	HMDB	1	27141542	1899
1252	MetaboAnalyst	1	27146975	4916
1253	ChemSpider	1	27148169	11650
1254	XCMS	1	27148319	3546
1255	HMDB	1	27148330	3085
1256	UniProtKB	1	27152931	996
1257	Escher	1	27152931	7858
1258	MetaboAnalyst	2	27158896	2714
1259	HMDB	1	27158896	5945
1260	Newton	1	27159635	1187
1261	MZmine 2	1	27159635	6014
1262	AMDIS	1	27163744	3745
1263	METLIN	1	27163744	4693
1264	HMDB	1	27163744	4796
1265	MetaboliteDetector	1	27168102	19303
1266	GAM	1	27178561	6438
1267	HMDB	1	27178561	11283
1268	MetaboAnalyst	2	27181907	6197
1269	XCMS	2	27182841	9963
1270	Mummichog	1	27182841	11054
1271	BioCyc	1	27182841	11097
1272	METLIN	2	27182841	11630
1273	HMDB	1	27182841	11641
1274	MetaboAnalyst	1	27182841	12592
1275	XCMS	2	27185473	983
1276	MetaboAnalyst	1	27185473	2854
1277	ChemSpider	1	27187439	2537
1278	HMDB	1	27187439	4006
1279	AMDIS	1	27188293	6382
1280	HMDB	1	27188343	5997
1281	METLIN	1	27188343	6025
1282	AMDIS	1	27189559	4901
1283	missForest	1	27189559	15301
1284	METLIN	1	27189771	6944
1285	HMDB	1	27194381	5879
1286	METLIN	1	27194381	5902
1287	ConsensusPathDB	1	27194471	8579
1288	METLIN	1	27194736	4721
1289	METLIN	1	27197730	5118
1290	HMDB	1	27197730	5157
1291	MetaboliteDetector	1	27199628	2202
1292	AMDIS	1	27199958	4735
1293	MetaboLights	1	27200001	9670
1294	XCMS	1	27203275	4423
1295	muma	2	27212081	4041
1296	XCMS	1	27212435	2113
1297	MetaboLyzer	2	27213362	6624
1298	SMART	1	27213624	1256
1299	XCMS	2	27215321	3974
1300	HMDB	1	27232336	7260
1301	METLIN	1	27232496	6446
1302	MetaboAnalyst	1	27241713	1034
1303	AMDIS	1	27255274	3699
1304	AMDIS	1	27255929	6878
1305	XCMS	2	27257346	3395
1306	MetFrag	9	27258318	423
1307	HMDB	1	27258318	8883
1308	MetaboAnalyst	1	27259235	1262
1309	AMDIS	1	27259554	9893
1310	HMDB	1	27265840	3035
1311	LIPID MAPS	1	27265840	3048
1312	icoshift	1	27276682	7359
1313	mixOmics	1	27276682	8163
1314	SMART	1	27286810	1232
1315	MetAlign	1	27295212	4630
1316	MetaboAnalyst	1	27295498	2522
1317	LipidXplorer	2	27295977	2557
1318	SMART	1	27296936	8980
1319	METLIN	1	27302757	12766
1320	MetaboAnalyst	1	27303373	4819
1321	IPO	1	27303376	1501
1322	MAIT	4	27307560	3883
1323	XCMS	1	27312151	9405
1324	CAMERA	1	27312151	9414
1325	LIPID MAPS	1	27312151	10219
1326	MetaboAnalyst	2	27313316	1407
1327	PathoLogic	2	27315089	660
1328	UniProtKB	1	27315089	799
1329	BioCyc	1	27315089	5614
1330	HMDB	1	27320815	7849
1331	HMDB	1	27322079	3617
1332	SMART	1	27329129	3236
1333	HMDB	1	27329129	7726
1334	MetaboLights	1	27331395	8633
1335	MetaboAnalyst	1	27331808	4900
1336	LIPID MAPS	1	27334049	4067
1337	BioCyc	1	27334049	4083
1338	WikiPathways	6	27336457	459
1339	HMDB	1	27336457	2137
1340	ChemSpider	1	27336457	2155
1341	GAM	4	27340092	9743
1342	AMDIS	1	27347875	7481
1343	MetaboliteDetector	1	27348622	7150
1344	AMDIS	1	27348810	10815
1345	GAM	3	27349443	2186
1346	MetaboAnalyst	1	27350877	6847
1347	ORCA	2	27353755	5521
1348	MetaboAnalyst	1	27357258	6635
1349	Skyline	1	27357947	3816
1350	libChEBI	1	27358602	410
1351	METLIN	2	27362422	3889
1352	SMART	1	27362730	787
1353	MetaboAnalyst	1	27364638	12423
1354	QCScreen	1	27367667	6644
1355	MetExtract	2	27367667	6868
1356	COVAIN	1	27375629	7296
1357	HMDB	2	27378919	4890
1358	XCMS	2	27379130	10079
1359	CAMERA	2	27379130	10174
1360	METLIN	1	27379130	11929
1361	UniProtKB	1	27381465	1722
1362	XCMS	2	27381941	7017
1363	MZmine 2	2	27383265	7038
1364	HMDB	1	27384078	3536
1365	METLIN	2	27384078	3650
1366	HMDB	1	27385275	3704
1367	YMDB	1	27385275	3747
1368	AMDIS	1	27385275	3902
1369	XCMS	1	27385485	4444
1370	ORCA	2	27390132	16269
1371	HMDB	1	27391145	2974
1372	mixOmics	1	27391145	4132
1373	MetaboLights	1	27398079	10228
1374	XCMS	1	27399695	3350
1375	MetaboLights	2	27399695	4206
1376	xMSanalyzer	1	27401458	6077
1377	apLCMS	1	27401458	6114
1378	XCMS	1	27403722	2005
1379	HMDB	1	27414401	8056
1380	MetaboAnalyst	1	27414401	8549
1381	MetaboAnalyst	1	27416811	10013
1382	GAM	1	27418149	126
1383	MetAlign	1	27428946	7301
1384	AMDIS	2	27428958	6261
1385	SpectConnect	1	27428958	6478
1386	METLIN	1	27429042	2683
1387	apLCMS	1	27434237	5732
1388	MSPrep	2	27434237	5913
1389	LIPID MAPS	1	27434237	6451
1390	LIPID MAPS	1	27436223	2422
1391	HMDB	2	27436591	5056
1392	VANTED	1	27436591	5917
1393	HMDB	1	27438829	5732
1394	XCMS	3	27441377	6561
1395	XCMS online	1	27441377	6561
1396	MetaboAnalyst	1	27442489	4825
1397	XCMS	2	27443631	2407
1398	MetaboAnalyst	1	27443631	4580
1399	UniProtKB	1	27446048	14979
1400	MetaboAnalyst	1	27446160	18045
1401	XCMS	2	27446929	545
1402	CAMERA	2	27446929	579
1403	BinBase	2	27453709	5183
1404	MZmine 2	1	27456714	4302
1405	HMDB	1	27456714	4491
1406	XCMS	1	27458587	2752
1407	XCMS	1	27462319	8150
1408	icoshift	1	27463020	2490
1409	MetaboAnalyst	1	27463020	4800
1410	BiGG Models	1	27467583	267
1411	DrugBank	2	27467583	5440
1412	MetaboAnalyst	1	27467775	7104
1413	HMDB	1	27471436	7674
1414	MetFrag	2	27471436	8097
1415	GNPS	6	27471437	4622
1416	MAGMa	1	27471437	7877
1417	LDA	1	27473171	9466
1418	Newton	1	27481446	6781
1419	LDA	1	27486204	4244
1420	MetaboAnalyst	1	27486204	4847
1421	AMDIS	1	27489236	9742
1422	SMART	1	27491393	973
1423	SMART	1	27493658	1019
1424	MetaboLab	2	27493727	1581
1425	MetaboLab	2	27499851	6921
1426	MetaboAnalyst	1	27500268	3560
1427	MetaboSearch	1	27502322	6707
1428	LIPID MAPS	1	27502322	6823
1429	MetFrag	1	27502322	6981
1430	MetaboAnalyst	1	27502322	7194
1431	AMDIS	1	27504198	2978
1432	XCMS	1	27504198	3754
1433	HMDB	1	27506289	5960
1434	TargetSearch	2	27512399	5990
1435	MetaboAnalyst	1	27524956	3756
1436	BioCyc	1	27524956	4965
1437	WikiPathways	4	27524956	5628
1438	HMDB	3	27526857	7845
1439	MetaboAnalyst	1	27526857	9398
1440	UniProtKB	2	27527502	1997
1441	MAIT	1	27527800	12804
1442	icoshift	1	27527852	4224
1443	MetaboAnalyst	1	27527852	13595
1444	MetaboAnalyst	1	27529220	4954
1445	XCMS	1	27530163	4548
1446	LDA	2	27535232	1375
1447	Skyline	2	27535232	19428
1448	XCMS	2	27539000	3602
1449	MetaboLights	1	27547172	2666
1450	CAMERA	4	27547172	4100
1451	HMDB	1	27560555	6532
1452	icoshift	1	27560937	2836
1453	HMDB	1	27561897	10960
1454	HMDB	1	27564096	5859
1455	MetaboAnalyst	1	27564096	6938
1456	MetaDB	1	27571918	4675
1457	Rdisop	1	27571918	5968
1458	CAMERA	4	27571918	6322
1459	GNPS	3	27571918	7263
1460	MetaboLights	1	27571918	22900
1461	SMART	1	27573891	7257
1462	IsoMS	2	27578275	4056
1463	Zero-fill	1	27578275	4559
1464	MyCompoundID	1	27578275	5037
1465	HMDB	1	27579980	8272
1466	HMDB	2	27589727	2728
1467	METLIN	1	27589727	2748
1468	MetaboAnalyst	1	27589739	6579
1469	VANTED	1	27589739	6775
1470	DrugBank	1	27597830	111
1471	muma	4	27598144	5022
1472	rNMR	1	27598144	5407
1473	MetaboAnalyst	1	27598887	8474
1474	GNPS	2	27602256	3197
1475	Pathview	1	27602759	4723
1476	HMDB	1	27609141	18841
1477	MetaboNetworks	1	27609333	5653
1478	MZmine 2	1	27616058	2647
1479	GNPS	1	27616058	3298
1480	MZmine 2	3	27618027	5450
1481	METLIN	1	27618027	6912
1482	HMDB	1	27618027	6957
1483	MetaboAnalyst	2	27618027	7222
1484	MetaboAnalyst	2	27619175	14927
1485	CMM	1	27620734	1453
1486	ORCA	1	27628562	11904
1487	ChemSpider	1	27629523	8247
1488	HMDB	1	27629523	8315
1489	METLIN	1	27629523	8344
1490	MetaboAnalyst	1	27629523	9052
1491	IPO	1	27643404	1329
1492	XCMS	1	27648224	7445
1493	MetaboAnalyst	1	27648224	13178
1494	MetaboLab	1	27651895	5575
1495	MetaboLab	1	27651926	1647
1496	MetaboAnalyst	1	27651926	2114
1497	MetaboAnalyst	1	27656890	9681
1498	METLIN	1	27656890	11836
1499	MetaboLights	1	27656890	13361
1500	BioCyc	1	27657048	3180
1501	ChemSpider	1	27657048	3708
1502	HMDB	1	27657885	7283
1503	LDA	1	27659481	2975
1504	icoshift	1	27662586	5274
1505	XCMS	1	27669323	5468
1506	mzMatch	2	27669323	5514
1507	AMDIS	1	27672507	4789
1508	MetaboAnalyst	1	27672507	5451
1509	XCMS	1	27683726	7730
1510	METLIN	2	27686065	6999
1511	HMDB	1	27686065	7063
1512	HMDB	1	27695004	4414
1513	METLIN	1	27695004	4445
1514	LIPID MAPS	1	27695004	4539
1515	MetaboAnalyst	1	27695004	8588
1516	UniProtKB	1	27695448	2689
1517	HMDB	1	27713504	7824
1518	LDA	2	27713750	4820
1519	MetaboAnalyst	1	27716121	7445
1520	mQTL	2	27716393	8241
1521	XCMS	2	27721472	4659
1522	CAMERA	1	27721472	4668
1523	MetaboAnalyst	1	27721472	5695
1524	ORCA	1	27723793	5511
1525	XCMS	3	27725730	3220
1526	HMDB	1	27725730	4478
1527	METLIN	1	27725730	4552
1528	RAMClustR	1	27729784	4809
1529	HMDB	1	27729828	6038
1530	BATMAN	1	27729829	2668
1531	XCMS	1	27729830	9578
1532	CAMERA	1	27729830	9607
1533	MZedDB	1	27729830	17806
1534	HMDB	1	27729830	20218
1535	HMDB	1	27729831	4187
1536	METLIN	1	27729833	3198
1537	ropls	1	27729833	3284
1538	MAVEN	2	27729904	4314
1539	MetaboAnalyst	2	27731396	6857
1540	Skyline	2	27733120	6834
1541	SMART	1	27733856	5487
1542	XCMS	2	27733862	6402
1543	LipidXplorer	1	27736893	3426
1544	XCMS	5	27746707	3845
1545	IPO	1	27746707	3997
1546	batchCorr	1	27746707	16768
1547	AMDIS	10	27747213	4595
1548	XCMS	1	27747213	11255
1549	CAMERA	2	27748798	3954
1550	MetaboSearch	1	27748798	7078
1551	HMDB	1	27748798	7196
1552	LIPID MAPS	1	27748798	7279
1553	TargetSearch	1	27760136	4351
1554	mQTL	3	27760136	9188
1555	AMDIS	1	27761073	3812
1556	AMDIS	1	27762345	6039
1557	mixOmics	1	27764146	7143
1558	HMDB	1	27764146	8459
1559	COLMAR	1	27764146	8603
1560	MetaboAnalyst	1	27764146	9714
1561	MetaboAnalyst	2	27767182	9416
1562	SMART	1	27775579	377
1563	LIPID MAPS	1	27775610	5442
1564	MetaboLights	3	27775610	6967
1565	HMDB	1	27775663	6200
1566	MetaboAnalyst	3	27775667	9336
1567	ChemSpider	1	27776479	2809
1568	MetaboAnalyst	1	27777729	6681
1569	HMDB	1	27779107	8311
1570	MetaboAnalyst	1	27781177	4954
1571	MetaboAnalyst	1	27786174	3565
1572	Mummichog	2	27786174	4284
1573	Newton	1	27798266	934
1574	LDA	1	27800444	5894
1575	MetaboAnalyst	1	27801841	6172
1576	MetaboAnalyst	2	27803670	5603
1577	MZmine 2	1	27803670	6191
1578	XCMS	4	27803705	4015
1579	XCMS online	2	27803705	4015
1580	METLIN	1	27803705	6531
1581	ChemSpider	1	27803705	8466
1582	Metab	1	27803950	18181
1583	SMART	1	27806050	414
1584	LDA	3	27806730	13087
1585	mixOmics	1	27807038	5424
1586	icoshift	1	27808173	4411
1587	HMDB	1	27808173	5738
1588	METLIN	1	27809246	4020
1589	HMDB	1	27809246	4028
1590	MetaboAnalyst	1	27809246	4135
1591	MetaboAnalyst	1	27809247	7741
1592	MetaboAnalyst	1	27809283	4614
1593	MetaboAnalyst	1	27811964	4745
1594	MetaboLights	1	27811964	5382
1595	apLCMS	2	27819279	3344
1596	XCMS	1	27819279	4436
1597	HMDB	1	27819279	5328
1598	UniProtKB	1	27820863	16791
1599	AMDIS	2	27821850	3133
1600	HMDB	1	27821850	3799
1601	XCMS	1	27821850	3819
1602	GNPS-MassIVE	1	27822524	10079
1603	GNPS	3	27822524	10079
1604	LIQUID	1	27822525	5503
1605	MetaboliteDetector	1	27822525	5984
1606	VANTED	1	27822525	8220
1607	GNPS	1	27822535	3257
1608	XCMS	2	27827845	4632
1609	MetMatch	10	27827849	46
1610	XCMS	1	27827849	7418
1611	AMDIS	4	27829376	9317
1612	TargetSearch	1	27833650	5141
1613	MetaboAnalyst	2	27834300	10547
1614	HMDB	1	27834866	6336
1615	UniProtKB	1	27835948	12087
1616	METLIN	1	27845433	3757
1617	MetaboAnalyst	1	27845433	3889
1618	MZmine 2	1	27845774	4621
1619	MetaboLights	1	27854294	2114
1620	ChemSpider	1	27872613	4440
1621	MAVEN	1	27874068	7845
1622	MetaboAnalyst	1	27874068	8142
1623	ConsensusPathDB	2	27874830	12871
1624	HMDB	1	27874830	12960
1625	SMART	1	27875982	13805
1626	METLIN	1	27883040	2877
1627	HMDB	1	27883040	2909
1628	LIPID MAPS	1	27883040	2915
1629	MetaboAnalyst	2	27883040	3665
1630	MetaboAnalyst	1	27891824	4951
1631	AMDIS	1	27892480	3802
1632	MetaboAnalyst	1	27892480	5760
1633	VANTED	1	27892480	6149
1634	AMDIS	1	27897248	6608
1635	METLIN	1	27899923	2297
1636	BinBase	5	27904455	4620
1637	MAVEN	1	27904735	3620
1638	MetaboAnalyst	2	27904735	4290
1639	HMDB	1	27905409	7515
1640	AMDIS	1	27907068	4500
1641	XCMS	2	27910903	4891
1642	CAMERA	2	27910903	4919
1643	METLIN	1	27910903	7443
1644	HMDB	1	27910903	7535
1645	MetaboAnalyst	1	27910903	7699
1646	MetaboAnalyst	1	27910906	4644
1647	HMDB	2	27910928	4876
1648	LIPID MAPS	2	27910928	4882
1649	MetaboAnalyst	1	27910928	5419
1650	XCMS	1	27912165	3603
1651	MetaboAnalyst	1	27912165	7493
1652	METLIN	2	27916965	3858
1653	MetaboAnalyst	1	27916965	3990
1654	MetAlign	1	27917180	2279
1655	MSClust	1	27917180	2323
1656	ICT	1	27922089	3225
1657	Bayesil	1	27923919	3956
1658	MetExtract	3	27929394	6743
1659	HMDB	1	27929400	6529
1660	ORCA	1	27932491	1899
1661	MetaboAnalyst	1	27933088	4731
1662	XCMS	1	27933298	5081
1663	RAMClustR	2	27933298	5523
1664	METLIN	1	27933363	3125
1665	mixOmics	1	27936081	6882
1666	mzMatch	1	27941966	4501
1667	MetaboAnalyst	1	27941966	4921
1668	PiMP	1	27941966	4940
1669	MetaboAnalyst	3	27941971	7285
1670	HMDB	1	27955696	10737
1671	METLIN	1	27955696	10749
1672	MetAlign	1	27965677	4960
1673	MSClust	2	27965677	5278
1674	METLIN	2	27974505	1674
1675	MetAlign	2	27976683	5883
1676	MSClust	1	27976683	6449
1677	BinBase	1	27976711	2024
1678	MetaboAnalyst	1	27976711	3400
1679	HMDB	1	27991567	3308
1680	METLIN	1	27991567	3335
1681	MetaboAnalyst	1	27991567	3408
1682	ChemSpider	1	27993127	2608
1683	MetaboAnalyst	1	27999311	163
1684	MetaboLights	1	28004739	8416
1685	METLIN	1	28005947	6838
1686	HMDB	1	28005947	6997
1687	MetaboAnalyst	1	28005947	10134
1688	HMDB	3	28005991	146
1689	DrugBank	1	28005991	1505
1690	VANTED	1	28007948	4602
1691	mQTL	5	28007948	5601
1692	MetaboAnalyst	1	28018221	3441
1693	METLIN	1	28018335	5036
1694	SMART	1	28018397	760
1695	HMDB	1	28049629	4681
1696	UniProtKB	1	28067231	3901
1697	Skyline	3	28067231	6360
1698	SMART	2	28068903	2965
1699	HMDB	1	28069010	13108
1700	LDA	2	28072421	3528
1701	AMDIS	3	28075408	11340
1702	MET-IDEA	1	28075408	11818
1703	HMDB	1	28082742	4177
1704	MetaboAnalyst	1	28085117	3838
1705	HMDB	1	28085117	4895
1706	METLIN	1	28085117	4922
1707	LIPID MAPS	1	28085117	4990
1708	HMDB	1	28085959	9978
1709	IsoMS	3	28091618	986
1710	IsoMS-Quant	2	28091618	986
1711	HMDB	1	28091618	5560
1712	XCMS	1	28095889	5787
1713	XCMS	4	28097070	6133
1714	CAMERA	2	28097070	6740
1715	METLIN	1	28097070	7584
1716	MetFrag	1	28097070	7653
1717	MetaboLights	1	28097070	7766
1718	HMDB	1	28098776	2489
1719	MetaboAnalyst	1	28098776	6199
1720	HMDB	1	28098892	5224
1721	HMDB	1	28106081	5804
1722	HMDB	2	28106735	11544
1723	LIPID MAPS	1	28106735	11779
1724	XCMS	2	28111530	3207
1725	LDA	1	28111530	3809
1726	MZedDB	1	28111530	5547
1727	HMDB	1	28111530	6779
1728	MetaboAnalyst	2	28112165	2797
1729	METLIN	2	28112165	3328
1730	HMDB	1	28112165	3530
1731	Mummichog	3	28112165	3664
1732	MetaboAnalyst	1	28115022	17167
1733	MetFrag	2	28117321	2729
1734	Newton	1	28117738	4211
1735	TargetSearch	1	28119698	4021
1736	MetaboAnalyst	1	28119732	11190
1737	XCMS	3	28123394	6765
1738	METLIN	1	28125052	4242
1739	HMDB	1	28134762	6117
1740	MetaboAnalyst	1	28134852	8689
1741	MetaboAnalyst	1	28138303	2844
1742	XCMS	2	28140394	8285
1743	LIQUID	1	28145528	4076
1744	mixOmics	1	28145528	8565
1745	XCMS	3	28151921	1376
1746	icoshift	1	28152423	3098
1747	MetaboLab	1	28152423	4589
1748	metabomxtr	1	28153035	5057
1749	AMDIS	2	28153035	15647
1750	MetaboAnalyst	1	28153035	18285
1751	METLIN	2	28154769	3369
1752	HMDB	2	28154769	3398
1753	AMDIS	1	28157703	1066
1754	MetaboAnalyst	1	28157703	3124
1755	VANTED	1	28157703	3241
1756	rNMR	1	28160205	3263
1757	XCMS	3	28161796	8444
1758	CAMERA	1	28161796	9664
1759	PAPi	1	28163917	7424
1760	MET-IDEA	1	28165361	4212
1761	UniProtKB	1	28165386	2291
1762	LDA	1	28166818	5819
1763	XCMS	1	28168106	5063
1764	METLIN	2	28168106	6461
1765	MetFrag	2	28168106	6631
1766	XCMS	1	28168227	2038
1767	BioCyc	1	28174747	5556
1768	MetaboLights	1	28176759	16062
1769	HMDB	1	28177875	3739
1770	XCMS	1	28177875	3983
1771	CAMERA	1	28177875	3992
1772	WikiPathways	1	28183786	8566
1773	METLIN	1	28185575	3954
1774	MetaboAnalyst	1	28187710	6122
1775	METLIN	1	28192465	4996
1776	HMDB	1	28192465	5009
1777	LIPID MAPS	1	28192465	5020
1778	MVAPACK	1	28194448	6124
1779	icoshift	1	28194448	6699
1780	HMDB	1	28194448	9671
1781	MetaboLights	1	28194448	10510
1782	HMDB	1	28195169	8462
1783	MetFrag	1	28195169	9564
1784	XCMS	3	28197141	2327
1785	mzMatch	2	28197141	2390
1786	HMDB	1	28197141	3355
1787	Pathos	2	28197141	5429
1788	CAMERA	1	28197141	12527
1789	FALCON	1	28199717	807
1790	HMDB	2	28201987	1331
1791	MetaboNetworks	1	28201987	8010
1792	MetScape	1	28203242	9043
1793	AMDIS	2	28205587	10940
1794	HMDB	1	28207855	7000
1795	MetaboLights	1	28208746	7825
1796	MetaboAnalyst	1	28209124	7161
1797	XCMS	1	28209995	8897
1798	HMDB	1	28211494	8452
1799	METLIN	1	28211494	8459
1800	METLIN	1	28212624	3250
1801	AMDIS	1	28213610	2182
1802	mixOmics	1	28213610	6328
1803	XCMS	1	28218290	11622
1804	MetAlign	1	28220901	1517
1805	IPO	1	28221771	1261
1806	XCMS	5	28221771	1692
1807	MetaboAnalyst	1	28223686	5016
1808	XCMS	1	28223996	2829
1809	CAMERA	2	28223996	3217
1810	CAMERA	2	28225065	1428
1811	MetaboAnalyst	1	28225065	4573
1812	MZmine 2	1	28225766	13325
1813	LipidXplorer	1	28230173	7838
1814	XCMS	1	28233811	6257
1815	XCMS	2	28234548	1853
1816	METLIN	1	28234548	1933
1817	HMDB	1	28234548	1992
1818	mQTL	1	28240269	14643
1819	MetaboAnalyst	1	28241423	4526
1820	icoshift	1	28243597	3829
1821	HMDB	1	28246392	5946
1822	XCMS	1	28253346	2287
1823	CAMERA	1	28253346	2994
1824	SMART	1	28253346	5508
1825	LDA	2	28256598	4575
1826	XCMS	2	28257062	2896
1827	XCMS online	1	28257062	2896
1828	METLIN	1	28257062	3303
1829	UniProtKB	1	28261627	1427
1830	GNPS	1	28266605	3881
1831	LDA	1	28270806	6509
1832	MAIT	7	28272391	124
1833	METLIN	1	28272510	1445
1834	HMDB	1	28272510	1456
1835	Escher	1	28280881	2791
1836	METLIN	1	28285370	14453
1837	HMDB	1	28285370	14696
1838	LDA	1	28285599	4516
1839	MetaboAnalyst	1	28285599	7596
1840	XCMS	1	28287618	3591
1841	VANTED	1	28288142	4554
1842	MetaboAnalyst	1	28296891	3715
1843	HMDB	2	28300186	5923
1844	METLIN	2	28300186	5950
1845	LIPID MAPS	1	28300186	5986
1846	MetaboAnalyst	1	28300186	11634
1847	MSClust	1	28303155	2284
1848	MS-FINDER	2	28316657	1302
1849	AMDIS	1	28321214	3810
1850	AMDIS	1	28321369	3417
1851	XCMS	1	28323825	5248
1852	mixOmics	1	28335378	8333
1853	MZmine 2	1	28335386	6184
1854	XCMS	2	28335577	5418
1855	TargetSearch	1	28338798	3196
1856	AMDIS	1	28340971	5109
1857	SpectConnect	1	28340971	5426
1858	MetaboAnalyst	2	28340971	12737
1859	Skyline	2	28345042	5273
1860	MetaboAnalyst	1	28358014	5104
1861	AMDIS	1	28358818	7687
1862	MetaboAnalyst	2	28358818	8098
1863	MetaboAnalyst	1	28374757	3109
1864	ICT	1	28377725	2561
1865	MetaboLights	2	28377754	4655
1866	HMDB	1	28378454	2781
1867	SMART	1	28378825	3408
1868	MetaboAnalyst	1	28381258	2830
1869	XCMS	3	28381824	4497
1870	BioCyc	1	28382331	2746
1871	XCMS	1	28382976	2647
1872	HMDB	1	28382976	4731
1873	METLIN	1	28382976	4753
1874	IsoCor	1	28387366	12733
1875	XCMS	1	28389631	117
1876	CAMERA	1	28389631	210
1877	Skyline	1	28389915	5262
1878	HMDB	1	28393918	5827
1879	XCMS	1	28393918	7956
1880	HMDB	1	28397817	5905
1881	METLIN	1	28397817	5915
1882	MetaboAnalyst	1	28397817	6020
1883	METLIN	1	28397872	1046
1884	MetaboAnalyst	2	28397872	1158
1885	batchCorr	1	28397877	4775
1886	METLIN	1	28397877	5923
1887	ChemSpider	1	28397877	5931
1888	MetaboAnalyst	1	28400566	5146
1889	MetaboQuant	1	28403191	2303
1890	HMDB	1	28403191	3979
1891	MetaboAnalyst	1	28408641	7496
1892	HMDB	1	28408641	8704
1893	METLIN	1	28408641	8713
1894	XCMS	1	28408641	8811
1895	XCMS	1	28409005	2713
1896	XCMS	3	28413374	2749
1897	METLIN	1	28413374	7257
1898	GAM	2	28415978	2220
1899	MetaboAnalyst	1	28420117	6631
1900	DrugBank	1	28420994	405
1901	MetaboLights	1	28421049	4560
1902	LIPID MAPS	1	28422089	5959
1903	HMDB	1	28422130	4688
1904	MetaboAnalyst	2	28424077	5341
1905	METLIN	1	28424674	9996
1906	MetaboAnalyst	1	28424787	2749
1907	VANTED	1	28429296	9348
1908	AMDIS	4	28431510	3372
1909	XCMS	1	28432333	1193
1910	CAMERA	1	28432333	1202
1911	HMDB	1	28432333	1784
1912	METLIN	1	28432333	1821
1913	HMDB	1	28436478	5466
1914	MetaboAnalyst	1	28436478	6085
1915	UniProtKB	1	28439259	10500
1916	MetaboAnalyst	1	28439279	9011
1917	pyQms	1	28441411	7631
1918	ORCA	1	28443619	9236
1919	METLIN	1	28446174	12914
1920	HMDB	1	28447334	3104
1921	LIPID MAPS	1	28448538	8920
1922	PathVisio	1	28448552	3915
1923	MetScape	1	28450497	6476
1924	MetaboAnalyst	1	28450732	7159
1925	METLIN	1	28452429	3327
1926	HMDB	1	28452429	3368
1927	ORCA	1	28453536	4492
1928	MZmine 2	1	28454235	3012
1929	HMDB	1	28454235	4563
1930	Newton	1	28454561	3271
1931	XCMS	1	28463998	5121
1932	MetaboAnalyst	1	28464016	2752
1933	MetaboAnalyst	1	28467501	4649
1934	HMDB	1	28468882	5647
1935	METLIN	1	28468882	5675
1936	MetaboAnalyst	1	28469699	3341
1937	XCMS	3	28473744	4226
1938	CAMERA	2	28473744	4389
1939	METLIN	1	28473744	6565
1940	HMDB	1	28473744	6636
1941	AMDIS	2	28473745	7555
1942	MET-IDEA	1	28473745	7666
1943	AMDIS	1	28473819	3130
1944	MetaboAnalyst	1	28473819	3413
1945	MetaboAnalyst	1	28481284	11501
1946	MET-IDEA	1	28481316	2472
1947	XCMS	1	28481316	5119
1948	MetAlign	2	28481937	5397
1949	MSClust	3	28481937	5437
1950	MetaboAnalyst	1	28481937	6711
1951	rNMR	3	28486539	3443
1952	MetaboAnalyst	1	28486539	4462
1953	MetaboLab	1	28487605	5316
1954	MetaboAnalyst	1	28487702	4999
1955	apLCMS	2	28487968	2743
1956	Mummichog	3	28487968	4290
1957	XCMS	1	28489938	3738
1958	CAMERA	2	28489938	4208
1959	METLIN	1	28489938	6504
1960	MetFrag	2	28489938	7102
1961	GNPS	7	28492366	2801
1962	MetScape	1	28492501	7670
1963	MetaboAnalyst	1	28494778	388
1964	Skyline	1	28504706	13794
1965	MetAlign	3	28508929	6808
1966	MSClust	4	28508929	7395
1967	METLIN	1	28508929	8117
1968	MAIT	2	28518056	819
1969	SMART	1	28535774	7389
1970	SMART	1	28538732	9503
1971	HMDB	1	28539642	8714
1972	ChemSpider	1	28539642	8742
1973	HMDB	1	28542375	6567
1974	MetaboAnalyst	1	28546635	6023
1975	HMDB	1	28558019	8453
1976	SMART	1	28559925	3313
1977	MetaboAnalyst	1	28566324	8905
1978	MetaboAnalyst	2	28566717	2891
1979	LIPID MAPS	1	28566741	3821
1980	MetaboAnalyst	2	28570676	5936
1981	AMDIS	1	28570677	3506
1982	SpectConnect	1	28570677	3656
1983	GAM	1	28571567	1047
1984	XCMS	1	28575008	2994
1985	mzMatch	1	28575008	3354
1986	LDA	3	28575008	4883
1987	HMDB	1	28575008	5929
1988	UniProtKB	1	28578394	6665
1989	METLIN	1	28578394	10305
1990	MetScape	1	28587097	4558
1991	XCMS	4	28587440	458
1992	HMDB	2	28587440	1229
1993	Skyline	1	28587595	11373
1994	MetaboAnalyst	2	28588266	6779
1995	ChemSpider	1	28588266	9606
1996	MetAlign	1	28589124	4643
1997	HMDB	2	28592827	3038
1998	XCMS	1	28592827	4732
1999	METLIN	1	28592827	5328
2000	LIPID MAPS	1	28592827	5406
2001	XCMS	1	28594855	3841
2002	XCMS online	1	28594855	3841
2003	LIPID MAPS	2	28596719	70
2004	HMDB	1	28596719	122
2005	XCMS	6	28604695	6214
2006	CAMERA	2	28604695	7975
2007	GNPS	1	28608848	4322
2008	XCMS	2	28608848	5087
2009	CAMERA	2	28608848	5542
2010	MetaboAnalyst	1	28611517	8553
2011	HMDB	1	28617219	302
2012	DrugBank	2	28617219	484
2013	METLIN	1	28620200	7996
2014	HMDB	1	28620200	8033
2015	MetaboAnalyst	1	28620200	9007
2016	MetaboAnalyst	1	28620397	6818
2017	XCMS	2	28621725	8049
2018	MetaboAnalyst	1	28621725	9342
2019	mzMatch	1	28622334	3687
2020	PathoLogic	1	28623667	3178
2021	XCMS	1	28623667	16689
2022	METLIN	1	28623667	16699
2023	HMDB	1	28624795	4035
2024	LIPID MAPS	1	28624795	4066
2025	MetaboAnalyst	2	28627685	1472
2026	UniProtKB	1	28631755	20971
2027	mQTL.NMR	1	28638075	5339
2028	mQTL	1	28638075	5339
2029	ICT	1	28640228	1068
2030	MetaboLights	1	28640868	4660
2031	XCMS	1	28642310	3631
2032	HMDB	1	28642310	3748
2033	BATMAN	2	28642462	13905
2034	mixOmics	1	28642462	15169
2035	XCMS	1	28644879	7758
2036	HMDB	1	28656061	5842
2037	MetaboAnalyst	4	28656061	6751
2038	MetaboLights	1	28656061	7758
2039	MetaboAnalyst	1	28659576	12941
2040	mixOmics	1	28659815	3475
2041	SMART	2	28662025	1011
2042	MetaboAnalyst	1	28662051	16762
2043	ropls	1	28663594	6786
2044	HMDB	1	28663594	8599
2045	MZmine 2	1	28665358	8874
2046	muma	1	28671681	6869
2047	MetaboAnalyst	1	28672794	8494
2048	HMDB	1	28674386	6418
2049	MetaboAnalyst	1	28674386	6462
2050	SMART	1	28674547	6281
2051	HMDB	4	28686599	12553
2052	METLIN	2	28686599	12587
2053	MetaboAnalyst	1	28686599	13863
2054	MetaboAnalyst	1	28690628	4786
2055	HMDB	7	28691704	664
2056	MassTRIX	1	28693422	11322
2057	ChemSpider	1	28693422	11345
2058	BioCyc	1	28693422	11403
2059	mixOmics	1	28693422	13034
2060	AMDIS	1	28694488	5785
2061	MetAlign	1	28694813	6227
2062	METLIN	1	28695330	4207
2063	HMDB	1	28695330	4220
2064	XCMS	1	28695330	4650
2065	mzMatch	1	28695330	4688
2066	GAM	1	28698467	1505
2067	SMART	1	28698552	1163
2068	METLIN	3	28698669	12615
2069	HMDB	3	28698669	12651
2070	GAM	1	28700638	9399
2071	GAM	2	28700670	343
2072	AMDIS	1	28700694	5577
2073	UniProtKB	1	28700717	7310
2074	HMDB	1	28701731	3534
2075	METLIN	1	28701731	3540
2076	MetAlign	1	28701756	5778
2077	MetaboAnalyst	1	28701756	11956
2078	HMDB	1	28706260	6414
2079	GNPS	3	28710400	12870
2080	MZmine 2	1	28713399	4387
2081	SMART	1	28717205	4146
2082	GNPS	4	28717220	6884
2083	HMDB	1	28720125	6658
2084	SMART	2	28721249	2669
2085	XCMS	1	28721263	3141
2086	METLIN	1	28726741	5739
2087	XCMS	1	28726741	6008
2088	SMPDB	1	28726741	6407
2089	ropls	1	28727768	4001
2090	BinBase	2	28733574	3996
2091	UniProtKB	1	28738779	3
2092	METLIN	1	28740079	8803
2093	HMDB	1	28740079	8840
2094	HMDB	1	28740230	4780
2095	alsace	1	28740499	3005
2096	LDA	2	28744195	6186
2097	MetaboAnalyst	1	28744296	7769
2098	MetaboLights	1	28744296	7946
2099	MetaboAnalyst	1	28744300	7440
2100	AMDIS	1	28745304	3822
2101	METLIN	1	28747646	7412
2102	HMDB	1	28747646	7462
2103	MetScape	1	28747920	6795
2104	MetaboAnalyst	2	28749430	4837
2105	XCMS	3	28749445	4251
2106	XCMS online	1	28749445	4269
2107	METLIN	2	28749445	6209
2108	MetaboAnalyst	1	28750641	2493
2109	HMDB	1	28750641	3331
2110	ChemSpider	1	28751896	23516
2111	MetaboAnalyst	1	28754994	1345
2112	AMDIS	1	28758940	3225
2113	SpectConnect	1	28758940	4418
2114	HMDB	1	28758940	6867
2115	PAPi	1	28760735	4326
2116	Ideom	1	28761049	9179
2117	GNPS	2	28761934	6783
2118	MetaboAnalyst	1	28763497	3254
2119	HMDB	1	28763497	4475
2120	mQTL	5	28764798	17111
2121	HMDB	1	28764799	4453
2122	LIPID MAPS	1	28764799	4464
2123	Newton	1	28771640	9811
2124	SMART	1	28775250	1412
2125	METLIN	1	28775250	23488
2126	GAM	2	28779005	4745
2127	GAM	1	28784627	13262
2128	MetAlign	1	28785036	8565
2129	mixOmics	1	28785036	9818
2130	XCMS	2	28785276	3262
2131	XCMS	1	28790923	5059
2132	HMDB	2	28790923	5185
2133	METLIN	2	28790923	5216
2134	mQTL	2	28797052	121
2135	missForest	1	28797221	9223
2136	XCMS	2	28798136	4804
2137	MetaboAnalyst	1	28798136	5919
2138	LDA	1	28798733	4852
2139	GAM	1	28808226	4599
2140	MVAPACK	1	28809118	2754
2141	icoshift	1	28809118	3103
2142	HMDB	1	28809118	4245
2143	MetaboAnalyst	1	28811491	9842
2144	METLIN	1	28811529	5144
2145	HMDB	1	28811529	5194
2146	AMDIS	1	28811829	2747
2147	MET-IDEA	1	28811829	2826
2148	MetaboAnalyst	1	28811829	3766
2149	apLCMS	1	28813452	5876
2150	xMSanalyzer	1	28813452	5918
2151	mixOmics	1	28813452	7744
2152	xMSannotator	3	28813452	9175
2153	HMDB	1	28813452	9229
2154	Mummichog	1	28813452	9678
2155	MetaboAnalyst	1	28813453	4040
2156	ORCA	1	28817722	3757
2157	HMDB	1	28819133	6753
2158	MetScape	2	28819133	7768
2159	ropls	1	28819265	7044
2160	BiGG Models	1	28820904	4149
2161	HMDB	1	28820912	10658
2162	XCMS	1	28821301	10543
2163	HMDB	2	28821301	10956
2164	MetaboAnalyst	1	28821636	7995
2165	LDA	1	28821731	7415
2166	HMDB	1	28821754	6071
2167	MetaboAnalyst	1	28821754	8412
2168	XCMS	1	28821813	4494
2169	LIPID MAPS	1	28821813	4744
2170	MZmine 2	1	28821838	5363
2171	MetaboLights	1	28821838	13039
2172	MetaboAnalyst	1	28821850	5850
2173	MetaMapR	1	28824870	2326
2174	ORCA	1	28827812	1866
2175	MZmine 2	1	28832555	5898
2176	mixOmics	1	28832555	9324
2177	icoshift	1	28837579	5730
2178	MetaboAnalyst	1	28842639	4511
2179	HMDB	2	28845460	2370
2180	AMDIS	1	28851999	15011
2181	SMART	6	28852120	1755
2182	MetaboAnalyst	2	28852120	9744
2183	LIPID MAPS	1	28852188	3783
2184	MetaboLyzer	1	28852188	4248
2185	HMDB	1	28852450	1116
2186	MetaboAnalyst	1	28854898	6286
2187	MetaboAnalyst	1	28860508	7585
2188	VANTED	1	28865488	11764
2189	AMDIS	1	28868138	2106
2190	MetNormalizer	1	28868138	5028
2191	MetaboAnalyst	1	28872622	11621
2192	XCMS	2	28874192	4755
2193	XCMS	2	28874840	6491
2194	METLIN	1	28874840	7546
2195	HMDB	1	28874840	7584
2196	MetaboAnalyst	1	28874840	7807
2197	HMDB	1	28883696	4786
2198	UniProtKB	2	28883813	9793
2199	XCMS	3	28887546	4659
2200	AMDIS	1	28887546	5195
2201	METLIN	1	28887546	5582
2202	LIPID MAPS	1	28887546	5666
2203	PAPi	4	28887546	6886
2204	MetScape	1	28893177	11787
2205	ChemSpider	1	28898255	4113
2206	MetaboAnalyst	1	28899914	4601
2207	MAVEN	2	28900116	3892
2208	XCMS	1	28900116	4160
2209	XCMS online	1	28900116	4160
2210	MetaboAnalyst	1	28900168	4990
2211	XCMS	2	28901489	1937
2212	MetaboAnalyst	1	28901489	4132
2213	GAM	1	28911616	1419
2214	XCMS	5	28911700	5511
2215	iPath	1	28912444	3634
2216	METLIN	1	28912717	2168
2217	icoshift	1	28914810	5539
2218	MetaboAnalyst	6	28920057	4383
2219	XCMS	2	28922385	3951
2220	ICT	6	28923537	105
2221	CAMERA	1	28924163	4949
2222	LIPID MAPS	1	28924163	7189
2223	MetaboAnalyst	1	28932251	3583
2224	ICT	1	28934848	740
2225	LIPID MAPS	1	28934963	9300
2226	XCMS	2	28934963	10053
2227	CAMERA	1	28934963	10101
2228	MetaboAnalyst	2	28936200	12137
2229	AMDIS	1	28936221	3946
2230	MetaboAnalyst	2	28936221	4227
2231	HMDB	1	28937622	2640
2232	BinBase	3	28937988	8072
2233	Newton	3	28939850	2040
2234	HMDB	1	28939850	12301
2235	METLIN	1	28939850	12321
2236	MetaboAnalyst	2	28941178	4678
2237	MetaboAnalyst	2	28943831	3405
2238	XCMS	1	28944830	690
2239	MetaboAnalyst	3	28945821	12007
2240	MetaboAnalyst	1	28946654	10498
2241	GNPS	2	28950891	21339
2242	MIDAS	1	28952540	7934
2243	MetaboAnalyst	1	28953266	5676
2244	HMDB	1	28953266	6231
2245	XCMS	1	28955207	3983
2246	METLIN	1	28955207	4386
2247	HMDB	1	28956853	5591
2248	AMDIS	1	28959247	7185
2249	MetaboAnalyst	1	28960374	7884
2250	GNPS	1	28968802	3906
2251	HMDB	1	28969038	2416
2252	METLIN	1	28969038	2450
2253	MetaboAnalyst	1	28973153	12753
2254	XCMS	1	28981781	4790
2255	CAMERA	1	28981781	4847
2256	MetaboAnalyst	1	28981781	9364
2257	XCMS	2	28983290	3358
2258	CAMERA	1	28983290	3666
2259	GNPS	2	28991212	2419
2260	AMDIS	1	28992054	3625
2261	AMDIS	1	28993661	5267
2262	MetaboAnalyst	2	28993661	11784
2263	AMDIS	1	29018323	1902
2264	XCMS	1	29018486	4409
2265	HMDB	1	29026156	7736
2266	MetaboAnalyst	1	29027971	6904
2267	HMDB	1	29027971	7393
2268	ChemSpider	1	29027971	7533
2269	HMDB	1	29028822	5360
2270	MetaboAnalyst	1	29028822	5459
2271	ChemSpider	1	29029409	11567
2272	HMDB	1	29029409	11579
2273	METLIN	1	29029409	11591
2274	MetaboAnalyst	1	29029409	11691
2275	MetaboAnalyst	1	29030588	7141
2276	MAVEN	1	29034329	7064
2277	VANTED	1	29034329	9423
2278	MetaboLights	1	29034329	12748
2279	ChemSpider	1	29034330	6115
2280	MAVEN	1	29034330	7117
2281	MetaboAnalyst	1	29034330	8139
2282	speaq	1	29036400	2765
2283	sampleDrift	2	29036400	7903
2284	UniProtKB	1	29038583	8326
2285	Pathview	1	29039583	7118
2286	XCMS	1	29039604	1568
2287	METLIN	1	29039604	2245
2288	HMDB	1	29039604	2261
2289	MetaboAnalyst	1	29039604	2453
2290	LIPID MAPS	2	29039849	8155
2291	HMDB	1	29042495	12106
2292	MassTRIX	1	29042495	12238
2293	XCMS	1	29044196	7213
2294	LipidXplorer	1	29046397	5536
2295	HMDB	1	29046620	7879
2296	mixOmics	2	29046620	9558
2297	MetAlign	1	29046998	2412
2298	MSClust	1	29046998	2445
2299	MetaboAnalyst	1	29046998	3177
2300	HMDB	2	29048351	2345
2301	rNMR	1	29048351	3019
2302	MetaboAnalyst	1	29051554	16833
2303	MetaboAnalyst	1	29051579	4313
2304	MetaboLights	1	29051579	4490
2305	MZmine 2	2	29053756	2588
2306	HMDB	1	29054837	4403
2307	XCMS	2	29059219	5458
2308	MetaboliteDetector	1	29059219	7454
2309	XCMS	1	29061108	9279
2310	HMDB	2	29061108	10091
2311	MetaboAnalyst	1	29061108	10105
2312	HMDB	1	29064436	4428
2313	XCMS	1	29067035	2246
2314	CAMERA	1	29067035	2255
2315	HMDB	1	29068394	5616
2316	METLIN	1	29068394	5752
2317	Skyline	1	29070843	13719
2318	HMDB	1	29075102	6567
2319	MetaboAnalyst	1	29075102	6909
2320	BioCyc	1	29075248	6026
2321	GNPS	21	29078340	1645
2322	MAVEN	1	29079593	5571
2323	MZmine 2	1	29084554	10498
2324	HMDB	1	29084954	6893
2325	ChemSpider	1	29086039	496
2326	MS-FINDER	5	29086039	1510
2327	CFM-ID	1	29086039	2514
2328	METLIN	1	29086039	3205
2329	MetFrag	1	29086039	3752
2330	MAGMa	6	29086039	4501
2331	HMDB	1	29086039	5139
2332	NP-StructurePredictor	1	29086161	884
2333	DrugBank	1	29086838	3956
2334	SMART	1	29088063	1457
2335	MAVEN	1	29090212	3722
2336	XCMS	2	29093815	4364
2337	XCMS	1	29097766	5442
2338	BinBase	2	29099052	4167
2339	MetaboAnalyst	1	29099052	5965
2340	HMDB	1	29099862	4265
2341	RankProd	1	29104520	7077
2342	SMART	2	29104824	812
2343	BioCyc	1	29109515	618
2344	HMDB	2	29109780	7525
2345	SMPDB	1	29109780	7781
2346	XCMS	2	29115374	3982
2347	CAMERA	1	29115374	4692
2348	HMDB	1	29117187	9105
2349	UniProtKB	2	29121861	8236
2350	SMART	1	29132307	579
2351	GNPS	8	29133785	35
2352	MetaboLights	1	29133888	22536
2353	XCMS	1	29133921	5353
2354	METLIN	1	29133921	6053
2355	BinBase	1	29137232	2126
2356	HMDB	1	29137365	4835
2357	FALCON	1	29138395	4286
2358	Skyline	1	29138395	9471
2359	MetaboAnalyst	1	29143783	8352
2360	MetaboAnalyst	2	29146975	4097
2361	XCMS	1	29149178	8804
2362	MetaboAnalyst	1	29149207	10123
2363	LDA	3	29151824	2723
2364	XCMS	1	29154014	5129
2365	HMDB	1	29154014	6254
2366	GAM	1	29155850	6593
2367	ChemSpider	1	29156005	2482
2368	MetaboAnalyst	3	29156692	4163
2369	MetaboAnalyst	1	29170520	7865
2370	HMDB	1	29170520	8543
2371	mzMatch	1	29176763	1953
2372	XCMS	1	29176763	2005
2373	HMDB	1	29176763	4040
2374	METLIN	1	29176763	4066
2375	mixOmics	1	29176781	10436
2376	XCMS	1	29179468	5938
2377	HMDB	1	29179468	7882
2378	METLIN	1	29179468	7902
2379	GAM	2	29179484	2110
2380	mixOmics	1	29184161	8889
2381	LIPID MAPS	1	29186558	4212
2382	HMDB	1	29186773	5082
2383	VANTED	1	29186773	5697
2384	VANTED	1	29187142	8071
2385	MetaboLights	1	29187153	5017
2386	ChemSpider	1	29187153	6485
2387	HMDB	8	29194434	1459
2388	ChemSpider	1	29200734	3968
2389	HMDB	2	29200734	4035
2390	METLIN	1	29200734	4360
2391	MetaboAnalyst	1	29200734	4538
2392	LDA	2	29200821	1154
2393	HMDB	1	29200821	5642
2394	MetaboAnalyst	1	29200821	6182
2395	XCMS	3	29200968	3079
2396	MET-COFEA	3	29204150	12039
2397	MET-XAlign	1	29204150	12427
2398	METLIN	1	29204150	13220
2399	HMDB	1	29204150	13264
2400	COVAIN	2	29204150	13645
2401	HMDB	2	29208926	10190
2402	HMDB	1	29209349	3188
2403	METLIN	1	29209349	3257
2404	COVAIN	1	29209414	7391
2405	HMDB	1	29211010	2590
2406	AMDIS	2	29211784	2321
2407	MetaboAnalyst	2	29211784	3759
2408	HMDB	1	29211784	4445
2409	MetaboAnalyst	1	29212259	9779
2410	MetaboAnalyst	1	29212488	3420
2411	METLIN	1	29213243	4151
2412	MetaboAnalyst	1	29213243	4240
2413	AMDIS	1	29213276	3978
2414	ICT	3	29218015	3123
2415	HMDB	1	29226804	9470
2416	MIDAS	1	29226804	24883
2417	HMDB	2	29228037	5465
2418	MetaboAnalyst	1	29228037	5789
2419	XCMS	1	29228577	6158
2420	CAMERA	1	29228577	6557
2421	HMDB	1	29228577	7365
2422	MetaboAnalyst	1	29228577	7815
2423	XCMS	1	29228968	6600
2424	MetaboAnalyst	2	29228968	7531
2425	METLIN	1	29228968	7839
2426	HMDB	1	29228968	7877
2427	LipidXplorer	1	29230001	8812
2428	HMDB	2	29230019	9316
2429	IsoMS	1	29230249	6923
2430	HMDB	2	29230249	7658
2431	MyCompoundID	1	29230249	7669
2432	MetaboAnalyst	1	29230249	7930
2433	MetScape	1	29230249	8025
2434	XCMS	1	29232862	3311
2435	ORCA	2	29239720	7546
2436	MAVEN	1	29240841	8503
2437	XCMS	1	29245963	1671
2438	MetaboAnalyst	1	29245963	2654
2439	AMDIS	1	29246190	4565
2440	MetAlign	2	29247597	6189
2441	MSClust	2	29247597	6696
2442	HMDB	1	29254157	6269
2443	MetaboAnalyst	2	29257137	6818
2444	XCMS	2	29257137	7261
2445	ChemSpider	2	29257137	9357
2446	CAMERA	2	29257137	12325
2447	MetaboAnalyst	2	29258187	2184
2448	METLIN	1	29258229	7745
2449	HMDB	1	29258229	7782
2450	MetaboAnalyst	1	29258276	12672
2451	SMART	2	29258435	836
2452	HMDB	1	29258572	6880
2453	METLIN	1	29258572	6889
2454	MetaboAnalyst	1	29259193	9203
2455	XCMS	2	29259228	8068
2456	METLIN	1	29259228	8831
2457	MetaboAnalyst	1	29259332	11283
2458	ConsensusPathDB	1	29262542	5333
2459	HMDB	1	29262542	6083
2460	AMDIS	1	29262594	4227
2461	XCMS	1	29262594	5950
2462	CAMERA	1	29262594	6038
2463	MetaboAnalyst	2	29262594	9866
2464	XCMS	1	29267346	5461
2465	mzMatch	2	29267346	5544
2466	BioCyc	1	29268770	248
2467	AMDIS	1	29269494	12533
2468	SpectConnect	1	29269494	12690
2469	XCMS	1	29269494	14341
2470	MetabolAnalyze	1	29269494	15250
2471	MetExtract	4	29271872	5367
2472	SMART	1	29273643	8337
2473	BiGG Models	1	29276507	11402
2474	HMDB	1	29279717	2333
2475	MetaboAnalyst	1	29281741	8935
2476	Newton	1	29283379	2743
2477	XCMS	1	29283379	5143
2478	MetaboLights	1	29291722	316
2479	BioCyc	3	29291722	1347
2480	HMDB	4	29291722	1568
2481	ChemSpider	1	29291722	1612
2482	METLIN	1	29291722	1671
2483	MetExplore	3	29291722	2213
2484	ConsensusPathDB	3	29291722	2469
2485	PAPi	2	29291722	3186
2486	MetaboAnalyst	5	29291722	3488
2487	MassTRIX	1	29291722	4512
2488	MetScape	1	29291722	4643
2489	PaintOmics	1	29291722	4669
2490	PathVisio	2	29291722	4697
2491	SMPDB	4	29291722	4761
2492	WikiPathways	1	29291722	4774
2493	XCMS	1	29291722	4796
2494	MetaboAnalyst	1	29292399	4918
2495	MetaboLights	1	29296020	9921
2496	HMDB	1	29299125	2149
2497	jQMM	4	29300340	1818
2498	MetaboAnalyst	1	29301359	3610
2499	DrugBank	1	29304175	0
2500	AMDIS	1	29311683	9684
2501	MetNormalizer	1	29311683	11078
2502	PAPi	2	29311683	11697
2503	AMDIS	1	29311718	4198
2504	HMDB	2	29311951	21757
2505	MetaboAnalyst	3	29312369	4143
2506	AMDIS	1	29312388	6783
2507	mixOmics	1	29312388	7841
2508	XCMS	1	29312570	2028
2509	LipidXplorer	1	29320510	5314
2510	HMDB	1	29321577	429
2511	METLIN	1	29321577	508
2512	XCMS	1	29321642	4720
2513	MetaboAnalyst	1	29321642	8453
2514	PathoLogic	1	29321769	2876
2515	XCMS	1	29324674	6426
2516	BinBase	1	29324762	2732
2517	MetaMapR	1	29324762	8011
2518	MetaboAnalyst	1	29330507	12020
2519	MetScape	1	29330507	12534
2520	MetImp	1	29330539	8809
2521	Newton	1	29332424	2449
2522	HMDB	1	29332424	3705
2523	MetFrag	2	29332424	3932
2524	ChemSpider	2	29342073	3528
2525	MetFrag	1	29342073	4848
2526	MAIT	7	29343684	1792
2527	AMDIS	2	29346327	2524
2528	CFM-ID	2	29346327	2751
2529	HMDB	1	29346327	2926
2530	MetaboloDerivatizer	2	29346327	3062
2531	HMDB	1	29354024	4032
2532	HMDB	1	29354030	4826
2533	HMDB	1	29354104	6931
2534	MetaboAnalyst	1	29354104	7496
2535	AMDIS	1	29358164	987
2536	MetaboAnalyst	1	29358164	1170
2537	XCMS	1	29359067	5186
2538	XCMS	1	29359109	7643
2539	XCMS online	1	29359109	7643
2540	HMDB	1	29359109	8597
2541	MetScape	1	29359109	9616
2542	XCMS	1	29360745	5576
2543	AMDIS	2	29360745	5847
2544	ChemSpider	1	29362361	4770
2545	HMDB	3	29362509	613
2546	HMDB	1	29364152	6994
2547	HMDB	1	29364889	7137
2548	METLIN	1	29364889	7165
2549	MetaboAnalyst	1	29364889	8452
2550	AMDIS	1	29367839	2586
2551	Metab	1	29367839	2759
2552	MetaboAnalyst	1	29372038	4111
2553	MetaboliteDetector	1	29374187	3167
2554	HMDB	1	29374230	7968
2555	MetaboAnalyst	1	29374230	9680
2556	IIS	2	29375488	3587
2557	MZmine 2	2	29375514	3430
2558	GNPS	1	29375514	5478
2559	MetaboAnalyst	1	29375644	5501
2560	MetaboAnalyst	1	29376041	2112
2561	METLIN	1	29376041	2338
2562	HMDB	1	29376041	2399
2563	GAM	1	29379115	4771
2564	RankProd	1	29382730	16797
2565	MetaboAnalyst	3	29382834	4152
2566	HMDB	1	29382834	4443
2567	MetaboAnalyst	1	29383505	13322
2568	MetaboAnalyst	1	29386753	3547
2569	XCMS	2	29389572	2051
2570	XCMS	1	29394282	6846
2571	YMDB	1	29394282	8315
2572	mixOmics	1	29396510	5324
2573	MetaboAnalyst	2	29402948	7076
2574	XCMS	1	29403462	5380
2575	DrugBank	2	29404805	2859
2576	BiGG Models	1	29410419	476
2577	Escher	1	29410419	1140
2578	HMDB	1	29410697	6429
2579	AMDIS	1	29412741	7269
2580	MET-IDEA	1	29412741	7584
2581	LDA	1	29415046	10714
2582	SMART	1	29416063	578
2583	HMDB	1	29416801	6685
2584	MetaboLights	1	29416801	7024
2585	BinBase	1	29419735	6148
2586	METLIN	1	29421944	3629
2587	HMDB	1	29421944	3701
2588	HMDB	1	29422017	12333
2589	MAVEN	1	29423103	5000
2590	HMDB	2	29423237	5858
2591	XCMS	1	29425148	4132
2592	PiMP	1	29425238	14171
2593	LDA	1	29437829	21957
2594	Ideom	1	29438325	3094
2595	METLIN	1	29438405	10573
2596	HMDB	1	29438405	10678
2597	MetaboAnalyst	1	29438405	11128
2598	MetaboAnalyst	1	29440987	5414
2599	MetaMapR	2	29440987	6448
2600	MetaboliteDetector	2	29443915	7328
2601	ChemSpider	1	29445362	10004
2602	HMDB	1	29447265	4883
2603	LIPID MAPS	1	29447265	4931
2604	SMART	1	29449854	329
2605	Newton	1	29451913	9663
2606	XCMS	1	29453510	8674
2607	HMDB	2	29453510	8808
2608	MAVEN	1	29459896	8443
2609	MetaboAnalyst	2	29459896	8894
2610	HMDB	1	29459896	9631
2611	METLIN	1	29464940	3608
2612	LIPID MAPS	1	29464940	3694
2613	HMDB	1	29464940	3803
2614	MetaboAnalyst	1	29464940	5116
2615	XCMS	2	29466368	5180
2616	XCMS online	1	29466368	5180
2617	MetaboAnalyst	2	29466368	6790
2618	mQTL	1	29467421	646
2619	ORCA	1	29467426	1033
2620	LDA	2	29469650	6306
2621	HMDB	2	29470400	52
2622	WikiPathways	4	29470400	78
2623	RaMP	15	29470400	227
2624	MassTRIX	1	29470709	11757
2625	AMDIS	1	29474408	5170
2626	MetAlign	1	29474408	5453
2627	GNPS	3	29487294	9687
2628	HMDB	1	29487708	2831
2629	METLIN	1	29487708	2837
2630	MetaboAnalyst	2	29487708	3278
2631	XCMS	1	29488618	4600
2632	MetaboAnalyst	1	29488618	4925
2633	HMDB	1	29489838	5529
2634	YMDB	1	29489838	5614
2635	MetaboLights	1	29491435	5001
2636	XCMS	1	29491683	3203
2637	MetaboAnalyst	1	29491683	4529
2638	MeRy-B	1	29491875	4155
2639	AMDIS	1	29497074	6912
2640	MetaboAnalyst	1	29497765	13627
2641	MetaboAnalyst	1	29498655	3131
2642	MetaboAnalyst	1	29503615	6791
2643	MVAPACK	1	29507209	8433
2644	Skyline	5	29507607	13315
2645	Normalyzer	1	29507607	18916
2646	UniProtKB	1	29507756	1509
2647	icoshift	1	29509799	3150
2648	muma	1	29509799	3983
2649	XCMS	1	29510516	11300
2650	AMDIS	1	29510516	12905
2651	HMDB	1	29510516	13691
2652	METLIN	1	29510516	13740
2653	MetaboAnalyst	2	29510516	14064
2654	XCMS	1	29511081	9437
2655	MetaboAnalyst	1	29511223	9805
2656	AMDIS	1	29511388	1673
2657	MetaboAnalyst	1	29511388	2472
2658	XCMS	1	29514433	4217
2659	MetaboAnalyst	1	29515623	1298
2660	SMPDB	1	29515623	1689
2661	MetaboLights	1	29520289	5552
2662	MetShot	1	29520358	5530
2663	IPO	1	29528408	1836
2664	GNPS	1	29529084	5083
2665	LIPID MAPS	1	29529084	5591
2666	LDA	2	29534465	5338
2667	MetaboAnalyst	2	29538444	7435
2668	XCMS	1	29538444	8484
2669	mzMatch	1	29538444	8494
2670	MetaboAnalyst	1	29545929	2407
2671	LDA	2	29549307	6065
2672	MetaboAnalyst	1	29551975	5148
2673	mzMatch	1	29554142	6984
2674	MetaboAnalyst	1	29554142	7040
2675	MetaboLights	1	29554142	7460
2676	BioCyc	1	29554291	5119
2677	XCMS	1	29556190	4535
2678	IPO	1	29556190	4596
2679	CFM-ID	1	29556190	5948
2680	FingerID	1	29556190	5987
2681	MetaboAnalyst	3	29556190	10394
2682	HMDB	1	29556190	11674
2683	UniProtKB	1	29558378	1754
2684	Skyline	1	29559655	14310
2685	LDA	1	29559675	7648
2686	MetaboAnalyst	2	29562689	6590
2687	WikiPathways	1	29562936	5283
2688	MAVEN	2	29564395	8349
2689	ChemSpider	1	29565828	3630
2690	HMDB	1	29565828	3690
2691	METLIN	1	29565828	3713
2692	MetaboAnalyst	1	29565828	3895
2693	MetaboAnalyst	1	29566661	11365
2694	HMDB	2	29568436	5362
2695	eRah	1	29570652	4830
2696	MET-IDEA	1	29572259	14945
2697	MetaboAnalyst	2	29572259	15674
2698	MetaboAnalyst	1	29572459	5225
2699	LDA	1	29581858	2788
2700	MetaboAnalyst	2	29582629	229
2701	MetScape	1	29588451	9330
2702	MetaboLights	1	29599523	4515
2703	XCMS	2	29599912	4650
2704	CAMERA	2	29599912	4659
2705	MetaboAnalyst	1	29600278	5307
2706	mixOmics	3	29600278	11554
2707	HMDB	1	29606928	14571
2708	XCMS	1	29607310	4663
2709	HMDB	1	29607310	5908
2710	MetaboAnalyst	1	29607310	6022
2711	icoshift	1	29614759	6304
2712	XCMS	1	29614820	13835
2713	MetaboAnalyst	1	29614820	13990
2714	XCMS	1	29615664	7997
2715	HMDB	1	29615664	9473
2716	IPO	1	29615722	738
2717	MetaboAnalyst	2	29615722	6761
2718	MZmine 2	1	29615816	5432
2719	MetaboAnalyst	3	29615816	6099
2720	HMDB	1	29615816	7645
2721	XCMS	1	29615930	2241
2722	MetaboAnalyst	1	29615930	3164
2723	XCMS	1	29618368	10284
2724	HMDB	1	29618368	11112
2725	METLIN	1	29618368	11122
2726	HMDB	1	29618747	7868
2727	HMDB	1	29618786	9171
2728	XCMS	3	29619037	5097
2729	XCMS	2	29619217	9649
2730	METLIN	1	29619217	10047
2731	HMDB	1	29621258	8657
2732	MetaboAnalyst	1	29623182	4692
2733	UniProtKB	1	29629557	4013
2734	MS-FINDER	1	29632321	7341
2735	MAVEN	1	29632386	9768
2736	MetaboAnalyst	2	29632386	10180
2737	HMDB	1	29632386	10783
2738	XCMS	1	29632554	3285
2739	MetaboAnalyst	1	29634739	5464
2740	MetaboAnalyst	1	29636049	6055
2741	AMDIS	1	29636520	7267
2742	HMDB	1	29636520	7655
2743	LDA	3	29651010	13146
2744	HMDB	1	29651140	10267
2745	MetaboAnalyst	1	29651140	11974
2746	XCMS	1	29651464	13488
2747	IPO	1	29651464	13532
2748	XCMS	1	29653559	7322
2749	BinBase	1	29654173	6953
2750	XCMS	1	29654235	4026
2751	HMDB	1	29654235	6016
2752	METLIN	1	29654235	6079
2753	Escher	1	29654338	4949
2754	HMDB	1	29659551	4253
2755	METLIN	1	29659551	4280
2756	MetaboLights	1	29662176	23033
2757	HMDB	5	29664467	5947
2758	MetaboLights	1	29664467	7820
2759	iPath	1	29664912	7558
2760	IsoCor	1	29666362	11013
2761	LDA	1	29670255	8973
2762	LDA	7	29670294	1359
2763	LIPID MAPS	2	29670294	8513
2764	METLIN	1	29670470	10136
2765	HMDB	1	29670470	10177
2766	LIPID MAPS	1	29670470	10209
2767	HMDB	1	29670527	5135
2768	METLIN	1	29670527	5163
2769	MetaboAnalyst	1	29670527	5414
2770	MetAlign	1	29670599	10556
2771	HMDB	4	29671398	851
2772	AMDIS	1	29671776	6681
2773	AMDIS	1	29672134	12484
2774	XCMS	2	29677216	4914
2775	HMDB	1	29677216	8526
2776	MetaboAnalyst	5	29677216	9411
2777	LIPID MAPS	1	29679203	6947
2778	HMDB	1	29679203	7076
2779	MetaboAnalyst	2	29681790	2441
2780	LIPID MAPS	1	29681790	5190
2781	MetaboAnalyst	1	29681858	1600
2782	HMDB	1	29681970	3467
2783	METLIN	1	29681970	3474
2784	UniProtKB	1	29682127	2355
2785	mzR	1	29686101	541
2786	MetaboLights	1	29686619	3046
2787	MetaboAnalyst	1	29686619	4501
2788	HMDB	2	29686991	12302
2789	COLMAR	1	29686991	12559
2790	ChemSpider	1	29688451	1384
2791	VANTED	1	29688451	1985
2792	HMDB	1	29691425	4672
2793	UniProtKB	2	29695975	4449
2794	LDA	4	29698490	181
2795	LDA	2	29700349	4725
2796	METLIN	1	29700349	10011
2797	HMDB	1	29700349	10044
2798	MetaboAnalyst	1	29700349	10237
2799	MetaboAnalyst	1	29701672	6966
2800	METLIN	1	29701702	14544
2801	LDA	5	29703925	8348
2802	mixOmics	2	29703927	10887
2803	MetaboAnalyst	1	29703927	12674
2804	mwTab	16	29706851	4
2805	XCMS	3	29707519	3209
2806	XCMS online	1	29707519	3209
2807	MetaboAnalyst	1	29707519	3737
2808	HMDB	1	29712562	5993
2809	AMDIS	5	29712949	4968
2810	MetaboAnalyst	1	29713170	3697
2811	XCMS	1	29713282	6406
2812	MetaboAnalyst	2	29713282	7584
2813	XCMS	1	29713366	5363
2814	MetaboAnalyst	1	29713366	5495
2815	MetaboAnalyst	3	29713650	779
2816	HMDB	1	29713650	1336
2817	MetaboAnalyst	1	29717213	2309
2818	METLIN	1	29719546	4979
2819	HMDB	1	29719546	4997
2820	mixOmics	2	29719546	9329
2821	MetaboAnalyst	1	29723245	4163
2822	XCMS	1	29728056	9863
2823	LDA	1	29731746	3842
2824	GNPS	8	29732263	6806
2825	MZmine 2	3	29732263	9208
2826	LDA	3	29735944	11185
2827	HMDB	1	29738573	5882
2828	XCMS	4	29739963	7028
2829	XCMS	3	29740076	5953
2830	CAMERA	1	29740076	6057
2831	MetaboAnalystR	2	29740076	7933
2832	MetaboAnalyst	1	29740076	8595
2833	XCMS	1	29740146	6300
2834	MetaboLights	1	29740463	4534
2835	UniProtKB	1	29740463	7103
2836	XCMS	1	29743087	6616
2837	METLIN	2	29743087	6868
2838	HMDB	1	29743087	7131
2839	MetaboAnalyst	1	29743087	8460
2840	MetaboAnalyst	1	29743812	6690
2841	SMART	4	29747587	2986
2842	LIPID MAPS	1	29750025	3671
2843	MetaboAnalyst	1	29751839	5930
2844	XCMS	2	29755207	5989
2845	GNPS	1	29758007	9158
2846	XCMS	1	29758036	8838
2847	Ideom	2	29758036	8987
2848	mzMatch	1	29758036	9080
2849	MetaboAnalyst	1	29759075	6388
2850	UniProtKB	2	29761785	22573
2851	MetaboAnalyst	1	29762486	1516
2852	XCMS	1	29762529	2539
2853	CAMERA	1	29762529	2545
2854	HMDB	1	29762529	2919
2855	MetaboAnalyst	1	29768470	7298
2856	HMDB	1	29769578	5123
2857	MetaboAnalyst	1	29769578	7004
2858	METLIN	1	29773876	6347
2859	HMDB	1	29773876	6437
2860	LIPID MAPS	1	29773876	6464
2861	MetaboAnalyst	1	29777128	8919
2862	MetAlign	1	29780370	7899
2863	AMDIS	1	29780370	8316
2864	mixOmics	1	29784925	12042
2865	AMDIS	1	29785207	5733
2866	SpectConnect	1	29785207	5850
2867	MAVEN	1	29788224	5410
2868	XCMS	1	29789359	11447
2869	MAVEN	1	29789359	11626
2870	XCMS	1	29789485	7749
2871	HMDB	1	29789485	8937
2872	METLIN	1	29789485	8952
2873	CMM	1	29789564	3182
2874	ChemSpider	1	29789708	10834
2875	CFM-ID	1	29789708	11142
2876	SMART	1	29794164	6698
2877	GNPS	7	29795809	23732
2878	FingerID	2	29795809	26633
2879	MetaboAnalyst	1	29795809	34554
2880	UniProtKB	1	29795809	45166
2881	XCMS	1	29796173	5058
2882	MetaboAnalyst	1	29796173	6457
2883	METLIN	1	29796254	9231
2884	HMDB	1	29796337	3595
2885	HMDB	1	29802267	6167
2886	mixOmics	1	29808030	14459
2887	MAVEN	1	29844353	2859
2888	MetaboAnalyst	2	29844353	3499
2889	HMDB	2	29844353	4218
2890	HMDB	1	29849010	3958
2891	HMDB	1	29849735	5879
2892	METLIN	1	29849735	5906
2893	METLIN	1	29849825	7614
2894	LIPID MAPS	1	29849825	7689
2895	MetaboAnalyst	1	29849950	4598
2896	LDA	2	29856120	3940
2897	TargetSearch	1	29861703	5616
2898	Skyline	1	29866910	4774
2899	XCMS	1	29867114	2381
2900	MetaboAnalyst	2	29867114	2617
2901	Mummichog	1	29867114	3039
2902	MetaboLights	1	29867918	4160
2903	MetaboAnalyst	1	29867918	4962
2904	XCMS	1	29868094	6704
2905	CAMERA	1	29868094	6895
2906	HMDB	1	29871692	4081
2907	MetaboAnalyst	2	29871692	5337
2908	MetaboLights	1	29875472	12434
2909	MetaboAnalyst	1	29875658	8978
2910	LDA	1	29875762	4888
2911	MetaboAnalyst	2	29879736	7665
2912	HMDB	1	29880867	13773
2913	METLIN	1	29880867	13783
2914	CROP	1	29880867	15815
2915	GNPS	2	29880868	7469
2916	XCMS	1	29881815	3183
2917	MetAlign	1	29887844	6169
2918	ChemSpider	1	29889072	7959
2919	MetaboAnalyst	1	29889072	9758
2920	HMDB	2	29889072	10131
2921	LIPID MAPS	1	29889072	10238
2922	MetaboAnalyst	1	29891694	2632
2923	apLCMS	2	29892192	4203
2924	MetaboAnalyst	3	29892192	4414
2925	Newton	1	29892307	1858
2926	HMDB	1	29892571	1827
2927	AMDIS	1	29892571	8008
2928	MetaboAnalyst	2	29892571	8291
2929	MAVEN	1	29892572	3709
2930	MetaboAnalyst	3	29897945	10317
2931	MetaboAnalyst	1	29898462	2217
2932	UniProtKB	2	29899267	2423
2933	AMDIS	1	29899363	7066
2934	SpectConnect	1	29899363	7396
2935	MetaboAnalyst	1	29899363	11677
2936	VANTED	1	29899363	12386
2937	GAM	2	29899364	1168
2938	LDA	2	29899739	6444
2939	MetaboAnalyst	1	29900174	3670
2940	XCMS	1	29904061	1552
2941	CAMERA	1	29904061	1686
2942	ProbMetab	1	29904061	1753
2943	MetaboAnalyst	1	29911073	4240
2944	AMDIS	1	29912913	4193
2945	HMDB	2	29914076	7993
2946	ChemSpider	1	29914076	8368
2947	PathoLogic	5	29914471	813
2948	MetaboAnalyst	2	29915201	7600
2949	UniProtKB	1	29915369	8539
2950	XCMS	1	29915533	5828
2951	IPO	1	29915533	5889
2952	CFM-ID	1	29915533	7412
2953	FingerID	1	29915533	7452
2954	MetaboAnalyst	2	29915533	8364
2955	mixOmics	1	29915533	11549
2956	METLIN	2	29915696	11450
2957	AMDIS	1	29924303	4057
2958	MetaboliteDetector	1	29925320	6866
2959	HMDB	2	29925785	4185
2960	MetaboAnalyst	1	29925785	4335
2961	HMDB	1	29925852	5723
2962	MetaboAnalyst	1	29928284	5436
2963	METLIN	1	29930200	7153
2964	HMDB	1	29930218	4116
2965	LDA	1	29930468	4294
2966	PathVisio	1	29930756	6238
2967	METLIN	1	29934622	4648
2968	HMDB	1	29934622	4740
2969	LIPID MAPS	1	29934632	3319
2970	BinBase	6	29937771	6022
2971	ChemSpider	1	29940863	5455
2972	Newton	1	29943290	5485
2973	MetaboAnalyst	1	29947894	5681
2974	MetExplore	1	29947894	7601
2975	METLIN	1	29950994	11041
2976	MetaboAnalyst	1	29959441	10228
2977	XCMS	2	29961916	7546
2978	CAMERA	2	29961916	7555
2979	MetaboAnalyst	1	29961916	11371
2980	XCMS	1	29962999	3684
2981	HMDB	1	29962999	5218
2982	XCMS	1	29963036	11071
2983	XCMS online	1	29963036	11071
2984	XCMS	1	29963064	8198
2985	CAMERA	2	29963064	8359
2986	GAM	2	29963630	3799
2987	MetaboAnalyst	1	29966242	5718
2988	HMDB	1	29966242	6743
2989	FALCON	6	29967444	2250
2990	XCMS	1	29967596	7216
2991	icoshift	1	29967793	5689
2992	LIPID MAPS	1	29968103	9992
2993	HMDB	1	29968103	10020
2994	MetAlign	1	29968381	8057
2995	HMDB	1	29969035	4277
2996	MetaboAnalyst	1	29969035	4877
2997	XCMS	1	29970880	5845
2998	Workflow4Metabolomics	1	29970880	8883
2999	MetaboAnalyst	2	29971006	2622
3000	BATMAN	1	29971006	3941
3001	GNPS	2	29973921	6196
3002	mixOmics	2	29973941	10791
3003	Newton	1	29976641	2760
3004	HMDB	1	29977569	5521
3005	MetaboAnalyst	2	29977569	6109
3006	HMDB	2	29977832	2395
3007	MetaboAnalyst	1	29977832	2562
3008	MSClust	2	29978430	6656
3009	MetaboAnalyst	2	29978430	7594
3010	METLIN	1	29979770	2851
3011	ChemSpider	1	29979770	2889
3012	METLIN	1	29986394	7972
3013	HMDB	1	29986394	8010
3014	MetaboAnalyst	1	29986394	8154
3015	LDA	2	29988433	8109
3016	TargetSearch	1	29988592	4104
3017	mixOmics	1	29988592	4556
3018	XCMS	1	29991685	2491
3019	MetaboAnalyst	1	29991685	3986
3020	MetaboAnalyst	1	29996772	5671
3021	ConsensusPathDB	1	30001349	13135
3022	XCMS	1	30001753	4058
3023	LIPID MAPS	1	30001753	6790
3024	HMDB	1	30001753	6829
3025	METLIN	1	30001753	6857
3026	ChemSpider	1	30002718	4951
3027	LIPID MAPS	1	30002718	5043
3028	MetaboAnalyst	1	30002718	5129
3029	HMDB	1	30002718	5230
3030	MRMPROBS	1	30002728	7921
3031	GNPS	1	30006464	2881
3032	MAIT	1	30006464	5009
3033	MAVEN	1	30006480	4782
3034	MZmine 2	1	30006587	7954
3035	MetaboLights	3	30006587	13162
3036	AMDIS	1	30008725	3132
3037	IsoCor	1	30012131	10795
3038	MetaboAnalyst	2	30013348	3828
3039	Skyline	1	30013371	4591
3040	HMDB	2	30013460	6274
3041	AMDIS	1	30013828	2770
3042	MET-IDEA	1	30013828	2931
3043	HMDB	1	30016349	8952
3044	MetaboAnalyst	2	30016349	9567
3045	MVAPACK	1	30021876	9728
3046	icoshift	1	30021876	9886
3047	UniProtKB	1	30024949	2776
3048	LDA	2	30024949	5484
3049	AMDIS	1	30025089	4323
3050	MetaboAnalyst	1	30026261	5329
3051	MetaboLights	1	30026575	7124
3052	ChemSpider	1	30027101	3834
3053	MetaboAnalyst	1	30027101	5726
3054	AMDIS	1	30029606	2306
3055	MetaMapR	1	30030452	3584
3056	specmine	2	30030452	6011
3057	ChemSpider	1	30034318	5685
3058	BinBase	1	30038854	4939
3059	MetaboAnalyst	1	30038854	7757
3060	BinBase	1	30039798	14219
3061	Pathview	1	30039798	15830
3062	MAVEN	2	30046159	6666
3063	MetaboAnalyst	1	30050440	12667
3064	GAM	1	30050535	3335
3065	MZmine 2	1	30050544	10193
3066	AMDIS	1	30050544	10242
3067	HMDB	1	30050661	3666
3068	PARADISe	1	30052654	1848
3069	XCMS	1	30067831	4110
3070	MetaboAnalyst	1	30068998	10402
3071	XCMS	2	30069830	5161
3072	GNPS	1	30072656	7684
3073	MetaboAnalyst	1	30072917	4990
3074	MetaboLab	1	30075744	3681
3075	METLIN	1	30078970	3825
3076	MSClust	1	30080887	2495
3077	IIS	2	30082754	7673
3078	MetaboAnalyst	1	30083258	2222
3079	AMDIS	1	30087282	8072
3080	SpectConnect	1	30087282	8284
3081	mixOmics	1	30087282	9145
3082	mixOmics	1	30087404	11106
3083	MetaboAnalyst	1	30087415	4027
3084	METLIN	2	30087415	4050
3085	AMDIS	1	30089028	2810
3086	MET-IDEA	1	30089028	3125
3087	HMDB	1	30089471	5685
3088	METLIN	1	30089471	5810
3089	LIPID MAPS	3	30089790	11138
3090	METLIN	1	30089798	4254
3091	BioCyc	1	30089798	4344
3092	BinBase	2	30089834	4891
3093	HMDB	1	30093719	7122
3094	HMDB	1	30097636	3824
3095	LDA	1	30097636	5005
3096	MetaboAnalyst	1	30098602	8655
3097	icoshift	1	30103400	6766
3098	HMDB	1	30103400	12509
3099	VANTED	1	30104643	6425
3100	MetAlign	1	30108060	1781
3101	MetaboAnalyst	1	30108060	3065
3102	mQTL	9	30108180	333
3103	AMDIS	1	30109234	11388
3104	GAVIN	1	30109234	11485
3105	HMDB	2	30111850	6139
3106	MetaboAnalyst	1	30111850	6914
3107	LDA	1	30111850	9958
3108	ChemSpider	1	30112104	4884
3109	MAIT	4	30115998	1229
3110	Skyline	1	30115998	13521
3111	missForest	2	30116286	6025
3112	specmine	1	30116286	6198
3113	mixOmics	1	30116286	9106
3114	MetaboAnalyst	1	30116286	9299
3115	MZmine 2	1	30116786	3445
3116	XCMS	2	30122883	3984
3117	HMDB	1	30122883	5113
3118	METLIN	1	30122883	5148
3119	LDA	1	30122883	5916
3120	MetaboAnalyst	1	30122883	6138
3121	MassTRIX	1	30124819	22801
3122	icoshift	1	30131771	7145
3123	MetaboAnalyst	1	30134906	4568
3124	MAIT	12	30135490	25
3125	Newton	1	30135490	4948
3126	MZmine 2	1	30138328	5951
3127	LDA	2	30139941	14908
3128	MetaboAnalyst	3	30140295	4211
3129	UniProtKB	1	30140739	6090
3130	MetaboAnalyst	1	30140739	10277
3131	LDA	1	30142973	4662
3132	METLIN	2	30148866	3320
3133	HMDB	2	30148866	3389
3134	LIPID MAPS	1	30148866	3421
3135	XCMS	1	30149503	5473
3136	MetaboAnalyst	1	30149503	7650
3137	METLIN	1	30149503	8509
3138	MetFrag	1	30149503	8522
3139	MetFusion	1	30149503	8539
3140	ChemSpider	1	30149593	1252
3141	FingerID	1	30149837	8389
3142	XCMS	1	30152755	13474
3143	CAMERA	1	30152755	13483
3144	XCMS	5	30152810	5479
3145	IPO	1	30152810	7384
3146	CAMERA	4	30152810	7559
3147	PhenoMeNal	1	30152810	11727
3148	MetaboAnalyst	2	30154509	750
3149	MZmine 2	1	30154509	5742
3150	HMDB	2	30154509	7067
3151	MetaboAnalyst	1	30154719	6587
3152	ChemSpider	1	30158424	6850
3153	PAPi	2	30158658	5720
3154	MetAlign	1	30159401	7821
3155	IsoCor	1	30159401	8132
3156	Newton	1	30159401	8874
3157	mixOmics	1	30165467	12363
3158	HMDB	1	30174764	5819
3159	LDA	5	30176828	4383
3160	MetaboAnalyst	1	30178754	4947
3161	Mummichog	1	30180851	2457
3162	MetaboAnalyst	1	30180851	2523
3163	LDA	3	30181501	2619
3164	MetFrag	1	30181601	6865
3165	HMDB	1	30181601	6980
3166	MoNA	1	30181601	7214
3167	BinBase	1	30181662	6205
3168	MetaboAnalyst	4	30181662	7674
3169	HMDB	1	30182861	4766
3170	HMDB	2	30186161	7917
3171	MetaboAnalyst	1	30186161	8172
3172	LDA	3	30186263	2607
3173	LDA	1	30186272	6815
3174	MetaboliteDetector	1	30186274	8696
3175	MetaboAnalyst	1	30190284	1339
3176	METLIN	1	30192945	5564
3177	ChemSpider	1	30192945	5639
3178	MetaboAnalyst	5	30192945	5994
3179	METLIN	1	30197481	3367
3180	HMDB	2	30197481	3418
3181	HMDB	2	30200467	5727
3182	AMDIS	1	30200472	4559
3183	MetaboAnalyst	1	30200600	7376
3184	LDA	1	30200669	7974
3185	MetaboAnalyst	1	30200669	8004
3186	AMDIS	1	30200885	3079
3187	LDA	1	30201970	10919
3188	XCMS	1	30202512	5304
3189	MetaboAnalyst	1	30202512	6298
3190	BinBase	2	30202643	5499
3191	BioCyc	2	30202653	1022
3192	XCMS	2	30205491	5553
3193	MetaboAnalyst	4	30205491	8482
3194	mixOmics	1	30205491	11301
3195	MetaboAnalyst	1	30208886	706
3196	MAIT	9	30208917	3771
3197	MetaboAnalyst	1	30210470	8427
3198	MetFrag	5	30210517	8496
3199	SMART	2	30213112	3303
3200	XCMS	3	30213972	9513
3201	UniProtKB	1	30214051	11203
3202	HMDB	2	30214633	3528
3203	XCMS	1	30216363	2013
3204	MetaboLights	1	30218007	10872
3205	MetScape	3	30221719	4937
3206	VANTED	1	30222592	2602
3207	XCMS	2	30223770	4655
3208	CAMERA	1	30223770	4757
3209	HMDB	1	30223770	5118
3210	HMDB	1	30227669	5702
3211	MZmine 2	3	30227669	6829
3212	MetaboAnalyst	1	30227669	7453
3213	MetaboLights	1	30229649	1299
3214	XCMS	1	30231483	10665
3215	MetaboAnalyst	1	30232320	5057
3216	HMDB	2	30233306	3135
3217	HMDB	2	30233366	5004
3218	MetaboAnalyst	1	30233366	5446
3219	MetFusion	1	30233611	9889
3220	HMDB	1	30235319	2922
3221	MetaboAnalyst	1	30235319	10607
3222	XCMS	1	30237415	3622
3223	HMDB	2	30237415	6019
3224	METLIN	1	30237415	6067
3225	XCMS	1	30237536	11721
3226	XCMS online	1	30237536	11721
3227	HMDB	1	30237536	12321
3228	MetaboAnalyst	2	30237765	6746
3229	GNPS	2	30237790	12480
3230	CFM-ID	2	30237790	13669
3231	mQTL	3	30239796	4923
3232	ChemSpider	1	30241383	7870
3233	MetFrag	1	30241383	7900
3234	HMDB	1	30241383	8210
3235	IsoCor	1	30244973	24328
3236	MetaboAnalyst	1	30245669	5019
3237	LDA	1	30245669	6019
3238	HMDB	2	30249062	9081
3239	METLIN	1	30249062	9223
3240	MetaboAnalyst	1	30249062	9375
3241	MetaboLyzer	2	30249301	4562
3242	HMDB	1	30249301	4700
3243	METLIN	1	30249301	5438
3244	HMDB	1	30250135	6715
3245	CMM	1	30252911	2414
3246	Pathos	2	30252911	11234
3247	NOMAD	1	30252911	14507
3248	UniProtKB	1	30256846	6335
3249	XCMS	1	30258081	2416
3250	HMDB	1	30258081	2607
3251	MetFrag	1	30261630	2277
3252	MetaboAnalyst	1	30271345	6819
3253	AMDIS	1	30271500	5461
3254	MET-IDEA	1	30271500	5540
3255	MetaboAnalyst	1	30271500	5964
3256	HMDB	1	30274334	3957
3257	MetaboAnalyst	1	30275502	9849
3258	METLIN	1	30275502	10024
3259	ORCA	1	30275512	4298
3260	XCMS	3	30275944	23183
3261	HMDB	1	30275944	24834
3262	METLIN	1	30275944	24848
3263	Escher	1	30275963	220
3264	UniProtKB	1	30275963	944
3265	HMDB	1	30279506	6663
3266	Skyline	3	30281662	4448
3267	MetaboAnalyst	1	30281662	8639
3268	MeltDB	4	30281675	6930
3269	MetaboAnalyst	2	30282911	3817
3270	MetaboAnalyst	1	30283018	7792
3271	AMDIS	1	30283472	4026
3272	METLIN	2	30283472	6702
3273	Newton	1	30283472	8298
3274	MetaboAnalyst	1	30287502	2276
3275	METLIN	1	30287502	2504
3276	HMDB	1	30287502	2647
3277	LDA	2	30287912	10251
3278	MZmine 2	1	30297858	8792
3279	AMDIS	1	30298076	8812
3280	MetaboAnalyst	2	30298076	11576
3281	MetaboAnalyst	4	30301464	8712
3282	METLIN	1	30301464	9334
3283	HMDB	1	30301464	9345
3284	SMART	1	30301915	486
3285	HMDB	1	30304872	5067
3286	MetaboAnalyst	1	30306746	8003
3287	UniProtKB	1	30311911	13666
3288	missForest	1	30318485	4093
3289	specmine	1	30318485	4266
3290	MetaboAnalyst	1	30318485	5880
3291	MetaboAnalyst	2	30319419	10997
3292	HMDB	1	30319461	4155
3293	XCMS	1	30319461	4392
3294	MetaboAnalyst	1	30319461	7005
3295	MetExplore	1	30319551	7039
3296	UniProtKB	1	30320222	6166
3297	Ideom	1	30322119	5967
3298	ChemSpider	1	30322392	7897
3299	MetaboAnalyst	1	30322408	6187
3300	CROP	1	30322445	21381
3301	HMDB	1	30323181	5995
3302	XCMS	1	30325459	3956
3303	XCMS online	1	30325459	3956
3304	METLIN	1	30325459	4733
3305	LIPID MAPS	1	30325459	4826
3306	LIPID MAPS	1	30325540	1960
3307	apLCMS	2	30326066	2716
3308	xMSanalyzer	1	30326066	3177
3309	mixOmics	1	30326066	4634
3310	xMSannotator	2	30326066	5412
3311	Mummichog	1	30326066	6013
3312	XCMS	1	30326066	7210
3313	MetFrag	1	30326066	7299
3314	MetaboAnalyst	1	30326981	4329
3315	UniProtKB	1	30328278	4467
3316	AMDIS	1	30332402	8165
3317	MetaboAnalyst	2	30332402	9076
3318	MetaboAnalyst	1	30332647	10722
3319	MetAlign	1	30333849	6683
3320	XCMS	1	30340609	4248
3321	CAMERA	1	30340609	4323
3322	compMS2Miner	1	30340609	4515
3323	HMDB	1	30340609	4638
3324	XCMS	1	30344613	2247
3325	HMDB	2	30345296	5794
3326	METLIN	1	30345296	6018
3327	MetaboAnalyst	1	30345296	6595
3328	MetaboAnalyst	1	30349552	6719
3329	MetaboLights	1	30351417	1641
3330	MWASTools	1	30351417	2272
3331	mQTL.NMR	1	30351417	5437
3332	mQTL	1	30351417	5437
3333	XCMS	1	30356046	7849
3334	MetaboAnalyst	2	30356046	8904
3335	MetaboAnalyst	1	30356850	11546
3336	LDA	1	30359203	5430
3337	MetaboAnalyst	1	30359203	6406
3338	MetaboAnalyst	1	30359396	9062
3339	HMDB	1	30359409	2404
3340	HMDB	1	30360494	2909
3341	XCMS	2	30360500	5270
3342	XCMS online	1	30360500	5270
3343	METLIN	1	30360500	5777
3344	METLIN	1	30363659	2139
3345	HMDB	1	30364174	5156
3346	GNPS	1	30364174	5162
3347	MoNA	1	30364174	5324
3348	MS-FLO	1	30364174	5363
3349	MetaboLights	1	30364178	3939
3350	AMDIS	3	30366381	4955
3351	MAVEN	1	30366991	5057
3352	MetaboAnalyst	3	30373890	5623
3353	Newton	1	30373914	9033
3354	MetaboAnalyst	2	30374076	6687
3355	METLIN	1	30374076	9491
3356	ChemSpider	1	30374076	9509
3357	METLIN	1	30374327	5769
3358	HMDB	1	30374327	5777
3359	SMART	1	30374327	6527
3360	METLIN	1	30375449	6556
3361	MetAlign	1	30379914	5342
3362	MetaboAnalyst	1	30379914	5876
3363	XCMS	3	30383773	10778
3364	CMM	2	30383867	473
3365	IsoCor	1	30383867	11969
3366	MetaboAnalyst	1	30384419	3921
3367	MetaboAnalyst	1	30386302	3688
3368	MetaboAnalyst	1	30386416	8171
3369	XCMS	1	30388807	3782
3370	HMDB	1	30388807	4361
3371	LIPID MAPS	1	30388843	6716
3372	MET-IDEA	1	30388843	8754
3373	MetaboAnalyst	1	30388843	8962
3374	VANTED	1	30388843	8986
3375	AMDIS	1	30393232	3673
3376	LDA	2	30395589	5938
3377	BinBase	1	30397150	5021
3378	Skyline	2	30397344	18503
3379	METLIN	2	30400243	7569
3380	LIPID MAPS	1	30400243	7615
3381	HMDB	1	30400243	7682
3382	MetaboAnalyst	1	30400246	3264
3383	ChemSpider	1	30401779	7843
3384	MetaboAnalyst	1	30401779	8304
3385	MetFrag	1	30401779	9126
3386	MetaboLights	1	30401779	9844
3387	METLIN	1	30403754	6409
3388	HMDB	2	30403754	6445
3389	PathVisio	1	30403754	6689
3390	Rhea	1	30404627	1784
3391	HMDB	1	30404627	7861
3392	MetScape	1	30404627	15224
3393	METLIN	1	30405156	8383
3394	METLIN	1	30405462	2962
3395	UniProtKB	1	30406389	3934
3396	MAIT	1	30410474	3317
3397	HMDB	1	30410553	6164
3398	HMDB	1	30410974	5351
3399	ChemSpider	1	30413031	2943
3400	GNPS	4	30413766	10059
3401	MetaboAnalyst	1	30417110	4020
3402	AMDIS	1	30417112	6583
3403	MetaboAnalyst	1	30417115	11320
3404	BioCyc	1	30420658	9365
3405	XCMS	4	30420658	15243
3406	MZmine 2	2	30422126	8976
3407	MetaboAnalyst	1	30424544	1486
3408	UniProtKB	1	30429500	3061
3409	HMDB	2	30439966	78
3410	BioCyc	1	30439966	15510
3411	SMPDB	1	30439966	15627
3412	MetaboAnalyst	1	30439966	16160
3413	batchCorr	1	30439966	16205
3414	MetaboAnalyst	2	30440048	6898
3415	MetaboAnalyst	1	30441779	10221
3416	XCMS	2	30441827	8867
3417	CAMERA	1	30441827	9745
3418	AMDIS	1	30441827	10039
3419	MetaboAnalyst	2	30441827	11326
3420	MAIT	14	30442667	196
3421	SMART	3	30442667	7133
3422	AMDIS	1	30442926	3936
3423	MET-IDEA	1	30442926	4297
3424	MetaboAnalyst	2	30442926	4563
3425	MetaboAnalyst	1	30444915	3227
3426	MetaboAnalyst	2	30446671	5758
3427	MetaboAnalyst	1	30450336	2190
3428	mQTL	5	30452450	2811
3429	LDA	1	30453603	3357
3430	MetaboAnalyst	2	30453603	6396
3431	AMDIS	1	30455673	7656
3432	METLIN	1	30459441	9829
3433	HMDB	1	30459441	9865
3434	ConsensusPathDB	1	30459441	17016
3435	XCMS	1	30459607	4840
3436	METLIN	1	30459607	6551
3437	ChemSpider	1	30459729	5130
3438	mixOmics	1	30459786	4844
3439	COVAIN	1	30459786	6397
3440	MetaboAnalyst	1	30464091	1409
3441	BATMAN	1	30464399	5457
3442	ConsensusPathDB	1	30466445	2344
3443	IIS	1	30467357	5384
3444	LDA	3	30470770	6586
3445	METLIN	1	30472698	5571
3446	METLIN	1	30473724	8288
3447	MetaboAnalyst	1	30473788	6520
3448	MetaboAnalyst	1	30476077	4936
3449	ORCA	1	30477161	8314
3450	HMDB	1	30477251	3412
3451	METLIN	1	30477251	3418
3452	BioCyc	2	30482178	2977
3453	GNPS	5	30483240	10229
3454	ropls	1	30483387	4174
3455	HMDB	1	30483387	4329
3456	MetAlign	1	30485320	4733
3457	BioCyc	1	30485320	6086
3458	HMDB	1	30485320	6171
3459	XCMS	2	30485366	5094
3460	CAMERA	1	30485366	5331
3461	MetaboAnalyst	1	30485366	6495
3462	HMDB	1	30486347	2321
3463	MetaboAnalyst	1	30486444	6395
3464	Pathview	1	30486822	10815
3465	ChemoSpec	3	30486822	13063
3466	muma	1	30486822	13210
3467	HMDB	3	30488036	6689
3468	MoNA	1	30488036	7481
3469	IIS	1	30496310	12756
3470	BinBase	4	30496310	18192
3471	Newton	2	30498504	3399
3472	MetaboLights	1	30498504	4640
3473	MetaboAnalyst	1	30498504	5023
3474	METLIN	1	30498504	5782
3475	MetAlign	2	30504416	1300
3476	MSClust	2	30504416	1333
3477	HMDB	1	30504416	2682
3478	XCMS	1	30507950	2223
3479	CAMERA	1	30507950	2238
3480	MetaboAnalyst	1	30507950	2957
3481	XCMS	1	30513683	2951
3482	HMDB	2	30513683	5669
3483	METLIN	2	30513683	5700
3484	SMART	1	30514210	3168
3485	UniProtKB	1	30514210	4444
3486	Skyline	1	30514331	4303
3487	MetaboAnalyst	1	30514398	6016
3488	XCMS	2	30515230	3447
3489	METLIN	1	30518059	11168
3490	CFM-ID	1	30518059	11770
3491	HMDB	2	30518126	8526
3492	HMDB	1	30521565	7425
3493	tmod	1	30523338	22597
3494	MAVEN	1	30524402	7793
3495	UniProtKB	1	30526481	3643
3496	GAM	1	30526483	9032
3497	MAVEN	1	30530686	663
3498	HMDB	3	30531976	12442
3499	MassTRIX	1	30532185	2487
3500	MetaboAnalyst	4	30532734	4349
3501	UniProtKB	1	30532780	2742
3502	XCMS	2	30534085	7486
3503	HMDB	1	30534085	8782
3504	MetaboAnalyst	1	30535578	8397
3505	MetaboAnalyst	1	30538212	3355
3506	HMDB	1	30538262	2930
3507	MyCompoundID	1	30538262	2953
3508	IsoMS	1	30538262	3294
3509	Rhea	1	30538977	32
3510	HMDB	3	30540827	2170
3511	MetScape	1	30544627	4373
3512	MetaboAnalyst	1	30545097	11909
3513	HMDB	1	30545412	6386
3514	METLIN	1	30545412	6438
3515	MetaboAnalyst	2	30546033	9000
3516	GNPS	4	30551573	5873
3517	XCMS	2	30551573	9128
3518	AMDIS	1	30552373	5650
3519	MetaboAnalyst	4	30552373	5826
3520	LDA	2	30555669	5648
3521	XCMS	1	30558181	3633
3522	XCMS online	1	30558181	3633
3523	XCMS	3	30558273	7841
3524	XCMS online	1	30558273	7841
3525	MetaboAnalyst	3	30558273	8792
3526	MS-FINDER	2	30558273	11801
3527	ChemSpider	1	30558273	12057
3528	HMDB	1	30558273	12126
3529	XCMS	1	30558291	4585
3530	HMDB	1	30558291	5563
3531	METLIN	1	30558291	5569
3532	LIPID MAPS	1	30559182	2840
3533	AMDIS	1	30559759	10485
3534	GAVIN	1	30559759	10525
3535	HMDB	1	30563510	5467
3536	XCMS	1	30563510	5659
3537	XCMS	2	30563511	8791
3538	CAMERA	1	30563511	9033
3539	GAM	2	30564208	7438
3540	Skyline	1	30564511	5643
3541	Bayesil	1	30567330	4370
3542	MetaboAnalyst	2	30567330	4800
3543	CFM-ID	1	30568641	15837
3544	MetaboLights	1	30571714	6044
3545	METLIN	1	30574129	6146
3546	MetaboAnalyst	1	30576340	12038
3547	MetaboAnalyst	1	30577438	5809
3548	mixOmics	1	30577516	10179
3549	MetaboAnalyst	1	30577613	5877
3550	IsoCor	1	30577858	18623
3551	MetaboAnalyst	1	30581445	5529
3552	METLIN	1	30583600	5660
3553	MetaboAnalyst	1	30584255	7203
3554	MetaboAnalyst	1	30584450	3175
3555	METLIN	1	30585483	6797
3556	HMDB	1	30585483	6845
3557	mzMatch	1	30589893	7278
3558	MetaboLights	1	30589893	7604
3559	ORCA	1	30590650	3919
3560	MetaboAnalyst	2	30590791	14069
3561	MetaboAnalyst	1	30594224	2471
3562	HMDB	1	30594224	6038
3563	XCMS	1	30597885	5234
3564	LIPID MAPS	1	30597885	5316
3565	BiGG Models	1	30602581	600
3566	metaMS	1	30603089	2691
3567	MetaboAnalyst	1	30603089	2923
3568	mQTL	4	30603090	1925
3569	MetaboLab	2	30607371	4460
3570	DrugBank	1	30612223	4065
3571	UniProtKB	1	30612223	7148
3572	Mummichog	1	30616514	8757
3573	MetaboAnalyst	1	30616569	8547
3574	MetaboLights	1	30616610	8135
3575	XCMS	1	30618762	7413
3576	Pathview	1	30618770	9211
3577	GNPS	3	30619120	5864
3578	MetaboAnalyst	1	30619201	7702
3579	LDA	2	30619212	7590
3580	MetaboliteDetector	1	30619231	36299
3581	XCMS	1	30619231	37842
3582	MZmine 2	1	30619387	8504
3583	MetaboAnalyst	1	30621047	3805
3584	UniProtKB	1	30621587	10452
3585	XCMS	1	30622379	6329
3586	XCMS online	1	30622379	6329
3587	METLIN	1	30622379	6824
3588	METLIN	1	30622605	6568
3589	MetaboAnalyst	1	30622605	6780
3590	GAM	1	30626871	2284
3591	AMDIS	1	30626938	9017
3592	SMART	1	30626970	6252
3593	HMDB	1	30627368	7354
3594	UniProtKB	1	30627484	2908
3595	GNPS	2	30627488	7551
3596	apLCMS	2	30630288	3095
3597	HMDB	1	30631318	7872
3598	MetaboAnalyst	2	30634471	5945
3599	HMDB	1	30635036	5608
3600	MetaboAnalyst	1	30635036	8111
3601	mzMatch	1	30637340	4124
3602	MetaboAnalyst	1	30637340	4367
3603	BioCyc	1	30637340	4971
3604	VANTED	1	30637340	5057
3605	MetaboLights	1	30637340	7576
3606	HMDB	1	30640898	18492
3607	HMDB	1	30641898	5285
3608	XCMS	1	30641909	3875
3609	HMDB	1	30641909	4265
3610	MetaboAnalyst	1	30641909	4885
3611	icoshift	1	30641988	5345
3612	mixOmics	1	30642070	6181
3613	SMART	1	30647410	7202
3614	apLCMS	1	30647410	10029
3615	LDA	2	30647410	11529
3616	BinBase	1	30651608	5323
3617	AMDIS	1	30654520	10582
3618	MZmine 2	1	30654589	4691
3619	GNPS	3	30654589	6161
3620	MetaboAnalyst	1	30661088	4908
3621	HMDB	1	30661088	5454
3622	METLIN	1	30661088	5481
3623	MetaboAnalyst	1	30662277	5661
3624	MetaboLights	1	30662445	12384
3625	MetaboAnalyst	1	30662445	14680
3626	MetaMapR	2	30662445	15835
3627	MetaboAnalystR	1	30667072	6228
3628	HMDB	1	30669279	6568
3629	MZmine 2	1	30669322	4908
3630	HMDB	1	30669322	6404
3631	LDA	1	30669471	4440
3632	MetaboAnalyst	1	30669471	6700
3633	GNPS	3	30669497	9659
3634	XCMS	2	30669498	6281
3635	RAMClustR	1	30669498	6760
3636	MetaboAnalyst	1	30669571	3927
3637	MetNormalizer	1	30669586	4480
3638	HMDB	1	30669586	9771
3639	Mummichog	3	30669586	10031
3640	MetaboAnalyst	1	30669586	11291
3641	LDA	1	30670726	4822
3642	MoNA	1	30671459	1903
3643	GNPS	1	30671459	1922
3644	MetaboAnalyst	2	30671459	1969
3645	muma	1	30671479	5017
3646	BinBase	1	30672092	4662
3647	MetaboAnalyst	2	30672092	4744
3648	MetaboAnalyst	1	30677091	9184
3649	MetaboAnalyst	1	30678351	8827
3650	MetabolAnalyze	2	30678374	7438
3651	ropls	2	30678374	7555
3652	GAM	1	30678677	6381
3653	Pathview	1	30679248	9682
3654	MetaboAnalyst	2	30679516	6295
3655	MetaboAnalyst	1	30679561	9719
3656	MetaboAnalyst	1	30682122	7722
3657	MetaboAnalyst	1	30683649	6153
3658	UniProtKB	1	30687126	5273
3659	XCMS	3	30687133	2289
3660	HMDB	1	30687133	3690
3661	MetaboAnalyst	1	30687133	3880
3662	AMDIS	1	30687358	3505
3663	METLIN	1	30689668	6321
3664	MetScape	1	30691143	7543
3665	XCMS	1	30691236	2352
3666	MetaboAnalyst	2	30691236	3062
3667	MS-FINDER	2	30691236	3411
3668	GNPS	1	30691236	3897
3669	HMDB	1	30691236	4455
3670	XCMS	1	30692547	7047
3671	RAMClustR	1	30692547	7138
3672	MetaboAnalyst	2	30692547	10008
3673	HMDB	1	30692917	5835
3674	icoshift	1	30692917	6017
3675	ChemSpider	1	30696057	5487
3676	HMDB	1	30696057	5551
3677	XCMS	1	30696813	7219
3678	MetaboAnalyst	1	30696813	8430
3679	MZmine 2	1	30696848	2589
3680	ropls	1	30696914	9619
3681	BinBase	1	30699916	4019
3682	XCMS	3	30700781	4889
3683	MetaboAnalyst	1	30700781	6672
3684	GNPS	3	30701193	4283
3685	HMDB	5	30704467	332
3686	MetaboAnalyst	3	30705347	11220
3687	MetaboAnalyst	1	30705366	8307
3688	GNPS	5	30705420	11985
3689	SMART	1	30705672	1409
3690	MZmine 2	2	30705672	6023
3691	XCMS	1	30708983	5068
3692	XCMS online	1	30708983	5068
3693	HMDB	1	30708983	6003
3694	METLIN	1	30708983	6031
3695	BinBase	1	30709324	5514
3696	MetaboAnalyst	2	30709324	6792
3697	XCMS	2	30711004	4132
3698	CAMERA	3	30711004	4141
3699	HMDB	1	30711004	6049
3700	LDA	1	30713531	4333
3701	SMART	1	30715639	3139
3702	PARADISe	2	30717245	2563
3703	AMDIS	1	30717245	2863
3704	MoNA	1	30717245	3201
3705	ropls	1	30717245	4334
3706	MetaboAnalyst	2	30717353	5233
3707	HMDB	1	30717353	6127
3708	VANTED	1	30718727	6635
3709	ORCA	1	30718847	4033
3710	Rdisop	1	30718847	11945
3711	MetaboLights	1	30718847	25545
3712	MetaboAnalyst	1	30721880	4418
3713	HMDB	1	30723413	5430
3714	GNPS	5	30729343	6151
3715	MetaboAnalyst	2	30729343	8541
3716	mixOmics	1	30733669	5107
3717	XCMS	1	30737304	6019
3718	MetaboAnalyst	1	30737304	7349
3719	MZmine 2	1	30741955	5263
3720	HMDB	1	30741955	7894
3721	METLIN	1	30741955	7923
3722	MetaboAnalyst	2	30741955	8581
3723	MetaboAnalyst	1	30742692	15827
3724	TargetSearch	1	30744558	4111
3725	VANTED	1	30744558	4883
3726	AMDIS	1	30753668	6895
3727	MZmine 2	1	30755686	8819
3728	METLIN	1	30755786	4555
3729	GNPS	6	30760325	4116
3730	MZmine 2	2	30760325	4580
3731	MetaboAnalyst	3	30765803	12676
3732	HMDB	1	30766606	6901
3733	COLMAR	1	30766606	6977
3734	MetaboAnalyst	1	30766606	7226
3735	MetaboAnalyst	1	30769815	5249
3736	XCMS	3	30770400	2165
3737	XCMS online	1	30770400	2165
3738	MetaboAnalyst	3	30770400	2814
3739	HMDB	1	30770400	3567
3740	UniProtKB	1	30778366	6366
3741	HMDB	1	30781495	3518
3742	METLIN	1	30781495	3546
3743	SMART	1	30781698	392
3744	UniProtKB	1	30783122	889
3745	HMDB	1	30783155	6921
3746	METLIN	2	30783155	7457
3747	MetaboAnalyst	1	30783155	8018
3748	HMDB	2	30783167	14190
3749	XCMS	1	30787921	6084
3750	MetaboAnalyst	1	30787921	6241
3751	LIPID MAPS	1	30788084	5572
3752	ropls	1	30788345	5536
3753	HMDB	1	30791599	3067
3754	METLIN	1	30791599	3098
3755	MetaboAnalyst	1	30791599	3295
3756	XCMS	1	30791630	3928
3757	MetaboAnalyst	1	30791630	4316
3758	HMDB	2	30792403	8078
3759	LDA	2	30792417	7600
3760	HMDB	3	30792417	9753
3761	MZmine 2	1	30796274	2885
3762	GNPS	1	30796274	4801
3763	MetaboAnalyst	1	30796299	4074
3764	HMDB	1	30796299	5344
3765	ChemSpider	1	30796299	5372
3766	icoshift	1	30800000	4670
3767	MetaboAnalyst	1	30800000	5760
3768	MetaboAnalyst	1	30800073	5552
3769	XCMS	1	30800169	4146
3770	MetaboAnalyst	1	30800679	4359
3771	MAVEN	1	30801024	9783
3772	MetaboAnalyst	2	30801385	6908
3773	XCMS	1	30804813	7525
3774	AMDIS	1	30804813	7846
3775	mixOmics	1	30804813	11350
3776	ORCA	1	30808763	12236
3777	MetImp	1	30808950	3679
3778	AMDIS	1	30808957	3293
3779	MetaboAnalyst	1	30813246	4850
3780	MetaboAnalyst	1	30814549	10433
3781	XCMS	1	30815007	2896
3782	MetaboAnalyst	1	30815007	3768
3783	LIPID MAPS	1	30816114	704
3784	METLIN	1	30816114	804
3785	LIPID MAPS	1	30816198	7098
3786	METLIN	2	30816198	7130
3787	HMDB	1	30816198	9002
3788	MetFrag	1	30816198	9079
3789	GNPS	4	30816229	4435
3790	HMDB	1	30816283	6191
3791	MetaboAnalyst	1	30816283	9300
3792	XCMS	1	30822271	2816
3793	METLIN	1	30822271	3458
3794	MetaboAnalyst	1	30822271	3527
3795	XCMS	2	30823457	7653
3796	Workflow4Metabolomics	1	30823457	7670
3797	CAMERA	1	30823457	7844
3798	HMDB	1	30823457	8392
3799	LDA	1	30823457	11351
3800	LIPID MAPS	1	30824725	6472
3801	PiMP	2	30830405	6759
3802	XCMS	1	30830405	6802
3803	GNPS	1	30830405	9229
3804	MeRy-B	2	30830438	6492
3805	HMDB	1	30830438	6783
3806	Workflow4Metabolomics	1	30830438	8231
3807	MoNA	1	30830438	8923
3808	XCMS	3	30830439	8817
3809	XCMS online	2	30830439	8817
3810	MSClust	1	30830450	5152
3811	MetAlign	2	30830456	10025
3812	MSClust	3	30830456	10512
3813	METLIN	1	30830456	11116
3814	HMDB	1	30830467	7713
3815	NMRProcFlow	1	30830467	9854
3816	HMDB	1	30830480	8952
3817	MetaboAnalyst	2	30830480	11027
3818	MetScape	1	30830480	12121
3819	MetaboAnalyst	3	30830939	6946
3820	MetaboAnalyst	1	30832316	3445
3821	HMDB	1	30833800	3689
3822	MetaboAnalyst	2	30833800	3953
3823	MAVEN	1	30834330	10978
3824	ConsensusPathDB	1	30837600	13560
3825	MetaboAnalyst	3	30837607	8355
3826	XCMS	1	30837975	7007
3827	PiMP	1	30837975	7185
3828	GNPS	1	30837975	7272
3829	MZmine 2	1	30837981	10444
3830	Newton	1	30842288	4002
3831	XCMS	2	30842473	5708
3832	MetaboAnalyst	1	30845742	8907
3833	XCMS	3	30846747	3626
3834	icoshift	1	30854017	2835
3835	XCMS	3	30854479	11952
3836	MetaboAnalyst	1	30854479	14624
3837	MetaboAnalyst	1	30857174	4236
3838	HMDB	2	30857174	4730
3839	HMDB	1	30858393	8483
3840	MetaboAnalyst	1	30858406	8386
3841	XCMS	1	30862046	4885
3842	MetaboAnalyst	2	30862046	5049
3843	METLIN	1	30862046	5095
3844	mixOmics	3	30862783	6843
3845	MetaboAnalyst	1	30862783	16065
3846	HMDB	3	30862783	16214
3847	XCMS	3	30863278	6339
3848	XCMS online	1	30863278	6339
3849	MetaboAnalyst	2	30863278	6681
3850	AMDIS	1	30866428	1841
3851	MET-IDEA	1	30866428	3783
3852	AMDIS	1	30866484	5296
3853	ChemSpider	1	30866537	4477
3854	XCMS	4	30866811	5044
3855	CAMERA	4	30866811	5053
3856	MetaboAnalyst	1	30867823	4489
3857	XCMS	4	30868357	10735
3858	XCMS online	3	30868357	10735
3859	LDA	1	30870465	8629
3860	MetaboAnalyst	1	30870465	9492
3861	Newton	1	30870965	3805
3862	XCMS	1	30870965	6528
3863	MetaboAnalyst	1	30871192	4493
3864	SMART	1	30871577	2129
3865	XCMS	2	30872634	6567
3866	MetaboAnalyst	2	30872634	14337
3867	MetaboAnalyst	1	30872747	5118
3868	MetaboAnalyst	1	30873029	5238
3869	HMDB	1	30873143	10354
3870	MetaboAnalyst	1	30873143	10565
3871	XCMS	2	30873192	6444
3872	XCMS online	1	30873192	6444
3873	MetaboAnalyst	1	30873192	7399
3874	AMDIS	2	30874619	1618
3875	LDA	1	30875994	4741
3876	XCMS	1	30881305	4383
3877	HMDB	1	30881305	5757
3878	XCMS	1	30881991	3066
3879	LDA	1	30884752	2495
3880	apLCMS	1	30886186	6968
3881	MetaboAnalyst	5	30886186	7201
3882	METLIN	1	30886186	9434
3883	ChemSpider	1	30889792	3241
3884	XCMS	4	30889850	2455
3885	XCMS online	1	30889850	2455
3886	BATMAN	1	30889850	4425
3887	METLIN	1	30889850	4494
3888	HMDB	1	30889850	4554
3889	XCMS	1	30892279	2998
3890	MetaboAnalyst	1	30892279	3584
3891	LIPID MAPS	1	30893377	8092
3892	MetaboAnalyst	1	30893827	6173
3893	XCMS	3	30894869	3997
3894	MetaboAnalyst	2	30894869	6944
3895	FALCON	1	30898970	2168
3896	SMART	1	30901924	514
3897	MetScape	1	30904021	10970
3898	MetaboAnalyst	1	30906866	5996
3899	MetaboAnalyst	1	30908309	4200
3900	XCMS	3	30909447	92
3901	CAMERA	3	30909447	117
3902	MetaboAnalystR	2	30909447	2084
3903	BioCyc	1	30909447	2527
3904	MetaboAnalyst	1	30917512	5274
3905	MetaboAnalyst	1	30918592	4727
3906	LIPID MAPS	2	30918592	4827
3907	METLIN	1	30918592	5298
3908	XCMS	2	30925828	10076
3909	NMRProcFlow	1	30929085	3509
3910	XCMS	1	30929085	4966
3911	mixOmics	2	30929085	5547
3912	METLIN	2	30931940	13458
3913	XCMS	1	30932002	7618
3914	HMDB	1	30932002	8098
3915	BioCyc	1	30932002	8119
3916	MetaboAnalyst	1	30932023	12200
3917	HMDB	1	30932023	13537
3918	MZmine 2	1	30934741	6532
3919	GNPS	2	30934741	7537
3920	METLIN	1	30934777	5136
3921	METLIN	1	30934961	6220
3922	MetaboAnalyst	1	30934961	7583
3923	XCMS	1	30939835	1776
3924	MetaboAnalyst	2	30939835	1835
3925	METLIN	2	30939835	2847
3926	LDA	3	30940702	10965
3927	VANTED	1	30941004	6215
3928	METLIN	1	30941040	7107
3929	HMDB	1	30941040	7128
3930	MetaboAnalyst	1	30941040	7353
3931	MetaboAnalyst	6	30943218	5597
3932	AMDIS	1	30943892	6742
3933	MAIT	1	30943912	1728
3934	AMDIS	1	30943912	4438
3935	mixOmics	1	30943912	6258
3936	Rdisop	1	30944337	5078
3937	XCMS	2	30944337	28375
3938	XCMS	2	30944407	4088
3939	MetaboAnalyst	1	30944407	5483
3940	HMDB	1	30944407	5889
3941	HMDB	1	30947742	3127
3942	METLIN	1	30947742	3159
3943	ChemSpider	1	30948730	10251
3944	MetaboAnalyst	1	30949447	9091
3945	MetaboAnalyst	1	30951537	10231
3946	HMDB	1	30951537	10630
3947	METLIN	1	30951537	10637
3948	GNPS	1	30951537	10956
3949	MetaboAnalyst	1	30958695	9827
3950	MetaboAnalyst	2	30961194	5965
3951	HMDB	1	30961592	7643
3952	icoshift	1	30961592	7687
3953	MetaboSearch	1	30967565	7160
3954	MetaboAnalyst	1	30967780	3638
3955	tmod	1	30967866	4617
3956	HMDB	1	30971408	5425
3957	MetaboAnalyst	1	30974861	3762
3958	AMDIS	2	30976014	6321
3959	MetaboAnalyst	1	30976014	7379
3960	AMDIS	1	30976468	6444
3961	Metab	1	30976468	6644
3962	XCMS	1	30976994	4350
3963	AMDIS	1	30976994	4561
3964	METLIN	1	30976994	7218
3965	MetaboAnalyst	1	30976994	9346
3966	HMDB	1	30978940	8070
3967	MetaboAnalyst	1	30978940	8351
3968	GNPS	2	30978942	11605
3969	XCMS	1	30983011	3609
3970	HMDB	1	30983011	4685
3971	LIPID MAPS	1	30983011	4801
3972	METLIN	1	30983011	4861
3973	MetaboAnalyst	1	30983951	3134
3974	SMART	2	30984581	3421
3975	IsoMS	2	30987728	4475
3976	IsoMS-Quant	1	30987728	5029
3977	MyCompoundID	2	30987728	5461
3978	HMDB	1	30987728	5594
3979	MZmine 2	1	30988456	5556
3980	MetaboAnalyst	1	30988456	8688
3981	MetaboAnalyst	1	30991712	7684
3982	HMDB	1	30991712	8100
3983	DrugBank	1	30991712	8218
3984	GNPS	4	30992350	5713
3985	MAVEN	1	30992358	4101
3986	LIPID MAPS	1	30992445	9644
3987	GNPS	1	30992445	9679
3988	AMDIS	2	30992998	12396
3989	PAPi	1	30992998	13199
3990	AMDIS	5	30995751	3501
3991	SpectConnect	3	30995751	3620
3992	MetaboAnalyst	2	30995751	3833
3993	Ideom	1	30995826	4971
3994	XCMS	1	31001226	2528
3995	XCMS	1	31004054	6048
3996	MSClust	1	31007274	6732
3997	HMDB	2	31007875	4418
3998	LIPID MAPS	1	31007875	4432
3999	MetaboAnalyst	1	31007875	5547
4000	HMDB	2	31007908	7779
4001	ChemSpider	1	31007908	7794
4002	XCMS	1	31009738	4359
4003	CAMERA	1	31009738	4594
4004	MetaboAnalyst	2	31009738	5687
4005	HMDB	2	31009738	5956
4006	Newton	1	31010044	15926
4007	XCMS	2	31010058	2776
4008	XCMS online	1	31010058	2776
4009	HMDB	1	31010058	3226
4010	HMDB	1	31010169	5530
4011	METLIN	1	31010169	5536
4012	XCMS	1	31011359	4174
4013	HMDB	1	31011359	5362
4014	HMDB	1	31011363	7705
4015	GNPS	2	31013880	3930
4016	CFM-ID	32	31013937	108
4017	LIPID MAPS	7	31013937	3364
4018	MoNA	6	31013937	3388
4019	FingerID	1	31013937	11264
4020	HMDB	4	31013937	13496
4021	ChemSpider	1	31013937	13507
4022	DrugBank	1	31013937	14474
4023	YMDB	1	31013937	14544
4024	GNPS	1	31013937	15720
4025	HMDB	1	31015435	4493
4026	HMDB	2	31015463	7665
4027	HMDB	1	31015483	14227
4028	MetaboAnalyst	1	31015483	15116
4029	HMDB	2	31015890	1081
4030	HMDB	1	31023251	10309
4031	MetaboAnalyst	3	31024306	7063
4032	MZmine 2	1	31024306	7725
4033	UniProtKB	2	31024465	4732
4034	HMDB	1	31027508	4092
4035	METLIN	1	31027508	4301
4036	MetaboAnalyst	1	31027508	4474
4037	LIPID MAPS	1	31028243	3102
4038	HMDB	1	31031621	8520
4039	MS-FLO	1	31035489	11386
4040	MetaboAnalyst	1	31035489	12055
4041	HMDB	1	31038126	3785
4042	SMART	1	31039176	1296
4043	SMART	1	31042480	12134
4044	MetaboAnalyst	1	31042480	14068
4045	MetaboAnalyst	3	31046764	5153
4046	Skyline	2	31049073	5974
4047	MetaboAnalyst	1	31052259	4499
4048	MetaboAnalystR	1	31052259	4813
4049	BioCyc	2	31052521	234
4050	HMDB	1	31052597	10300
4051	METLIN	1	31052597	10328
4052	MetaboAnalyst	1	31052597	10516
4053	GNPS	3	31057509	4757
4054	mQTL	2	31057604	5388
4055	mixOmics	2	31060614	11432
4056	LDA	1	31060614	18406
4057	ChemSpider	1	31065079	7895
4058	MetaboAnalyst	1	31065079	8051
4059	Skyline	1	31065079	9199
4060	MetaboAnalyst	1	31065240	8756
4061	MetaboliteDetector	3	31067731	4231
4062	LDA	1	31068624	8965
4063	METLIN	1	31068624	14813
4064	HMDB	1	31068711	18572
4065	XCMS	1	31069173	4254
4066	BioCyc	2	31071110	1115
4067	Rhea	1	31071110	1427
4068	HMDB	1	31071948	5466
4069	LDA	1	31072371	15376
4070	MAIT	5	31073372	3017
4071	HMDB	1	31074304	10503
4072	icoshift	1	31074304	11148
4073	MetaboAnalyst	2	31074304	13705
4074	MetaboAnalyst	1	31080482	9808
4075	HMDB	1	31080482	9929
4076	HMDB	1	31080489	24121
4077	MetaboAnalyst	1	31080495	9504
4078	MetaboAnalyst	1	31083449	5004
4079	XCMS	1	31083512	2157
4080	HMDB	1	31083533	6191
4081	MassTRIX	1	31083533	6276
4082	XCMS	1	31083591	4726
4083	HMDB	1	31083591	4930
4084	METLIN	1	31083591	4936
4085	HMDB	1	31083625	7955
4086	ChemSpider	1	31083625	8337
4087	Pathview	1	31086360	21435
4088	TargetSearch	1	31087074	2148
4089	HMDB	1	31088568	4816
4090	GNPS	1	31088928	10064
4091	MetaboAnalyst	4	31089393	4797
4092	MetaboAnalyst	2	31091237	4857
4093	MetaboAnalyst	2	31096578	5397
4094	MetaboAnalyst	2	31096611	5582
4095	SMART	1	31096671	2465
4096	GNPS	1	31097761	7691
4097	MetaboAnalyst	3	31100982	1884
4098	SMPDB	1	31100982	4269
4099	MetaboAnalyst	1	31101394	12834
4100	MetaboAnalyst	1	31105563	10985
4101	XCMS	1	31106063	5120
4102	rNMR	1	31106208	2546
4103	HMDB	1	31106208	3008
4104	UniProtKB	1	31106836	1943
4105	XCMS	1	31109094	3092
4106	MetaboAnalyst	1	31109325	6040
4107	XCMS	2	31110494	8489
4108	CAMERA	1	31110494	8672
4109	HMDB	1	31110494	9891
4110	METLIN	1	31110494	9915
4111	MAVEN	1	31113899	4378
4112	MetaboAnalyst	1	31114499	6723
4113	Newton	1	31114791	3831
4114	HMDB	1	31114791	11216
4115	BinBase	1	31116776	5080
4116	METLIN	1	31117282	11803
4117	MetaboAnalyst	4	31117294	4916
4118	MetaboAnalyst	1	31118641	6143
4119	HMDB	1	31118641	6389
4120	GNPS	1	31120241	8590
4121	MoNA	2	31121816	1539
4122	BinBase	7	31121816	1754
4123	MZmine 2	1	31121822	3354
4124	MetaboAnalyst	1	31121837	7524
4125	HMDB	1	31121837	7736
4126	METLIN	1	31121837	7802
4127	MZmine 2	1	31126114	6460
4128	LIPID MAPS	2	31126114	6991
4129	apLCMS	1	31126114	8542
4130	xMSanalyzer	1	31126114	8593
4131	xMSannotator	1	31126114	8867
4132	MetaboAnalyst	1	31130618	6388
4133	MoNA	1	31130958	2302
4134	HMDB	1	31130958	2349
4135	MetaboAnalyst	5	31130958	2676
4136	MetaboAnalyst	1	31131051	2909
4137	XCMS	1	31134130	5756
4138	HMDB	1	31137456	4905
4139	ChemSpider	1	31137456	4936
4140	AMDIS	1	31138818	9145
4141	icoshift	1	31138818	10954
4142	MetaboAnalyst	1	31141612	11883
4143	HMDB	2	31142787	14493
4144	HMDB	1	31143240	2556
4145	XCMS	1	31145785	7932
4146	CAMERA	1	31145785	8544
4147	XCMS	1	31146329	7403
4148	LDA	2	31149398	9200
4149	BioCyc	1	31149398	9406
4150	Newton	1	31150449	10023
4151	MetaboAnalyst	1	31150490	3637
4152	GNPS	1	31151240	7657
4153	MetaboAnalyst	1	31151312	2081
4154	UniProtKB	3	31156689	6193
4155	XCMS	1	31160669	5242
4156	CAMERA	1	31160669	5284
4157	ropls	1	31160669	5849
4158	MetaboAnalyst	1	31160672	15847
4159	MetaboAnalyst	5	31160916	4604
4160	HMDB	2	31160916	4992
4161	METLIN	1	31161037	7859
4162	MetaboAnalyst	1	31161037	9222
4163	icoshift	1	31164448	3280
4164	MetaboAnalyst	2	31164448	4274
4165	LDA	1	31164869	4915
4166	GAVIN	1	31171772	10535
4167	MetaboAnalyst	1	31173494	1588
4168	MetaboAnalyst	1	31174528	5371
4169	MetaboAnalyst	1	31175317	12148
4170	HMDB	1	31175317	12822
4171	MSClust	1	31175497	5560
4172	HMDB	1	31178969	8878
4173	MetaboAnalyst	1	31178969	9564
4174	MetaboAnalyst	1	31185597	4646
4175	XCMS	2	31186337	10226
4176	CAMERA	2	31186337	22276
4177	MetaboAnalyst	1	31186337	23733
4178	HMDB	1	31186665	5018
4179	MetaboAnalyst	1	31186665	7607
4180	BinBase	1	31188532	1205
4181	MetaboAnalyst	3	31188532	1757
4182	ropls	2	31189018	16978
4183	GNPS	1	31189698	6281
4184	MZmine 2	1	31190850	582
4185	METLIN	1	31193444	5364
4186	ropls	1	31193444	6194
4187	XCMS	1	31195667	1989
4188	MetaboAnalyst	1	31195667	3604
4189	HMDB	1	31195973	10388
4190	METLIN	1	31195973	10458
4191	MetaboAnalyst	1	31195973	11763
4192	MetaboAnalyst	1	31196218	1720
4193	GSimp	2	31199438	5392
4194	AMDIS	1	31200467	8752
4195	LDA	2	31205521	2618
4196	DrugBank	3	31206254	289
4197	MAVEN	1	31207887	3587
4198	MetaboAnalyst	1	31207887	4469
4199	XCMS	2	31208020	11285
4200	IPO	1	31208020	11674
4201	CAMERA	1	31208020	12426
4202	METLIN	1	31208020	15130
4203	MetFrag	21	31209548	14
4204	RMassBank	4	31209548	3436
4205	ChemSpider	2	31209548	14465
4206	SMART	1	31211131	2157
4207	MZmine 2	1	31212620	4937
4208	SMART	1	31212620	8101
4209	GAM	1	31212712	1628
4210	METLIN	1	31212804	3693
4211	HMDB	1	31212804	3778
4212	MetaboAnalyst	1	31212947	8203
4213	ORCA	1	31213556	13872
4214	tmod	2	31213562	4413
4215	XCMS	1	31213562	6486
4216	MetaboAnalyst	1	31213562	6653
4217	Mummichog	1	31213562	6680
4218	HMDB	2	31214133	7499
4219	MetaboAnalyst	1	31214133	7759
4220	MZmine 2	1	31215866	16885
4221	METLIN	1	31216740	8368
4222	HMDB	1	31216740	8805
4223	SMART	1	31216753	3760
4224	SIMAT	2	31218095	5345
4225	mixOmics	1	31220133	5709
4226	TargetSearch	1	31221078	4974
4227	MetaboAnalyst	2	31221078	5690
4228	HMDB	1	31223312	12811
4229	MetaboAnalyst	1	31226775	6231
4230	UniProtKB	1	31226935	2928
4231	MAIT	1	31231693	6043
4232	AMDIS	1	31233506	8942
4233	MetaboAnalyst	1	31233506	9265
4234	METLIN	1	31234776	5113
4235	HMDB	1	31234776	5131
4236	MetaboAnalyst	2	31234801	4786
4237	HMDB	1	31235863	8078
4238	METLIN	1	31235863	8088
4239	MetaboAnalyst	1	31235863	8149
4240	LDA	1	31237071	7338
4241	MetaboLights	1	31238512	9
4242	XCMS	2	31238512	756
4243	MZmine 2	1	31238512	771
4244	MRMPROBS	2	31238512	1361
4245	ChemSpider	1	31242564	7281
4246	AMDIS	2	31248049	1589
4247	MetaboAnalyst	1	31248049	2369
4248	HMDB	1	31248127	4961
4249	MetaboAnalyst	1	31248127	5607
4250	MetaboAnalyst	2	31249318	12069
4251	XCMS	2	31249327	9146
4252	CAMERA	1	31249327	9848
4253	HMDB	1	31249327	12119
4254	XCMS	1	31249646	2722
4255	CAMERA	1	31249646	2731
4256	LIPID MAPS	1	31249646	3115
4257	HMDB	1	31249646	3153
4258	DrugBank	2	31249827	4374
4259	ChemSpider	2	31249827	5515
4260	MetaboAnalyst	1	31249865	9710
4261	HMDB	1	31249865	9850
4262	XCMS	1	31252591	7120
4263	MetaboAnalyst	1	31252591	7235
4264	XCMS	3	31252691	6214
4265	LipidXplorer	1	31256696	3823
4266	XCMS	1	31257446	6835
4267	CAMERA	1	31257446	6844
4268	CFM-ID	1	31257446	9534
4269	GNPS	3	31257446	9969
4270	METLIN	1	31258626	3450
4271	MetaboAnalyst	1	31258626	5171
4272	SMART	1	31259099	1811
4273	MetaboAnalyst	1	31259100	3998
4274	METLIN	1	31261711	6885
4275	HMDB	1	31261711	6923
4276	MetaboAnalyst	1	31261711	7249
4277	MassComp	14	31262247	0
4278	GAM	1	31263141	10503
4279	XCMS	1	31263197	3743
4280	HMDB	1	31263778	7197
4281	LIPID MAPS	1	31263778	7213
4282	Skyline	2	31264961	10335
4283	MetaboAnalyst	1	31266959	8573
4284	LDA	2	31269076	4516
4285	Newton	1	31269636	5449
4286	HMDB	2	31270333	18429
4287	MetaboAnalyst	1	31270333	24593
4288	CMM	1	31270362	7618
4289	HMDB	1	31272480	11757
4290	AMDIS	1	31273246	5842
4291	HMDB	2	31275280	5454
4292	MetNormalizer	1	31275280	5938
4293	LDA	1	31275280	7442
4294	icoshift	1	31275747	5080
4295	XCMS	4	31275982	4961
4296	XCMS	2	31276563	1216
4297	MetFrag	12	31277571	3278
4298	CFM-ID	1	31277571	45413
4299	MoNA	3	31277571	46589
4300	GNPS	3	31277571	46661
4301	XCMS	2	31281253	5741
4302	MetaboAnalyst	2	31281253	7711
4303	LDA	1	31281534	3941
4304	AMDIS	3	31281534	5730
4305	MetaboAnalyst	1	31281534	7644
4306	HMDB	1	31281534	7852
4307	METLIN	1	31281595	2040
4308	ChemSpider	1	31284429	5403
4309	ChemSpider	1	31284490	6280
4310	ORCA	1	31285422	2752
4311	XCMS	1	31286856	5934
4312	MetaboAnalyst	1	31286856	7016
4313	CAMERA	1	31289324	8026
4314	MetaboAnalyst	1	31289941	9568
4315	MetaboAnalyst	1	31291287	5228
4316	HMDB	1	31293545	5417
4317	MetaboAnalyst	2	31293545	5685
4318	MetaboAnalyst	1	31295280	2542
4319	AMDIS	1	31297156	6657
4320	Newton	1	31298609	1218
4321	MetaboAnalyst	1	31300671	10020
4322	MetaboAnalyst	2	31304256	6085
4323	HMDB	1	31304256	6354
4324	Newton	1	31304444	3790
4325	PathVisio	1	31306552	14402
4326	HMDB	1	31307473	5865
4327	MetaboAnalyst	1	31307473	5992
4328	LDA	1	31307473	7112
4329	MetaboAnalyst	1	31308419	15907
4330	MetaboAnalyst	1	31308751	402
4331	XCMS	1	31308751	433
4332	MassTRIX	1	31308797	3260
4333	HMDB	1	31308797	3396
4334	MetaboAnalyst	1	31308797	3643
4335	XCMS	1	31310630	4640
4336	HMDB	3	31310630	10179
4337	MetaboAnalyst	1	31311090	7642
4338	MetaboAnalyst	1	31311515	2712
4339	GNPS	3	31315242	189
4340	FingerID	2	31315242	335
4341	pyMolNetEnhancer	1	31315242	1369
4342	RMolNetEnhancer	1	31315242	1424
4343	HMDB	1	31315563	4326
4344	MelonnPan	2	31316056	6376
4345	MetaboAnalyst	1	31316347	6364
4346	HMDB	1	31316347	6410
4347	YMDB	1	31316482	5143
4348	LDA	1	31316490	5095
4349	AMDIS	3	31316583	8858
4350	XCMS	1	31318855	21628
4351	MIDAS	1	31319524	1075
4352	LDA	1	31321013	10958
4353	apLCMS	3	31323682	2570
4354	xMSanalyzer	1	31323682	3045
4355	HMDB	1	31323682	3506
4356	xMSannotator	2	31323682	3550
4357	MetFrag	1	31323682	4822
4358	XCMS	1	31323682	5674
4359	mixOmics	1	31323682	7439
4360	Mummichog	2	31323682	8212
4361	HMDB	1	31323867	5362
4362	MetaboLights	1	31323867	5988
4363	XCMS	1	31323921	4120
4364	HMDB	1	31323921	4909
4365	SMART	1	31328154	3556
4366	AMDIS	1	31332157	1732
4367	XCMS	1	31332211	4338
4368	MetaboAnalyst	1	31333466	6006
4369	COLMAR	4	31336728	1777
4370	HMDB	1	31336728	7246
4371	LIPID MAPS	1	31337054	7922
4372	MetaboAnalyst	1	31338124	7087
4373	MetaboAnalyst	1	31340428	12488
4374	METLIN	1	31340453	5889
4375	MetaboAnalyst	1	31340453	6111
4376	MetaboAnalyst	4	31340509	4032
4377	XCMS	1	31340592	5620
4378	MetaboAnalyst	1	31340592	6462
4379	MetaboAnalyst	1	31340604	7955
4380	MetaboAnalyst	1	31340667	6755
4381	BinBase	4	31341175	7494
4382	MetaboAnalyst	4	31341175	8936
4383	MetaboAnalyst	3	31341490	7011
4384	METLIN	1	31341490	7266
4385	HMDB	2	31341490	7317
4386	XCMS	1	31341492	5581
4387	HMDB	1	31341492	6624
4388	LDA	1	31344888	6362
4389	MZmine 2	1	31344982	2902
4390	METLIN	1	31346188	10934
4391	MetaboAnalyst	1	31348786	3809
4392	GNPS	2	31349703	9132
4393	METLIN	1	31349744	3443
4394	HMDB	1	31349744	3479
4395	MSPrep	1	31349744	4035
4396	MetaboAnalyst	2	31350457	6019
4397	METLIN	1	31350457	6839
4398	mixOmics	2	31354670	15799
4399	MAIT	7	31354725	3272
4400	MetaboAnalyst	2	31354891	3301
4401	HMDB	1	31357410	11194
4402	MetaboAnalyst	1	31357410	11350
4403	HMDB	1	31358041	4329
4404	MetaboAnalyst	1	31367041	13462
4405	MetaboAnalyst	2	31368421	2125
4406	XCMS	1	31369628	6811
4407	XCMS online	1	31369628	6811
4408	HMDB	1	31370187	4940
4409	METLIN	1	31370187	5006
4410	MetaboAnalyst	1	31370187	11287
4411	Newton	2	31371786	17840
4412	MetaboAnalyst	3	31371967	2561
4413	HMDB	1	31371967	2875
4414	LDA	4	31372515	13988
4415	BioCyc	2	31372515	17573
4416	icoshift	1	31373318	12013
4417	XCMS	1	31374906	1259
4418	METLIN	1	31374906	1373
4419	MetaboAnalyst	2	31374906	1770
4420	SMART	1	31375673	6847
4421	MetaboAnalyst	1	31375818	10400
4422	MetaboAnalyst	1	31375927	6298
4423	icoshift	1	31379582	6197
4424	HMDB	1	31379582	6630
4425	MetaboAnalyst	2	31379582	9077
4426	MIDAS	2	31379726	3279
4427	Pathview	1	31380274	2609
4428	MetaboAnalyst	3	31380290	2973
4429	Mummichog	2	31380290	4779
4430	HMDB	1	31380290	5144
4431	HMDB	2	31382415	6268
4432	LIMSA	1	31382484	4292
4433	MAVEN	1	31382484	4877
4434	LDA	2	31383855	8172
4435	MetaboLights	1	31383865	2258
4436	MetaboAnalyst	1	31388077	7738
4437	HMDB	1	31391093	6738
4438	MZmine 2	1	31395834	1661
4439	MetaboAnalyst	1	31395965	6495
4440	MetaboAnalyst	2	31396262	5061
4441	mixOmics	1	31396262	5132
4442	HMDB	1	31396262	6300
4443	GAM	7	31398918	2506
4444	MetaboLights	1	31398922	5420
4445	HMDB	1	31398922	7170
4446	FingerID	1	31398922	7258
4447	METLIN	1	31398922	7323
4448	ChemSpider	1	31399602	14004
4449	GAM	4	31402862	1383
4450	GNPS	2	31405168	2684
4451	METLIN	1	31408959	9067
4452	MetScape	1	31409856	4335
4453	ChemSpider	1	31416118	8948
4454	XCMS	1	31417510	8331
4455	mixOmics	1	31417510	9242
4456	UniProtKB	4	31417524	6267
4457	COVAIN	1	31417580	15585
4458	MZmine 2	1	31426312	14118
4459	GAM	1	31426490	5725
4460	PathVisio	1	31426490	7084
4461	GNPS	2	31426532	7187
4462	XCMS	2	31426820	11243
4463	METLIN	1	31426820	12497
4464	AMDIS	1	31427618	3453
4465	CAMERA	1	31428077	10422
4466	ChemSpider	1	31428077	14647
4467	MetaboAnalystR	1	31428077	15358
4468	FingerID	1	31428077	16095
4469	ChemSpider	1	31428172	3150
4470	MetaboAnalyst	1	31428172	3719
4471	XCMS	1	31431550	6733
4472	METLIN	1	31431550	7286
4473	HMDB	1	31431550	7332
4474	MetaboAnalyst	1	31431652	7851
4475	METLIN	2	31434286	7501
4476	MetaboAnalyst	4	31434286	7689
4477	WiPP	13	31438611	14
4478	eRah	1	31438611	9624
4479	MetAlign	2	31443304	6514
4480	XCMS	1	31443304	6542
4481	AMDIS	1	31443304	6573
4482	LDA	1	31443304	7781
4483	ChemSpider	1	31443304	8783
4484	LDA	1	31443482	4400
4485	BinBase	1	31444411	9573
4486	MetaboAnalyst	1	31444528	8577
4487	MetaboAnalyst	3	31445519	9039
4488	COLMAR	1	31447694	3912
4489	MetaboAnalyst	1	31447694	4295
4490	SMART	3	31451693	3378
4491	WikiPathways	1	31451693	10778
4492	XCMS	1	31452461	2882
4493	MetaboAnalyst	1	31455267	5851
4494	MetaboAnalyst	1	31455396	4080
4495	LDA	1	31456084	6186
4496	XCMS	1	31456800	6785
4497	UniProtKB	1	31457004	3768
4498	METLIN	1	31460043	6795
4499	GNPS	3	31461939	5128
4500	Newton	1	31462649	8605
4501	UniProtKB	7	31462649	24071
4502	RankProd	1	31464373	11903
4503	GNPS	2	31466223	4796
4504	MetaboAnalyst	1	31466223	5069
4505	XCMS	1	31466282	5631
4506	HMDB	2	31466312	7923
4507	METLIN	1	31466312	7950
4508	MetaboAnalyst	2	31466312	8010
4509	icoshift	1	31466413	4779
4510	GAM	1	31467584	9779
4511	MetaboAnalyst	2	31467589	7802
4512	HMDB	1	31467589	8356
4513	MetaboAnalyst	1	31470570	4958
4514	MetaboAnalyst	1	31470601	11136
4515	XCMS	1	31475023	8011
4516	MetaboAnalyst	1	31475603	5202
4517	XCMS	3	31480539	4582
4518	CAMERA	2	31480539	4607
4519	MetaboAnalyst	1	31480764	5719
4520	Escher	1	31485322	3544
4521	XCMS	1	31487859	3615
4522	XCMS online	1	31487859	3615
4523	MetaboAnalyst	1	31487859	4019
4524	XCMS	2	31488860	9766
4525	CAMERA	1	31488860	12181
4526	MetaboAnalyst	1	31488947	2059
4527	MetaboLights	2	31490922	8077
4528	XCMS	1	31491957	1722
4529	ropls	2	31491957	2655
4530	MetaboAnalyst	1	31492111	5969
4531	SMART	1	31492848	19127
4532	MetaboAnalyst	1	31492908	1792
4533	XCMS	1	31493240	3341
4534	MetaboAnalyst	1	31493765	2208
4535	MetaboAnalyst	1	31495890	9391
4536	MZmine 2	1	31496682	2946
4537	MetaboAnalyst	1	31496775	4258
4538	PARADISe	1	31497392	8787
4539	AMDIS	1	31497392	9033
4540	MoNA	1	31497392	9361
4541	MZmine 2	1	31500092	5513
4542	HMDB	2	31500101	6125
4543	MetaboAnalyst	2	31501473	21822
4544	AMDIS	2	31504781	9937
4545	HMDB	1	31506092	5611
4546	MetaboAnalyst	1	31506092	6324
4547	GNPS	1	31511527	7065
4548	HMDB	1	31511561	10184
4549	MetaboAnalyst	1	31511561	10638
4550	XCMS	1	31511592	4315
4551	XCMS online	1	31511592	4315
4552	AMDIS	1	31511592	4385
4553	MetaboAnalyst	1	31511592	5813
4554	UniProtKB	1	31513012	28
4555	GAM	2	31521198	1018
4556	XCMS	2	31523502	4131
4557	HMDB	1	31523502	5489
4558	METLIN	1	31523502	5498
4559	BioCyc	1	31523510	13432
4560	MetAlign	1	31527409	2714
4561	HMDB	1	31527454	5329
4562	METLIN	1	31527454	5357
4563	MetaboAnalyst	1	31527462	6333
4564	MIDAS	1	31527595	8912
4565	MetaboAnalyst	1	31528339	12230
4566	HMDB	1	31529072	5856
4567	MZmine 2	1	31530825	11549
4568	MetaboAnalyst	1	31533250	6442
4569	AMDIS	1	31533283	2237
4570	MetaboAnalyst	1	31533338	5670
4571	GNPS	5	31533358	3309
4572	HMDB	1	31533629	6851
4573	MetaboAnalyst	2	31533629	6926
4574	MetaboAnalyst	1	31535082	27068
4575	METLIN	1	31538263	7859
4576	MetaboAnalyst	1	31538263	8583
4577	XCMS	1	31539079	3847
4578	METLIN	1	31539079	9064
4579	MetaboAnalyst	1	31540015	4175
4580	Skyline	2	31540069	5610
4581	MetAlign	1	31540263	4878
4582	MZmine 2	1	31540326	7191
4583	ORCA	1	31541094	10318
4584	METLIN	1	31541139	1805
4585	HMDB	1	31541307	4840
4586	HMDB	1	31543914	4618
4587	XCMS	1	31544064	11309
4588	CAMERA	1	31544064	11813
4589	HMDB	1	31544064	13070
4590	METLIN	1	31544064	13077
4591	MetaboAnalyst	1	31544064	13670
4592	MetaboAnalyst	3	31544106	5644
4593	HMDB	1	31544106	6146
4594	HMDB	1	31545404	3847
4595	MetaboAnalyst	3	31545404	3966
4596	SMPDB	1	31545404	4224
4597	iPath	1	31545404	4406
4598	MZmine 2	1	31545840	7909
4599	MetaboAnalyst	1	31545840	8890
4600	XCMS	4	31546636	3948
4601	ChemSpider	1	31546743	4408
4602	METLIN	1	31546743	4655
4603	HMDB	1	31547329	2088
4604	METLIN	1	31547329	2107
4605	ropls	2	31547329	2778
4606	MetaboAnalyst	1	31547524	9330
4607	PathVisio	2	31547524	9415
4608	XCMS	1	31547563	2104
4609	MetaboAnalyst	2	31547563	2365
4610	HMDB	1	31547563	4037
4611	AMDIS	2	31548366	6548
4612	XCMS	1	31548423	3303
4613	MetaboAnalyst	1	31548423	4110
4614	AMDIS	3	31548567	7774
4615	XCMS	3	31548567	8686
4616	mixOmics	1	31548567	11971
4617	El-MAVEN	2	31548575	6204
4618	MAVEN	2	31548575	6207
4619	GNPS	3	31551401	8736
4620	MetaboAnalyst	1	31551458	5346
4621	MetaboAnalyst	1	31551786	6511
4622	METLIN	1	31551789	3518
4623	HMDB	1	31551789	3565
4624	MetaboAnalyst	1	31551789	3818
4625	HMDB	2	31552266	4659
4626	MetaboAnalyst	7	31552266	4741
4627	GNPS	4	31552366	3032
4628	MetaboliteDetector	5	31554296	6527
4629	HMDB	1	31554818	13905
4630	LIPID MAPS	2	31554842	5268
4631	XCMS	1	31554869	2339
4632	CAMERA	2	31554869	2421
4633	HMDB	1	31554869	2967
4634	missForest	1	31554919	7890
4635	MetaboAnalyst	1	31555234	6451
4636	MetaboAnalyst	2	31555238	9508
4637	Pathos	1	31555238	9696
4638	GNPS	3	31555245	8112
4639	MoNA	1	31557052	5301
4640	MSClust	1	31557301	6831
4641	MetaboAnalyst	1	31557301	7318
4642	HMDB	1	31557978	6604
4643	UniProtKB	1	31557978	10223
4644	HMDB	1	31569403	3537
4645	SpectConnect	1	31569489	2554
4646	MetaboAnalyst	2	31569489	3090
4647	HMDB	1	31569489	3547
4648	SMPDB	2	31569489	3996
4649	Bayesil	1	31569638	4898
4650	MetaboAnalyst	3	31569638	5622
4651	MetaboAnalyst	1	31569727	5352
4652	Bayesil	1	31569727	6165
4653	MetaboLights	1	31569727	7250
4654	METLIN	1	31569792	5969
4655	XCMS	1	31569805	2146
4656	HMDB	1	31569818	4373
4657	LDA	1	31570867	22415
4658	ropls	1	31570867	40559
4659	MetaboAnalyst	2	31572179	2987
4660	YMDB	1	31572189	6512
4661	HMDB	1	31572189	6521
4662	MetaboAnalyst	2	31572189	6844
4663	HMDB	1	31572201	7163
4664	METLIN	1	31572201	7225
4665	HMDB	1	31572329	3065
4666	XCMS	1	31572687	4447
4667	mzMatch	1	31572687	4535
4668	XCMS	2	31572816	3296
4669	XCMS online	1	31572816	3296
4670	MetaboAnalyst	1	31572816	5069
4671	XCMS	2	31575902	8169
4672	HMDB	1	31575902	8697
4673	METLIN	1	31575902	8706
4674	MetaboLights	1	31575902	8974
4675	mixOmics	2	31575902	10287
4676	MetaboAnalyst	1	31575902	10359
4677	MetaboLights	1	31578326	3706
4678	Pathview	1	31578346	10433
4679	HMDB	1	31578452	7113
4680	LIPID MAPS	1	31578452	7136
4681	GNPS	4	31579568	4814
4682	HMDB	1	31582732	8838
4683	MetaboAnalyst	1	31582732	8892
4684	METLIN	1	31583028	3387
4685	LDA	1	31583109	8330
4686	Newton	1	31584976	12268
4687	LIPID MAPS	3	31587110	4926
4688	IPO	1	31588123	1563
4689	MZmine 2	1	31588123	6513
4690	MetaboAnalyst	3	31588123	7538
4691	MetaboAnalyst	1	31588173	7962
4692	MZmine 2	1	31590271	4113
4693	XCMS	2	31590271	4930
4694	METLIN	2	31590271	4961
4695	MetaboAnalyst	1	31590307	7350
4696	apLCMS	1	31592078	4173
4697	xMSanalyzer	1	31592078	4185
4698	xMSannotator	4	31592078	4606
4699	MetaboAnalyst	1	31592223	3814
4700	MetaboAnalyst	1	31594244	7101
4701	LDA	1	31595156	3484
4702	Skyline	1	31597247	7219
4703	MetaboAnalyst	2	31597247	7630
4704	MetaboAnalyst	2	31597357	8614
4705	HMDB	2	31600991	6039
4706	XCMS	1	31600991	8546
4707	CFM-ID	1	31600991	10863
4708	SMART	1	31601271	5174
4709	AMDIS	1	31602497	16618
4710	GNPS	1	31605112	1206
4711	HMDB	1	31605112	4094
4712	SMART	1	31606043	5030
4713	MetaboAnalyst	1	31607752	6706
4714	XCMS	2	31607915	8052
4715	METLIN	1	31607915	8708
4716	MetaboAnalyst	1	31607915	9203
4717	MetaboAnalyst	2	31608024	14361
4718	HMDB	1	31608024	15637
4719	LDA	1	31608024	17481
4720	UniProtKB	1	31608025	4072
4721	GNPS	1	31608025	10379
4722	MetFrag	1	31608025	10864
4723	ChemSpider	1	31608041	8467
4724	LIPID MAPS	1	31608088	3858
4725	mixOmics	1	31610847	5526
4726	MAVEN	1	31611868	12558
4727	ropls	1	31612079	4966
4728	MZmine 2	1	31614908	5617
4729	MRMPROBS	1	31614916	4057
4730	LDA	1	31615012	3281
4731	XCMS	1	31615012	4556
4732	XCMS	1	31615066	5494
4733	HMDB	1	31615066	6224
4734	SMPDB	1	31615066	6564
4735	MetaboAnalyst	1	31615066	6576
4736	LDA	1	31615518	2761
4737	MetaboAnalyst	1	31615518	3180
4738	Rhea	1	31617753	606
4739	Pathview	1	31618222	6277
4740	MZmine 2	1	31618919	4629
4741	HMDB	2	31618919	8806
4742	XCMS	1	31619276	10205
4743	METLIN	1	31619276	10643
4744	MetFrag	2	31619276	10813
4745	SMART	1	31619761	5273
4746	PARADISe	2	31619962	5199
4747	MetaboSearch	1	31622418	3580
4748	HMDB	1	31622418	3799
4749	MetaboAnalyst	4	31622418	6038
4750	Escher	1	31622418	8548
4751	MetaboAnalyst	1	31623107	4394
4752	MetAlign	1	31623116	5675
4753	BioCyc	1	31623116	6910
4754	HMDB	1	31623116	6995
4755	AMDIS	1	31623561	5352
4756	AMDIS	2	31624370	4528
4757	XCMS	3	31624370	4709
4758	PAPi	1	31624370	6197
4759	LipidXplorer	1	31626640	5818
4760	Skyline	1	31627392	1610
4761	MAVEN	2	31628327	4535
4762	SMART	1	31632371	1958
4763	HMDB	1	31634382	2446
4764	XCMS	1	31635316	6384
4765	MetaboAnalyst	1	31635316	7415
4766	Newton	1	31636291	10764
4767	LDA	8	31636291	13663
4768	MetaboAnalyst	1	31636291	15207
4769	HMDB	1	31636695	5263
4770	AMDIS	2	31636709	2741
4771	SpectConnect	1	31636709	13740
4772	XCMS	1	31637422	13948
4773	mixOmics	1	31637422	15189
4774	HMDB	3	31638165	6195
4775	MetaboAnalyst	2	31638165	6499
4776	MetScape	1	31638165	9430
4777	ChemSpider	1	31638916	5455
4778	NOREVA	1	31645572	3364
4779	XCMS	1	31645949	4983
4780	HMDB	1	31645949	5701
4781	METLIN	1	31645949	5710
4782	MetaboAnalyst	2	31648597	5611
4783	MetaboAnalyst	2	31651077	4767
4784	GNPS	1	31652707	2336
4785	MetaboLights	1	31652940	3193
4786	MetaboAnalyst	3	31652958	10236
4787	XCMS	2	31653057	2051
4788	CAMERA	2	31653057	2158
4789	XCMS	2	31653085	905
4790	MetaboAnalyst	1	31653085	1619
4791	MetaboAnalyst	1	31653965	4728
4792	HMDB	1	31653965	5533
4793	METLIN	1	31653965	5585
4794	XCMS	1	31655554	12952
4795	XCMS online	1	31655554	12952
4796	MetaboAnalyst	1	31656110	3611
4797	HMDB	1	31659215	21641
4798	ChemSpider	2	31659512	8519
4799	HMDB	1	31659512	8748
4800	LDA	1	31660061	7488
4801	MetaboAnalyst	2	31661772	3339
4802	MetaboAnalyst	2	31661783	11460
4803	Escher	1	31662430	974
4804	HMDB	2	31662455	9213
4805	BioCyc	1	31662455	9928
4806	SMPDB	1	31662455	9976
4807	MetaboLights	1	31662455	12802
4808	GNPS	2	31662458	18255
4809	METLIN	1	31662458	19469
4810	MetaboAnalyst	3	31665641	9486
4811	MetaboAnalyst	1	31666259	4376
4812	MetaboLights	1	31666506	5727
4813	MetaboAnalyst	1	31666506	6372
4814	SMART	1	31666570	355
4815	HMDB	1	31666611	8665
4816	Mummichog	2	31666611	9945
4817	HMDB	1	31666664	4228
4818	MetaboAnalyst	1	31666664	6893
4819	XCMS	1	31670380	2626
4820	GNPS	2	31671549	5212
4821	SistematX	1	31671588	393
4822	XCMS	1	31671768	5216
4823	CAMERA	1	31671768	5481
4824	MetaboAnalyst	2	31671805	4283
4825	MetaboAnalystR	1	31671805	4425
4826	CAMERA	1	31671836	8324
4827	mzMatch	1	31671836	8332
4828	MetaboAnalyst	1	31671836	8574
4829	METLIN	1	31671836	9328
4830	mQTL	2	31672980	36311
4831	MetaboAnalyst	1	31673279	6048
4832	LDA	1	31680997	3337
4833	MetaboLights	1	31680997	4580
4834	MetaboAnalyst	1	31680997	5381
4835	MZmine 2	1	31681226	10046
4836	MetaboAnalyst	1	31681226	11127
4837	HMDB	1	31681602	3447
4838	MetaboAnalyst	1	31683565	4095
4839	HMDB	1	31683565	4396
4840	COLMAR	1	31683565	4466
4841	XCMS	1	31683654	2750
4842	MetaboAnalyst	1	31683654	4345
4843	HMDB	1	31683654	4516
4844	MetaboAnalyst	1	31683679	7317
4845	MetaboAnalyst	3	31683902	4688
4846	ChemSpider	3	31683910	148
4847	MetaboAnalyst	1	31683916	6000
4848	XCMS	1	31683926	7804
4849	MetaboAnalyst	1	31683926	7813
4850	CAMERA	7	31684881	14201
4851	XCMS	1	31689907	4101
4852	Skyline	2	31690314	7838
4853	XCMS	2	31690722	4334
4854	METLIN	1	31690722	5955
4855	MetaboAnalyst	1	31690722	7917
4856	Mummichog	1	31690722	8625
4857	MetaboAnalyst	1	31690790	4263
4858	MZmine 2	1	31693694	2198
4859	XCMS	1	31694277	12360
4860	METLIN	1	31694277	13245
4861	mQTL	1	31694867	2482
4862	HMDB	1	31695617	7040
4863	METLIN	1	31695617	7047
4864	MetaboAnalyst	1	31699790	1213
4865	XCMS	1	31700086	7376
4866	HMDB	1	31700086	8952
4867	METLIN	1	31700086	8961
4868	AMDIS	1	31700104	2501
4869	XCMS	1	31703286	6613
4870	METLIN	1	31703286	7571
4871	MetaboAnalyst	1	31703286	7807
4872	HMDB	1	31703396	3781
4873	XCMS	2	31704924	5584
4874	CAMERA	1	31704924	5758
4875	MetaboAnalyst	1	31704956	5535
4876	MetaboAnalyst	1	31706326	3231
4877	GNPS	6	31708947	3777
4878	CFM-ID	1	31708947	7584
4879	MINEs	1	31708947	9025
4880	MS-FINDER	1	31708947	9192
4881	FingerID	1	31708947	9816
4882	UniProtKB	1	31708949	8093
4883	MetExtract	5	31708958	7918
4884	ChemSpider	2	31708958	11705
4885	XCMS	1	31711412	6025
4886	CAMERA	1	31711412	6060
4887	HMDB	1	31717456	3424
4888	MetaboAnalyst	1	31717456	3536
4889	MS-FINDER	4	31717785	2481
4890	MetaboLights	1	31717785	3493
4891	MoNA	1	31717785	3612
4892	MetaboAnalyst	2	31717805	5696
4893	IsoMS	1	31718082	2884
4894	MyCompoundID	1	31718082	4026
4895	MetaboAnalyst	1	31718082	4771
4896	VANTED	2	31719648	4997
4897	HMDB	1	31722069	6776
4898	HMDB	1	31722195	19453
4899	VMH	5	31722195	19596
4900	HMDB	1	31723166	4675
4901	MetaboAnalyst	1	31723166	10667
4902	HMDB	1	31726468	8089
4903	MZmine 2	1	31726761	7868
4904	MetaboAnalyst	1	31726761	9939
4905	HMDB	1	31726782	6048
4906	METLIN	1	31726782	6063
4907	SMART	1	31726984	1946
4908	mQTL	9	31728196	582
4909	XCMS	4	31728196	2710
4910	METLIN	1	31730022	4692
4911	HMDB	1	31730022	4727
4912	AMDIS	1	31731430	5777
4913	HMDB	1	31731809	4120
4914	LIPID MAPS	1	31732502	6600
4915	LipidXplorer	1	31732502	7449
4916	MetaboAnalyst	2	31732502	9065
4917	MetaboAnalyst	1	31736886	7542
4918	MetExtract	2	31736983	4962
4919	MetMatch	1	31736983	6133
4920	AMDIS	1	31738431	10335
4921	CROP	1	31739562	790
4922	UniProtKB	1	31739562	2019
4923	MetaboAnalyst	1	31739579	6963
4924	LIPID MAPS	1	31739579	7191
4925	LDA	2	31740441	6709
4926	UniProtKB	1	31740929	16245
4927	ChemSpider	1	31744149	8293
4928	MetaboAnalyst	1	31744152	2931
4929	LDA	1	31744152	6999
4930	PathoLogic	1	31744163	2826
4931	HMDB	1	31744229	5661
4932	BioCyc	1	31748517	8598
4933	LIPID MAPS	1	31748628	2035
4934	HMDB	1	31751406	2194
4935	MetFrag	3	31752824	10177
4936	ChemSpider	1	31752824	10359
4937	MetaboAnalyst	1	31754546	7976
4938	Newton	1	31762727	4655
4939	HMDB	1	31766161	5006
4940	LDA	4	31766198	90
4941	MetaboAnalyst	1	31767918	3870
4942	MetaboAnalyst	1	31768073	10335
4943	MetaboAnalyst	1	31769789	5547
4944	LDA	2	31769789	10462
4945	XCMS	1	31771277	2984
4946	HMDB	1	31771277	3121
4947	METLIN	1	31771277	3127
4948	MAIT	6	31772015	3357
4949	HMDB	1	31772240	6170
4950	ChemSpider	1	31772240	6176
4951	METLIN	1	31773355	8546
4952	MetaboAnalyst	2	31775252	7814
4953	SMART	2	31775391	2083
4954	metaMS	1	31775619	5601
4955	AMDIS	1	31775619	5914
4956	MetaboAnalyst	1	31775619	7843
4957	MetaboAnalyst	2	31779102	4598
4958	HMDB	1	31779119	6624
4959	MetaboAnalyst	1	31779252	7178
4960	MoNA	1	31779252	8492
4961	METLIN	1	31779252	8534
4962	LIPID MAPS	1	31779252	8584
4963	MetaboAnalyst	1	31780628	1080
4964	GNPS	3	31780665	3205
4965	MetaboLights	1	31780665	3910
4966	XCMS	2	31780788	5639
4967	HMDB	1	31780788	6991
4968	MetaboAnalyst	2	31780941	2588
4969	HMDB	1	31780949	4499
4970	MetaboAnalyst	1	31781063	8264
4971	HMDB	3	31781258	8937
4972	MetaboAnalyst	1	31781258	10026
4973	XCMS	2	31781354	7805
4974	CAMERA	1	31781354	7983
4975	MetaboAnalyst	1	31781354	16461
4976	HMDB	1	31781588	2045
4977	METLIN	1	31781588	2125
4978	AMDIS	1	31781604	3784
4979	XCMS	1	31781604	4190
4980	HMDB	1	31783473	3089
4981	METLIN	1	31783473	3117
4982	LIPID MAPS	1	31783473	3157
4983	MetaboAnalyst	1	31783598	10422
4984	HMDB	1	31783598	10604
4985	NOREVA	1	31783638	5099
4986	XCMS	1	31783790	3017
4987	HMDB	1	31783790	3465
4988	apLCMS	1	31784886	5495
4989	xMSanalyzer	1	31784886	5524
4990	CAMERA	2	31787747	6933
4991	MSClust	1	31790413	6125
4992	MetaboAnalyst	1	31790488	3967
4993	XCMS	1	31791229	9477
4994	AMDIS	1	31791229	9629
4995	ChemSpider	2	31792390	7204
4996	XCMS	1	31794270	2367
4997	METLIN	1	31794270	3272
4998	XCMS	1	31794572	2761
4999	NOREVA	1	31794572	3088
5000	HMDB	1	31794572	3731
5001	GNPS	3	31795148	6154
5002	MetAlign	2	31795288	7256
5003	HMDB	1	31795288	9642
5004	MetaboAnalyst	1	31795367	10362
5005	Newton	1	31795447	1207
5006	TargetSearch	2	31797944	2851
5007	LIPID MAPS	1	31797944	15405
5008	GSimp	1	31798278	5120
5009	MetaboAnalyst	2	31801490	8050
5010	HMDB	1	31803069	5302
5011	MetaboAnalyst	1	31803070	5598
5012	PathVisio	1	31804581	6957
5013	XCMS	2	31805041	8015
5014	ORCA	1	31805041	9515
5015	LDA	1	31805086	3891
5016	LDA	3	31810197	7156
5017	MAVEN	1	31813848	5540
5018	XCMS	1	31814987	2950
5019	METLIN	1	31814987	3831
5020	HMDB	1	31815960	4405
5021	MetaboAnalyst	2	31815960	4887
5022	Newton	1	31816972	7721
5023	XCMS	2	31817081	5861
5024	METLIN	1	31817081	7170
5025	MetaboAnalyst	1	31817081	7675
5026	SMART	1	31817263	1696
5027	GNPS	2	31819142	5370
5028	LIPID MAPS	1	31819450	4920
5029	MetaboAnalyst	1	31819450	5021
5030	MetaboAnalyst	1	31822292	3471
5031	SMART	1	31822493	3188
5032	HMDB	1	31822493	5386
5033	MAVEN	1	31822583	12507
5034	METLIN	1	31822583	12752
5035	XCMS	2	31822601	6106
5036	CAMERA	2	31822601	6830
5037	AMDIS	1	31822601	8298
5038	MetaboLights	1	31822601	15756
5039	MetaboAnalyst	1	31822686	8396
5040	mixOmics	1	31822686	9054
5041	MetaboAnalyst	1	31822799	5784
5042	UniProtKB	1	31823730	6350
5043	MetaboAnalyst	1	31824447	7678
5044	MetaboAnalyst	1	31824541	3075
5045	SMART	1	31824757	1427
5046	LDA	7	31827172	8342
5047	AMDIS	1	31827172	8756
5048	XCMS	1	31827206	5019
5049	MetaboAnalyst	1	31827206	5623
5050	AMDIS	2	31827413	2171
5051	ChemSpider	1	31827788	3591
5052	HMDB	1	31828159	4152
5053	AMDIS	1	31829143	8885
5054	MetaboAnalyst	4	31829143	11216
5055	VANTED	1	31829143	14317
5056	MetaboAnalyst	1	31835615	3658
5057	XCMS	1	31836628	3414
5058	MetaboAnalyst	4	31836628	4403
5059	MetaboLights	1	31836628	7943
5060	XCMS	1	31836784	4752
5061	METLIN	1	31836784	6768
5062	HMDB	1	31836784	6807
5063	MetaboAnalyst	1	31836784	7147
5064	MZmine 2	1	31836843	7358
5065	MetaboAnalyst	2	31836843	7889
5066	MetaboAnalystR	1	31836843	8067
5067	METLIN	1	31836843	8798
5068	Skyline	2	31841108	11634
5069	MetaboAnalyst	2	31847385	3146
5070	MetaboAnalyst	1	31847430	12391
5071	MAVEN	3	31848455	4804
5072	BinBase	1	31849495	5271
5073	TargetSearch	1	31850025	11573
5074	MetaboAnalyst	1	31850025	12080
5075	SMART	1	31850332	2788
5076	METLIN	1	31852435	16453
5077	LDA	4	31852859	301
5078	XCMS	1	31852859	4007
5079	MetaboAnalyst	3	31852859	4744
5080	MetaboAnalyst	1	31853226	3498
5081	HMDB	1	31857639	4390
5082	MetaboAnalyst	2	31861317	3992
5083	MetaboAnalyst	2	31861341	4416
5084	MassTRIX	1	31861717	3375
5085	SMART	1	31861760	2086
5086	MSClust	1	31861822	2971
5087	MetaboAnalyst	2	31863066	6158
5088	XCMS	1	31867339	5478
5089	METLIN	1	31867339	7096
5090	HMDB	1	31867339	7143
5091	MetaboAnalyst	1	31867339	7601
5092	Skyline	1	31871051	7657
5093	El-MAVEN	1	31871051	12568
5094	MAVEN	1	31871051	12571
5095	MetaboAnalyst	1	31877133	2035
5096	SMPDB	1	31877133	3457
5097	VANTED	1	31877908	5226
5098	GNPS	3	31878034	7735
5099	MetaboAnalyst	1	31878351	8771
5100	MetScape	1	31878351	9027
5101	LDA	2	31881974	14538
5102	ChemSpider	1	31882610	7199
5103	LDA	1	31882929	1522
5104	LIQUID	3	31883096	1304
5105	AMDIS	1	31883096	23189
5106	MetaboAnalyst	1	31885782	12217
5107	GAM	2	31885873	8804
5108	MZmine 2	2	31887193	6949
5109	MetaboAnalyst	1	31888078	6588
5110	MetaboAnalystR	2	31888144	5510
5111	HMDB	1	31888194	5453
5112	MetaboAnalyst	1	31888194	6899
5113	MetaboliteDetector	1	31888773	19683
5114	Mummichog	2	31892368	2085
5115	apLCMS	1	31892368	4700
5116	xMSanalyzer	1	31892368	4729
5117	LDA	2	31892851	2362
5118	AMDIS	1	31896793	4628
5119	HMDB	1	31897184	1905
5120	LIPID MAPS	1	31897184	1963
5121	MetaboAnalyst	1	31897184	2583
5122	MetaboAnalystR	1	31905759	8309
5123	GNPS	3	31905978	687
5124	XCMS	1	31906303	3902
5125	UniProtKB	1	31906542	5512
5126	MetaboAnalyst	1	31906853	7166
5127	mixOmics	1	31906915	7078
5128	HMDB	1	31908738	5180
5129	MetaboAnalyst	1	31908738	5427
5130	HMDB	3	31911595	7993
5131	MetaboAnalystR	1	31914144	2219
5132	Skyline	2	31914155	6256
5133	MetaboAnalyst	1	31917799	1186
5134	HMDB	1	31919742	8260
5135	AMDIS	1	31920474	3653
5136	SMART	1	31920518	3184
5137	ChemSpider	1	31920639	7866
5138	HMDB	1	31920639	7938
5139	BioCyc	1	31921009	3257
5140	Escher	1	31921009	6466
5141	LDA	2	31921049	6489
5142	MetaboAnalyst	2	31921241	6119
5143	UniProtKB	2	31921252	11513
5144	MetaboAnalyst	2	31923191	4795
5145	ChemSpider	1	31923191	5818
5146	GNPS	4	31924833	4778
5147	XCMS	1	31924834	4107
5148	MetaboAnalyst	1	31932622	8796
5149	Skyline	1	31932721	17459
5150	MetaboAnalyst	1	31935843	7223
5151	XCMS	2	31936106	5745
5152	MetaboAnalyst	1	31936106	6102
5153	MetaboAnalyst	2	31936230	2936
5154	XCMS	2	31936230	3577
5155	IPO	1	31936230	3678
5156	RAMClustR	1	31936230	4755
5157	METLIN	1	31936230	5949
5158	MetFrag	1	31936230	6007
5159	HMDB	4	31937890	4804
5160	GNPS	5	31940767	9454
5161	MZmine 2	1	31940785	5798
5162	XCMS	3	31941030	6950
5163	CAMERA	1	31941030	7588
5164	HMDB	1	31941086	3876
5165	mixOmics	1	31941086	6201
5166	METLIN	1	31941114	3921
5167	HMDB	1	31941143	4235
5168	METLIN	1	31941143	4267
5169	MetaboAnalyst	2	31941951	3768
5170	METLIN	1	31941951	4054
5171	HMDB	1	31941951	4065
5172	MetScape	3	31941951	4142
5173	BATMAN	1	31941973	5023
5174	MetaboAnalyst	3	31941993	6952
5175	ORCA	1	31942031	7068
5176	METLIN	1	31944670	8208
5177	GNPS	2	31945070	44
5178	FingerID	4	31945070	2500
5179	MetaboAnalyst	1	31947545	1595
5180	GNPS	2	31947697	4338
5181	SMART	2	31947808	7922
5182	MetAlign	1	31952145	8723
5183	HMDB	1	31952145	9892
5184	AMDIS	1	31952342	10439
5185	MetFamily	2	31952342	10765
5186	HMDB	1	31952342	11157
5187	MetaboAnalyst	2	31952342	11685
5188	MassTRIX	2	31952343	4775
5189	HMDB	1	31952343	4994
5190	MetaboAnalyst	3	31952343	9160
5191	SMPDB	1	31952343	9391
5192	XCMS	1	31954373	4779
5193	MetaboAnalyst	1	31956411	6777
5194	SMART	1	31959105	6048
5195	MetaboAnalyst	1	31959821	13565
5196	MetaboAnalyst	1	31959849	3613
5197	MetaboAnalyst	1	31959868	7227
5198	MAIT	1	31959982	4193
5199	XCMS	2	31960114	4047
5200	CAMERA	3	31960114	4966
5201	METLIN	1	31960114	6976
5202	HMDB	1	31960908	6963
5203	MAVEN	1	31961855	17405
5204	GNPS	3	31963137	7374
5205	MetaboAnalyst	1	31963255	2969
5206	HMDB	1	31963255	3086
5207	Rhea	1	31963562	13148
5208	UniProtKB	2	31963736	2323
5209	MetaboAnalyst	1	31963766	1649
5210	XCMS	2	31963878	1546
5211	XCMS online	1	31963878	1546
5212	MetaboAnalyst	1	31964388	4273
5213	mixOmics	2	31964751	4987
5214	LDA	2	31964941	4240
5215	VANTED	1	31964942	5577
5216	MZmine 2	1	31965056	3318
5217	AMDIS	1	31965396	10372
5218	SMART	1	31968683	823
5219	IsoMS	1	31973046	5815
5220	MetaboAnalyst	3	31973046	6167
5221	MyCompoundID	1	31973046	8516
5222	MetaboAnalyst	1	31974423	2917
5223	GNPS	1	31974423	3149
5224	AMDIS	1	31974599	4909
5225	XCMS	1	31974599	5296
5226	MetaboLights	1	31976312	110
5227	IntLIM	1	31976312	1598
5228	MetaboAnalyst	1	31978163	4098
5229	GNPS	3	31979050	1622
5230	ChemSpider	1	31979422	11719
5231	IPO	1	31982958	1341
5232	MetaboAnalyst	1	31984424	10730
5233	MetaboAnalyst	1	31988297	6903
5234	LDA	4	31988370	10839
5235	MetaboAnalyst	2	31988748	10890
5236	HMDB	1	31988748	12017
5237	LDA	1	31988748	13610
5238	HMDB	2	31991659	6312
5239	METLIN	1	31991659	6340
5240	CFM-ID	1	31991659	7182
5241	LDA	1	31991725	6474
5242	MetaboAnalyst	1	31992312	5360
5243	MassTRIX	1	31992728	6755
5244	XCMS	1	31992792	5707
5245	MetaboAnalyst	1	31992792	7654
5246	BioCyc	1	31993376	5499
5247	DrugBank	3	31993376	7628
5248	MetaboAnalyst	1	31993417	9013
5249	MetScape	2	31993417	10072
5250	UniProtKB	1	31996138	7477
5251	HMDB	1	31996423	4556
5252	LDA	1	31996423	5384
5253	Skyline	1	31996678	12396
5254	MetaboAnalyst	1	31998279	3594
5255	MIDAS	2	32002761	4685
5256	HMDB	1	32005241	6900
5257	LIPID MAPS	1	32005798	15011
5258	HMDB	1	32005897	5593
5259	METLIN	1	32009955	5116
5260	HMDB	2	32009955	5187
5261	XCMS	1	32010801	3080
5262	MS-FINDER	1	32010801	3385
5263	HMDB	1	32012663	3296
5264	MetaboAnalyst	2	32012663	3361
5265	Workflow4Metabolomics	2	32012845	5700
5266	GNPS	2	32013082	12023
5267	PathVisio	3	32015415	7937
5268	WikiPathways	1	32015415	8054
5269	LDA	2	32015679	7303
5270	COVAIN	3	32023468	9400
5271	AMDIS	1	32023823	1789
5272	METLIN	2	32023823	4008
5273	HMDB	1	32023823	4340
5274	MZmine 2	1	32023992	4896
5275	MetaboAnalyst	1	32023992	5663
5276	Skyline	1	32024021	7857
5277	HMDB	2	32024143	5102
5278	GAM	1	32024885	8642
5279	WikiPathways	3	32025735	10360
5280	MetScape	1	32025735	11162
5281	BinBase	1	32029481	891
5282	METLIN	1	32029818	3972
5283	HMDB	1	32033223	8896
5284	MAVEN	1	32034133	13697
5285	HMDB	3	32034255	12718
5286	MetaboAnalyst	1	32034255	13651
5287	LDA	3	32038574	4560
5288	MetaboAnalyst	1	32038574	5710
5289	XCMS	1	32038581	3409
5290	MetaboAnalyst	1	32038581	4567
5291	METLIN	2	32039191	9137
5292	MetaboAnalyst	3	32041181	4382
5293	AMDIS	2	32041987	6875
5294	MET-IDEA	1	32041987	7211
5295	MetaboAnalyst	1	32041987	9388
5296	LIPID MAPS	1	32043185	4096
5297	ICT	2	32046123	312
5298	MetaboAnalyst	1	32046170	7844
5299	METLIN	1	32047062	6901
5300	LIPID MAPS	2	32047202	4635
5301	HMDB	1	32047520	3708
5302	LIPID MAPS	1	32047520	3736
5303	icoshift	1	32047921	3349
5304	HMDB	1	32049975	4980
5305	METLIN	1	32049975	5009
5306	MetaboAnalyst	1	32049975	6741
5307	UniProtKB	1	32050592	3607
5308	MZmine 2	1	32050703	2788
5309	XCMS	1	32051464	6498
5310	MetaboAnalyst	1	32051464	6663
5311	mixOmics	3	32051515	6580
5312	LDA	1	32051515	7034
5313	xMSanalyzer	1	32051515	8043
5314	apLCMS	1	32051515	8067
5315	BinBase	1	32053695	7583
5316	MetaboAnalyst	1	32053982	11026
5317	MetaboAnalyst	1	32058642	5409
5318	XCMS	1	32059529	5746
5319	AMDIS	1	32059529	6901
5320	mixOmics	1	32063116	5548
5321	MetScape	1	32063116	6820
5322	MetaboAnalyst	2	32066442	5152
5323	HMDB	1	32066442	5753
5324	HMDB	2	32070008	5976
5325	mixOmics	1	32071312	17549
5326	XCMS	1	32071608	4757
5327	XCMS online	1	32071608	4757
5328	HMDB	1	32071608	5606
5329	METLIN	1	32071608	5634
5330	ChemSpider	1	32071608	5675
5331	MetaboAnalyst	1	32071608	5778
5332	HMDB	1	32071609	6641
5333	MET-IDEA	1	32071791	3895
5334	HMDB	1	32074082	6204
5335	MetaboAnalyst	2	32074082	7001
5336	DrugBank	1	32074470	4060
5337	MetMask	1	32075002	5847
5338	Ideom	1	32075690	7697
5339	MetaboAnalyst	1	32075690	10581
5340	GNPS	4	32078624	10224
5341	Newton	1	32079258	1908
5342	METLIN	1	32079306	5735
5343	HMDB	1	32079306	5758
5344	FALCON	3	32080175	753
5345	MZmine 2	2	32081987	8419
5346	HMDB	1	32081987	8696
5347	CROP	1	32082288	14162
5348	GNPS	2	32082294	3588
5349	XCMS	1	32082482	5107
5350	HMDB	1	32082482	6225
5351	METLIN	1	32082482	6256
5352	MetaboAnalyst	1	32082482	6464
5353	LDA	3	32083022	10618
5354	LIPID MAPS	1	32084137	14593
5355	HMDB	1	32084137	14625
5356	MetaboAnalystR	1	32084137	14934
5357	XCMS	3	32084196	5076
5358	GNPS	4	32085602	4591
5359	METLIN	1	32085644	8372
5360	HMDB	1	32085644	8407
5361	METLIN	1	32087672	6888
5362	XCMS	1	32091406	4494
5363	HMDB	1	32092872	8276
5364	GNPS	3	32092934	6061
5365	MetaboAnalyst	1	32092934	8487
5366	BinBase	2	32092943	2814
5367	MetaboAnalyst	1	32092943	4063
5368	BinBase	1	32093009	4499
5369	MetaboAnalyst	1	32093075	10613
5370	ropls	1	32093149	3891
5371	HMDB	1	32093293	2157
5372	METLIN	1	32093293	2210
5373	Skyline	2	32093365	6676
5374	BinBase	2	32094453	8598
5375	MetaboAnalyst	1	32094453	12589
5376	LDA	2	32094453	14238
5377	LDA	2	32095237	6354
5378	IsoMS	2	32095237	6744
5379	MyCompoundID	1	32095237	7376
5380	MetaboAnalyst	2	32098243	4537
5381	GNPS	3	32098306	3522
5382	CFM-ID	1	32098306	5247
5383	MAVEN	1	32098820	10594
5384	AMDIS	1	32098988	6337
5385	MetaboAnalyst	3	32098988	7743
5386	XCMS	1	32099475	1464
5387	CAMERA	1	32099475	1604
5388	HMDB	1	32099562	3468
5389	HMDB	1	32102363	5994
5390	MetaboAnalyst	1	32102363	6092
5391	MetaboAnalyst	1	32103083	3095
5392	HMDB	1	32104193	6781
5393	METLIN	1	32104193	6868
5394	MetaboAnalyst	1	32104193	6979
5395	XCMS	1	32106420	9820
5396	RAMClustR	2	32106420	9904
5397	MS-FINDER	1	32106420	10144
5398	HMDB	1	32106420	10742
5399	AMDIS	1	32106420	11140
5400	LDA	1	32106586	4654
5401	MAVEN	1	32107279	3557
5402	MetaboAnalyst	2	32107279	3801
5403	MetaboAnalyst	1	32107447	4202
5404	MetaboAnalyst	1	32107855	4816
5405	LIPID MAPS	1	32108917	6129
5406	METLIN	1	32108917	6209
5407	MetaboAnalyst	1	32108917	7220
5408	MetaboAnalyst	1	32110985	4419
5409	iPath	1	32110985	4534
5410	XCMS	4	32111061	2654
5411	BioCyc	1	32111061	3652
5412	MS-LAMP	2	32111061	3785
5413	XCMS	2	32111089	5688
5414	XCMS	1	32111319	10571
5415	BinBase	2	32116206	6739
5416	MetaboAnalyst	2	32116206	8094
5417	LDA	1	32116693	9560
5418	ADAP-GC	1	32116775	2708
5419	MetaboAnalyst	1	32116775	3503
5420	MetaboAnalyst	4	32117186	4518
5421	XCMS	2	32117186	4559
5422	MetaboAnalyst	1	32117356	8526
5423	MetaboAnalyst	1	32117598	5030
5424	CMM	2	32117720	7303
5425	METLIN	1	32118038	3793
5426	Skyline	1	32118050	4886
5427	MetaboAnalyst	1	32118429	10027
5428	NMRProcFlow	1	32120848	8519
5429	HMDB	1	32120848	9226
5430	MetaboAnalyst	1	32120852	5656
5431	ChemSpider	1	32121166	10385
5432	MetaboAnalyst	1	32121379	5143
5433	GNPS	4	32121489	7135
5434	GNPS	3	32123201	12269
5435	FingerID	1	32123201	13380
5436	XCMS	1	32123298	10230
5437	MetaboAnalyst	1	32123298	10642
5438	MetScape	2	32128093	3074
5439	LipidXplorer	1	32130438	3749
5440	LDA	1	32130438	6441
5441	MAIT	5	32130899	825
5442	HMDB	1	32131411	4367
5443	MetaboHunter	1	32131411	4419
5444	XCMS	1	32133322	1651
5445	HMDB	1	32133322	2829
5446	METLIN	1	32133322	2856
5447	mzMatch	1	32133420	3242
5448	speaq	1	32134946	4961
5449	MWASTools	2	32134946	5084
5450	METLIN	1	32138587	6492
5451	MetaboAnalyst	1	32138587	6639
5452	UniProtKB	2	32140149	2885
5453	apLCMS	1	32140295	4356
5454	xMSanalyzer	1	32140295	4370
5455	Mummichog	3	32140295	6775
5456	UniProtKB	1	32141811	5536
5457	MetaboAnalyst	2	32142554	17452
5458	LDA	3	32143645	5237
5459	MetaboAnalyst	1	32143654	3851
5460	MetaboAnalyst	1	32144454	12304
5461	AMDIS	1	32146828	3887
5462	AMDIS	4	32147713	7138
5463	apLCMS	1	32148965	5146
5464	xMSanalyzer	1	32148965	5162
5465	xMSannotator	1	32148965	5718
5466	METLIN	2	32148965	6204
5467	HMDB	1	32148965	6378
5468	Mummichog	2	32148965	9284
5469	HMDB	1	32150567	1764
5470	METLIN	1	32150567	1791
5471	METLIN	1	32150984	4745
5472	MetaboAnalyst	1	32150984	5587
5473	XCMS	1	32151072	1961
5474	MetaboLights	1	32152087	8968
5475	IsoCor	1	32152301	11261
5476	icoshift	1	32153399	4992
5477	UC2	1	32153648	6301
5478	HMDB	1	32153648	6523
5479	LIPID MAPS	1	32153648	6552
5480	ChemSpider	1	32154184	3715
5481	BioCyc	1	32155669	10155
5482	XCMS	1	32155921	8248
5483	MetaboAnalyst	2	32155921	8605
5484	METLIN	1	32155921	9506
5485	MetaboAnalyst	1	32158203	2041
5486	MetaboAnalyst	1	32158766	13358
5487	ChemSpider	1	32158766	14171
5488	HMDB	1	32159764	2795
5489	METLIN	1	32159764	2804
5490	METLIN	2	32160915	4712
5491	HMDB	1	32160915	8034
5492	TargetSearch	1	32161322	3786
5493	MetaboAnalyst	8	32161522	2903
5494	SMPDB	1	32161522	6155
5495	mixOmics	1	32161752	14003
5496	VANTED	1	32161752	14310
5497	MetAlign	1	32163433	6225
5498	MetaboAnalyst	1	32163433	6235
5499	XCMS	1	32164285	2127
5500	XCMS online	1	32164285	2127
5501	LipidXplorer	1	32165635	10392
5502	XCMS	1	32165688	9276
5503	XCMS	3	32168803	7349
5504	MetaboAnalyst	2	32168894	2724
5505	MetaboLights	1	32170068	4989
5506	MetaboAnalyst	1	32170160	8164
5507	BinBase	1	32170472	3382
5508	MetaboAnalyst	3	32170472	3732
5509	LDA	2	32175081	8495
5510	HMDB	1	32175081	9787
5511	MAVEN	1	32178453	5096
5512	XCMS	1	32178453	6274
5513	MetaboLights	1	32178453	8649
5514	UniProtKB	3	32178739	6942
5515	XCMS	1	32179862	4403
5516	XCMS online	1	32179862	4403
5517	HMDB	1	32179862	5714
5518	MetaboAnalyst	1	32179862	6000
5519	BinBase	2	32182259	5727
5520	SMART	1	32182273	2355
5521	HMDB	1	32182912	5563
5522	METLIN	1	32182912	5650
5523	MetAlign	2	32183449	2625
5524	HMDB	1	32184236	11037
5525	METLIN	1	32184236	11044
5526	MetaboAnalyst	1	32184236	11348
5527	MetaboAnalyst	1	32184254	4173
5528	LDA	2	32184361	11375
5529	MAVEN	1	32184362	4946
5530	MetaboAnalyst	1	32184365	8982
5531	GNPS	5	32184365	9476
5532	MZmine 2	1	32184365	9490
5533	XCMS	2	32184446	6452
5534	CAMERA	1	32184446	6721
5535	XCMS	2	32186739	4079
5536	MetaboAnalyst	1	32186739	5327
5537	MetaboAnalyst	1	32186768	10329
5538	GNPS	2	32188118	10274
5539	XCMS	2	32188118	11148
5540	MZmine 2	1	32188118	13979
5541	MetaboAnalyst	2	32188755	6665
5542	HMDB	1	32188755	6988
5543	MetaboAnalyst	1	32190096	5477
5544	GNPS	3	32190683	7935
5545	MetaboAnalyst	1	32192187	5637
5546	LDA	2	32192197	11991
5547	ConsensusPathDB	2	32193374	19850
5548	AMDIS	1	32193438	5792
5549	Newton	1	32194374	5884
5550	MAGMa	1	32194427	6772
5551	MetaboAnalyst	2	32194428	8445
5552	HMDB	1	32194428	9936
5553	METLIN	1	32194428	10021
5554	LDA	1	32194870	6265
5555	HMDB	1	32197406	5385
5556	Workflow4Metabolomics	1	32197406	5864
5557	CAMERA	1	32197406	6150
5558	MetaboAnalyst	1	32197406	7204
5559	ChemSpider	1	32197420	6163
5560	HMDB	1	32198676	5910
5561	MetaboAnalyst	2	32204361	9361
5562	MetaboliteDetector	1	32208465	6365
5563	Skyline	1	32209718	7147
5564	HMDB	1	32209718	8511
5565	LIPID MAPS	1	32209718	8526
5566	MZmine 2	1	32210093	8528
5567	AMDIS	1	32210262	4073
5568	MetaboAnalyst	2	32210549	6824
5569	XCMS	2	32210815	6529
5570	XCMS online	1	32210815	6529
5571	METLIN	1	32210815	7392
5572	MetaboAnalyst	1	32210815	7848
5573	MetAlign	1	32211010	3818
5574	NMRProcFlow	1	32213896	2788
5575	MetaboAnalyst	2	32213896	3628
5576	MSClust	1	32213984	8174
5577	XCMS	1	32213984	10846
5578	XCMS	1	32214106	9600
5579	CAMERA	1	32214106	9814
5580	XCMS	1	32214144	7654
5581	NMRProcFlow	2	32214197	5542
5582	MetaboAnalyst	1	32214197	7937
5583	HMDB	1	32214212	4990
5584	CMM	1	32214395	2744
5585	HMDB	1	32214395	2854
5586	METLIN	1	32214395	2865
5587	LIPID MAPS	1	32214395	2893
5588	MetaboAnalyst	1	32214395	3216
5589	MetExplore	5	32215752	8351
5590	SMART	1	32221328	3749
5591	PathoLogic	1	32221328	4294
5592	BiGG Models	1	32221328	6219
5593	GNPS	3	32221330	8592
5594	SMART	1	32224874	445
5595	TargetSearch	1	32225015	5253
5596	HMDB	3	32225041	4014
5597	XCMS	3	32225041	5327
5598	CAMERA	4	32225041	5979
5599	METLIN	2	32225041	6569
5600	MetaboLights	1	32227958	5104
5601	MetaboAnalyst	1	32227958	14547
5602	METLIN	1	32228647	12364
5603	AMDIS	1	32231228	3811
5604	HMDB	3	32231385	5156
5605	SMPDB	1	32231385	5804
5606	MetaboAnalyst	3	32235493	2387
5607	MetaboAnalyst	2	32235609	5801
5608	MassTRIX	1	32235609	6379
5609	HMDB	1	32235609	6474
5610	MAVEN	1	32244435	5737
5611	MZmine 2	2	32244549	7556
5612	MetaboAnalyst	1	32244654	3930
5613	MetAlign	2	32244725	2602
5614	XCMS	1	32245176	8392
5615	MetaboAnalyst	3	32245176	10904
5616	HMDB	1	32245176	11549
5617	METLIN	1	32245176	11562
5618	Escher	2	32245365	263
5619	XCMS	1	32245415	7130
5620	HMDB	1	32245415	7600
5621	MetaboAnalyst	2	32249791	6771
5622	METLIN	1	32249791	7558
5623	MetaboAnalyst	1	32252461	9030
5624	SMART	1	32252624	1093
5625	MAVEN	5	32255806	2780
5626	MetaboAnalyst	1	32256072	3825
5627	HMDB	1	32256098	4260
5628	MetaboAnalyst	2	32256343	9638
5629	XCMS	2	32256469	9117
5630	XCMS online	1	32256469	9117
5631	SMART	1	32256471	4269
5632	IIS	1	32257241	2742
5633	XCMS	1	32260190	13346
5634	MetaboAnalyst	1	32260275	6987
5635	XCMS	1	32260407	5212
5636	CAMERA	1	32260407	5288
5637	metaMS	1	32260407	5392
5638	statTarget	1	32260407	5539
5639	MS-FINDER	2	32260407	8152
5640	CFM-ID	2	32260407	8848
5641	TargetSearch	1	32260421	7904
5642	LDA	1	32260563	5375
5643	MetaboAnalyst	1	32265199	12526
5644	HMDB	1	32265710	6676
5645	MetaboHunter	1	32265728	8451
5646	MetaboLights	1	32265728	9298
5647	MetaboAnalyst	1	32265728	9824
5648	XCMS	1	32266065	6270
5649	METLIN	1	32266065	6542
5650	MetaboAnalyst	1	32266065	7941
5651	HMDB	1	32266113	5155
5652	METLIN	1	32266113	5255
5653	ropls	1	32266113	6274
5654	IPO	2	32267860	6021
5655	XCMS	2	32267860	6034
5656	Workflow4Metabolomics	3	32267860	6264
5657	metaMS	1	32267860	6296
5658	CAMERA	1	32267860	6313
5659	MetaboAnalyst	1	32267860	6950
5660	Mummichog	1	32267860	7034
5661	HMDB	1	32267860	7494
5662	LIPID MAPS	1	32268618	4362
5663	MetaboAnalyst	1	32269156	3380
5664	MAVEN	1	32269157	5257
5665	XCMS	1	32269157	5272
5666	MetaboLights	1	32269256	34
5667	BioDendro	13	32269256	1077
5668	GNPS	1	32269256	3886
5669	LDA	1	32269525	4593
5670	LDA	1	32269559	5915
5671	XCMS	1	32269559	9375
5672	Workflow4Metabolomics	1	32269559	9386
5673	eRah	1	32269559	9597
5674	GNPS	1	32269559	11596
5675	MetaboAnalyst	1	32269559	13531
5676	HMDB	1	32269617	3104
5677	HMDB	1	32271564	6682
5678	MetaboAnalyst	1	32271564	7545
5679	MetaboLights	1	32271564	7949
5680	LDA	1	32272885	6583
5681	XCMS	3	32272885	7467
5682	AMDIS	2	32273833	4028
5683	METLIN	2	32276345	5526
5684	HMDB	2	32276345	5610
5685	MetaboAnalyst	1	32276345	6024
5686	mixOmics	1	32276396	4994
5687	MetExtract	4	32280362	17542
5688	XCMS	1	32280707	2523
5689	MetaboAnalyst	1	32283598	5194
5690	MetaboAnalyst	3	32283755	6310
5691	MAVEN	1	32284564	7119
5692	MetaboAnalyst	1	32285223	9192
5693	XCMS	2	32286374	5333
5694	HMDB	1	32286374	5770
5695	HMDB	2	32286457	6798
5696	MetaboMiner	1	32286457	7230
5697	MetaboAnalyst	4	32286457	8305
5698	HMDB	1	32289751	4002
5699	METLIN	2	32289751	4068
5700	XCMS	1	32290188	7358
5701	IPO	1	32290188	7407
5702	MetaboAnalyst	1	32290528	4636
5703	MetaboAnalyst	3	32290837	2480
5704	HMDB	3	32290837	2576
5705	MetScape	1	32292347	3727
5706	LipidXplorer	2	32292781	12831
5707	HMDB	1	32293402	8123
5708	XCMS	1	32294136	9796
5709	HMDB	1	32294136	9907
5710	METLIN	1	32294136	9936
5711	SMART	1	32294438	17679
5712	Escher	1	32294438	22693
5713	IsoCor	2	32295054	6191
5714	HMDB	1	32295282	4712
5715	MetaboAnalyst	1	32295282	5612
5716	MZmine 2	1	32295884	3050
5717	MetaboLights	1	32295884	4281
5718	MetaboAnalyst	1	32296541	3389
5719	METLIN	1	32299366	2452
5720	HMDB	1	32299366	2491
5721	MetaboAnalyst	1	32299366	4363
5722	MetaboAnalyst	1	32299433	3639
5723	mixOmics	1	32300166	13024
5724	MetaboAnalyst	1	32300172	5617
5725	MetaboAnalyst	4	32300196	7510
5726	METLIN	1	32300308	7208
5727	HMDB	1	32300308	7226
5728	XCMS	2	32301705	9233
5729	MetAlign	2	32306368	8503
5730	SMART	2	32308365	927
5731	MetaboAnalyst	2	32308365	5582
5732	XCMS	1	32308800	5103
5733	HMDB	1	32308800	5656
5734	MetaboAnalyst	1	32308800	5811
5735	SMART	2	32308977	35788
5736	MetaboAnalyst	1	32312953	1182
5737	MZmine 2	1	32313165	8709
5738	MetaboAnalyst	1	32313165	9975
5739	XCMS	3	32313995	11515
5740	CAMERA	1	32313995	11660
5741	METLIN	1	32313995	14413
5742	BinBase	3	32316167	5567
5743	MetaboAnalyst	1	32316167	7224
5744	HMDB	1	32316363	7231
5745	XCMS	2	32316396	6983
5746	CAMERA	1	32316396	7116
5747	HMDB	1	32316396	7303
5748	MetaboAnalyst	1	32316396	7985
5749	XCMS	2	32316423	4820
5750	MetaboAnalyst	2	32318262	6290
5751	MetaboAnalyst	1	32321427	4043
5752	SMART	1	32321971	4449
5753	LDA	1	32322243	9685
5754	MetaboAnalyst	1	32324822	7148
5755	METLIN	1	32324822	7218
5756	ropls	3	32325810	5119
5757	MetaboAnalyst	2	32326296	4462
5758	XCMS	1	32327655	4385
5759	XCMS online	1	32327655	4385
5760	HMDB	1	32328198	2211
5761	METLIN	1	32328198	2217
5762	MoNA	1	32328198	2225
5763	MetaboAnalyst	1	32328198	3337
5764	XCMS	1	32331455	3214
5765	Skyline	1	32332738	9666
5766	XCMS	1	32332903	6144
5767	XCMS	1	32334588	6346
5768	ropls	1	32334588	6629
5769	HMDB	1	32334588	6826
5770	UC2	1	32335721	4778
5771	HMDB	1	32335721	4988
5772	LIPID MAPS	1	32335721	5016
5773	MetaboAnalyst	1	32335721	6553
5774	SMART	1	32336053	3223
5775	HMDB	1	32336980	7061
5776	METLIN	1	32336980	7072
5777	MetaboAnalyst	1	32336980	7155
5778	HMDB	1	32340213	7396
5779	METLIN	1	32340213	7424
5780	AMDIS	1	32340305	4776
5781	MetaboAnalyst	1	32340341	9710
5782	HMDB	1	32340341	10157
5783	MetaboAnalyst	1	32340350	2820
5784	HMDB	2	32340350	4180
5785	HMDB	1	32340530	10301
5786	xMSannotator	1	32340530	10375
5787	MetaboAnalyst	1	32340530	10546
5788	Mummichog	1	32340530	10565
5789	MAIT	11	32341160	131
5790	METLIN	1	32341369	4081
5791	HMDB	1	32341369	4196
5792	XCMS	1	32341411	4841
5793	METLIN	1	32341411	5598
5794	mixOmics	1	32341411	9422
5795	BinBase	3	32344578	2397
5796	MetaboAnalyst	1	32344578	6065
5797	MetaboAnalyst	1	32344839	3738
5798	LIPID MAPS	1	32344934	3988
5799	Skyline	1	32349240	11173
5800	GNPS	2	32349314	3006
5801	icoshift	1	32349447	4462
5802	MZmine 2	1	32349455	5411
5803	MetaboAnalyst	2	32349455	6954
5804	HMDB	1	32351483	4055
5805	MetaboAnalyst	1	32351881	2736
5806	Workflow4Metabolomics	2	32352849	7637
5807	mixOmics	1	32352849	11040
5808	MetaboLights	1	32353013	5134
5809	mixOmics	1	32353013	9102
5810	XCMS	1	32354125	4388
5811	icoshift	1	32354152	4622
5812	XCMS	1	32354336	5274
5813	HMDB	1	32354336	5469
5814	ORCA	1	32354742	6294
5815	PiMP	5	32354742	16824
5816	METLIN	1	32355228	7242
5817	XCMS	1	32357834	8086
5818	MetaboAnalyst	1	32357834	9911
5819	PAPi	1	32357834	10235
5820	BATMAN	1	32358672	9264
5821	MAVEN	1	32359426	11845
5822	UniProtKB	1	32359426	13526
5823	XCMS	2	32359590	4478
5824	MetaboAnalyst	1	32363202	8026
5825	XCMS	1	32365112	6165
5826	GAM	4	32365713	13312
5827	LDA	1	32366032	3403
5828	XCMS	1	32366330	14991
5829	XCMS online	1	32366330	14991
5830	BioCyc	1	32366330	17296
5831	HMDB	1	32370168	3810
5832	SMART	1	32370222	1860
5833	XCMS	2	32373070	2152
5834	CAMERA	1	32373070	2384
5835	HMDB	1	32373070	3082
5836	LDA	3	32373209	5475
5837	LDA	2	32373220	8785
5838	MZmine 2	1	32375235	5084
5839	MetaboAnalyst	1	32375707	4255
5840	MetaboAnalyst	1	32375889	5139
5841	Mummichog	1	32375889	5278
5842	MZmine 2	1	32380771	5185
5843	METLIN	1	32380771	5413
5844	XCMS	2	32382038	6196
5845	HMDB	1	32382092	6408
5846	MetaboAnalyst	1	32382278	4361
5847	MetaboDiff	1	32384694	2596
5848	HMDB	2	32384705	8601
5849	CFM-ID	2	32384705	8810
5850	Skyline	4	32384705	12689
5851	MetaboAnalyst	1	32384794	7813
5852	cosmiq	1	32385409	5454
5853	MetaboAnalyst	2	32385409	7108
5854	MetaboAnalyst	1	32386519	2972
5855	MetaboAnalyst	1	32390846	6049
5856	LDA	2	32390991	8909
5857	HMDB	2	32391103	1788
5858	MetaboAnalyst	3	32391298	6272
5859	XCMS	2	32392181	4568
5860	CAMERA	1	32392181	4750
5861	IPO	5	32392884	2184
5862	CAMERA	1	32392884	2437
5863	Mummichog	1	32392884	5170
5864	MetaboAnalystR	2	32392884	5657
5865	METLIN	1	32392900	4248
5866	MetaboAnalyst	3	32398126	4470
5867	MIDAS	4	32403117	3151
5868	icoshift	1	32403423	3554
5869	MetaboAnalyst	2	32403423	3700
5870	HMDB	1	32404047	3424
5871	SMART	1	32404131	5904
5872	HMDB	1	32404908	8459
5873	MetaboAnalyst	1	32405339	4720
5874	XCMS	1	32405339	6702
5875	HMDB	1	32405339	7306
5876	MetaboAnalyst	2	32408521	4646
5877	HMDB	1	32408578	8480
5878	YMDB	1	32408578	8490
5879	MetFrag	2	32408578	9329
5880	CFM-ID	2	32408578	9703
5881	MRMPROBS	1	32408619	4796
5882	MetaboAnalyst	1	32408619	5833
5883	HMDB	1	32410647	7632
5884	Newton	1	32410680	5054
5885	HMDB	1	32414184	5740
5886	mixOmics	1	32415101	29296
5887	MetaboAnalyst	1	32419822	2498
5888	HMDB	1	32422870	932
5889	METLIN	1	32422870	994
5890	XCMS	1	32423380	2771
5891	MetaboAnalyst	1	32423380	4639
5892	MAVEN	1	32425810	3490
5893	MetaboAnalyst	1	32425810	5159
5894	METLIN	1	32426606	5391
5895	MetaboAnalyst	1	32426606	5869
5896	MS-FINDER	2	32426807	3455
5897	MZmine 2	1	32427928	8405
5898	MetaboAnalyst	2	32427948	7062
5899	HMDB	1	32427948	8567
5900	Skyline	1	32427974	5463
5901	MetaboLights	2	32429265	442
5902	SMART	1	32429385	506
5903	AMDIS	1	32432031	14037
5904	SpectConnect	1	32432031	14081
5905	ChemSpider	1	32434586	7599
5906	MetaboAnalyst	1	32438599	2911
5907	mQTL	3	32438708	6618
5908	HMDB	2	32440317	2520
5909	MetaboAnalyst	1	32440317	2768
5910	BinBase	1	32442163	4655
5911	HMDB	2	32442208	5158
5912	KIMBLE	1	32442208	6415
5913	MetaboAnalyst	1	32443550	3131
5914	HMDB	2	32443577	7144
5915	MetaboAnalyst	1	32443844	5268
5916	GAM	1	32444846	1108
5917	LipidXplorer	1	32447426	21157
5918	IsoCor	1	32451440	8049
5919	XCMS	1	32455594	3828
5920	IPO	1	32455594	4370
5921	CMM	1	32455594	4980
5922	HMDB	1	32455594	5012
5923	LIPID MAPS	1	32455594	5023
5924	METLIN	1	32455594	5040
5925	Mummichog	1	32455594	5682
5926	HMDB	1	32455603	3912
5927	icoshift	1	32455691	10432
5928	HMDB	2	32455691	10625
5929	SMART	2	32455735	1235
5930	LDA	1	32455745	5980
5931	XCMS	1	32455800	5491
5932	AMDIS	1	32455856	2422
5933	HMDB	1	32455924	4051
5934	AMDIS	1	32455938	2069
5935	MET-IDEA	1	32455938	4927
5936	MZmine 2	1	32456212	3474
5937	GNPS	1	32456220	3507
5938	XCMS	2	32456338	6305
5939	MetaboAnalyst	1	32456338	6979
5940	MetaboLights	1	32456338	7304
5941	GNPS	1	32456338	7497
5942	HMDB	1	32457236	7275
5943	XCMS	1	32457238	6145
5944	MetaboAnalyst	2	32457238	7403
5945	MetaboLights	1	32457238	13805
5946	MetaboAnalyst	1	32457630	5106
5947	xMSanalyzer	1	32458724	3145
5948	xMSannotator	2	32458724	3340
5949	HMDB	1	32458724	3393
5950	Mummichog	1	32458724	4880
5951	METLIN	1	32458724	5160
5952	MAVEN	2	32460306	2119
5953	MetaboAnalyst	1	32460306	2886
5954	Pathview	1	32460579	5139
5955	HMDB	1	32461830	5838
5956	METLIN	1	32461830	5880
5957	VMH	2	32463598	25071
5958	MetaboAnalyst	4	32466096	10429
5959	UniProtKB	1	32466531	3258
5960	HMDB	3	32467615	9713
5961	GAM	1	32467627	4947
5962	MetaboAnalyst	1	32467730	20124
5963	XCMS	1	32469892	4888
5964	AMDIS	1	32470037	10257
5965	MetaboAnalyst	1	32471147	6199
5966	ChemSpider	1	32471154	9041
5967	BioCyc	1	32471154	9053
5968	LDA	2	32473086	2285
5969	ChemSpider	1	32476799	4393
5970	Mummichog	1	32476799	4513
5971	MetaboAnalyst	1	32477136	6689
5972	ropls	1	32477276	6873
5973	MetaboAnalyst	1	32477276	7689
5974	LDA	1	32477276	8875
5975	ChemSpider	1	32477292	8997
5976	Newton	1	32481497	1888
5977	XCMS	1	32481497	2978
5978	HMDB	3	32481497	3192
5979	Workflow4Metabolomics	1	32481497	3496
5980	MetaboAnalyst	2	32481497	5589
5981	SMPDB	1	32481497	6652
5982	AMDIS	1	32481553	8055
5983	MZmine 2	1	32481767	2753
5984	MetaboAnalyst	2	32481767	3146
5985	XCMS	1	32483270	4633
5986	CAMERA	2	32483270	4676
5987	METLIN	1	32486092	2419
5988	HMDB	1	32486092	2524
5989	MetaboAnalyst	1	32486279	6971
5990	XCMS	1	32486312	1462
5991	mixOmics	1	32486312	9699
5992	MetaboAnalyst	1	32486916	6412
5993	LDA	1	32487762	2633
5994	XCMS	1	32487762	3515
5995	mixOmics	2	32488118	5844
5996	MetaboAnalyst	1	32489401	3487
5997	HMDB	1	32489569	7257
5998	UniProtKB	1	32492067	1839
5999	BinBase	1	32492072	2370
6000	UniProtKB	1	32492406	7594
6001	Escher	1	32492406	14870
6002	Skyline	1	32492406	15096
6003	Binner	1	32492693	6959
6004	LDA	1	32492805	10048
6005	SpectConnect	1	32492856	8845
6006	MetaboAnalyst	1	32492856	8948
6007	MetaboAnalyst	1	32493222	4876
6008	XCMS	1	32493433	7331
6009	HMDB	1	32493433	8143
6010	METLIN	1	32493433	8174
6011	METLIN	1	32498307	10183
6012	LDA	3	32498677	8995
6013	Newton	1	32499757	4155
6014	MetaboAnalyst	3	32500026	3808
6015	HMDB	1	32500026	4451
6016	LIPID MAPS	1	32500026	4462
6017	METLIN	1	32500026	4474
6018	Mummichog	1	32500026	4744
6019	METLIN	1	32500084	6225
6020	HMDB	1	32503480	6648
6021	METLIN	1	32503480	6679
6022	XCMS	1	32503480	7046
6023	Workflow4Metabolomics	1	32503480	7176
6024	MetaboAnalyst	1	32508632	7550
6025	BATMAN	1	32508632	7844
6026	XCMS	1	32509585	5846
6027	CAMERA	1	32509585	5907
6028	METLIN	1	32509585	5956
6029	MetaboAnalyst	1	32509585	6024
6030	MAIT	25	32511236	0
6031	MetaboAnalyst	1	32512855	7466
6032	GNPS	1	32512855	9888
6033	HMDB	1	32513689	6386
6034	HMDB	4	32517015	11717
6035	PathBank	1	32517015	14452
6036	MZmine 2	1	32517053	2584
6037	GNPS	3	32517053	3733
6038	LipidXplorer	1	32517352	4866
6039	VMH	1	32517799	27014
6040	MZmine 2	1	32518324	12693
6041	MetaboAnalyst	1	32518724	5476
6042	LIPID MAPS	1	32521649	7696
6043	MetaboAnalyst	4	32521675	2769
6044	MetaboLyzer	2	32521675	3267
6045	HMDB	2	32521675	3625
6046	METLIN	1	32521675	3926
6047	XCMS	1	32522991	5578
6048	MAVEN	1	32522991	5587
6049	Skyline	2	32522993	19047
6050	Metab	1	32523012	4264
6051	METLIN	1	32526851	10574
6052	HMDB	1	32526851	10622
6053	XCMS	3	32526912	3458
6054	MetabolAnalyze	1	32526912	4315
6055	ChemSpider	1	32527018	1467
6056	XCMS	1	32528691	6641
6057	MZmine 2	1	32528927	4092
6058	GNPS	4	32528927	5146
6059	XCMS	1	32531990	3146
6060	mzMatch	1	32531990	3265
6061	XCMS	1	32537472	3854
6062	HMDB	1	32537472	4491
6063	METLIN	1	32537472	4519
6064	LIPID MAPS	1	32537472	4591
6065	MVAPACK	1	32539684	6449
6066	icoshift	1	32539684	6822
6067	HMDB	1	32539684	7387
6068	BioCyc	1	32539684	7882
6069	MetaboAnalyst	1	32539684	8779
6070	LIPID MAPS	1	32544198	5581
6071	MZmine 2	1	32544203	9619
6072	MetaboAnalyst	1	32545251	13246
6073	MetaboAnalyst	2	32545699	5675
6074	HMDB	1	32545699	6646
6075	GNPS	10	32545808	5482
6076	muma	1	32545923	4383
6077	ropls	1	32545923	4439
6078	METLIN	1	32545923	4714
6079	GNPS	4	32545923	4806
6080	mzMatch	1	32546260	19106
6081	XCMS	1	32546787	5022
6082	MetaboLights	1	32546787	6006
6083	MetaboAnalyst	1	32547129	2607
6084	HMDB	1	32547402	3248
6085	METLIN	1	32547402	3279
6086	MetaboAnalyst	1	32547402	3480
6087	mixOmics	1	32547651	6828
6088	HMDB	1	32549232	2454
6089	MetaboAnalyst	1	32549232	2837
6090	XCMS	5	32549240	6085
6091	XCMS online	1	32549240	6085
6092	METLIN	1	32549240	6731
6093	LDA	1	32552668	3816
6094	XCMS	1	32552668	5580
6095	MetaboAnalyst	2	32552668	6525
6096	ORCA	1	32553153	10651
6097	MetaboAnalyst	1	32555688	4428
6098	IsoCor	2	32560048	11558
6099	MetaboAnalyst	1	32560175	17413
6100	CAMERA	1	32560283	6965
6101	METLIN	1	32560283	7004
6102	MetaboMiner	1	32560419	9814
6103	HMDB	1	32560419	10063
6104	MetaboAnalyst	1	32560419	10284
6105	HMDB	1	32560547	3725
6106	ChemSpider	1	32560547	3888
6107	MetaboAnalyst	1	32560547	3939
6108	MetaboLights	1	32560628	5325
6109	missForest	2	32561768	14122
6110	HMDB	10	32561832	2016
6111	BinBase	2	32561884	3178
6112	NOREVA	1	32561884	3771
6113	XCMS	1	32563250	4903
6114	LDA	2	32566075	8652
6115	HMDB	1	32566075	12921
6116	MetaboAnalyst	2	32566075	12993
6117	icoshift	1	32568257	6356
6118	HMDB	1	32570933	11396
6119	MetFrag	1	32572060	11426
6120	SMART	1	32572216	4893
6121	FingerID	2	32575527	8922
6122	Skyline	1	32575611	5932
6123	XCMS	1	32575789	1251
6124	CAMERA	1	32575789	1403
6125	MetaboAnalystR	1	32575789	5792
6126	NMRProcFlow	2	32575903	5371
6127	GNPS	5	32576651	6971
6128	LIPID MAPS	1	32576934	3517
6129	mixOmics	1	32577129	5868
6130	MetaboLights	1	32577508	4614
6131	XCMS	1	32577508	5248
6132	CAMERA	2	32577508	6273
6133	HMDB	1	32578071	8726
6134	muma	1	32580166	6677
6135	HMDB	1	32580166	7288
6136	Rhea	2	32580422	6790
6137	SMPDB	1	32581232	4225
6138	METLIN	2	32581242	22787
6139	MetaboLights	1	32581368	678
6140	HMDB	1	32581368	6729
6141	HMDB	1	32581811	6894
6142	METLIN	1	32581811	6923
6143	MetaboAnalyst	1	32581811	8344
6144	MAVEN	1	32581847	4396
6145	MetaboAnalyst	1	32581847	4798
6146	MZmine 2	1	32581873	5101
6147	CAMERA	1	32581873	5196
6148	MAIT	16	32582206	1224
6149	MZmine 2	1	32582236	8217
6150	MetaboAnalyst	3	32582236	11596
6151	METLIN	1	32582236	14703
6152	MetaboAnalyst	1	32585915	5379
6153	HMDB	1	32585915	5503
6154	GAM	6	32586036	662
6155	LDA	2	32586265	9028
6156	XCMS	1	32587477	6381
6157	HMDB	1	32587515	6005
6158	ORCA	1	32588679	15822
6159	MetaboAnalyst	1	32588698	3717
6160	mixOmics	1	32589958	7892
6161	HMDB	2	32589958	8334
6162	MoNA	2	32589958	8340
6163	METLIN	2	32589958	8356
6164	MetaboAnalystR	4	32589958	12931
6165	Skyline	1	32597706	5176
6166	XCMS	1	32597706	7190
6167	CAMERA	1	32597706	7264
6168	HMDB	1	32597706	7846
6169	MetFrag	1	32597706	7962
6170	CFM-ID	1	32597706	7979
6171	HMDB	1	32600265	6163
6172	MetaboAnalyst	1	32600265	9651
6173	MetFrag	1	32600361	13059
6174	XCMS	4	32600361	13185
6175	LDA	2	32600361	14788
6176	ChemSpider	1	32602074	467
6177	MSClust	2	32604798	8494
6178	HMDB	1	32604798	9272
6179	LIPID MAPS	1	32604966	2204
6180	MZmine 2	1	32604974	6028
6181	GNPS	3	32604974	7694
6182	MetaboLights	1	32604974	13457
6183	XCMS	1	32604977	5986
6184	GNPS	2	32604977	8535
6185	MAVEN	1	32605166	11372
6186	MetaboAnalyst	2	32605166	11776
6187	HMDB	1	32605166	12295
6188	METLIN	1	32605241	6638
6189	HMDB	1	32606601	7272
6190	MZmine 2	1	32607087	3665
6191	Skyline	1	32609300	5321
6192	METLIN	1	32609782	2464
6193	HMDB	1	32610096	10373
6194	METLIN	1	32610096	10398
6195	AMDIS	3	32612960	7900
6196	XCMS	1	32612960	8822
6197	LDA	1	32612960	10269
6198	MetaboAnalyst	3	32612989	4489
6199	HMDB	1	32612989	5758
6200	MetaboAnalyst	1	32615973	13642
6201	HMDB	1	32616735	11279
6202	MetaboAnalyst	1	32616735	11367
6203	HMDB	1	32616821	3298
6204	MetaboAnalyst	1	32616893	4145
6205	MetaboAnalyst	1	32620791	9939
6206	GAM	1	32625093	4155
6207	HMDB	1	32625095	4842
6208	MetaboAnalyst	1	32625095	4901
6209	MetaboAnalyst	1	32625224	8554
6210	MetScape	1	32625230	7115
6211	MetaboAnalyst	1	32625246	7412
6212	Mummichog	2	32626659	390
6213	MetaboLyzer	2	32629836	2926
6214	HMDB	1	32629836	3014
6215	METLIN	1	32629836	4098
6216	MetaboAnalyst	1	32630518	5049
6217	MetaboAnalyst	1	32630672	4583
6218	MetAlign	1	32632328	9746
6219	HMDB	2	32635215	4323
6220	METLIN	2	32635215	4333
6221	MetaboAnalyst	1	32635220	5924
6222	LDA	2	32635315	6277
6223	METLIN	1	32635401	4487
6224	BiGG Models	1	32635563	693
6225	BioCyc	1	32635563	2221
6226	Escher	1	32635563	4812
6227	MetaboAnalyst	1	32636751	7876
6228	DrugBank	1	32636751	8533
6229	SMART	1	32637125	5246
6230	XCMS	1	32637154	7601
6231	IPO	1	32637154	7818
6232	CAMERA	1	32637154	7939
6233	HMDB	5	32637154	9464
6234	GNPS	3	32637174	4625
6235	FingerID	1	32637174	5392
6236	XCMS	1	32640515	7383
6237	CAMERA	1	32640515	7621
6238	MetaboAnalyst	1	32640515	8470
6239	XCMS	1	32640711	7542
6240	MetaboAnalyst	1	32640711	8802
6241	MetaboAnalyst	2	32641834	14213
6242	MetaboAnalyst	1	32645838	3575
6243	MetAlign	1	32645847	9025
6244	MetaboAnalyst	1	32645907	3267
6245	MetScape	1	32645907	3381
6246	PathVisio	1	32645907	3413
6247	XCMS	1	32646417	5834
6248	XCMS online	1	32646417	5834
6249	MetaboLab	3	32648823	4135
6250	MZmine 2	1	32650478	2085
6251	ChemSpider	1	32651957	4068
6252	MetaboAnalystR	1	32651957	4221
6253	MAIT	5	32652598	1481
6254	LDA	1	32652929	3928
6255	HMDB	1	32655770	6097
6256	ropls	1	32655770	6537
6257	MetaboAnalyst	1	32655770	7635
6258	CAMERA	1	32655986	2622
6259	XCMS	1	32656431	1011
6260	XCMS online	1	32656431	1011
6261	XCMS	1	32658450	9431
6262	XCMS	1	32660149	5093
6263	MetaboAnalyst	1	32661677	9598
6264	GNPS	3	32664387	1856
6265	XCMS	2	32668689	4746
6266	specmine	1	32668689	5527
6267	AMDIS	1	32668691	6482
6268	CAMERA	1	32668735	5968
6269	GNPS	1	32668735	6457
6270	MetaboLights	1	32668735	7138
6271	MetaboAnalyst	3	32669636	6245
6272	HMDB	1	32669636	9531
6273	MetaboAnalyst	1	32670053	3854
6274	MZmine 2	3	32671097	11789
6275	MetaboAnalyst	1	32671097	13427
6276	MetaboLights	1	32671097	13601
6277	HMDB	1	32673287	8415
6278	MetaboLights	1	32673287	10163
6279	ChemSpider	1	32676084	7960
6280	GNPS	1	32676116	7161
6281	MetaboAnalyst	4	32677954	5810
6282	ORCA	1	32678093	4102
6283	HMDB	1	32678093	13630
6284	MetaboAnalyst	2	32678093	13975
6285	AMDIS	2	32678173	17063
6286	UniProtKB	1	32678822	4413
6287	mixOmics	1	32679761	7700
6288	MetaboAnalyst	1	32679819	17711
6289	HMDB	1	32685836	3048
6290	MetaboAnalyst	1	32686726	8946
6291	UniProtKB	1	32689941	6586
6292	BinBase	1	32693791	2400
6293	MetaboAnalyst	1	32693791	2491
6294	LIQUID	1	32694525	5211
6295	MetScape	1	32694525	13932
6296	MetDisease	1	32694525	14151
6297	SMART	1	32694623	1199
6298	ORCA	1	32694660	27142
6299	MAVEN	1	32694786	5624
6300	XCMS	1	32694994	8101
6301	MetaboAnalyst	1	32694994	8793
6302	ChemSpider	1	32695217	2892
6303	MetaboAnalyst	1	32695259	5825
6304	XCMS	1	32695549	5992
6305	MetaboAnalyst	1	32695549	7384
6306	HMDB	1	32695549	7690
6307	METLIN	1	32695549	7718
6308	ChemSpider	1	32695549	7799
6309	LIPID MAPS	1	32697195	16105
6310	apLCMS	1	32699106	5775
6311	xMSanalyzer	1	32699106	5787
6312	Mummichog	2	32699106	6971
6313	xMSannotator	4	32699106	8996
6314	XCMS	1	32699417	25088
6315	MetaboAnalyst	2	32699629	5460
6316	HMDB	1	32701294	5637
6317	MetaboLights	1	32701294	5702
6318	MetaboAnalyst	2	32701294	10979
6319	HMDB	1	32703155	4588
6320	METLIN	1	32703155	4606
6321	Rhea	3	32703946	8530
6322	LDA	1	32704052	8213
6323	LIPID MAPS	1	32704146	8249
6324	Newton	1	32704701	12155
6325	MetaboAnalyst	1	32708128	6779
6326	GNPS	2	32708620	6425
6327	MetaboAnalyst	1	32708621	4728
6328	MAVEN	1	32708819	2417
6329	MetaboAnalyst	1	32708819	2924
6330	Skyline	1	32708994	4230
6331	GNPS	3	32709006	5772
6332	SigMa	3	32709034	3201
6333	icoshift	1	32709034	3347
6334	LDA	1	32709038	1456
6335	MetaboAnalyst	1	32709038	3102
6336	XCMS	1	32710540	4714
6337	XCMS	1	32711499	8965
6338	HMDB	2	32714143	2871
6339	MetaboAnalyst	1	32714143	3925
6340	LDA	1	32714307	15928
6341	icoshift	1	32717850	3301
6342	MetaboAnalyst	1	32717850	5847
6343	MetaboAnalyst	2	32717953	6624
6344	IPO	1	32717987	344
6345	MZmine 2	1	32717987	1720
6346	MetaboAnalyst	2	32717987	2007
6347	ORCA	1	32719541	11422
6348	MetaboAnalyst	1	32719702	8938
6349	MZmine 2	1	32720192	6622
6350	MetaboAnalyst	1	32722105	8200
6351	xMSannotator	3	32722194	3832
6352	HMDB	1	32722194	3982
6353	MetaboAnalyst	1	32722505	10361
6354	MetAlign	2	32722640	3454
6355	HMDB	1	32722640	4775
6356	MetaboAnalyst	1	32722640	4847
6357	MetaboAnalyst	1	32723826	2391
6358	MetAlign	1	32726342	7564
6359	METLIN	1	32726975	10700
6360	HMDB	1	32726975	10749
6361	XCMS	1	32727023	10583
6362	METLIN	1	32727023	10730
6363	AMDIS	1	32728091	6404
6364	MetaboAnalyst	1	32728091	11796
6365	iPath	1	32732914	5868
6366	ConsensusPathDB	1	32732995	5729
6367	HMDB	1	32732995	6213
6368	MetaboAnalyst	1	32732995	6547
6369	ChemSpider	1	32733080	5677
6370	LDA	1	32733406	8217
6371	UniProtKB	1	32733541	2715
6372	MetaboAnalyst	1	32733621	5393
6373	IsoMS	2	32734144	5943
6374	IsoMS-Quant	1	32734144	6149
6375	XCMS	1	32736527	3212
6376	MZmine 2	1	32737117	3368
6377	AMDIS	1	32742371	5102
6378	MetaboAnalyst	1	32742371	5696
6379	MetScape	1	32742371	6141
6380	AMDIS	5	32742642	7906
6381	XCMS	1	32742642	9219
6382	MetaboAnalyst	1	32742642	11328
6383	Skyline	1	32743614	14583
6384	CAMERA	1	32747678	7340
6385	LDA	1	32747678	8373
6386	MetaboAnalyst	3	32748828	2661
6387	MetAlign	1	32751065	2400
6388	MSClust	1	32751065	2421
6389	HMDB	1	32751065	2616
6390	LIPID MAPS	1	32751065	2654
6391	mixOmics	2	32751315	8890
6392	LDA	2	32751315	10505
6393	MetFrag	2	32751412	9125
6394	METLIN	1	32751412	9171
6395	GNPS	1	32751412	9207
6396	HMDB	1	32751412	9281
6397	XCMS	1	32751412	10402
6398	XCMS online	1	32751412	10402
6399	AMDIS	1	32751448	7676
6400	MET-IDEA	1	32751448	7832
6401	Workflow4Metabolomics	1	32751478	5787
6402	LDA	1	32751784	4238
6403	MetaboAnalyst	1	32751784	5501
6404	MZmine 2	1	32751925	4030
6405	XCMS	1	32751925	4264
6406	XCMS online	1	32751925	4264
6407	METLIN	1	32751925	4584
6408	AMDIS	1	32752177	4929
6409	MZmine 2	1	32752177	6763
6410	SMART	1	32758143	4921
6411	METLIN	1	32758241	2504
6412	RaMP	2	32759684	7567
6413	HMDB	3	32760300	475
6414	DrugBank	1	32760300	999
6415	XCMS	2	32760365	1512
6416	mixOmics	1	32760678	6706
6417	HMDB	1	32764239	1527
6418	MetaboAnalyst	1	32764239	1872
6419	MAVEN	1	32764664	26557
6420	BioCyc	1	32764789	4828
6421	HMDB	1	32765256	5484
6422	LIPID MAPS	1	32765256	5512
6423	METLIN	1	32765256	5573
6424	MS-FINDER	1	32765438	5819
6425	MS-FLO	1	32765438	5982
6426	HMDB	1	32765450	3612
6427	MetaboAnalyst	2	32766148	1519
6428	MetaboAnalyst	1	32766476	2749
6429	LDA	1	32766476	3444
6430	MetaboAnalyst	3	32770044	8517
6431	LDA	2	32770049	4404
6432	MetaboAnalyst	1	32774300	7023
6433	HMDB	1	32774853	9694
6434	HMDB	1	32775446	5101
6435	MAVEN	1	32775494	10979
6436	HMDB	1	32778716	6433
6437	GNPS	5	32778716	6494
6438	MetaboAnalystR	1	32781584	2734
6439	MZmine 2	1	32781598	2439
6440	GSimp	1	32781624	5833
6441	MetaboAnalyst	6	32781793	5145
6442	HMDB	1	32781793	9658
6443	DrugBank	1	32781793	9714
6444	MetaboAnalyst	1	32784373	9383
6445	HMDB	1	32784479	6791
6446	HMDB	1	32785071	2768
6447	METLIN	1	32785071	2791
6448	MetaboAnalystR	1	32785071	4613
6449	HMDB	1	32786495	3540
6450	Pathos	2	32786495	3664
6451	DrugBank	7	32786543	111
6452	HMDB	1	32786839	6075
6453	MetaboAnalyst	1	32786839	7972
6454	MAIT	7	32788367	2201
6455	METLIN	1	32788682	3774
6456	HMDB	1	32788682	3821
6457	SMART	1	32792480	13915
6458	MoNA	1	32792566	8642
6459	HMDB	1	32792566	8652
6460	MetaboAnalyst	1	32792566	9111
6461	MetaboAnalyst	1	32792906	3844
6462	XCMS	1	32793136	7166
6463	MetaboAnalyst	1	32793136	7534
6464	MAVEN	1	32795388	8699
6465	MetaboAnalyst	2	32795388	10429
6466	AMDIS	1	32796601	4130
6467	SMART	1	32799801	678
6468	MZmine 2	1	32801693	3708
6469	XCMS	1	32802186	6084
6470	MetaboAnalyst	2	32802867	4181
6471	GAM	1	32804944	357
6472	BioCyc	1	32804944	889
6473	MAVEN	1	32804944	8712
6474	AMDIS	1	32805028	6918
6475	VANTED	1	32807817	5325
6476	MetaboAnalyst	1	32808022	5356
6477	LDA	2	32808768	5705
6478	XCMS	1	32810196	5171
6479	MetFrag	1	32810196	5263
6480	apLCMS	2	32810196	5477
6481	xMSannotator	3	32810196	7830
6482	Mummichog	2	32810196	9072
6483	GAM	1	32811478	5198
6484	LDA	3	32811478	10512
6485	mQTL	1	32811491	5453
6486	MetaboAnalyst	1	32811807	7092
6487	LIPID MAPS	1	32811851	8189
6488	Skyline	1	32811851	11020
6489	DrugBank	1	32813697	2084
6490	Skyline	3	32814773	11033
6491	LDA	2	32817452	12533
6492	XCMS	3	32817615	4141
6493	MetaboAnalyst	2	32819075	2572
6494	MetaboAnalyst	1	32821265	13057
6495	MetaboAnalyst	2	32823484	2950
6496	mixOmics	1	32823827	4903
6497	MetaboAnalyst	2	32823827	7142
6498	MetaboAnalyst	2	32824041	3162
6499	MetaboAnalyst	1	32824262	8211
6500	SMART	1	32824436	1283
6501	MetaboAnalyst	1	32824755	7400
6502	MetaboAnalyst	3	32824900	4755
6503	AMDIS	1	32825166	3629
6504	AMDIS	1	32825248	5395
6505	MetaboAnalyst	1	32825248	5591
6506	SMPDB	1	32825248	6331
6507	SMART	1	32825526	2333
6508	MoNA	1	32825768	4735
6509	LDA	3	32826221	12684
6510	MAVEN	1	32833957	26335
6511	XCMS	1	32839531	9358
6512	METLIN	1	32839531	10485
6513	HMDB	1	32839531	10525
6514	MetaboAnalyst	1	32839531	10934
6515	MoNA	1	32840693	8273
6516	AMDIS	1	32842542	6196
6517	MetaboAnalyst	1	32842952	8286
6518	XCMS	1	32843016	4825
6519	BioCyc	1	32843654	12704
6520	AMDIS	1	32847042	16172
6521	MetaboAnalyst	1	32847042	16552
6522	LDA	1	32848839	8248
6523	SMART	1	32849341	547
6524	ORCA	1	32849341	10701
6525	XCMS	4	32849439	7749
6526	MetaboAnalyst	2	32849439	8467
6527	CAMERA	1	32849439	9820
6528	METLIN	1	32849439	10242
6529	HMDB	1	32849439	10287
6530	AMDIS	1	32849560	1887
6531	MetaboAnalyst	1	32849560	2381
6532	MAIT	45	32849590	1104
6533	XCMS	1	32849599	1517
6534	HMDB	1	32849599	1988
6535	SMART	1	32849657	7379
6536	LDA	2	32850904	2681
6537	MAVEN	1	32851035	3865
6538	ropls	1	32851199	5276
6539	SMPDB	1	32851199	5777
6540	MetaboAnalyst	3	32851199	5801
6541	LDA	3	32854199	4266
6542	BioCyc	1	32854199	4836
6543	XCMS	2	32854199	6613
6544	CAMERA	1	32854199	6987
6545	METLIN	1	32854199	7042
6546	MetSign	1	32854360	5549
6547	MetaboAnalyst	1	32855636	6283
6548	METLIN	1	32859057	9044
6549	AMDIS	1	32859120	3979
6550	GAVIN	1	32859120	4508
6551	Rhea	1	32859898	19770
6552	LDA	2	32859898	24181
6553	ropls	1	32859898	34107
6554	LDA	2	32864208	5540
6555	ChemSpider	1	32864208	11101
6556	HMDB	1	32864208	11138
6557	GNPS	2	32867085	4596
6558	HMDB	1	32867141	4176
6559	LIPID MAPS	1	32867761	6048
6560	MetaboAnalyst	1	32868003	5100
6561	XCMS	1	32869121	4644
6562	GAM	3	32872254	2059
6563	MetaboAnalyst	4	32872300	4157
6564	Mummichog	1	32872300	6461
6565	ChemSpider	1	32872300	7630
6566	HMDB	1	32872562	796
6567	MetaboAnalyst	1	32872655	7999
6568	MVAPACK	1	32873608	2341
6569	icoshift	1	32873608	2738
6570	GNPS	1	32878176	6500
6571	MetaboAnalyst	2	32878308	3853
6572	HMDB	1	32878621	4055
6573	MetaboAnalyst	2	32878621	4083
6574	iPath	1	32878621	4221
6575	VANTED	1	32881962	5359
6576	MetaboAnalyst	1	32882816	5319
6577	XCMS	1	32883206	3964
6578	HMDB	2	32884056	7432
6579	MetaboAnalyst	1	32884088	5651
6580	LDA	1	32884220	3345
6581	MetaboAnalyst	1	32884304	5899
6582	MassTRIX	1	32885047	3580
6583	HMDB	1	32885047	3641
6584	YMDB	1	32885047	3762
6585	HMDB	1	32887276	4663
6586	METLIN	1	32887276	4737
6587	GNPS	1	32887375	4690
6588	MetaboAnalyst	4	32887433	3565
6589	XCMS	1	32887433	6409
6590	MoNA	1	32887471	4447
6591	MRMPROBS	1	32894761	2623
6592	MAVEN	1	32895477	16294
6593	ORCA	1	32895482	5149
6594	LIPID MAPS	1	32899418	6047
6595	XCMS	2	32899418	6343
6596	MetaboAnalyst	1	32899614	4832
6597	HMDB	2	32899667	5525
6598	MetaboAnalyst	1	32899727	5095
6599	LipidXplorer	1	32899843	9534
6600	HMDB	1	32899962	8041
6601	MetaboAnalyst	2	32899962	8748
6602	XCMS	1	32900869	5420
6603	METLIN	1	32900869	5695
6604	SMPDB	1	32900869	6536
6605	MassTRIX	1	32900869	12492
6606	MAVEN	1	32900873	7568
6607	HMDB	1	32901869	1247
6608	MetaboAnalyst	2	32901869	1396
6609	Skyline	2	32903271	10687
6610	MetaboLights	1	32903271	11319
6611	XCMS	1	32903457	1158
6612	HMDB	1	32903457	2213
6613	MetaboAnalyst	1	32903457	2529
6614	MetaboAnalyst	1	32904659	1248
6615	METLIN	1	32907579	11267
6616	MetFrag	1	32907579	11565
6617	LDA	1	32907952	3153
6618	HMDB	1	32907952	4855
6619	HMDB	1	32908562	7334
6620	MetaboAnalyst	1	32908856	3433
6621	HMDB	1	32908856	3536
6622	MetaboAnalyst	1	32908958	6195
6623	MetaboAnalyst	1	32909121	6642
6624	HMDB	2	32909121	7992
6625	MetaboAnalyst	1	32910715	3344
6626	LIPID MAPS	1	32913236	5776
6627	HMDB	1	32914213	3835
6628	METLIN	1	32917958	7259
6629	LIPID MAPS	1	32917958	7337
6630	mixOmics	1	32919472	16241
6631	PARADISe	1	32919472	20354
6632	HMDB	1	32922426	3804
6633	METLIN	1	32922426	3896
6634	MetaboAnalystR	1	32923834	5666
6635	ORCA	2	32925933	3739
6636	MetaboAnalyst	1	32927597	6579
6637	MetFrag	1	32927597	7955
6638	SMART	1	32927732	17269
6639	MZmine 2	1	32929085	2734
6640	Metab	4	32929121	15971
6641	MetaboAnalyst	1	32929373	4721
6642	LDA	2	32932711	5933
6643	BioCyc	3	32932853	357
6644	GAM	3	32932853	3033
6645	MetaboAnalyst	1	32932861	3887
6646	MetaboAnalyst	1	32933101	5996
6647	ChemSpider	1	32935784	4332
6648	MetaboAnalyst	1	32937786	7723
6649	XCMS	1	32937957	4228
6650	MetaboAnalyst	1	32942590	9978
6651	LipidXplorer	1	32943450	5786
6652	MetaboAnalyst	1	32944177	8771
6653	XCMS	1	32948809	5544
6654	HMDB	1	32953937	8956
6655	LDA	2	32957914	9406
6656	mixOmics	1	32958038	8660
6657	MetaboAnalyst	1	32958938	18621
6658	METLIN	1	32960942	2697
6659	MetAlign	1	32961634	4660
6660	SigMa	1	32961859	5216
6661	XCMS	1	32962264	1592
6662	MetaboAnalyst	1	32962264	4270
6663	HMDB	2	32967218	5811
6664	METLIN	2	32967218	5888
6665	MetaboAnalyst	1	32967314	2187
6666	HMDB	1	32967314	2291
6667	XCMS	6	32967365	5719
6668	CAMERA	1	32967365	6012
6669	MetaboAnalyst	2	32967365	6971
6670	ChemSpider	1	32967365	8419
6671	METLIN	1	32967365	9343
6672	HMDB	1	32968276	16110
6673	MZmine 2	1	32971728	4183
6674	MetaboAnalyst	1	32971728	5493
6675	HMDB	1	32972369	5442
6676	mixOmics	1	32972369	7782
6677	MetaboAnalyst	1	32973179	9004
6678	XCMS	1	32973203	6413
6679	METLIN	1	32973203	9852
6680	HMDB	1	32973203	9863
6681	MetaboAnalyst	4	32973203	10868
6682	MetExplore	6	32973203	10921
6683	MZmine 2	1	32973288	7330
6684	MassTRIX	2	32973337	6415
6685	HMDB	1	32973337	6705
6686	LDA	2	32973805	5894
6687	Newton	1	32973833	3461
6688	Pathview	1	32973833	13681
6689	GNPS	5	32973846	15866
6690	SMART	1	32973858	2523
6691	Metab	1	32974309	19988
6692	MetaboAnalyst	1	32974387	6210
6693	HMDB	1	32977370	7620
6694	MAVEN	1	32977543	5764
6695	MetaboAnalyst	2	32977543	6184
6696	XCMS	1	32982422	1226
6697	XCMS online	1	32982422	1226
6698	HMDB	1	32982422	1550
6699	MetaboAnalyst	2	32982422	1945
6700	ChemSpider	1	32982808	3730
6701	MAVEN	1	32983059	5720
6702	METLIN	1	32984013	3412
6703	HMDB	1	32984013	3455
6704	MetaboAnalyst	1	32984324	7323
6705	XCMS	1	32984417	5543
6706	LDA	2	32984417	6821
6707	MetaboAnalyst	1	32984417	7824
6708	MetaboAnalyst	1	32985541	10458
6709	LDA	2	32985541	11865
6710	MetaboAnalyst	1	32985578	4639
6711	LIPID MAPS	1	32986697	10925
6712	METLIN	1	32986708	7744
6713	HMDB	1	32986708	7790
6714	MetaboAnalyst	2	32986754	3310
6715	ConsensusPathDB	2	32987857	2702
6716	MetaboAnalyst	1	32987857	4490
6717	MAIT	3	32989174	3595
6718	ChemSpider	1	32992670	4690
6719	MetaboAnalyst	2	32992670	5039
6720	HMDB	1	32993487	3264
6721	GNPS	7	32994287	5844
6722	MetaboAnalyst	1	32994287	8184
6723	AMDIS	1	32994291	7801
6724	MZmine 2	1	32994526	12199
6725	LDA	2	32995714	2551
6726	MAVEN	1	32998280	11352
6727	MetaboAnalyst	1	32998280	11750
6728	UniProtKB	1	32998685	2039
6729	HMDB	1	32999294	3229
6730	MetaboAnalyst	2	32999308	12657
6731	UniProtKB	2	33001992	5538
6732	MetaboAnalyst	2	33003404	10453
6733	LDA	3	33003447	8123
6734	MelonnPan	7	33003447	9333
6735	MetaboDiff	2	33003509	8914
6736	UniProtKB	1	33004079	6820
6737	HMDB	1	33004940	5109
6738	METLIN	1	33004940	5127
6739	MetaboAnalystR	1	33004940	6724
6740	Escher	1	33005401	1484
6741	XCMS	1	33007922	2750
6742	RAMClustR	2	33007922	2887
6743	ChemSpider	2	33013291	3993
6744	BioCyc	1	33013291	4093
6745	HMDB	1	33013291	4341
6746	XCMS	3	33013433	5914
6747	HMDB	1	33013433	7207
6748	LDA	2	33013704	5837
6749	LDA	2	33013794	4508
6750	LDA	1	33013799	6209
6751	MetaboAnalyst	1	33013903	2078
6752	XCMS	2	33014185	2690
6753	XCMS online	2	33014185	2690
6754	MetaboAnalyst	1	33014185	3726
6755	METLIN	1	33014185	3804
6756	HMDB	1	33014185	4024
6757	LDA	1	33014185	5499
6758	MetaboAnalyst	2	33014794	1244
6759	Mummichog	2	33014794	2916
6760	mzAccess	1	33016215	1928
6761	HMDB	2	33016215	2741
6762	GNPS	2	33016215	2882
6763	MetaboAnalyst	1	33019551	6492
6764	MetaboAnalyst	1	33019763	9362
6765	apLCMS	1	33024014	7678
6766	xMSanalyzer	1	33024014	7694
6767	mixOmics	1	33024014	8171
6768	MetaboAnalyst	1	33024014	8270
6769	MetaboAnalyst	1	33025530	2879
6770	GNPS	1	33026068	4291
6771	BioCyc	1	33027936	7220
6772	MetaboAnalyst	2	33028628	6079
6773	MetaboAnalyst	1	33033346	11779
6774	HMDB	1	33033375	13050
6775	MZmine 2	1	33033923	9014
6776	mixOmics	2	33036444	6094
6777	MetaboAnalyst	2	33037261	2759
6778	HMDB	1	33041788	6121
6779	LIPID MAPS	1	33041788	6208
6780	MetaboAnalyst	1	33041788	9407
6781	MetaboAnalyst	2	33041831	4019
6782	HMDB	1	33041831	4312
6783	METLIN	1	33041831	4322
6784	LDA	2	33042089	8516
6785	MetaboAnalystR	1	33042192	4012
6786	METLIN	1	33043885	24799
6787	Newton	1	33044904	1067
6788	mixOmics	1	33049981	6664
6789	XCMS	1	33050077	3283
6790	PiMP	1	33050077	3904
6791	MetaboAnalyst	2	33050176	12889
6792	MetScape	1	33050308	5247
6793	HMDB	1	33050369	5349
6794	UniProtKB	1	33050904	13658
6795	GNPS	4	33051377	7196
6796	MetaboAnalyst	1	33051377	8815
6797	XCMS	1	33053160	6954
6798	MZmine 2	1	33053668	13658
6799	LIPID MAPS	1	33053668	14445
6800	MetaboAnalyst	2	33053766	9509
6801	METLIN	1	33053777	6996
6802	MetaboAnalyst	1	33053871	3616
6803	MetaboAnalyst	1	33054133	718
6804	IIS	1	33057137	2430
6805	icoshift	1	33057167	3806
6806	MetaboAnalyst	1	33057764	3911
6807	XCMS	1	33062031	4744
6808	ChemSpider	1	33062444	2730
6809	XCMS	2	33063667	12540
6810	GNPS	3	33066019	1488
6811	MZmine 2	1	33066019	1809
6812	METLIN	1	33066422	5865
6813	HMDB	1	33066485	7704
6814	MetaboAnalyst	2	33066485	7915
6815	MetaboAnalyst	1	33066541	3533
6816	MetAlign	1	33066640	5514
6817	MetaboAnalyst	1	33067464	12161
6818	METLIN	1	33067464	13308
6819	HMDB	1	33067464	13346
6820	MetaboAnalyst	1	33072009	9754
6821	LDA	2	33072010	5310
6822	MetaboAnalyst	1	33072045	9057
6823	MetaboAnalyst	1	33076393	3897
6824	HMDB	1	33076419	6590
6825	METLIN	1	33076419	6660
6826	MetaboAnalyst	2	33076419	9695
6827	CMM	1	33076930	894
6828	MetaboliteDetector	1	33077707	6524
6829	HMDB	1	33077739	3950
6830	MetAlign	1	33077799	6876
6831	MZmine 2	1	33078273	13474
6832	HMDB	1	33078273	13749
6833	Rhea	1	33079623	3259
6834	MetaboliteDetector	1	33080992	2828
6835	XCMS	1	33080992	4779
6836	CAMERA	1	33080992	5273
6837	apLCMS	1	33081124	2517
6838	xMSanalyzer	1	33081124	2528
6839	MetaboLights	1	33081373	7081
6840	TidyMS	1	33081373	7661
6841	LDA	2	33082280	10255
6842	MAVEN	1	33082573	5998
6843	LDA	1	33083493	2968
6844	MetaboAnalyst	1	33086062	16035
6845	Skyline	1	33086062	17625
6846	GNPS	3	33086565	1729
6847	XCMS	1	33086565	2745
6848	HMDB	1	33087042	8869
6849	METLIN	1	33087042	8942
6850	MetaboAnalyst	1	33087820	9225
6851	LDA	1	33092019	5330
6852	VANTED	1	33092030	4525
6853	HMDB	1	33092034	4169
6854	Mummichog	1	33092034	4432
6855	LDA	2	33092137	10504
6856	MetaboAnalyst	1	33092532	4746
6857	HMDB	2	33092571	6397
6858	MAIT	1	33092571	7713
6859	MetaboAnalyst	1	33092650	5803
6860	HMDB	1	33093440	9261
6861	MetaboAnalyst	4	33093530	7595
6862	WikiPathways	1	33093559	778
6863	MetaboAnalyst	1	33093559	5214
6864	MetaboAnalyst	5	33093609	5146
6865	Mummichog	1	33093609	6230
6866	HMDB	1	33093609	7428
6867	MetFrag	1	33093609	7495
6868	MetaboAnalyst	1	33095758	2989
6869	Skyline	2	33096026	352
6870	Newton	1	33096598	2899
6871	GNPS	2	33096635	8560
6872	METLIN	1	33096979	2263
6873	ropls	2	33096979	2876
6874	HMDB	1	33101021	4923
6875	MetaboAnalyst	1	33101021	5057
6876	ropls	1	33101022	4669
6877	ChemSpider	1	33101022	5060
6878	BioCyc	1	33101022	5131
6879	HMDB	1	33101022	5139
6880	MetaboAnalyst	1	33101022	6005
6881	MetAlign	1	33101331	5309
6882	BioCyc	1	33101331	6170
6883	XCMS	1	33101449	3828
6884	MetaboAnalyst	1	33101449	4513
6885	HMDB	1	33101674	9835
6886	MetaboAnalyst	1	33101674	10073
6887	UniProtKB	2	33102464	1305
6888	MetaboAnalyst	2	33103907	3551
6889	LDA	2	33104864	7117
6890	HMDB	1	33105554	13951
6891	LDA	1	33105628	6000
6892	MetFrag	1	33105803	11186
6893	MetaboAnalyst	1	33106349	4927
6894	mixOmics	1	33108391	4653
6895	Mummichog	2	33108391	6486
6896	MetaboAnalyst	3	33108391	6517
6897	SMPDB	1	33108391	7318
6898	HMDB	1	33110217	5076
6899	HMDB	1	33110968	6678
6900	MAVEN	1	33112692	7115
6901	HMDB	1	33112692	12321
6902	METLIN	1	33114083	9571
6903	HMDB	1	33114083	9579
6904	MetaboAnalyst	1	33114083	10053
6905	Newton	1	33114083	10985
6906	GAM	3	33114404	868
6907	HMDB	1	33114410	11535
6908	ChemSpider	1	33114490	5842
6909	HMDB	1	33115522	6577
6910	METLIN	1	33115522	6583
6911	MetaboAnalyst	1	33115522	7056
6912	SMART	2	33117306	2926
6913	MAIT	2	33117384	7698
6914	MetaboAnalyst	1	33117761	9373
6915	icoshift	1	33119606	8221
6916	SMART	2	33120967	650
6917	MetExtract	1	33121096	4164
6918	MetaboDiff	3	33122657	2186
6919	XCMS	1	33123032	4557
6920	ropls	1	33123032	4683
6921	HMDB	5	33123071	4158
6922	MetaboAnalyst	1	33123723	10100
6923	MetFrag	1	33124043	3451
6924	HMDB	1	33125128	4165
6925	LIPID MAPS	1	33125128	4192
6926	METLIN	1	33125128	4250
6927	MetaboAnalyst	1	33125128	4416
6928	MetaboAnalyst	1	33126435	0
6929	UniProtKB	1	33128234	5221
6930	BinBase	3	33133033	8684
6931	MS-FLO	1	33133033	10524
6932	MetaboAnalyst	1	33133033	11745
6933	Mummichog	5	33133033	13303
6934	XCMS	1	33133054	12820
6935	XCMS	1	33133457	4170
6936	XCMS	2	33134312	4266
6937	CAMERA	1	33134312	4441
6938	MetaboAnalyst	1	33134312	6503
6939	ChemSpider	1	33134328	6839
6940	XCMS	2	33134693	10534
6941	MetaboAnalyst	1	33134693	14114
6942	apLCMS	1	33134764	4929
6943	xMSanalyzer	2	33134764	4945
6944	MetaboAnalystR	1	33134764	8213
6945	xMSannotator	1	33134764	9685
6946	HMDB	1	33137957	4562
6947	MetaboAnalyst	1	33137957	5358
6948	MeltDB	1	33137957	5915
6949	Skyline	1	33138039	9022
6950	LIPID MAPS	1	33138039	9584
6951	MetaboAnalyst	1	33138039	10974
6952	HMDB	1	33138215	2312
6953	METLIN	1	33138215	2342
6954	MetaboAnalyst	1	33138215	3496
6955	MetaboAnalyst	1	33138273	3839
6956	ORCA	3	33138275	1986
6957	HMDB	1	33138805	8390
6958	MetaboAnalyst	1	33139708	8245
6959	GNPS	3	33139882	5842
6960	GAM	4	33141308	11286
6961	XCMS	3	33142471	10852
6962	MetaboAnalyst	1	33142471	11238
6963	METLIN	1	33142471	12393
6964	HMDB	1	33142471	12430
6965	MetaboAnalyst	4	33142543	5972
6966	MetaboAnalyst	2	33142690	4439
6967	MetaboAnalyst	2	33142859	4881
6968	HMDB	1	33142859	8413
6969	MetAlign	1	33143004	4234
6970	HMDB	1	33143004	4593
6971	GNPS	2	33143202	12093
6972	MetaboLights	1	33144591	9870
6973	XCMS	1	33144604	9423
6974	MetaboAnalyst	4	33145006	9331
6975	XCMS	1	33147768	3277
6976	XCMS online	1	33147768	3277
6977	ropls	1	33147768	4996
6978	MetaboAnalyst	1	33147768	5271
6979	Mummichog	1	33147768	5329
6980	SMART	1	33148611	6017
6981	Skyline	1	33150952	8019
6982	GAM	6	33153091	514
6983	MetaboAnalyst	1	33153091	10553
6984	AMDIS	3	33153225	1003
6985	Metab	1	33153225	2304
6986	MetaboAnalyst	1	33153225	2453
6987	MZmine 2	1	33154449	5593
6988	VANTED	1	33154644	3268
6989	MetaboAnalyst	1	33156835	5091
6990	MeRy-B	1	33156865	3110
6991	UniProtKB	2	33162891	966
6992	LDA	1	33162984	5648
6993	MetaboAnalyst	2	33162984	10470
6994	METLIN	1	33162984	11444
6995	MetaboAnalyst	2	33163791	6021
6996	MZmine 2	1	33167375	12489
6997	GNPS	2	33167375	13240
6998	CFM-ID	1	33167375	14383
6999	ChemSpider	1	33167507	1455
7000	XCMS	1	33171998	8411
7001	HMDB	1	33171998	10195
7002	HMDB	2	33172086	7647
7003	LIPID MAPS	1	33172086	7656
7004	MetaboAnalyst	1	33172086	11484
7005	GNPS	3	33172970	8914
7006	XCMS	2	33173435	2989
7007	MetaboAnalyst	2	33173435	3924
7008	MetaboAnalyst	2	33174185	7740
7009	XCMS	1	33175875	3354
7010	ChemSpider	1	33176684	5028
7011	HMDB	1	33176684	5197
7012	MetaboAnalyst	1	33176684	5889
7013	ropls	2	33177242	8345
7014	MetaboAnalyst	1	33177242	8953
7015	GAM	1	33177599	3557
7016	MetaboAnalyst	1	33177599	7949
7017	ORCA	1	33177605	15354
7018	HMDB	1	33177654	5907
7019	METLIN	1	33177654	5936
7020	ropls	1	33177654	6588
7021	HMDB	1	33178024	9580
7022	MetaboAnalyst	1	33178024	9731
7023	AMDIS	1	33178169	9994
7024	MetaboAnalyst	1	33178317	2671
7025	HMDB	1	33178317	3110
7026	MetaboAnalyst	2	33178572	3449
7027	METLIN	1	33182315	1358
7028	MetaboAnalyst	2	33182594	5519
7029	HMDB	1	33182594	5682
7030	METLIN	1	33182594	5710
7031	MetaboAnalyst	1	33182805	6701
7032	HMDB	1	33184308	5984
7033	MetaboAnalyst	1	33184308	6842
7034	MetaboAnalyst	1	33184349	9268
7035	HMDB	1	33184375	9086
7036	GNPS	3	33184402	9367
7037	XCMS	1	33186350	5365
7038	XCMS online	1	33186350	5365
7039	MetaboAnalyst	1	33186350	5605
7040	MetaboLights	1	33187120	8466
7041	icoshift	1	33187152	3830
7042	LDA	1	33187155	6463
7043	HMDB	1	33187155	8799
7044	MetaboAnalyst	1	33187201	7493
7045	MetaboAnalyst	2	33192530	2429
7046	GAM	1	33192604	8199
7047	El-MAVEN	1	33192610	3335
7048	MAVEN	1	33192610	3338
7049	MetaboAnalyst	1	33192610	3595
7050	MetaboLights	1	33193245	4531
7051	icoshift	1	33193515	6510
7052	SMART	1	33193712	2824
7053	ChemSpider	1	33194384	338
7054	DrugBank	1	33194384	1148
7055	MetaboAnalyst	1	33194790	7732
7056	XCMS	1	33194878	1093
7057	MetaboAnalyst	1	33194878	1876
7058	HMDB	1	33195257	4183
7059	LDA	2	33195257	5871
7060	MetaboAnalystR	1	33195525	9269
7061	HMDB	1	33195612	7315
7062	ConsensusPathDB	3	33196681	3242
7063	SMPDB	1	33196681	4123
7064	LDA	1	33198236	4427
7065	XCMS	1	33198236	11141
7066	CAMERA	1	33198236	11420
7067	METLIN	1	33198236	17551
7068	MetFrag	1	33198236	17592
7069	SMART	4	33198241	7320
7070	HMDB	1	33199803	8053
7071	XCMS	1	33199803	8293
7072	MetaboAnalyst	1	33199803	9164
7073	MZmine 2	1	33199804	7013
7074	GNPS	3	33199804	7128
7075	MetaboAnalyst	1	33199820	10164
7076	mixOmics	1	33201860	7264
7077	icoshift	1	33202669	10070
7078	MetaboAnalyst	1	33202684	7961
7079	MetaboLights	1	33202890	5462
7080	MetaboAnalyst	2	33203021	4429
7081	XCMS	1	33203688	12402
7082	MetaboLights	2	33203688	13874
7083	MAVEN	1	33203858	3007
7084	MetaboAnalyst	1	33204525	3610
7085	ChemSpider	1	33207638	6340
7086	MetaboAnalyst	2	33207638	6597
7087	LDA	1	33207675	6856
7088	Skyline	1	33207699	7725
7089	MetaboAnalyst	1	33207699	8455
7090	MetaboLights	1	33207699	8709
7091	MetaboAnalyst	3	33207837	5302
7092	mixOmics	1	33207993	2377
7093	METLIN	1	33208098	6791
7094	MetFrag	3	33208098	6981
7095	ORCA	1	33208737	3408
7096	mixOmics	3	33208745	6922
7097	FALCON	7	33208749	3585
7098	PathoLogic	1	33208749	20539
7099	MetaboAnalyst	1	33208758	6474
7100	XCMS	1	33211407	2656
7101	MetaboAnalyst	1	33211407	3767
7102	LDA	1	33213092	9461
7103	XCMS	1	33213095	479
7104	Mummichog	2	33213095	3982
7105	HMDB	1	33213368	6849
7106	mQTL	1	33214566	5852
7107	MetaboAnalyst	1	33217646	2924
7108	Newton	2	33217921	2969
7109	MetaboAnalyst	1	33218188	2876
7110	METLIN	1	33222147	11146
7111	HMDB	2	33222147	11175
7112	MetaboAnalyst	1	33222147	12066
7113	FALCON	1	33222596	1028
7114	LDA	1	33222612	3675
7115	MetaboAnalyst	1	33225162	7391
7116	PathVisio	1	33225162	7500
7117	WikiPathways	1	33225162	7643
7118	UniProtKB	1	33228196	8991
7119	Pathview	1	33228196	10663
7120	mixOmics	1	33228226	8061
7121	UniProtKB	1	33229573	15456
7122	AMDIS	2	33235278	6132
7123	MET-IDEA	1	33235278	6175
7124	MetaboAnalyst	2	33235278	7050
7125	MetaboLights	1	33235278	8487
7126	MetaboliteDetector	1	33235278	12654
7127	HMDB	1	33235303	7050
7128	MetaboAnalyst	1	33235303	11839
7129	XCMS	1	33235638	8978
7130	AMDIS	1	33238400	4969
7131	MetaboAnalyst	1	33238400	7494
7132	MetaMapR	3	33238400	8555
7133	MZmine 2	1	33238431	6993
7134	MetaboAnalyst	1	33238497	7252
7135	MelonnPan	1	33238618	3698
7136	LDA	1	33238618	7919
7137	ChemSpider	1	33239694	358
7138	MetaboAnalyst	2	33239722	6036
7139	MetaboAnalyst	1	33240093	6234
7140	HMDB	1	33240917	2116
7141	METLIN	1	33240917	2147
7142	XCMS	2	33240942	1750
7143	MetaboLights	1	33240942	2384
7144	MetaboAnalyst	1	33240942	4593
7145	XCMS	2	33243144	9541
7146	CAMERA	1	33243144	9670
7147	LIPID MAPS	1	33243144	9796
7148	MetaboAnalyst	1	33243243	7173
7149	XCMS	2	33243278	4719
7150	AMDIS	1	33244630	5454
7151	XCMS	1	33246956	10080
7152	CAMERA	1	33246956	10316
7153	METLIN	1	33246956	13323
7154	mzAccess	2	33249526	1817
7155	HMDB	2	33249526	2993
7156	GNPS	2	33249526	3218
7157	Skyline	2	33253182	17466
7158	XCMS	1	33253201	8073
7159	MetaboAnalyst	1	33253201	9528
7160	HMDB	1	33255384	2632
7161	MetaboAnalyst	1	33255518	6939
7162	SMART	1	33255547	3591
7163	MetaboAnalyst	1	33255770	6485
7164	SMART	1	33255784	1185
7165	AMDIS	1	33255896	2835
7166	MetaboAnalyst	1	33255896	3846
7167	SMPDB	1	33255926	3322
7168	MAVEN	2	33256042	5053
7169	MetaboAnalyst	3	33256042	5936
7170	LDA	1	33256731	3994
7171	HMDB	1	33256731	7741
7172	ChemSpider	1	33256731	7871
7173	MetaboAnalyst	2	33256731	8384
7174	ADAP-GC	1	33256802	8341
7175	MetaboAnalyst	1	33257643	2888
7176	HMDB	1	33257643	3040
7177	MetaboLights	1	33257713	9114
7178	SMART	1	33257855	13539
7179	XCMS	1	33259543	6257
7180	MetaboAnalyst	1	33259543	8644
7181	HMDB	1	33259548	1506
7182	MetaboAnalyst	1	33259548	1648
7183	MassTRIX	2	33260822	3891
7184	HMDB	1	33260822	3924
7185	ChemSpider	1	33260822	4189
7186	MetaboAnalyst	1	33260822	4897
7187	UniProtKB	2	33261190	6288
7188	WikiPathways	1	33262249	2727
7189	SMART	1	33262396	1541
7190	PathVisio	1	33264316	9844
7191	MetaboAnalyst	1	33264316	10115
7192	MetaboAnalyst	3	33266049	7178
7193	HMDB	1	33266099	8739
7194	METLIN	1	33266099	8776
7195	XCMS	1	33266201	4842
7196	ChemSpider	1	33266201	5840
7197	MetaboAnalyst	1	33266330	6720
7198	MetaboAnalyst	1	33267585	16112
7199	MetaboAnalyst	1	33267887	11338
7200	LDA	1	33268363	3457
7201	MetaboAnalyst	1	33269100	6248
7202	METLIN	1	33271860	14122
7203	LipidXplorer	1	33273595	6636
7204	PiMP	1	33275612	10502
7205	Skyline	2	33276584	6383
7206	GNPS	3	33276611	14329
7207	MetaboLights	1	33277521	13792
7208	XCMS	1	33281602	4250
7209	METLIN	1	33281602	5266
7210	HMDB	1	33281602	5305
7211	MetaboAnalyst	1	33281602	5706
7212	MetaboAnalyst	1	33282726	6637
7213	XCMS	1	33282958	3022
7214	HMDB	1	33282958	5000
7215	MetaboLights	1	33284478	5751
7216	AMDIS	1	33287102	6484
7217	SMART	1	33287703	454
7218	MetaboAnalyst	1	33288952	11307
7219	XCMS	1	33291756	4397
7220	HMDB	2	33291756	6898
7221	MetaboAnalyst	2	33292300	5667
7222	HMDB	1	33292300	6448
7223	ChemSpider	1	33292300	6521
7224	mixOmics	1	33292307	5526
7225	XCMS	2	33292307	6824
7226	MetaboAnalyst	1	33292307	7330
7227	MetaboLights	1	33292307	8416
7228	XCMS	3	33292338	8219
7229	HMDB	2	33292338	9605
7230	MetaboAnalyst	2	33292338	11423
7231	MetaboAnalyst	4	33292391	4758
7232	HMDB	1	33292391	6085
7233	METLIN	1	33292391	6126
7234	ConsensusPathDB	1	33292639	6465
7235	BinBase	1	33292701	5918
7236	MetaboAnalyst	2	33292701	6150
7237	MAVEN	1	33293381	9414
7238	MetaboAnalyst	2	33294127	2302
7239	LDA	5	33294938	5693
7240	ChemSpider	1	33296063	7379
7241	LIPID MAPS	1	33296063	7419
7242	GNPS	1	33296063	7443
7243	MetFrag	1	33296063	7449
7244	SMART	1	33297327	530
7245	VANTED	1	33298635	6590
7246	MetaboAnalyst	1	33298924	18081
7247	SMART	1	33298946	4597
7248	METLIN	1	33299048	4644
7249	HMDB	1	33299048	4655
7250	MetaboAnalyst	1	33299048	6215
7251	MetaboAnalyst	2	33302441	2806
7252	AMDIS	1	33302448	6323
7253	MetaboAnalyst	1	33302528	6633
7254	HMDB	1	33302593	3655
7255	XCMS	1	33302870	8758
7256	HMDB	1	33303834	12163
7257	METLIN	1	33303880	13034
7258	MZmine 2	1	33303913	3193
7259	VANTED	1	33305258	2426
7260	icoshift	1	33305277	3446
7261	GNPS	1	33311514	6432
7262	METLIN	1	33311514	6650
7263	ICT	1	33315148	105
7264	MoNA	1	33315148	5549
7265	MetaboAnalyst	1	33315148	7557
7266	MetaboAnalyst	7	33315875	5856
7267	MRMPROBS	2	33317193	2893
7268	MetaboAnalyst	1	33317193	5932
7269	MZmine 2	1	33318532	3631
7270	MetaboAnalyst	3	33318532	3855
7271	ChemSpider	1	33318532	4730
7272	MetaboAnalyst	1	33318553	8316
7273	Skyline	1	33318574	4782
7274	mixOmics	1	33318574	5270
7275	MetaboAnalyst	2	33319750	6384
7276	HMDB	1	33320902	2745
7277	ropls	1	33321888	3501
7278	mixOmics	1	33321888	3511
7279	MetaboAnalyst	1	33321888	6293
7280	SMPDB	1	33321888	6461
7281	XCMS	2	33322065	5541
7282	LIPID MAPS	1	33322065	5741
7283	MetaboAnalyst	1	33322104	7246
7284	MetaboAnalyst	1	33322743	7408
7285	MetaboAnalyst	1	33323506	6482
7286	LDA	1	33323514	5142
7287	MetaboAnalyst	1	33323514	5856
7288	XCMS	1	33325755	19352
7289	METLIN	1	33325755	20490
7290	HMDB	1	33325755	20545
7291	MetaboAnalyst	2	33325755	20631
7292	MetaboAnalyst	2	33328455	5114
7293	METLIN	1	33328455	6247
7294	MetaboAnalyst	1	33329411	9168
7295	BinBase	1	33329496	14970
7296	LipidXplorer	1	33329576	8019
7297	HMDB	1	33329635	7043
7298	METLIN	1	33329635	7112
7299	XCMS	1	33329734	5716
7300	HMDB	1	33330044	5094
7301	MetaboAnalyst	2	33330044	6286
7302	HMDB	1	33330117	1776
7303	SMART	1	33330447	2517
7304	MetaboLights	1	33330447	12796
7305	HMDB	1	33330447	14231
7306	LIPID MAPS	1	33330447	14266
7307	MetaboAnalyst	1	33330447	14705
7308	Skyline	1	33330447	15768
7309	METLIN	1	33330552	4333
7310	MetaboAnalyst	1	33330552	4449
7311	ChemSpider	1	33334025	4022
7312	MetaboAnalyst	1	33335149	16462
7313	NMRProcFlow	1	33339337	12264
7314	MetaboAnalyst	1	33339337	12419
7315	MetaboAnalyst	1	33339375	6474
7316	HMDB	1	33339375	6883
7317	METLIN	1	33339375	6931
7318	LDA	2	33339953	4804
7319	BioCyc	3	33343536	5130
7320	MelonnPan	5	33343536	7208
7321	XCMS	2	33343553	9098
7322	METLIN	1	33343553	10533
7323	MetaboAnalyst	2	33343553	11342
7324	MetaboAnalyst	1	33343599	6013
7325	MetFrag	1	33343599	9555
7326	ChemSpider	1	33343599	9635
7327	DrugBank	1	33343682	455
7328	mzR	1	33344495	10033
7329	ChemSpider	1	33347309	5173
7330	LDA	1	33347309	6366
7331	GNPS	2	33347500	4603
7332	MZmine 2	2	33347500	5787
7333	SMART	1	33348553	3432
7334	MetaboAnalystR	1	33348685	10332
7335	mixOmics	1	33348685	12251
7336	METLIN	1	33348766	3010
7337	LDA	2	33348766	3167
7338	ICT	4	33349631	4508
7339	MetaboAnalyst	1	33351786	2768
7340	WikiPathways	2	33351794	562
7341	XCMS	1	33352793	2297
7342	CluMSID	1	33352793	3320
7343	MetFrag	1	33352793	4573
7344	MetaboAnalyst	1	33352805	2833
7345	icoshift	1	33352805	3365
7346	ChemSpider	1	33353233	4346
7347	HMDB	1	33353233	5118
7348	SMPDB	1	33353233	5283
7349	HMDB	2	33353236	29
7350	GNPS	3	33361318	9360
7351	MetaboAnalyst	4	33361318	10693
7352	MoNA	1	33361318	12958
7353	ChemSpider	1	33362263	1847
7354	MetaboAnalyst	1	33362263	2691
7355	MZmine 2	1	33362543	5701
7356	GNPS	9	33362543	6442
7357	ropls	1	33362869	5438
7358	MetaboAnalyst	3	33363202	9489
7359	MetaboAnalyst	1	33363546	4626
7360	MetaboAnalyst	2	33363559	14699
7361	HMDB	1	33363571	2185
7362	HMDB	1	33364938	2476
7363	MetaboAnalyst	1	33364938	2616
7364	MetaboAnalyst	2	33364971	9081
7365	BinBase	2	33365033	2658
7366	MetaboAnalyst	3	33365033	3186
7367	XCMS	2	33367148	4013
7368	CAMERA	1	33367148	4197
7369	ChemSpider	2	33370295	122
7370	MZmine 2	1	33371387	3446
7371	MetaboAnalyst	2	33374417	7373
7372	MIDAS	1	33374464	4082
7373	IPO	1	33374845	7507
7374	XCMS	2	33374857	7680
7375	MetaboAnalyst	3	33374857	7712
7376	METLIN	1	33374857	10406
7377	METLIN	3	33374892	2975
7378	MetaboAnalyst	1	33375173	10676
7379	ChemSpider	2	33375472	14439
7380	crmn	2	33375624	6597
7381	MetNorm	1	33375624	7103
7382	MetaboAnalyst	1	33376359	5158
7383	MetaboAnalyst	1	33376499	2397
7384	MetaboAnalystR	1	33376502	4858
7385	MetaboAnalyst	1	33376601	4040
7386	HMDB	1	33379404	4422
7387	MetaboliteDetector	1	33380899	11709
7388	MetaboAnalyst	1	33380899	12287
7389	LDA	3	33381090	3867
7390	MetaboAnalyst	1	33381093	6180
7391	IsoMS	2	33381523	4536
7392	MyCompoundID	1	33381523	5804
7393	MetaboAnalyst	2	33382700	12981
7394	MetaboAnalyst	2	33382876	4536
7395	HMDB	2	33383698	1199
7396	GNPS	2	33383796	5590
7397	ChemSpider	1	33384418	7786
7398	MetaboLights	1	33384418	9594
7399	HMDB	1	33390941	8904
7400	METLIN	1	33390941	8956
7401	MetaboAnalyst	1	33390941	9577
7402	HMDB	1	33390982	10580
7403	MetaboAnalyst	1	33391044	1767
7404	HMDB	1	33391196	2171
7405	METLIN	1	33391196	2185
7406	MetaboLights	1	33391196	2673
7407	LDA	1	33391246	6666
7408	XCMS	1	33391334	7307
7409	MetaboAnalyst	1	33391334	8724
7410	MetaboAnalyst	1	33391487	11964
7411	MetaboAnalyst	1	33392105	8816
7412	METLIN	1	33392217	2389
7413	HMDB	1	33392217	2427
7414	MetaboAnalyst	1	33392217	2753
7415	MetaboAnalyst	3	33392224	2746
7416	BioCyc	1	33392255	2686
7417	HMDB	1	33392255	2915
7418	MetaboAnalyst	2	33392255	3900
7419	MetaboAnalyst	2	33396419	6341
7420	LDA	1	33396441	3609
7421	MetaboAnalyst	1	33396441	5532
7422	Metab	1	33396473	3833
7423	HMDB	1	33396480	2957
7424	GNPS	9	33396687	9013
7425	MZmine 2	1	33396687	12087
7426	CFM-ID	1	33396687	15157
7427	LIQUID	1	33396843	4184
7428	MetaboAnalyst	1	33396843	6593
7429	MetaboAnalyst	2	33398019	15459
7430	GAM	1	33398099	11902
7431	HMDB	2	33398476	12989
7432	MetaboAnalyst	1	33400690	6963
7433	IsoMS	3	33401431	2667
7434	IsoMS-Quant	1	33401431	2868
7435	VANTED	1	33401680	2102
7436	mixOmics	1	33401746	5556
7437	LDA	2	33401746	6001
7438	XCMS	1	33402348	6152
7439	BioCyc	1	33402348	6354
7440	HMDB	1	33402844	5144
7441	METLIN	1	33402844	5154
7442	HMDB	1	33402881	1713
7443	MetaboAnalyst	1	33402881	2311
7444	Skyline	1	33402881	2551
7445	XCMS	1	33403280	3059
7446	METLIN	1	33403305	5559
7447	SMART	1	33407149	2589
7448	LDA	1	33407409	5495
7449	ChemSpider	1	33407409	7125
7450	MetaboAnalyst	2	33407555	12146
7451	GAM	1	33407779	5313
7452	SMART	1	33408738	542
7453	SMART	1	33409299	4922
7454	HMDB	1	33409869	9750
7455	METLIN	1	33409869	9818
7456	DrugBank	4	33411719	3
7457	ropls	1	33412999	3273
7458	SMART	1	33413404	6414
7459	ChemSpider	1	33413404	7906
7460	HMDB	1	33413411	5154
7461	METLIN	1	33413411	5160
7462	MetaboAnalyst	1	33413450	10963
7463	HMDB	1	33413450	11213
7464	BioCyc	2	33413621	16023
7465	HMDB	1	33414730	10849
7466	MetaboAnalyst	1	33414730	10906
7467	Mummichog	1	33415121	4900
7468	XCMS	1	33415132	6412
7469	LIPID MAPS	1	33415132	8782
7470	HMDB	1	33415132	8821
7471	METLIN	1	33415132	8849
7472	HMDB	2	33416496	36773
7473	METLIN	1	33416496	36803
7474	LIPID MAPS	1	33416496	41448
7475	MetaboAnalyst	1	33416496	41529
7476	CMM	6	33417618	2258
7477	MetaboAnalyst	1	33417618	11047
7478	GNPS	4	33418911	8808
7479	AMDIS	1	33418964	5606
7480	ChemSpider	1	33419030	4445
7481	MetaboAnalyst	2	33419189	6775
7482	MetaboAnalyst	1	33420045	6786
7483	LDA	1	33420064	12449
7484	MetAlign	1	33420064	18224
7485	HMDB	1	33420162	11678
7486	MetAlign	1	33420169	4452
7487	AMDIS	1	33420340	33414
7488	SpectConnect	1	33420340	33442
7489	METLIN	1	33423040	6314
7490	HMDB	1	33423040	6322
7491	MIDAS	6	33424566	2797
7492	MetAlign	2	33424907	4203
7493	MSClust	2	33424907	5006
7494	MetaboLights	1	33426374	355
7495	LDA	1	33426525	3699
7496	HMDB	1	33428599	6655
7497	MetaboAnalyst	1	33428599	7345
7498	XCMS	1	33429872	3362
7499	MoNA	1	33429872	3968
7500	MetFrag	1	33429872	3996
7501	MetAlign	2	33429987	3794
7502	HMDB	1	33429987	5932
7503	MS-FINDER	1	33430143	7348
7504	CFM-ID	1	33430143	7359
7505	MetFrag	1	33430143	7399
7506	FingerID	1	33430143	7416
7507	MetaboAnalyst	1	33430143	7720
7508	MetaboAnalyst	1	33430468	4238
7509	ChemSpider	1	33430473	1526
7510	LDA	1	33430687	3756
7511	MetaboAnalyst	1	33430702	6303
7512	XCMS	1	33430779	4836
7513	LDA	1	33431052	9515
7514	GNPS	2	33431904	12490
7515	ropls	1	33435322	3225
7516	MetaboAnalyst	2	33435339	5294
7517	XCMS	2	33436670	5596
7518	PAPi	2	33436670	7597
7519	SMART	1	33436960	7381
7520	MetaboAnalyst	1	33439228	10183
7521	LIPID MAPS	1	33440400	6360
7522	SMART	1	33440756	1613
7523	MetaboAnalyst	1	33441568	8099
7524	MetaboAnalyst	2	33444819	10520
7525	UniProtKB	26	33445429	0
7526	Rhea	5	33445429	1939
7527	MetaboAnalyst	1	33446684	11857
7528	MetaboAnalyst	1	33446721	8919
7529	SMPDB	1	33446721	9008
7530	mixOmics	2	33446779	4654
7531	UniProtKB	1	33446901	5009
7532	NMRProcFlow	1	33451115	4826
7533	PathoLogic	1	33452249	33305
7534	MetaboAnalyst	1	33461307	5433
7535	mixOmics	1	33461385	8520
7536	HMDB	1	33462176	5000
7537	LIPID MAPS	1	33462176	5339
7538	MetaboAnalyst	1	33462176	6012
7539	MetaboAnalyst	2	33466323	4824
7540	HMDB	1	33466475	5334
7541	METLIN	1	33466475	5376
7542	MetaboAnalyst	1	33466475	5946
7543	AMDIS	1	33466549	1011
7544	MetaboAnalyst	1	33466549	2996
7545	LDA	2	33466889	4748
7546	MetaboAnalyst	1	33467110	5697
7547	METLIN	1	33467110	5764
7548	HMDB	2	33467557	3581
7549	ropls	1	33467557	4287
7550	MetaboAnalyst	2	33467662	6474
7551	IsoCor	1	33468252	6905
7552	LDA	3	33469094	2313
7553	GNPS	3	33469429	26838
7554	HMDB	1	33469429	26955
7555	GNPS-MassIVE	1	33469429	27188
7556	METLIN	1	33469506	4731
7557	HMDB	2	33476461	5931
7558	MAVEN	2	33477427	5093
7559	MetaboAnalyst	1	33477427	5395
7560	METLIN	1	33477743	5759
7561	MetaboAnalystR	2	33477940	4829
7562	MetaboAnalyst	1	33478060	7002
7563	XCMS	2	33478393	4564
7564	CAMERA	1	33478393	4733
7565	HMDB	1	33478393	5015
7566	METLIN	1	33478393	5039
7567	PaintOmics	1	33479266	5530
7568	METLIN	1	33479270	9461
7569	HMDB	1	33479270	9475
7570	UniProtKB	1	33479482	3762
7571	SMART	1	33483501	2190
7572	UniProtKB	1	33483501	10945
7573	MAVEN	1	33483501	18264
7574	MetaboAnalyst	1	33483501	20177
7575	HMDB	1	33483512	4039
7576	HMDB	1	33487791	6830
7577	MetaboAnalyst	2	33487791	7022
7578	MetaboAnalyst	1	33488101	5613
7579	HMDB	1	33488101	5798
7580	METLIN	1	33488101	5807
7581	MetaboliteDetector	1	33488537	6246
7582	XCMS	1	33488537	7123
7583	MetaboAnalyst	1	33488655	4401
7584	HMDB	1	33488655	6189
7585	MetFrag	1	33488726	12184
7586	ChemSpider	1	33488726	12359
7587	METLIN	1	33488726	12430
7588	XCMS	1	33490088	3601
7589	HMDB	2	33490775	2592
7590	XCMS	2	33490775	2984
7591	CAMERA	1	33490775	3132
7592	XCMS	2	33494144	4827
7593	CAMERA	2	33494144	5265
7594	Workflow4Metabolomics	1	33494144	5380
7595	HMDB	1	33494144	6058
7596	METLIN	2	33494144	6068
7597	MetaboLights	2	33494351	305
7598	HMDB	1	33494351	1050
7599	HMDB	1	33497373	14422
7600	BioCyc	1	33497373	15228
7601	MetaboAnalyst	3	33497421	11030
7602	MetaboAnalyst	2	33498542	2657
7603	ropls	1	33498542	2685
7604	mixOmics	1	33498542	2699
7605	SMPDB	1	33498542	4725
7606	MetaboAnalyst	2	33498757	4121
7607	AMDIS	2	33499165	6609
7608	SpectConnect	2	33499165	6904
7609	MetaboAnalyst	1	33499165	9472
7610	MZmine 2	1	33499273	1464
7611	MetaboAnalyst	2	33499273	2656
7612	HMDB	1	33499273	4053
7613	GNPS	1	33499273	4085
7614	mixOmics	1	33499981	3734
7615	XCMS	1	33499981	4676
7616	MetaboAnalyst	1	33499981	5353
7617	HMDB	1	33502243	4222
7618	MetaboNetworks	1	33502243	6966
7619	MetaboAnalyst	1	33502259	18811
7620	mQTL	3	33502483	751
7621	MetaboAnalyst	1	33503424	11385
7622	HMDB	2	33505234	2187
7623	MetaboAnalyst	1	33505234	3080
7624	ChemSpider	1	33510749	5674
7625	MetaboAnalyst	1	33510749	8579
7626	XCMS	1	33511154	3781
7627	MetaboAnalyst	1	33511154	5167
7628	NMRProcFlow	1	33513807	4235
7629	XCMS	1	33513853	7060
7630	CAMERA	1	33513853	7074
7631	MetaboAnalyst	2	33513853	7874
7632	CFM-ID	1	33513853	8900
7633	MetFrag	1	33513853	8913
7634	XCMS	3	33514042	3632
7635	METLIN	2	33514042	3692
7636	HAMMER	1	33515286	5276
7637	GNPS	5	33515432	8943
7638	FingerID	2	33515432	11683
7639	MetaboAnalyst	1	33515432	14197
7640	ropls	1	33519440	5673
7641	HMDB	1	33521083	4032
7642	MetaboAnalyst	1	33521449	3579
7643	HMDB	1	33521449	5125
7644	TargetSearch	1	33523261	12263
7645	MetaboAnalystR	2	33523930	3276
7646	MZmine 2	1	33525412	2585
7647	GNPS	1	33525412	4501
7648	MetaboAnalyst	1	33525435	2369
7649	MetaboAnalyst	1	33529218	8454
7650	MZmine 2	1	33531542	5032
7651	MetaboAnalyst	1	33531542	5394
7652	MSClust	3	33531553	9458
7653	XCMS	3	33531582	6806
7654	XCMS online	1	33531582	6806
7655	CFM-ID	2	33531582	9844
7656	AMDIS	1	33532002	2843
7657	MetaboAnalyst	2	33532002	3864
7658	METLIN	1	33536529	12920
7659	LIPID MAPS	1	33536529	13158
7660	MetaboAnalyst	1	33539341	4608
7661	HMDB	1	33539421	2840
7662	METLIN	1	33539421	2859
7663	ChemSpider	1	33540686	3171
7664	METLIN	1	33540686	3188
7665	icoshift	1	33540754	5143
7666	GNPS	1	33540787	8077
7667	Mummichog	1	33542199	5271
7668	METLIN	1	33544161	6033
7669	mixOmics	1	33544865	6710
7670	MZmine 2	1	33546180	10437
7671	MetaboAnalyst	1	33546180	11534
7672	ChemSpider	1	33546196	10093
7673	METLIN	1	33546377	8424
7674	MetaboAnalyst	1	33546377	10231
7675	HMDB	1	33546377	10526
7676	LIPID MAPS	1	33546377	10541
7677	MetaboAnalyst	2	33546743	4586
7678	MetaboAnalystR	1	33546743	4878
7679	LIPID MAPS	1	33547383	1533
7680	MetaboAnalyst	1	33547384	8054
7681	XCMS	1	33549051	5788
7682	CAMERA	1	33549051	5990
7683	HMDB	1	33549051	7740
7684	GAM	1	33549144	7099
7685	COLMAR	1	33552029	4255
7686	MetaboAnalyst	1	33553267	1677
7687	METLIN	1	33553289	4947
7688	MetaboAnalyst	2	33553289	5392
7689	XCMS	1	33557667	8254
7690	HMDB	1	33557667	8475
7691	mixOmics	1	33557667	13134
7692	HMDB	1	33557959	10940
7693	ropls	2	33557959	11350
7694	LDA	2	33558627	5117
7695	MetaboAnalyst	2	33559270	4693
7696	MetaboAnalyst	1	33562384	2455
7697	SMART	1	33562387	2851
7698	MetaboAnalyst	1	33563321	5686
7699	SMPDB	1	33563321	5993
7700	BinBase	3	33563783	4303
7701	MetaboAnalyst	3	33563783	7346
7702	MZmine 2	1	33563821	7539
7703	XCMS	2	33563828	11835
7704	XCMS online	1	33563828	11835
7705	MetaboAnalyst	1	33563828	12202
7706	GNPS	2	33563828	12938
7707	LIPID MAPS	2	33564392	2963
7708	HMDB	1	33564793	6135
7709	MoNA	1	33564793	6211
7710	Skyline	1	33564793	6433
7711	missForest	1	33567505	8075
7712	MetaboAnalyst	3	33568175	7559
7713	MetaboAnalystR	1	33572231	5494
7714	MetaboAnalyst	1	33572277	7967
7715	mixOmics	1	33572291	4144
7716	LDA	1	33572291	5837
7717	DrugBank	1	33572445	8109
7718	METLIN	1	33572445	8156
7719	MS-FINDER	1	33572445	8436
7720	GNPS	1	33572445	9017
7721	CAMERA	1	33572622	6679
7722	MZmine 2	1	33572622	7593
7723	LipidXplorer	1	33572690	5992
7724	HMDB	1	33572739	2399
7725	SMPDB	2	33572739	2414
7726	MetaboAnalyst	1	33572739	3512
7727	SMART	1	33573171	0
7728	MZmine 2	1	33573182	6009
7729	GNPS	1	33573182	7215
7730	MetFrag	1	33573182	7708
7731	BinBase	2	33573321	4686
7732	MetaboAnalyst	1	33573321	5215
7733	Workflow4Metabolomics	1	33574413	7839
7734	MetaboAnalyst	1	33574413	9118
7735	MetaboAnalyst	2	33575283	4915
7736	mixOmics	2	33575625	5056
7737	XCMS	1	33575816	6991
7738	MetaboAnalyst	1	33577623	3714
7739	GNPS	12	33578887	4474
7740	NMRProcFlow	1	33578992	7857
7741	HMDB	1	33578995	6746
7742	MetaboAnalyst	3	33579268	3462
7743	GNPS	5	33579268	4783
7744	MS-FINDER	2	33579268	5143
7745	MZmine 2	1	33579268	6706
7746	PARADISe	1	33579683	8558
7747	XCMS	1	33579905	8884
7748	CAMERA	1	33579905	9314
7749	HMDB	1	33579905	10562
7750	METLIN	1	33579905	10572
7751	MetaboAnalyst	1	33579905	10875
7752	HMDB	1	33580129	6297
7753	MetaboLights	1	33580996	3964
7754	METLIN	1	33580996	4122
7755	MetaboAnalyst	1	33580996	4213
7756	XCMS	1	33584283	6510
7757	MetaboAnalyst	2	33584288	1180
7758	HMDB	1	33584288	2053
7759	MetaboAnalyst	2	33584759	8974
7760	BiGG Models	3	33585414	546
7761	Escher	1	33585414	2094
7762	LIQUID	1	33585414	9717
7763	MZmine 2	1	33585414	10130
7764	AMDIS	1	33585860	5261
7765	XCMS	2	33589511	13007
7766	CAMERA	1	33589511	13358
7767	HMDB	1	33589511	13588
7768	SMART	1	33589820	15946
7769	mQTL	6	33592061	9731
7770	MetaboAnalyst	1	33593748	2289
7771	LIPID MAPS	1	33594255	958
7772	MetaboAnalyst	1	33596203	5240
7773	MetaboAnalystR	1	33596203	5335
7774	MetScape	1	33596203	6145
7775	XCMS	1	33596934	5476
7776	MetaboAnalyst	1	33596934	5962
7777	HMDB	1	33596934	7983
7778	MetaboAnalyst	2	33597930	4991
7779	HMDB	3	33598460	2553
7780	MetaboAnalyst	3	33598460	4677
7781	ICT	1	33599728	10041
7782	MetaboAnalyst	1	33600468	3965
7783	XCMS	1	33602246	5110
7784	MetaboAnalyst	1	33602246	8119
7785	LDA	2	33603667	4894
7786	mzAccess	1	33608280	26493
7787	Skyline	2	33608280	31049
7788	VANTED	1	33608574	6275
7789	MSClust	1	33609141	11591
7790	HMDB	1	33613491	6429
7791	HMDB	1	33613517	7462
7792	LDA	2	33614635	4648
7793	HMDB	1	33618656	4142
7794	METLIN	1	33618656	4170
7795	XCMS	1	33622251	3046
7796	MathDAMP	1	33622389	5607
7797	MetaboAnalyst	1	33623001	15935
7798	XCMS	1	33623411	7633
7799	METLIN	1	33623411	8397
7800	ropls	1	33628183	6043
7801	MetaboAnalyst	1	33628361	6721
7802	XCMS	1	33628390	5308
7803	UniProtKB	1	33628390	18430
7804	HMDB	1	33628768	4795
7805	METLIN	1	33628768	4863
7806	MetaboAnalyst	1	33628838	4986
7807	GNPS	3	33630860	6241
7808	ChemSpider	1	33630889	11430
7809	HMDB	1	33630889	11987
7810	MetaboAnalyst	2	33633252	7802
7811	XCMS	1	33637520	10306
7812	HMDB	1	33639841	5880
7813	METLIN	1	33639841	5951
7814	FALCON	1	33642569	810
7815	mQTL	1	33642588	3957
7816	Workflow4Metabolomics	1	33643071	3257
7817	HMDB	1	33643073	4900
7818	iPath	1	33643073	4974
7819	ChemSpider	1	33643392	6234
7820	MetaboAnalyst	2	33643392	6321
7821	XCMS	1	33644632	8412
7822	RAMClustR	1	33644632	8540
7823	MetaboAnalyst	1	33646155	2900
7824	SMART	2	33648455	358
7825	MetaboAnalyst	1	33648471	4925
7826	ropls	1	33648480	5537
7827	InterpretMSSpectrum	1	33652667	5989
7828	MetaboAnalyst	1	33652907	3312
7829	MetScape	1	33652907	4045
7830	XCMS	1	33653408	6827
7831	LIPID MAPS	1	33653408	7207
7832	MetaboLights	2	33653825	5517
7833	XCMS	1	33653825	7832
7834	mixOmics	1	33653825	9909
7835	LIPID MAPS	1	33655337	8017
7836	MetaboAnalyst	1	33655725	2490
7837	HMDB	1	33655799	4861
7838	AMDIS	2	33658330	14127
7839	ropls	1	33658330	15217
7840	MetaboDiff	1	33658330	15694
7841	BinBase	1	33658594	12851
7842	MoNA	1	33658594	14948
7843	MS-FLO	1	33658594	17593
7844	MetaboAnalyst	2	33658594	18051
7845	LDA	1	33658635	14791
7846	XCMS	1	33658635	14974
7847	Skyline	1	33660934	8304
7848	MetaboAnalyst	2	33661419	5590
7849	HMDB	1	33664285	5080
7850	HMDB	1	33664384	5744
7851	MetaboAnalyst	2	33664384	6037
7852	XCMS	2	33664392	4191
7853	ropls	1	33664579	5151
7854	IsoMS	2	33664666	3997
7855	MyCompoundID	1	33664666	4669
7856	MetaboAnalyst	1	33664666	5252
7857	Newton	1	33665185	9936
7858	MetaboAnalyst	2	33665513	6574
7859	WikiPathways	1	33665961	10042
7860	MetaboAnalyst	1	33668370	8923
7861	Mummichog	1	33668448	10127
7862	MetaboAnalyst	1	33668870	6743
7863	MetaboAnalyst	1	33669117	8453
7864	AMDIS	1	33669279	8175
7865	SpectConnect	1	33669279	8372
7866	MetaboAnalyst	1	33669538	14145
7867	xMSanalyzer	4	33669564	7209
7868	apLCMS	3	33669564	7872
7869	HMDB	2	33669564	10324
7870	MetaboAnalyst	2	33669564	10727
7871	HMDB	1	33669644	3657
7872	MetaboAnalyst	1	33669644	4873
7873	GNPS	2	33670102	3673
7874	MAVEN	1	33670443	2519
7875	HMDB	1	33670443	2936
7876	MetaboAnalyst	1	33670443	3318
7877	HMDB	1	33670496	11726
7878	METLIN	1	33670629	6908
7879	HMDB	2	33671190	4149
7880	HMDB	1	33671194	6077
7881	MetaboAnalyst	1	33672326	1840
7882	ropls	1	33672542	9506
7883	HMDB	1	33672654	3414
7884	METLIN	1	33672654	3791
7885	MetaboAnalystR	2	33672654	5027
7886	MetaboAnalyst	1	33672725	5664
7887	GAM	1	33672760	3028
7888	BioCyc	1	33672760	3415
7889	GNPS	8	33673148	7509
7890	MZmine 2	1	33673168	3448
7891	MetaboAnalyst	1	33673198	7577
7892	LIMSA	1	33673500	6116
7893	XCMS	1	33673519	4929
7894	mzMatch	1	33673519	5009
7895	MetaboAnalyst	3	33673519	5732
7896	AMDIS	2	33673570	4534
7897	MetaboAnalyst	4	33673570	5619
7898	HMDB	1	33673678	5489
7899	mixOmics	1	33673678	6673
7900	MetaboAnalyst	1	33674554	5675
7901	LDA	2	33674681	5963
7902	XCMS	1	33674681	9455
7903	VANTED	1	33674745	2736
7904	HMDB	3	33674782	3283
7905	MetaboAnalyst	1	33675821	4247
7906	HMDB	1	33676096	4217
7907	MetaboAnalyst	1	33679418	4918
7908	XCMS	1	33679652	2415
7909	CAMERA	1	33679652	2421
7910	HMDB	1	33679652	2513
7911	MetaboAnalyst	1	33679652	3237
7912	MetAlign	1	33679815	6324
7913	GNPS	1	33679815	6412
7914	AMDIS	2	33679815	7043
7915	XCMS	1	33679891	5362
7916	HMDB	1	33679891	5795
7917	MetaboAnalyst	3	33679891	5835
7918	XCMS	2	33681165	5114
7919	MetAlign	1	33681165	5144
7920	mzMatch	2	33681165	5191
7921	METLIN	1	33681574	3230
7922	MetaboAnalyst	2	33681574	3584
7923	XCMS	1	33690602	4674
7924	MetaboAnalyst	2	33690602	5298
7925	METLIN	1	33690602	7760
7926	LIPID MAPS	1	33690602	8817
7927	GNPS	1	33692772	5406
7928	MetaboAnalyst	1	33693346	5789
7929	PathVisio	1	33693346	6340
7930	mixOmics	3	33693866	9962
7931	MetaboAnalyst	2	33705428	3587
7932	XCMS	2	33707505	9099
7933	XCMS online	1	33707505	9099
7934	HMDB	1	33707505	12157
7935	ConsensusPathDB	1	33707505	12497
7936	BinBase	2	33707534	9944
7937	MetaboAnalyst	1	33707534	10059
7938	LDA	2	33708182	7748
7939	MetaboAnalyst	1	33708234	8609
7940	HMDB	2	33711049	5255
7941	MetaboAnalyst	1	33711049	5566
7942	Skyline	3	33711926	10403
7943	GAM	2	33712622	15550
7944	METLIN	1	33712730	21772
7945	GNPS	3	33712730	23128
7946	MetaboAnalyst	1	33714949	2888
7947	MetaboAnalyst	1	33716757	3833
7948	XCMS	1	33718166	1707
7949	XCMS online	1	33718166	1707
7950	XCMS	2	33718374	4201
7951	CAMERA	1	33718374	4328
7952	HMDB	1	33718374	5167
7953	MetaboAnalystR	1	33719298	15037
7954	MetaboAnalyst	1	33719298	15257
7955	MetaboAnalyst	2	33719301	11616
7956	MetaboAnalyst	1	33721894	5750
7957	XCMS	1	33723244	7361
7958	MetaboAnalyst	2	33723244	8042
7959	ropls	1	33723320	5377
7960	mixOmics	1	33723320	5387
7961	GAM	1	33723382	5540
7962	UniProtKB	1	33724958	5869
7963	Qemistree	1	33727399	2825
7964	ChemSpider	1	33727648	4763
7965	HMDB	1	33727648	4968
7966	GNPS	1	33730076	1315
7967	HMDB	1	33730076	6027
7968	MetaboAnalyst	1	33730076	6327
7969	XCMS	1	33731015	5229
7970	MetaboAnalyst	1	33731015	7201
7971	HMDB	1	33731064	4871
7972	MetaboAnalyst	1	33731064	5628
7973	Skyline	1	33731064	5887
7974	MetaboAnalyst	1	33731120	13157
7975	HMDB	1	33731680	6994
7976	METLIN	1	33731680	7023
7977	XCMS	2	33732231	1819
7978	XCMS online	1	33732231	1819
7979	HMDB	1	33732231	2929
7980	MetaboAnalyst	1	33732231	3389
7981	METLIN	1	33732274	5047
7982	MetaboAnalyst	1	33732274	5570
7983	MetaboAnalyst	1	33735311	8855
7984	XCMS	2	33736704	13355
7985	CAMERA	1	33736704	13456
7986	HMDB	1	33736704	13827
7987	GAM	21	33736704	16050
7988	XCMS	1	33738264	5283
7989	XCMS	1	33741031	3813
7990	XCMS online	1	33741031	3813
7991	MetaboAnalyst	1	33741031	4055
7992	LDA	2	33741035	10094
7993	XCMS	1	33742087	7339
7994	MetaboAnalyst	1	33744860	2011
7995	MetaboAnalyst	1	33746944	7301
7996	CAMERA	1	33746996	7281
7997	HMDB	1	33746996	7550
7998	XCMS	2	33748187	4702
7999	MetaboAnalyst	1	33748187	6196
8000	MetaboAnalyst	2	33750383	8015
8001	MetaboAnalyst	1	33750414	7991
8002	LDA	1	33750430	3916
8003	XCMS	1	33750430	4827
8004	XCMS	2	33752593	6236
8005	HMDB	1	33752593	7153
8006	MetaboAnalystR	1	33752593	7376
8007	HMDB	1	33754018	7481
8008	MetaboDiff	1	33754045	7380
8009	XCMS	2	33755601	4538
8010	MAVEN	2	33758082	10587
8011	MetaboAnalyst	3	33758231	3010
8012	SMART	2	33761872	2237
8013	XCMS	2	33762013	3620
8014	CAMERA	1	33762013	3798
8015	HMDB	1	33762013	4182
8016	SMART	1	33762684	8188
8017	HMDB	1	33762950	8268
8018	MetaboliteDetector	2	33763411	8650
8019	mixOmics	1	33764039	10413
8020	MetExplore	1	33764039	10713
8021	MetaboAnalyst	1	33765008	7085
8022	SMART	1	33765093	2365
8023	XCMS	2	33765220	3460
8024	MS-FINDER	1	33765220	4592
8025	mixOmics	1	33766933	4660
8026	UniProtKB	1	33767186	6263
8027	HMDB	1	33767223	10627
8028	MetaboAnalyst	1	33767227	10310
8029	XCMS	1	33767263	3694
8030	HMDB	1	33767263	5726
8031	XCMS	1	33771114	5812
8032	XCMS	2	33771231	8610
8033	UniProtKB	1	33771987	27915
8034	HMDB	1	33771987	33644
8035	MetaboAnalyst	1	33772024	25703
8036	MetaboAnalyst	1	33772084	6431
8037	ORCA	2	33773110	26913
8038	MAIT	2	33777010	5276
8039	SMART	2	33777010	6648
8040	MetaboAnalyst	1	33777059	5663
8041	MetaboAnalyst	1	33777509	8411
8042	METLIN	1	33777509	9297
8043	HMDB	1	33777793	6940
8044	MetaboAnalyst	1	33777793	7182
8045	MetaboAnalyst	1	33777794	7424
8046	MetaboAnalyst	2	33777984	3162
8047	METLIN	1	33778068	1888
8048	LDA	3	33778322	3385
8049	XCMS	1	33780532	13820
8050	CAMERA	1	33780532	14061
8051	METLIN	1	33780532	15364
8052	LIPID MAPS	1	33780532	15454
8053	XCMS	1	33782411	19110
8054	CAMERA	1	33782411	19328
8055	MoNA	1	33782445	9271
8056	MS-FINDER	1	33782445	9327
8057	MetaboAnalyst	1	33782445	9904
8058	GAM	2	33782462	8488
8059	XCMS	1	33784814	215
8060	MetaboAnalyst	2	33785570	11706
8061	HMDB	2	33785573	2956
8062	MetaboliteDetector	1	33785616	10267
8063	LIQUID	1	33785616	10748
8064	MZmine 2	1	33785616	10838
8065	mixOmics	1	33785628	8556
8066	XCMS	1	33785628	10652
8067	MetaboAnalyst	1	33785817	8444
8068	HMDB	1	33790218	2094
8069	MetaboAnalyst	1	33790218	3082
8070	mixOmics	1	33790248	6111
8071	MetaboAnalyst	1	33790561	5501
8072	GNPS	4	33790875	8405
8073	SMART	1	33790924	5485
8074	MetaboAnalyst	1	33790973	5263
8075	MetScape	1	33790973	5374
8076	LDA	1	33790981	3314
8077	MetaboAnalyst	1	33790982	1806
8078	MoNA	1	33795633	6368
8079	GNPS	1	33795633	6411
8080	MetaboAnalyst	1	33795823	2111
8081	muma	1	33795871	13002
8082	XCMS	1	33796091	5820
8083	GNPS	2	33796117	3185
8084	GNPS	2	33796505	7262
8085	MetaboAnalyst	1	33796579	9245
8086	icoshift	4	33800512	4232
8087	HMDB	1	33800512	8559
8088	AMDIS	1	33800643	4726
8089	SpectConnect	3	33800643	4851
8090	MetaboAnalyst	1	33800812	6346
8091	HMDB	1	33801258	4908
8092	MetaboAnalyst	1	33801258	6500
8093	GNPS	4	33801258	6793
8094	HMDB	1	33801551	2117
8095	METLIN	1	33801551	2123
8096	Newton	5	33801673	867
8097	MetaboAnalyst	1	33801898	4432
8098	MetaboLights	1	33802096	3983
8099	Skyline	1	33802096	5848
8100	XCMS	2	33802740	5876
8101	LDA	2	33802740	8752
8102	Workflow4Metabolomics	1	33802747	2231
8103	MZmine 2	2	33802747	2307
8104	GNPS	1	33802747	7685
8105	METLIN	2	33802822	3107
8106	HMDB	1	33802822	3171
8107	LIPID MAPS	1	33802822	3187
8108	MetaboAnalyst	2	33802822	3316
8109	METLIN	1	33802916	11639
8110	ChemSpider	1	33802916	11670
8111	WikiPathways	1	33803109	4516
8112	MetaboAnalyst	1	33803435	4337
8113	UniProtKB	1	33803511	3861
8114	HMDB	1	33803572	2488
8115	MetaboAnalyst	1	33803572	3442
8116	HMDB	1	33803678	6651
8117	MassTRIX	1	33803764	1581
8118	ChemSpider	1	33803764	1858
8119	MetaboAnalyst	1	33803764	2011
8120	UniProtKB	1	33803797	3665
8121	ChemSpider	1	33803974	10412
8122	METLIN	1	33804237	7673
8123	HMDB	1	33804237	7687
8124	LIPID MAPS	1	33804237	7696
8125	AMDIS	1	33804275	8969
8126	MetaboAnalyst	2	33804285	8028
8127	SMART	1	33804713	7946
8128	MetaboAnalystR	2	33804948	1739
8129	UniProtKB	1	33805133	11288
8130	XCMS	1	33805148	5160
8131	MetaboAnalyst	1	33805234	10596
8132	HMDB	1	33805234	10619
8133	SMPDB	1	33805234	10925
8134	GAM	1	33805842	12338
8135	HMDB	1	33805924	5245
8136	MetaboAnalyst	1	33805924	6961
8137	MetaboAnalyst	1	33805952	5731
8138	MetaboAnalyst	2	33806264	4843
8139	MetaboLights	3	33806786	4756
8140	XCMS	1	33806786	5121
8141	MetFamily	6	33806786	6144
8142	MoNA	1	33806786	6450
8143	mixOmics	2	33806786	10489
8144	AMDIS	1	33807505	2953
8145	MetaboAnalyst	1	33807544	11489
8146	HMDB	1	33807817	4471
8147	AMDIS	1	33808142	3305
8148	MET-IDEA	1	33808142	3488
8149	MetaboAnalyst	6	33808177	4901
8150	MetAlign	2	33808188	6672
8151	MSClust	2	33808188	7012
8152	MetaboAnalyst	1	33808188	7934
8153	HMDB	1	33808279	2529
8154	METLIN	1	33808279	2609
8155	MetaboAnalyst	1	33808359	6830
8156	XCMS	2	33808519	3691
8157	XCMS online	1	33808519	3691
8158	MetaboAnalyst	1	33808519	4384
8159	GNPS	5	33808519	5388
8160	GNPS	2	33808894	2700
8161	mwTab	19	33808985	4
8162	METLIN	1	33809004	3215
8163	ChemSpider	1	33809127	6280
8164	MetaboAnalyst	2	33809127	6982
8165	METLIN	2	33809281	8606
8166	HMDB	1	33809281	8650
8167	MetaboAnalyst	1	33809487	4457
8168	MetaboAnalyst	2	33809510	7366
8169	LDA	2	33809624	2975
8170	GNPS	2	33809861	999
8171	HMDB	1	33810184	7988
8172	MetaboAnalystR	1	33810372	3984
8173	HMDB	1	33810579	4531
8174	MetaboAnalyst	1	33810579	5558
8175	IPO	1	33810601	692
8176	LIPID MAPS	1	33813137	13297
8177	LDA	3	33820543	9997
8178	AMDIS	1	33820558	11239
8179	XCMS	2	33821563	3880
8180	METLIN	4	33821563	4480
8181	HMDB	1	33821563	5282
8182	MetaboAnalyst	1	33821563	6246
8183	LDA	2	33824200	6709
8184	ChemSpider	1	33824200	7869
8185	GAM	1	33824313	12339
8186	MetAlign	2	33824368	7214
8187	MetaboAnalyst	1	33824368	8190
8188	Pathview	1	33826607	48549
8189	XCMS	3	33826687	1552
8190	MZmine 2	1	33826687	2899
8191	XCMS	1	33828161	4952
8192	xMSannotator	1	33828161	5296
8193	HMDB	2	33828161	5356
8194	MetaboAnalyst	1	33828161	7241
8195	mixOmics	2	33828194	14401
8196	MAIT	1	33828558	1841
8197	mixOmics	1	33828820	4036
8198	HMDB	1	33833781	2164
8199	HMDB	1	33835816	7436
8200	LIPID MAPS	2	33836757	3438
8201	MetaboAnalyst	1	33837233	4675
8202	HMDB	1	33838654	5090
8203	LDA	2	33841376	2881
8204	AMDIS	1	33842015	2685
8205	MET-IDEA	1	33842015	2906
8206	XCMS	1	33842015	5441
8207	SMART	1	33842016	1738
8208	METLIN	2	33842285	2288
8209	XCMS	1	33845816	7729
8210	HMDB	1	33845816	8494
8211	MetaboLab	1	33848268	5185
8212	XCMS	2	33849979	9484
8213	Mummichog	1	33849979	10159
8214	MetaboAnalyst	1	33849979	10342
8215	Skyline	1	33849979	11079
8216	HMDB	1	33850939	5494
8217	GNPS	1	33853672	1679
8218	ropls	1	33854195	18883
8219	MetaboAnalyst	2	33855035	2384
8220	HMDB	1	33855035	3050
8221	UniProtKB	2	33855276	8634
8222	AMDIS	1	33855720	4789
8223	AMDIS	1	33857218	2055
8224	HMDB	1	33858428	5544
8225	MetaboAnalyst	1	33858428	5708
8226	MZmine 2	1	33859311	4235
8227	HMDB	1	33859311	5846
8228	XCMS	1	33859560	6354
8229	HMDB	1	33859560	7307
8230	METLIN	1	33859560	7336
8231	MetaboAnalyst	1	33859560	7604
8232	LDA	1	33863898	11726
8233	MZmine 2	1	33863934	4427
8234	SMPDB	1	33867992	4439
8235	HMDB	1	33867992	4466
8236	MetaboAnalystR	1	33868179	2111
8237	LDA	2	33868268	4028
8238	MetaboAnalyst	1	33868271	2131
8239	HMDB	1	33868283	8834
8240	UniProtKB	1	33868285	10184
8241	MetaboAnalyst	1	33868339	8212
8242	LDA	2	33869074	2348
8243	HMDB	1	33869160	5537
8244	mixOmics	1	33869994	9123
8245	MetaboAnalyst	1	33869994	10970
8246	METLIN	1	33875792	13552
8247	HMDB	1	33875792	13563
8248	MetaboAnalyst	1	33875792	14093
8249	XCMS	1	33875796	8550
8250	MetaboAnalyst	3	33876723	857
8251	MZmine 2	1	33879061	2501
8252	MetaboAnalyst	1	33879061	2783
8253	mQTL	1	33879569	2716
8254	SMPDB	1	33882315	26191
8255	HMDB	1	33883556	14875
8256	LIQUID	2	33883556	14955
8257	mixOmics	1	33883599	5280
8258	MetaboAnalyst	1	33883599	9248
8259	HMDB	1	33883599	10217
8260	LIPID MAPS	2	33883599	10227
8261	MassTRIX	2	33883599	10249
8262	XCMS	2	33884424	6619
8263	MetaboAnalyst	1	33884424	7358
8264	MAIT	9	33885944	2377
8265	XCMS	2	33885999	4779
8266	HMDB	1	33885999	5213
8267	MetaboAnalyst	1	33885999	5590
8268	LIPID MAPS	1	33887680	6539
8269	UniProtKB	1	33888726	7084
8270	MetaboAnalyst	2	33888726	11025
8271	GNPS	1	33888726	11657
8272	SMART	1	33888781	3162
8273	NMRProcFlow	4	33889396	9180
8274	HMDB	1	33889396	10580
8275	MetaboAnalyst	1	33890477	6623
8276	MetaboAnalyst	3	33893555	4928
8277	GNPS	3	33897472	4669
8278	UniProtKB	1	33898308	3019
8279	MetaboliteDetector	1	33898308	3097
8280	Skyline	1	33898398	12908
8281	BioCyc	1	33898504	4684
8282	HMDB	1	33898504	4769
8283	HMDB	1	33902135	4448
8284	UniProtKB	1	33904924	5707
8285	DrugBank	1	33905441	20143
8286	MIDAS	3	33906945	14628
8287	CANOPUS	1	33906945	16109
8288	GNPS	2	33906945	17504
8289	MetaboAnalyst	2	33907214	8925
8290	mQTL	1	33909991	6183
8291	MetaboAnalyst	2	33915836	8546
8292	MetaboAnalyst	1	33915990	8328
8293	UniProtKB	1	33916015	9754
8294	LDA	1	33916356	2512
8295	MZmine 2	1	33916648	7860
8296	XCMS	1	33916648	8496
8297	UniProtKB	1	33916842	5388
8298	MetaboAnalyst	2	33917018	1737
8299	MetaboAnalyst	1	33917224	7162
8300	GNPS	2	33917975	5509
8301	HMDB	1	33918080	2769
8302	MetaboAnalyst	1	33918321	4359
8303	LDA	1	33918541	5276
8304	MetaboAnalyst	1	33918546	2139
8305	mixOmics	1	33918649	6851
8306	XCMS	4	33918649	8329
8307	BioCyc	1	33918785	5115
8308	ChemSpider	1	33918785	5127
8309	HMDB	2	33918785	5286
8310	HMDB	1	33918931	5967
8311	GNPS	1	33918939	10745
8312	XCMS	1	33919302	3305
8313	XCMS	1	33919944	9028
8314	mzMatch	1	33919944	9140
8315	MetScape	1	33919944	14356
8316	HMDB	3	33919953	3651
8317	FingerID	1	33919953	5780
8318	GNPS	1	33919953	6069
8319	HMDB	2	33920167	2270
8320	MZmine 2	1	33920213	5058
8321	AMDIS	2	33920240	13176
8322	LIPID MAPS	1	33920347	3896
8323	HMDB	1	33920347	3908
8324	METLIN	1	33920347	3914
8325	UniProtKB	1	33920371	6897
8326	XCMS	1	33920519	6374
8327	HMDB	1	33920519	6620
8328	HMDB	1	33921143	6349
8329	MetFrag	1	33921143	6387
8330	MetaboAnalyst	2	33921149	3630
8331	XCMS	1	33921244	3777
8332	MetaboAnalyst	2	33921244	6716
8333	HMDB	1	33921318	6897
8334	LIPID MAPS	1	33921318	6961
8335	MetaboAnalyst	2	33921318	7272
8336	WikiPathways	1	33921318	7846
8337	MetaboAnalyst	1	33921445	6780
8338	HMDB	1	33921805	9017
8339	MetaboAnalyst	1	33921805	9736
8340	ChemSpider	1	33921826	10350
8341	MetAlign	1	33922065	5150
8342	MSClust	1	33922065	5392
8343	MetaboAnalyst	1	33922065	5554
8344	HMDB	1	33922209	7062
8345	MetaboAnalyst	1	33922306	7396
8346	HMDB	1	33922306	7660
8347	hRUV	1	33922315	6259
8348	LDA	2	33922321	6848
8349	MetaboAnalyst	1	33922352	14037
8350	MetaboAnalystR	1	33922352	14065
8351	SMART	1	33922426	630
8352	MetaboAnalystR	1	33922536	4778
8353	METLIN	1	33922544	6018
8354	METLIN	1	33922964	4949
8355	GNPS	1	33922964	5006
8356	HMDB	2	33922964	5278
8357	MAGMa	2	33922964	5758
8358	HMDB	1	33923171	3861
8359	LIMSA	1	33923179	5268
8360	AMDIS	2	33923301	4258
8361	MetaboAnalyst	1	33923301	5077
8362	XCMS	1	33923591	16483
8363	GNPS	8	33923591	17401
8364	MetaboAnalyst	1	33923604	8593
8365	LDA	1	33923841	7410
8366	MetaboAnalyst	1	33924061	3117
8367	XCMS	1	33924291	4112
8368	MetaboAnalyst	1	33924291	4920
8369	AMDIS	1	33924291	5700
8370	MetAlign	1	33924479	1854
8371	AMDIS	1	33924579	6513
8372	MET-IDEA	1	33924579	6523
8373	XCMS	2	33925473	3724
8374	MetNormalizer	1	33925473	4390
8375	HMDB	1	33926485	3829
8376	GNPS	1	33926485	3845
8377	MetaboAnalyst	1	33927231	11512
8378	XCMS	2	33927779	3612
8379	MetaboLights	1	33928121	9990
8380	AMDIS	6	33928121	12727
8381	HMDB	1	33928121	14226
8382	ORCA	1	33928249	5295
8383	SMART	1	33931613	4682
8384	MetaboAnalyst	1	33935712	4649
8385	GNPS	1	33935766	2763
8386	MetaboAnalyst	1	33935766	8079
8387	DrugBank	1	33935993	11046
8388	iPath	1	33936009	10176
8389	ChemSpider	1	33936167	5849
8390	MetaboLights	1	33936167	7455
8391	BioCyc	1	33937210	2232
8392	MetaboAnalyst	1	33937325	7003
8393	MetaboAnalyst	1	33939923	12415
8394	tmod	1	33942347	11855
8395	GNPS	2	33942718	7392
8396	METLIN	1	33946050	2280
8397	HMDB	1	33946050	2363
8398	HMDB	1	33946139	5978
8399	MetaboAnalyst	1	33946139	7870
8400	SMART	1	33946196	4393
8401	ConsensusPathDB	1	33946220	2534
8402	MetaboAnalyst	1	33946522	8037
8403	MetaboAnalyst	1	33946710	4973
8404	GNPS	1	33946792	7883
8405	AMDIS	2	33946812	4881
8406	HMDB	1	33946812	5510
8407	MetaboAnalyst	1	33946812	7817
8408	ChemSpider	1	33946867	6580
8409	MetaboAnalyst	3	33946867	7170
8410	LIPID MAPS	1	33946875	14438
8411	SigMa	1	33946896	3756
8412	XCMS	1	33947802	6510
8413	BioCyc	1	33947802	7179
8414	MS-LAMP	2	33947802	7285
8415	Skyline	1	33947808	4027
8416	HMDB	1	33948567	13301
8417	Newton	1	33949213	1865
8418	MetaboAnalyst	2	33949213	7373
8419	XCMS	1	33952214	3673
8420	MetaboAnalyst	1	33952214	5111
8421	LDA	1	33953214	11292
8422	GAM	3	33955700	4062
8423	MetaboAnalyst	1	33956857	2922
8424	LDA	1	33957990	8111
8425	XCMS	1	33957990	8847
8426	ropls	1	33957990	9350
8427	MetaboAnalyst	1	33958644	7415
8428	MetaboAnalyst	1	33958645	9560
8429	ConsensusPathDB	1	33958659	6583
8430	HMDB	1	33958659	6750
8431	METLIN	1	33959028	6565
8432	HMDB	1	33959028	6611
8433	MetaboAnalyst	1	33959028	10000
8434	XCMS	2	33959104	2990
8435	CAMERA	1	33959104	3255
8436	AMDIS	1	33959135	3434
8437	Viime	1	33963215	6107
8438	LDA	1	33968068	10688
8439	MRMPROBS	1	33968107	5096
8440	mixOmics	1	33968107	7531
8441	SMART	2	33968807	5459
8442	LDA	1	33968807	7404
8443	MetaboAnalyst	1	33968807	8857
8444	MetaboAnalyst	1	33969016	8449
8445	HMDB	1	33970919	1784
8446	MetaboHunter	1	33970919	1829
8447	MetaboAnalyst	1	33973485	2351
8448	SMART	1	33976123	42
8449	Skyline	3	33981220	9908
8450	MetaboAnalyst	1	33981220	12558
8451	ORCA	1	33983919	14625
8452	MetaboAnalyst	1	33983919	20543
8453	XCMS	1	33986608	3918
8454	METLIN	1	33986608	4470
8455	MoNA	1	33986608	4499
8456	MetaboAnalyst	1	33986608	5138
8457	XCMS	1	33987020	3144
8458	HMDB	1	33987020	3820
8459	MetaboAnalyst	1	33987020	4399
8460	HMDB	1	33987551	2832
8461	xMSannotator	1	33987620	6333
8462	MSPrep	2	33987620	10796
8463	MoDentify	1	33987620	16585
8464	XCMS	1	33989107	3857
8465	HMDB	1	33989107	4091
8466	MetaboAnalyst	1	33989107	4730
8467	Skyline	1	33995016	1670
8468	MetaboAnalyst	1	33995016	3211
8469	UniProtKB	1	33996285	755
8470	PathoLogic	1	33996285	2110
8471	MetaboAnalyst	1	33996940	515
8472	Skyline	1	33997298	8151
8473	ropls	1	33997784	7657
8474	MetaboAnalyst	1	33997957	2398
8475	PARADISe	1	33999962	3158
8476	AMDIS	1	33999962	3323
8477	HMDB	1	34001006	5788
8478	METLIN	1	34001006	5808
8479	MetaboAnalyst	1	34001214	13039
8480	METLIN	1	34002024	7554
8481	HMDB	1	34002024	7565
8482	METLIN	1	34006628	6557
8483	HMDB	1	34006628	6581
8484	MetFrag	1	34006628	7076
8485	MetaboAnalyst	2	34006909	2101
8486	FingerID	1	34007033	16558
8487	GNPS	1	34007033	16646
8488	XCMS	1	34007406	2854
8489	HMDB	1	34007406	3430
8490	METLIN	1	34007406	3457
8491	MetaboAnalyst	1	34007406	3637
8492	XCMS	1	34011385	4349
8493	PiMP	1	34011385	4432
8494	MetaboLights	1	34011385	5344
8495	MetaboAnalyst	2	34012394	9393
8496	MAVEN	1	34013258	7561
8497	HMDB	1	34013258	8363
8498	MetaboAnalyst	3	34013258	10419
8499	LDA	2	34016169	11665
8500	ChemSpider	1	34016789	2052
8501	HMDB	1	34016789	2123
8502	METLIN	1	34016789	2151
8503	LDA	1	34016954	7512
8504	MetaboAnalyst	1	34017039	1734
8505	MetaboAnalyst	1	34017341	6839
8506	ropls	2	34017341	7837
8507	HMDB	1	34021116	5538
8508	METLIN	1	34021261	3764
8509	HMDB	1	34021261	3811
8510	MetaboAnalyst	1	34021361	9406
8511	ropls	1	34026726	11262
8512	MetaboAnalystR	1	34026726	11269
8513	ChemSpider	1	34026822	4994
8514	METLIN	1	34026879	1258
8515	BinBase	1	34030631	2593
8516	MetaboAnalyst	1	34031416	13490
8517	MetaboAnalyst	1	34036096	3129
8518	AMDIS	3	34036777	1164
8519	MetaboAnalyst	1	34036777	1926
8520	MetaboAnalyst	2	34037347	6154
8521	MZmine 2	2	34040646	8350
8522	XCMS	1	34041223	5464
8523	HMDB	1	34041223	8841
8524	HMDB	1	34043632	9060
8525	HMDB	2	34045475	4507
8526	MetaboAnalyst	1	34045558	9356
8527	GSimp	1	34045614	901
8528	specmine	1	34045614	31859
8529	MetaboAnalyst	1	34045626	6488
8530	MetaboAnalyst	1	34045978	7400
8531	MetaboAnalyst	1	34046051	7614
8532	METLIN	1	34046077	3272
8533	HMDB	1	34046077	3309
8534	MetaboAnalyst	2	34046077	3370
8535	LDA	2	34046146	4603
8536	GNPS	4	34050176	13083
8537	XCMS	1	34054845	8174
8538	XCMS online	1	34054845	8174
8539	HMDB	1	34054845	8293
8540	METLIN	1	34054845	8523
8541	MetaboAnalyst	2	34054845	8832
8542	MetaboLights	1	34055742	5298
8543	MetaboAnalyst	1	34055742	5562
8544	MoNA	1	34055742	5719
8545	HMDB	1	34055742	6142
8546	MetaboAnalyst	1	34055773	5860
8547	XCMS	1	34055834	2877
8548	HMDB	1	34055834	3298
8549	METLIN	1	34055834	3326
8550	MetaboAnalyst	1	34055834	3492
8551	MetaboAnalyst	1	34055874	2889
8552	MetaboAnalyst	1	34055883	309
8553	METLIN	1	34055894	6522
8554	HMDB	1	34055894	6551
8555	MetaboAnalyst	1	34055894	7307
8556	XCMS	2	34057605	3837
8557	FALCON	1	34059662	1043
8558	METLIN	1	34061574	5136
8559	HMDB	1	34061574	5187
8560	GNPS	2	34061574	5276
8561	GNPS	1	34061874	2894
8562	MetaboAnalyst	2	34063031	4311
8563	Pathos	1	34063040	5017
8564	XCMS	3	34063084	749
8565	ropls	1	34063084	826
8566	batchCorr	2	34063084	894
8567	MetaboLights	1	34063107	4290
8568	XCMS	4	34063107	5224
8569	CAMERA	1	34063107	5776
8570	MAVEN	1	34063124	5644
8571	MetaboAnalyst	2	34063124	5874
8572	XCMS	2	34063409	5587
8573	XCMS online	1	34063409	5587
8574	HMDB	1	34063409	6708
8575	METLIN	1	34063409	6735
8576	ChemSpider	1	34063409	6771
8577	LIPID MAPS	1	34063409	6812
8578	MetFrag	1	34063409	6852
8579	MetaboAnalyst	1	34063409	6917
8580	HMDB	1	34063498	4997
8581	CMM	11	34063531	1198
8582	HMDB	2	34063531	1737
8583	METLIN	2	34063531	8489
8584	METLIN	1	34063678	1703
8585	MetaboAnalyst	1	34063723	5451
8586	PARADISe	1	34063836	5595
8587	AMDIS	1	34063836	5841
8588	MoNA	1	34063836	6042
8589	MoNA	1	34064666	6692
8590	CFM-ID	1	34064666	7955
8591	MetFrag	1	34064666	7980
8592	mzR	1	34064765	7221
8593	XCMS	1	34064765	7292
8594	XCMS	2	34064997	5898
8595	XCMS	1	34066071	4052
8596	HMDB	1	34066071	5546
8597	METLIN	1	34066071	5694
8598	icoshift	1	34066501	4801
8599	METLIN	2	34066694	12537
8600	MetFrag	2	34066694	12593
8601	XCMS	5	34066694	13715
8602	IPO	1	34066694	13960
8603	CAMERA	1	34066694	14871
8604	GNPS	2	34066694	15568
8605	CFM-ID	1	34066694	16158
8606	FingerID	2	34066831	11144
8607	CANOPUS	2	34066831	11190
8608	MetaboAnalyst	1	34066879	14598
8609	XCMS	1	34066958	2702
8610	XCMS	3	34067001	7012
8611	XCMS online	1	34067001	7601
8612	XCMS	1	34067305	1866
8613	XCMS online	1	34067305	1866
8614	MZmine 2	1	34068119	3427
8615	GNPS	3	34068119	4266
8616	MetaboAnalyst	2	34068241	5028
8617	GNPS	1	34068285	4959
8618	CFM-ID	1	34068285	5313
8619	MoNA	1	34068285	5532
8620	MetaboAnalyst	1	34068285	6981
8621	missForest	1	34068443	7202
8622	METLIN	1	34068821	2114
8623	HMDB	1	34068821	2126
8624	METLIN	2	34069591	5897
8625	MetaboAnalyst	1	34069591	7714
8626	LipidXplorer	1	34069977	3345
8627	MetaboAnalyst	1	34070154	8992
8628	AMDIS	1	34070374	2487
8629	AMDIS	1	34070718	4337
8630	MetaboAnalyst	2	34070718	6299
8631	LDA	1	34070764	6100
8632	HMDB	1	34070818	4177
8633	MetaboAnalyst	1	34070818	4296
8634	METLIN	1	34070938	8697
8635	MetaboAnalyst	1	34070938	9015
8636	MetaboAnalyst	1	34071153	5986
8637	MoNA	1	34071443	2721
8638	GNPS	1	34071728	10332
8639	HMDB	1	34071848	3383
8640	MetaboAnalyst	1	34071848	5108
8641	MetaboAnalyst	1	34072010	6137
8642	HMDB	3	34072178	7640
8643	MetaboAnalyst	1	34072178	8673
8644	XCMS	1	34072305	3125
8645	LipidXplorer	1	34072406	8456
8646	MetaboAnalyst	1	34072483	1254
8647	Skyline	1	34072643	7755
8648	UniProtKB	1	34073016	83
8649	MetaboAnalyst	1	34073358	7040
8650	AMDIS	2	34073647	2880
8651	MetaboAnalyst	2	34073686	7787
8652	HMDB	1	34073706	2458
8653	METLIN	1	34073706	2511
8654	HMDB	1	34075137	4666
8655	METLIN	1	34075137	4737
8656	MetaboAnalystR	1	34075137	6272
8657	IPO	1	34075191	9883
8658	XCMS	2	34075191	9959
8659	Mummichog	1	34075191	11108
8660	UniProtKB	1	34075561	3849
8661	XCMS	1	34077503	4462
8662	CAMERA	1	34077503	4467
8663	BioCyc	1	34077503	8986
8664	MetaboAnalyst	2	34077503	9309
8665	MetaboLights	1	34077733	969
8666	MetaboAnalyst	1	34077733	17176
8667	Workflow4Metabolomics	2	34078434	19880
8668	mixOmics	1	34078434	20730
8669	ropls	1	34078468	5737
8670	HMDB	1	34078468	7761
8671	METLIN	1	34078468	7780
8672	LIPID MAPS	1	34078963	17371
8673	ChemSpider	1	34079016	5640
8674	MetaboAnalyst	1	34079016	7978
8675	MelonnPan	1	34079079	5422
8676	MIDAS	1	34079079	5953
8677	LDA	2	34079556	4690
8678	ChemSpider	1	34079565	2622
8679	MetaboAnalyst	1	34079794	6191
8680	HMDB	7	34088912	386
8681	MetaboAnalyst	1	34089174	5779
8682	ropls	1	34090341	6701
8683	MetaboAnalyst	1	34090512	12801
8684	MetaboAnalyst	3	34093172	8046
8685	MIDAS	1	34093404	515
8686	HMDB	1	34093433	3010
8687	MetaboAnalyst	2	34093433	3274
8688	HMDB	1	34093611	4034
8689	METLIN	1	34093611	4085
8690	ropls	1	34093611	4430
8691	METLIN	1	34094035	727
8692	MetaboAnalyst	4	34095069	3842
8693	METLIN	1	34095186	13630
8694	XCMS	1	34095230	5205
8695	MoNA	1	34095230	5719
8696	HMDB	1	34095230	5729
8697	XCMS	2	34095243	3171
8698	MetaboAnalyst	1	34095243	4919
8699	SMPDB	1	34095873	4633
8700	CAMERA	1	34095879	10987
8701	MetaboAnalyst	1	34095879	11760
8702	SMART	1	34098881	277
8703	ChemSpider	1	34099056	7033
8704	MetaboAnalyst	1	34099056	8478
8705	MetaboAnalyst	2	34099621	9328
8706	MetaboAnalyst	1	34099838	5977
8707	MetaboAnalyst	1	34100638	7660
8708	MIDAS	2	34103529	3139
8709	Newton	1	34103611	11186
8710	LDA	1	34103897	1682
8711	HMDB	1	34106111	6251
8712	MZmine 2	1	34106569	11435
8713	HMDB	1	34108489	9515
8714	METLIN	1	34108489	9583
8715	MAIT	3	34108957	316
8716	ChemSpider	1	34108957	12956
8717	HMDB	1	34108957	13014
8718	HMDB	1	34109234	6321
8719	MetaboAnalyst	1	34109234	6422
8720	FALCON	1	34112794	1965
8721	MetaboAnalyst	1	34113373	10283
8722	XCMS	1	34113404	4464
8723	MetaboAnalyst	1	34113404	5757
8724	METLIN	1	34113836	21111
8725	MetaboAnalyst	2	34121924	952
8726	MetaboAnalyst	2	34121984	6963
8727	LDA	1	34121997	7789
8728	HMDB	1	34122100	6215
8729	SMPDB	1	34122100	6362
8730	DrugBank	1	34122100	7194
8731	LDA	2	34122394	7576
8732	HMDB	1	34122597	4310
8733	XCMS	2	34124100	2552
8734	MetaboAnalyst	2	34124100	3103
8735	HMDB	1	34124164	5895
8736	MetaboAnalyst	2	34124164	7076
8737	BinBase	1	34124225	3711
8738	MetaboAnalyst	1	34124225	6533
8739	MetaboAnalyst	3	34124268	4175
8740	METLIN	1	34124268	4936
8741	MetaboAnalyst	1	34126236	14778
8742	XCMS	1	34127037	2642
8743	missForest	1	34127671	11389
8744	mixOmics	1	34127774	8674
8745	Skyline	1	34132745	14394
8746	MetaboAnalyst	2	34132979	5337
8747	UniProtKB	1	34135321	11235
8748	MetaboAnalyst	2	34135327	15684
8749	XCMS	6	34135882	5059
8750	MetaboAnalyst	1	34135882	6602
8751	MZmine 2	1	34135882	8087
8752	FingerID	1	34135882	8802
8753	GNPS	2	34135882	8980
8754	HMDB	1	34135987	4091
8755	METLIN	1	34135987	4159
8756	HMDB	1	34136379	4163
8757	MetaboAnalyst	1	34136571	3793
8758	HMDB	1	34140582	4437
8759	MetaboAnalyst	2	34140582	6687
8760	MetaboAnalyst	1	34140596	12018
8761	MetaboAnalystR	1	34140959	5067
8762	MoNA	1	34140966	7703
8763	GAM	1	34140966	9843
8764	LDA	2	34141627	3500
8765	MetaboAnalyst	1	34141983	4311
8766	MetaboAnalyst	1	34143769	13216
8767	CAMERA	1	34144983	12132
8768	CANOPUS	1	34144983	20869
8769	MetaboAnalyst	1	34145025	6002
8770	MetaboAnalyst	1	34145340	165
8771	HMDB	1	34145340	8598
8772	MetaboAnalyst	1	34145354	6408
8773	Rhea	1	34146566	5218
8774	SMART	1	34147088	1504
8775	MetaboAnalyst	1	34149478	4560
8776	MetaboAnalyst	2	34149684	5442
8777	AMDIS	1	34149768	6238
8778	MetaboAnalyst	2	34149776	10050
8779	XCMS	1	34150732	2339
8780	GNPS	1	34151125	3016
8781	MoNA	1	34151772	12695
8782	HMDB	1	34151772	12859
8783	MetaboAnalyst	1	34151801	6112
8784	MetaboAnalyst	1	34155207	14126
8785	MetaboLights	1	34155311	9133
8786	MetaboAnalyst	1	34155969	2112
8787	HMDB	1	34157945	2735
8788	LIPID MAPS	1	34157945	2750
8789	MetaboAnalyst	1	34158563	14168
8790	MetaboAnalyst	1	34158597	7505
8791	METLIN	1	34158620	17471
8792	XCMS	1	34159625	1486
8793	MetaboAnalyst	1	34159625	3524
8794	LDA	1	34160281	8077
8795	LipidXplorer	2	34161347	7708
8796	HMDB	1	34162906	7595
8797	SMART	1	34162906	19123
8798	MIDAS	3	34163382	1412
8799	UniProtKB	1	34163450	1304
8800	LDA	2	34164424	4168
8801	XCMS	1	34164424	5147
8802	XCMS online	1	34164424	5147
8803	MetaboAnalyst	1	34164441	3230
8804	MetaboAnalyst	1	34165243	8116
8805	LDA	1	34168259	6467
8806	LIPID MAPS	1	34168259	9002
8807	HMDB	1	34168265	14401
8808	MetaboAnalyst	1	34168621	17646
8809	HMDB	1	34168621	17947
8810	HMDB	3	34172058	6728
8811	MetaboAnalystR	1	34172715	1151
8812	HMDB	1	34174965	4985
8813	METLIN	1	34174965	5013
8814	MoNA	1	34176512	3432
8815	HMDB	1	34176512	3631
8816	XCMS	1	34177565	6281
8817	MAIT	1	34177565	6375
8818	HMDB	3	34177565	6530
8819	MetScape	1	34177565	8325
8820	MetDisease	1	34177565	8380
8821	MetaboAnalyst	1	34178663	2473
8822	MetaboAnalyst	3	34179074	2002
8823	DrugBank	1	34181531	3950
8824	GAM	1	34181531	8371
8825	MetaboAnalyst	1	34184544	7811
8826	LDA	2	34185184	5432
8827	XCMS	1	34185184	7663
8828	CAMERA	1	34185184	7802
8829	IPO	1	34188039	14439
8830	XCMS	1	34193189	5539
8831	MetaboAnalyst	1	34193905	6693
8832	MetaboAnalyst	2	34194320	1831
8833	MetaboAnalyst	2	34194326	7033
8834	ChemSpider	1	34194402	17197
8835	HMDB	1	34194524	3500
8836	MetaboAnalyst	1	34194524	3583
8837	MetaboAnalyst	2	34195679	5125
8838	HMDB	1	34195679	6294
8839	METLIN	1	34198476	6585
8840	HMDB	1	34198476	6608
8841	MetaboAnalyst	1	34199226	1369
8842	HMDB	1	34199254	5110
8843	BioCyc	1	34199260	2190
8844	MetaboAnalystR	1	34199317	3553
8845	AMDIS	3	34199338	1560
8846	Metab	1	34199338	1726
8847	VANTED	2	34199555	3176
8848	MetaboAnalyst	2	34200012	9399
8849	HMDB	1	34200012	9584
8850	METLIN	1	34200012	11429
8851	MetFrag	2	34200012	11614
8852	MetAlign	1	34200252	4300
8853	XCMS	1	34200295	6011
8854	CAMERA	1	34200295	6225
8855	METLIN	1	34200295	10118
8856	MetAlign	2	34200451	4028
8857	HMDB	1	34200451	5288
8858	XCMS	2	34200487	8168
8859	CAMERA	1	34200487	8320
8860	MS-FLO	1	34200487	8408
8861	MetaboAnalyst	1	34200487	9234
8862	MetaboAnalyst	1	34200519	5801
8863	MetaboAnalyst	1	34200589	5337
8864	MetaboAnalyst	1	34200685	6295
8865	ChemSpider	1	34201193	735
8866	HMDB	1	34201365	2527
8867	HMDB	1	34201416	6894
8868	MetaboAnalyst	1	34201416	7657
8869	HMDB	1	34201720	15394
8870	SMPDB	1	34201720	15446
8871	HMDB	1	34201735	6314
8872	MetaboAnalyst	1	34201735	9580
8873	GNPS	2	34201744	4835
8874	ChemSpider	1	34201792	13884
8875	DrugBank	1	34202092	1076
8876	MetaboAnalyst	1	34202322	15171
8877	MetaboAnalyst	1	34202329	6857
8878	MetaboAnalyst	1	34202618	10779
8879	HMDB	1	34202839	3495
8880	METLIN	1	34202839	3547
8881	MassTRIX	3	34202850	4788
8882	HMDB	2	34202850	4911
8883	LIPID MAPS	1	34202850	4955
8884	MetaboAnalyst	1	34202850	6251
8885	MetaboAnalyst	1	34202988	4920
8886	Newton	1	34203256	4918
8887	MetAlign	1	34203585	2089
8888	HMDB	1	34203585	3245
8889	SMART	1	34203691	1406
8890	MetaboAnalyst	1	34203768	10859
8891	MetaboAnalyst	1	34203786	4301
8892	SMART	1	34204142	2069
8893	XCMS	2	34204263	5932
8894	MetaboAnalyst	1	34204371	4987
8895	XCMS	1	34204563	10480
8896	MetaboAnalyst	1	34204744	4761
8897	AMDIS	1	34205012	3557
8898	MetaboAnalyst	1	34205226	4010
8899	SMART	1	34205451	7403
8900	XCMS	2	34205639	6181
8901	MetExplore	1	34205708	4091
8902	ropls	1	34205708	6898
8903	MetaboAnalyst	1	34205785	7654
8904	XCMS	1	34206151	3999
8905	ropls	1	34206151	4919
8906	AMDIS	1	34206279	3984
8907	MetaboAnalyst	1	34206279	4624
8908	GAM	1	34206279	9730
8909	GNPS	4	34206861	7097
8910	MetaboAnalyst	1	34206906	5920
8911	muma	1	34207187	5571
8912	MetaboAnalyst	3	34207535	7262
8913	SMPDB	1	34207535	8109
8914	HMDB	1	34207535	9117
8915	PathVisio	1	34207595	16650
8916	WikiPathways	1	34207595	16832
8917	MIDAS	2	34207758	2073
8918	MetaboAnalyst	1	34208076	4654
8919	HMDB	1	34208076	5272
8920	ChemSpider	1	34208076	5289
8921	MetaboAnalyst	3	34208228	3843
8922	XCMS	1	34208357	3000
8923	MetaboAnalyst	2	34208357	3620
8924	HMDB	1	34208545	3930
8925	HMDB	1	34209136	5560
8926	MetaboAnalyst	1	34209136	6082
8927	HMDB	2	34209150	4213
8928	AMDIS	1	34209624	3619
8929	mixOmics	1	34209624	4619
8930	MetaboAnalyst	1	34209638	3585
8931	XCMS	3	34209716	3724
8932	LDA	1	34209897	9410
8933	XCMS	1	34209960	3904
8934	MetaboAnalyst	1	34209960	4037
8935	MetaboAnalystR	1	34209960	4235
8936	ChemSpider	1	34209960	4714
8937	MZmine 2	2	34210084	3904
8938	GNPS	2	34210084	6968
8939	LDA	2	34211003	6623
8940	XCMS	1	34211003	8210
8941	HMDB	1	34211003	8493
8942	MetaboAnalyst	3	34212003	9415
8943	LipidXplorer	1	34214073	9325
8944	HMDB	1	34214075	6127
8945	METLIN	1	34214075	6133
8946	MetaboAnalystR	3	34214075	8148
8947	Mummichog	2	34214075	8459
8948	UniProtKB	1	34215750	4374
8949	HMDB	1	34220499	9080
8950	MetaboAnalyst	1	34220499	9108
8951	HMDB	1	34220530	8477
8952	METLIN	1	34220530	8483
8953	HMDB	1	34220856	6513
8954	METLIN	1	34220856	6582
8955	MetaboAnalyst	1	34220909	7774
8956	SMART	2	34220919	288
8957	MetaboAnalyst	1	34221092	4555
8958	BATMAN	1	34221092	4858
8959	HMDB	1	34222036	2947
8960	MetaboAnalyst	1	34222036	3214
8961	HMDB	1	34222037	7924
8962	HMDB	1	34222398	10011
8963	MetaboAnalyst	3	34222398	10376
8964	MetaboAnalyst	1	34225733	4918
8965	AMDIS	1	34226581	4283
8966	MetaboAnalyst	1	34226581	9260
8967	XCMS	1	34226693	4463
8968	HMDB	1	34226721	8369
8969	LDA	1	34226721	16784
8970	LIPID MAPS	1	34226721	21388
8971	MetaboAnalyst	2	34228201	5932
8972	MetaboAnalyst	1	34229732	8098
8973	XCMS	2	34230210	7854
8974	CAMERA	1	34230210	8124
8975	METLIN	1	34230210	8945
8976	HMDB	1	34230210	8956
8977	XCMS	2	34233735	14994
8978	HMDB	1	34233735	15613
8979	MetaboAnalyst	3	34233759	4197
8980	HMDB	1	34233759	8809
8981	HMDB	2	34234268	177
8982	METLIN	1	34234761	11842
8983	MetaboAnalyst	1	34234776	5528
8984	HMDB	1	34234776	6115
8985	HMDB	3	34234835	4769
8986	METLIN	2	34234835	4800
8987	MetaboAnalyst	1	34234835	5625
8988	MetaboAnalystR	1	34234932	5778
8989	UniProtKB	1	34235635	6138
8990	UniProtKB	1	34239012	3858
8991	MoNA	10	34239103	4311
8992	HMDB	9	34239103	4345
8993	Skyline	1	34239103	7551
8994	XCMS	1	34239103	28634
8995	MetaboLights	2	34239103	30222
8996	MetaboAnalyst	1	34239317	1907
8997	MetaboAnalyst	2	34239914	5460
8998	ChemSpider	1	34239914	5600
8999	XCMS	1	34242379	5657
9000	GNPS	1	34242379	6032
9001	HMDB	1	34242379	6492
9002	YMDB	1	34242379	7107
9003	Workflow4Metabolomics	1	34244531	1969
9004	XCMS	1	34244531	6492
9005	GNPS	1	34244531	9460
9006	XCMS	1	34246248	10537
9007	HMDB	1	34246248	11038
9008	XCMS	1	34248618	3660
9009	MetaboSignal	1	34248891	9108
9010	ropls	1	34248900	4860
9011	XCMS	2	34249588	8383
9012	MetaboAnalyst	1	34249588	10349
9013	XCMS	1	34250503	4537
9014	MAVEN	1	34251339	17833
9015	MetaboAnalyst	1	34251979	3591
9016	iPath	1	34251979	3643
9017	HMDB	1	34253752	8340
9018	MetaboAnalyst	2	34253752	12067
9019	mixOmics	2	34253752	14585
9020	XCMS	2	34254824	5543
9021	CAMERA	1	34254824	5702
9022	MetaboAnalyst	1	34255797	5064
9023	SMPDB	1	34255797	7457
9024	rDolphin	1	34257400	5461
9025	XCMS	1	34257684	2520
9026	MetaboAnalyst	1	34258280	6549
9027	HMDB	1	34258280	6858
9028	SMPDB	1	34258280	6927
9029	LIPID MAPS	2	34267224	10196
9030	Skyline	1	34267224	16820
9031	MetaboAnalyst	1	34267957	4533
9032	HMDB	1	34272244	27568
9033	UniProtKB	1	34272434	7613
9034	mixOmics	1	34272434	9008
9035	Workflow4Metabolomics	1	34272460	5299
9036	MetaboAnalyst	1	34272460	5351
9037	XCMS	1	34274980	8339
9038	METLIN	1	34274980	12225
9039	HMDB	1	34274980	12482
9040	SMART	2	34276370	14007
9041	HMDB	1	34276585	2425
9042	BioCyc	2	34276593	8583
9043	HMDB	1	34276593	8617
9044	ChemSpider	1	34276593	9194
9045	XCMS	1	34276745	11912
9046	MetaboAnalyst	1	34276745	13112
9047	XCMS	2	34276752	3198
9048	MetaboAnalyst	1	34276752	4131
9049	DrugBank	1	34277429	1894
9050	BATMAN	2	34277435	279
9051	MetaboAnalyst	2	34277704	3748
9052	ropls	1	34278270	9452
9053	MetaboAnalyst	3	34280208	6753
9054	ropls	2	34280650	4333
9055	LDA	1	34280650	6656
9056	SMART	1	34281077	2488
9057	MetaboAnalyst	1	34281271	4831
9058	MAVEN	1	34281384	7549
9059	XCMS	1	34281384	7564
9060	HMDB	2	34282162	4738
9061	METLIN	1	34282162	4744
9062	SMPDB	1	34282162	5497
9063	MetaboAnalyst	1	34282188	3416
9064	MetaboAnalyst	1	34282210	8542
9065	METLIN	1	34282765	5936
9066	HMDB	1	34282765	5965
9067	XCMS	1	34283861	6178
9068	MZmine 2	1	34286355	3002
9069	GNPS	3	34286355	3490
9070	XCMS	1	34288731	11885
9071	MetaboAnalyst	2	34289240	17090
9072	MetScape	1	34289240	18305
9073	ChemSpider	1	34289974	3775
9074	HMDB	1	34289974	3873
9075	Skyline	1	34289974	7266
9076	MAVEN	1	34290243	21013
9077	XCMS	1	34293940	2521
9078	SMART	1	34294718	9021
9079	MetaboAnalyst	1	34294851	8299
9080	LDA	1	34295318	2747
9081	MetaboAnalyst	1	34295876	8777
9082	MAVEN	1	34297729	10537
9083	HMDB	2	34299017	6079
9084	METLIN	1	34299017	6107
9085	CFM-ID	1	34299017	6994
9086	MetaboAnalyst	1	34299043	1400
9087	MetaboAnalyst	2	34299050	7196
9088	MetaboAnalyst	1	34299216	3784
9089	Workflow4Metabolomics	1	34299389	5366
9090	XCMS	1	34299389	5461
9091	iPath	1	34299389	7621
9092	XCMS	2	34299399	5370
9093	CAMERA	1	34299399	5473
9094	HMDB	1	34299399	5837
9095	NMRProcFlow	1	34299421	5854
9096	MetaboAnalyst	1	34299421	6407
9097	AMDIS	2	34299456	1160
9098	MetaboAnalyst	1	34299456	8120
9099	GAM	1	34301288	25
9100	DrugBank	1	34301291	1811
9101	MetaboAnalyst	2	34301294	7256
9102	MetaboAnalyst	3	34302029	18360
9103	HMDB	1	34302029	19168
9104	HMDB	1	34303354	4860
9105	METLIN	1	34303354	4879
9106	MetaboAnalystR	1	34303354	7173
9107	XCMS	1	34305597	5003
9108	XCMS online	1	34305597	5003
9109	Workflow4Metabolomics	1	34305649	3562
9110	XCMS	1	34305653	3560
9111	XCMS	1	34305679	4012
9112	MetaboAnalyst	1	34305679	5451
9113	ropls	1	34307370	1949
9114	HMDB	1	34307393	1410
9115	MetaboAnalyst	1	34307393	1501
9116	LDA	1	34307537	3419
9117	MetaboAnalyst	1	34308298	640
9118	HMDB	1	34308390	8526
9119	MoNA	1	34308390	8579
9120	Skyline	1	34308390	8784
9121	LDA	1	34308934	6520
9122	HMDB	1	34309751	2497
9123	METLIN	2	34310644	8977
9124	GNPS	3	34311583	4515
9125	XCMS	1	34312460	3016
9126	MS-FINDER	1	34316339	3012
9127	MAVEN	1	34320357	7274
9128	MetaboAnalyst	1	34321534	2616
9129	XCMS	1	34322107	2661
9130	CAMERA	1	34322107	2666
9131	HMDB	1	34322107	2843
9132	missForest	1	34324142	5931
9133	Skyline	1	34326354	4081
9134	MetaboAnalyst	1	34326354	4228
9135	LDA	1	34326769	2239
9136	BATMAN	2	34326918	1373
9137	HMDB	1	34326918	7173
9138	METLIN	1	34326918	7201
9139	MetaboAnalyst	5	34328183	13013
9140	AMDIS	1	34328183	17151
9141	MetaboAnalyst	1	34330901	18710
9142	MZmine 2	1	34330957	4675
9143	GNPS	1	34330957	5539
9144	Skyline	1	34330982	15124
9145	XCMS	1	34330982	17892
9146	METLIN	1	34335266	5638
9147	MetaboAnalyst	1	34335266	6304
9148	ropls	1	34335628	9306
9149	ChemSpider	1	34335655	6529
9150	MetaboAnalyst	1	34335892	5126
9151	HMDB	1	34335977	6756
9152	METLIN	1	34335977	6791
9153	MetaboAnalyst	3	34336106	2905
9154	MetScape	1	34336106	3459
9155	MetaboAnalyst	2	34336118	10543
9156	MetaboAnalyst	1	34336923	5127
9157	IsoMS	1	34336977	3688
9158	IsoMS-Quant	1	34336977	3688
9159	MetaboAnalyst	2	34336977	4490
9160	HMDB	1	34339394	2087
9161	mixOmics	1	34339477	11117
9162	DrugBank	5	34340747	770
9163	HMDB	2	34340747	7796
9164	SMART	1	34340747	17171
9165	NMRProcFlow	2	34341439	4206
9166	HMDB	1	34341439	5173
9167	MetaboAnalyst	1	34341439	5395
9168	XCMS	1	34341764	4080
9169	GNPS	21	34342533	400
9170	MZmine 2	3	34342533	1257
9171	MetaboAnalyst	2	34342533	15215
9172	XCMS	2	34344378	3865
9173	CAMERA	2	34344378	3946
9174	MetFrag	1	34344994	2511
9175	Skyline	1	34346752	9987
9176	METLIN	1	34347189	8231
9177	MetaboAnalyst	1	34348131	15071
9178	MAVEN	1	34348140	17578
9179	GNPS	2	34348672	6980
9180	mixOmics	1	34348785	10415
9181	HMDB	2	34348794	2590
9182	XCMS	1	34348794	4910
9183	MetaboAnalyst	4	34348794	5898
9184	AMDIS	1	34349620	6081
9185	HMDB	1	34349647	3754
9186	METLIN	1	34349647	3760
9187	MetaboAnalyst	1	34349647	3773
9188	UniProtKB	1	34349874	6667
9189	XCMS	1	34350171	3927
9190	MetaboAnalyst	1	34350171	4249
9191	FALCON	1	34354044	2140
9192	HMDB	1	34354095	12713
9193	MetaboAnalyst	1	34354095	14256
9194	MS-FINDER	1	34354980	6606
9195	MetFrag	1	34354980	6639
9196	HMDB	1	34354980	6706
9197	XCMS	1	34354984	4198
9198	MetaboAnalyst	2	34355150	7511
9199	HMDB	1	34355150	8278
9200	FingerID	1	34355150	8498
9201	CANOPUS	1	34355150	8583
9202	UniProtKB	1	34355890	4705
9203	ChemSpider	1	34356310	9216
9204	XCMS	1	34356310	9358
9205	XCMS online	1	34356310	9358
9206	MetaboAnalyst	1	34356352	15372
9207	UniProtKB	1	34356514	2881
9208	GNPS	3	34356822	8844
9209	METLIN	1	34357327	12424
9210	SMPDB	1	34357334	9161
9211	HMDB	1	34357334	9250
9212	GNPS	2	34357338	16919
9213	BinBase	1	34357342	1595
9214	MetaboAnalyst	3	34357342	2180
9215	CMM	3	34357350	275
9216	VMH	2	34357350	1493
9217	missForest	1	34357357	16269
9218	LIPID MAPS	1	34357360	2329
9219	METLIN	1	34359365	4224
9220	HMDB	1	34359365	4293
9221	MetaboAnalyst	1	34359365	4487
9222	HMDB	1	34360606	5767
9223	GNPS	1	34360606	5793
9224	MetaboAnalyst	1	34360606	20949
9225	MZmine 2	1	34360651	6958
9226	MetaboAnalyst	1	34360674	11688
9227	MetaboAnalyst	9	34360722	4174
9228	HMDB	4	34360722	6605
9229	LIPID MAPS	4	34360852	5680
9230	XCMS	2	34360852	10331
9231	MetaboAnalyst	1	34360852	15254
9232	MetaboLights	1	34360938	3447
9233	UniProtKB	1	34360938	10816
9234	XCMS	1	34361691	4442
9235	XCMS	1	34361751	2065
9236	HMDB	1	34361796	2562
9237	LDA	2	34361914	11986
9238	BioCyc	1	34361918	13221
9239	MetaboAnalyst	1	34362286	5597
9240	LDA	3	34366839	8467
9241	UniProtKB	1	34366899	1479
9242	LDA	1	34367139	4900
9243	MetaboAnalyst	2	34367220	3781
9244	METLIN	1	34367220	4593
9245	HMDB	2	34367220	4659
9246	LDA	2	34370722	6869
9247	LIPID MAPS	1	34371592	2691
9248	HMDB	1	34371592	2796
9249	MetaboAnalyst	1	34371592	3277
9250	HMDB	1	34371638	6127
9251	MetAlign	1	34371638	7518
9252	GNPS	2	34371722	3714
9253	HMDB	1	34373444	4493
9254	LIPID MAPS	1	34373444	4521
9255	HMDB	1	34373532	5121
9256	MetaboAnalyst	1	34373532	5389
9257	MetScape	1	34373532	5832
9258	XCMS	1	34374964	6593
9259	ChemSpider	1	34376166	7624
9260	HMDB	1	34376166	7660
9261	XCMS	1	34377024	1773
9262	MetaboAnalyst	1	34378323	4172
9263	ChemSpider	2	34379420	4679
9264	BioCyc	1	34379420	4705
9265	HMDB	1	34379420	4750
9266	MetaboAnalyst	4	34379420	4955
9267	Skyline	1	34379420	10108
9268	METLIN	1	34380455	3915
9269	HMDB	1	34380455	3933
9270	XCMS	1	34380554	2664
9271	XCMS online	1	34380554	2664
9272	MetaboliteDetector	1	34380725	9796
9273	HMDB	2	34381036	12998
9274	ChemSpider	1	34381081	16609
9275	HMDB	1	34381081	16637
9276	MetaboAnalyst	1	34381081	17613
9277	LIPID MAPS	1	34381485	4530
9278	MetaboAnalyst	1	34381487	3720
9279	MetaboAnalyst	2	34381832	5883
9280	MetaboAnalyst	1	34384363	5830
9281	LDA	4	34384375	5759
9282	METLIN	1	34384375	8586
9283	LipidXplorer	2	34384788	5198
9284	CANOPUS	2	34385537	9521
9285	HMDB	1	34385924	6116
9286	METLIN	1	34385924	6172
9287	MetaboAnalyst	1	34385924	6523
9288	LDA	1	34385924	8403
9289	XCMS	1	34386520	4988
9290	HMDB	1	34386520	5219
9291	MetaboAnalyst	2	34387942	2203
9292	MetaboAnalyst	1	34389039	3728
9293	BATMAN	1	34393777	3188
9294	MetaboAnalyst	2	34393777	8257
9295	MAVEN	1	34393798	11670
9296	XCMS	1	34394382	4430
9297	LDA	1	34394382	5822
9298	NMRProcFlow	2	34395249	680
9299	MetaboAnalyst	1	34395249	1801
9300	XCMS	1	34395309	4674
9301	MetaboAnalyst	3	34395309	5209
9302	HMDB	1	34395309	7385
9303	SMPDB	1	34395309	7433
9304	icoshift	1	34395316	4610
9305	HMDB	1	34395316	4843
9306	mixOmics	1	34395316	5394
9307	AMDIS	1	34395404	2203
9308	GNPS	3	34395628	7546
9309	GNPS	6	34398662	6893
9310	MetaboAnalyst	1	34398662	12395
9311	HMDB	1	34399766	3492
9312	MetaboAnalyst	1	34399766	3604
9313	LDA	1	34399766	4774
9314	SMART	1	34399856	6102
9315	METLIN	1	34400650	10247
9316	HMDB	1	34400650	10547
9317	MetaboAnalyst	1	34400650	13999
9318	NMRProcFlow	1	34400733	2949
9319	MetaboAnalyst	2	34400733	3752
9320	MetaboLights	1	34402867	5673
9321	MetaboAnalyst	2	34402867	6265
9322	hRUV	10	34404777	3900
9323	HMDB	1	34404870	5691
9324	LIPID MAPS	2	34408170	10394
9325	HMDB	1	34408722	3025
9326	MetaboAnalyst	3	34408722	3173
9327	MetaboAnalyst	2	34412589	5802
9328	LDA	2	34412707	10365
9329	BinBase	4	34412707	14800
9330	LIPID MAPS	1	34413784	3430
9331	MetaboAnalyst	4	34414180	4502
9332	MetaboAnalyst	1	34414211	14717
9333	XCMS	3	34414480	7726
9334	CAMERA	1	34414480	8018
9335	METLIN	1	34414480	10073
9336	HMDB	1	34414480	10236
9337	MetFrag	2	34414480	10413
9338	MetaboAnalyst	1	34414480	11323
9339	MoNA	1	34416878	7705
9340	HMDB	1	34416878	7865
9341	MetaboAnalyst	1	34416878	10450
9342	XCMS	1	34418957	8171
9343	SMPDB	1	34419103	6911
9344	SMART	1	34419147	16242
9345	MetNormalizer	1	34419147	23122
9346	HMDB	1	34419147	23375
9347	MetaboAnalyst	1	34419147	24141
9348	LDA	2	34419147	28685
9349	MetaboAnalyst	1	34421595	3489
9350	HMDB	1	34421595	3834
9351	METLIN	1	34421595	3843
9352	SMART	1	34421606	1357
9353	HMDB	1	34421618	7966
9354	MetaboAnalyst	1	34421618	10079
9355	MetaboAnalyst	1	34421832	6097
9356	HMDB	1	34421842	5876
9357	MetaboAnalyst	1	34421957	7605
9358	VANTED	1	34421957	8027
9359	HMDB	1	34422677	6460
9360	ropls	2	34422677	6904
9361	HMDB	1	34422886	5215
9362	MetaboAnalyst	1	34423788	4871
9363	XCMS	1	34424431	6189
9364	MetaboAnalyst	2	34424431	8141
9365	missForest	2	34426590	17149
9366	UniProtKB	1	34428253	2563
9367	Newton	1	34429473	14793
9368	ORCA	1	34429473	15024
9369	Newton	1	34432431	5902
9370	XCMS	1	34434104	3706
9371	METLIN	1	34434104	3950
9372	HMDB	1	34434104	4075
9373	HMDB	1	34434168	2282
9374	MetaboAnalyst	1	34434174	7406
9375	HMDB	1	34434206	5902
9376	ropls	1	34434206	6313
9377	MetaboAnalyst	3	34434895	4210
9378	METLIN	1	34434895	5568
9379	HMDB	1	34434895	5607
9380	MZmine 2	1	34435049	6895
9381	ChemSpider	1	34435049	7003
9382	METLIN	1	34435049	7019
9383	BiGG Models	1	34436106	2824
9384	LDA	2	34436125	6680
9385	GNPS	2	34436250	5009
9386	MZmine 2	1	34436254	4518
9387	MZmine 2	1	34436278	6218
9388	GNPS	2	34436278	6473
9389	GNPS	1	34436280	12924
9390	MetaboAnalyst	1	34436412	6911
9391	HMDB	1	34436413	5464
9392	MetaboAnalyst	2	34436413	5699
9393	MetaboAnalyst	2	34436416	6720
9394	Skyline	1	34436417	9727
9395	AMDIS	1	34436419	3552
9396	SpectConnect	1	34436419	3728
9397	HMDB	2	34436422	6077
9398	MetaboAnalyst	1	34436422	9007
9399	MetAlign	2	34436423	3436
9400	HMDB	1	34436423	4258
9401	MetaboAnalyst	1	34436423	5126
9402	HMDB	1	34436427	5846
9403	MetaboAnalyst	3	34436427	6835
9404	XCMS	1	34436428	4829
9405	MetaboAnalyst	2	34436428	5590
9406	mixOmics	1	34436431	2139
9407	XCMS	2	34436433	444
9408	MetaboAnalyst	1	34436446	3761
9409	icoshift	1	34436446	4509
9410	MetaboAnalyst	1	34436447	3836
9411	MetaboAnalyst	2	34436448	4302
9412	MetaboAnalyst	1	34436452	5375
9413	BinBase	1	34436461	8291
9414	MetaboAnalyst	1	34436461	9098
9415	MoNA	1	34436464	5811
9416	MetAlign	1	34436465	3741
9417	HMDB	1	34436466	2533
9418	MetaboAnalyst	1	34436466	2598
9419	MetaboLights	1	34436476	4168
9420	mixOmics	2	34436476	5719
9421	MetaboAnalyst	1	34436476	6249
9422	MetExplore	1	34436476	6366
9423	MZmine 2	1	34436482	7285
9424	GNPS	4	34436482	8332
9425	MetaboLights	1	34436485	6659
9426	MetaboAnalyst	1	34436485	8614
9427	MetaboAnalyst	1	34436492	5956
9428	LIPID MAPS	1	34436495	6467
9429	METLIN	1	34436495	6479
9430	XCMS	1	34437761	5079
9431	CAMERA	1	34437761	5084
9432	Pathview	1	34437761	7641
9433	HMDB	1	34438746	4563
9434	LIPID MAPS	1	34438746	4631
9435	METLIN	1	34439018	4146
9436	ChemSpider	1	34439061	293
9437	GNPS	1	34439134	4390
9438	Skyline	2	34439283	3503
9439	HMDB	1	34439333	5001
9440	MetaboAnalyst	1	34439333	6053
9441	SMART	1	34439431	167
9442	MAVEN	1	34439445	9825
9443	MetaboAnalyst	1	34439445	11585
9444	LDA	1	34439458	4126
9445	MetaboAnalyst	1	34439475	8213
9446	XCMS	1	34439509	6047
9447	Newton	1	34439843	673
9448	ORCA	1	34439843	7292
9449	IsoMS	1	34440002	3127
9450	MyCompoundID	1	34440002	3851
9451	MetaboAnalyst	2	34440002	4012
9452	LDA	1	34440269	6220
9453	GAM	2	34440269	7904
9454	MZmine 2	1	34440277	13113
9455	XCMS	1	34440531	8412
9456	MetaboAnalyst	1	34441485	2838
9457	LDA	1	34442626	5561
9458	XCMS	1	34442638	4602
9459	MoNA	1	34442758	8288
9460	HMDB	1	34443394	4690
9461	Skyline	1	34443686	5369
9462	MetaboAnalyst	1	34443686	5624
9463	HMDB	2	34443686	6089
9464	LDA	2	34444002	5792
9465	PARADISe	1	34444679	3814
9466	MZmine 2	1	34445180	4952
9467	GNPS	2	34445541	3497
9468	AMDIS	1	34445573	1904
9469	MetaboAnalyst	1	34445736	9960
9470	AMDIS	3	34445744	3806
9471	SpectConnect	2	34445744	4173
9472	MZmine 2	1	34445761	10330
9473	GNPS	2	34445761	11265
9474	XCMS	1	34446058	6192
9475	HMDB	1	34446738	4766
9476	FingerID	2	34446738	4931
9477	MetaboAnalyst	2	34446738	5607
9478	METLIN	2	34446754	8201
9479	MetaboAnalyst	1	34451575	9641
9480	SMART	1	34451684	786
9481	GNPS	5	34451756	1662
9482	MetAlign	1	34451796	4751
9483	HMDB	1	34451796	5926
9484	XCMS	1	34454525	3723
9485	MetaboAnalyst	1	34454525	6523
9486	MetaboAnalyst	1	34458353	5158
9487	MetaboAnalyst	1	34460840	9777
9488	ORCA	1	34461853	5525
9489	ropls	2	34462546	11764
9490	HMDB	1	34465274	3922
9491	METLIN	1	34465274	3985
9492	HMDB	1	34465311	17450
9493	METLIN	1	34465311	17478
9494	MetScape	3	34466570	598
9495	MetaboAnalyst	1	34468196	8931
9496	MetaboAnalyst	1	34469070	9146
9497	ORCA	1	34471141	10217
9498	XCMS	1	34473644	1395
9499	LDA	1	34473644	3183
9500	XCMS	2	34473691	5679
9501	METLIN	1	34473691	5791
9502	ChemSpider	1	34474531	1277
9503	MetaboAnalyst	1	34475489	5694
9504	XCMS	2	34483850	6510
9505	CAMERA	1	34483850	6672
9506	HMDB	1	34484294	3165
9507	METLIN	1	34484294	3175
9508	MetaboAnalyst	1	34484294	4317
9509	MetaboAnalyst	1	34485269	7176
9510	AMDIS	1	34488832	6522
9511	SMPDB	1	34488832	7659
9512	HMDB	1	34488839	5026
9513	METLIN	1	34488839	5046
9514	ropls	1	34488839	6211
9515	mQTL	1	34488839	9734
9516	LDA	1	34489732	1981
9517	MetaboAnalyst	1	34489737	3169
9518	MetaboAnalyst	2	34489846	4355
9519	LDA	1	34489885	4265
9520	HMDB	2	34489885	6810
9521	MetaboAnalyst	2	34489885	7617
9522	LDA	2	34489916	4693
9523	UniProtKB	1	34490218	4137
9524	XCMS	2	34490390	5911
9525	CAMERA	1	34490390	6086
9526	MetaboAnalyst	1	34490390	7267
9527	METLIN	1	34491455	23799
9528	MZmine 2	1	34492634	3413
9529	MetaboLights	1	34492634	4549
9530	METLIN	1	34492635	2907
9531	MetaboAnalyst	1	34492635	3804
9532	HMDB	1	34492635	3897
9533	MSPrep	3	34493210	61196
9534	missForest	1	34493210	63799
9535	MZmine 2	2	34493657	3729
9536	MIDAS	1	34493790	4728
9537	Newton	1	34494481	655
9538	HMDB	1	34494854	13691
9539	ropls	1	34494854	13776
9540	HMDB	1	34496740	4205
9541	MetaboAnalyst	1	34497510	7769
9542	VMH	1	34497510	7895
9543	HMDB	1	34497512	3016
9544	MetaboAnalyst	1	34497512	3158
9545	MetaboAnalyst	2	34497521	434
9546	MetaboAnalyst	1	34497820	2794
9547	IsoCor	1	34498076	18776
9548	HMDB	1	34498202	7251
9549	XCMS	2	34498202	7414
9550	MetaboAnalyst	1	34498714	3885
9551	HMDB	1	34500559	3771
9552	GNPS	6	34500798	5466
9553	METLIN	1	34500850	5939
9554	MassTRIX	1	34500850	7022
9555	LDA	2	34501329	3766
9556	MetaboAnalyst	2	34502454	4435
9557	MetaboAnalyst	1	34502505	4417
9558	ropls	1	34504837	6625
9559	Skyline	1	34504837	6979
9560	LDA	1	34507608	5491
9561	HMDB	1	34507608	7603
9562	MetaboAnalyst	1	34507608	8679
9563	GAM	1	34507608	8935
9564	HMDB	1	34511531	3087
9565	LDA	1	34512358	6444
9566	MetaboAnalyst	1	34512584	5356
9567	MetaboAnalyst	1	34512700	10625
9568	HMDB	1	34512866	2283
9569	MetaboAnalyst	2	34512866	2373
9570	MetaboAnalyst	1	34512869	4940
9571	Pathview	1	34512869	5559
9572	rNMR	1	34517928	7565
9573	MZmine 2	2	34518599	1956
9574	MetaboAnalyst	1	34518615	9785
9575	SMPDB	1	34518615	9854
9576	MetaboAnalyst	1	34521427	7999
9577	HMDB	1	34521429	36515
9578	XCMS	1	34521943	5903
9579	MetaboAnalyst	3	34521943	6762
9580	HMDB	1	34522117	3472
9581	MetaboAnalyst	3	34522117	4116
9582	XCMS	1	34523684	13633
9583	mzMatch	2	34523684	13694
9584	XCMS	1	34523982	7185
9585	HMDB	1	34523982	7593
9586	ropls	1	34523982	12469
9587	Skyline	2	34526566	6454
9588	METLIN	1	34526905	2343
9589	HMDB	1	34526905	2383
9590	MetaboAnalyst	1	34526905	2420
9591	XCMS	5	34527005	7081
9592	CAMERA	4	34527005	7090
9593	GNPS	6	34527005	7928
9594	UniProtKB	2	34527085	6785
9595	HMDB	1	34531898	7571
9596	METLIN	1	34531898	7609
9597	MetaboAnalyst	1	34537001	3801
9598	METLIN	1	34537901	7126
9599	MetaboAnalyst	2	34537901	8374
9600	LIPID MAPS	1	34539643	5821
9601	METLIN	1	34539643	5847
9602	MAIT	4	34539656	665
9603	MetaboAnalystR	1	34539690	6473
9604	MetaboAnalyst	1	34539707	16940
9605	HMDB	1	34540890	6569
9606	MetaboAnalystR	1	34540890	7939
9607	speaq	1	34540890	11661
9608	BATMAN	1	34540890	11718
9609	MetaboAnalyst	1	34541422	5056
9610	SMART	1	34544751	2344
9611	MetaboAnalyst	2	34545253	6850
9612	MetaboAnalyst	1	34546070	6046
9613	iPath	1	34546070	6098
9614	MetaboAnalyst	1	34547020	5364
9615	HMDB	1	34547020	5467
9616	GAM	3	34547184	3576
9617	LIPID MAPS	1	34547287	13655
9618	METLIN	1	34548018	2524
9619	ChemSpider	1	34548030	14265
9620	LDA	1	34548111	6265
9621	Skyline	1	34548906	5848
9622	XCMS	1	34549263	1103
9623	MetaboAnalyst	1	34549263	2066
9624	SMART	1	34549988	465
9625	XCMS	2	34549988	6432
9626	HMDB	2	34551799	2907
9627	MetaboAnalyst	1	34552080	14150
9628	MetFrag	5	34552125	6266
9629	BioCyc	3	34552125	6554
9630	msPurity	2	34552125	6718
9631	HMDB	1	34552125	9895
9632	METLIN	1	34552271	18549
9633	MassTRIX	1	34552271	18633
9634	MetaboAnalyst	1	34552271	18833
9635	LIPID MAPS	1	34552688	5737
9636	XCMS	1	34552973	3308
9637	MetaboAnalyst	2	34553313	4737
9638	HMDB	1	34553313	4955
9639	SMPDB	1	34553313	6090
9640	LDA	1	34555041	6480
9641	MetaboAnalyst	1	34556726	4679
9642	HMDB	1	34557088	8668
9643	HMDB	1	34557102	11476
9644	MetaboAnalyst	1	34557163	4977
9645	XCMS	1	34557457	3570
9646	HMDB	2	34557741	3967
9647	MetaboAnalyst	1	34557741	4182
9648	MoNA	1	34558676	8894
9649	MetaboAnalyst	1	34559203	4080
9650	LDA	1	34560884	6070
9651	MetaboAnalyst	1	34560884	12095
9652	MetaboSignal	1	34561442	6617
9653	AMDIS	2	34563235	12523
9654	ChemoSpec	1	34564386	5446
9655	MetaboAnalyst	2	34564391	3281
9656	MetaboAnalyst	3	34564394	7638
9657	ChemSpider	1	34564394	12061
9658	LDA	1	34564399	6157
9659	MetImp	2	34564400	17933
9660	MetAlign	2	34564401	1820
9661	HMDB	1	34564401	3348
9662	MetaboAnalyst	1	34564402	2709
9663	Bayesil	2	34564406	5156
9664	MetaboAnalyst	1	34564406	8341
9665	HMDB	1	34564407	2656
9666	MetaboAnalyst	1	34564407	4422
9667	AMDIS	1	34564409	7694
9668	METLIN	1	34564409	8736
9669	MoNA	1	34564409	8910
9670	mixOmics	2	34564409	9880
9671	MoNA	1	34564411	3653
9672	MetaboAnalyst	1	34564411	4081
9673	AMDIS	1	34564421	3086
9674	LDA	4	34564421	4232
9675	MetaboAnalyst	1	34564423	4880
9676	MetaboAnalyst	1	34564428	4081
9677	METLIN	1	34564431	4563
9678	HMDB	2	34564435	3478
9679	RaMP	1	34564435	5068
9680	SMPDB	1	34564435	5237
9681	MAVEN	2	34564438	4665
9682	MetaboAnalyst	4	34564443	6655
9683	METLIN	1	34564444	2348
9684	MetaboAnalyst	2	34564444	4010
9685	MetaboAnalyst	1	34564449	5907
9686	MZmine 2	1	34564454	6003
9687	MetImp	2	34564454	9046
9688	MetaboAnalyst	2	34564454	9741
9689	HMDB	1	34564454	9913
9690	MetaboAnalyst	1	34564459	6389
9691	HMDB	1	34564460	11208
9692	XCMS	3	34564460	15357
9693	MetaboAnalyst	1	34564460	20160
9694	MetaboAnalyst	2	34564461	5675
9695	HMDB	1	34564461	10000
9696	MetaboAnalyst	1	34565479	3483
9697	GNPS	2	34565804	1522
9698	ropls	1	34566645	3993
9699	MetaboAnalyst	2	34566645	4541
9700	LDA	1	34566901	2164
9701	MetaboAnalyst	1	34566901	3493
9702	SMART	1	34566957	1014
9703	MetaboAnalyst	1	34567002	2630
9704	MetaboAnalystR	1	34567035	9113
9705	XCMS	1	34567215	2445
9706	HMDB	1	34568427	3811
9707	LIPID MAPS	1	34568427	3880
9708	MAVEN	1	34568829	4094
9709	MetaboAnalyst	1	34568829	9699
9710	XCMS	1	34569311	3080
9711	mixOmics	2	34571893	7320
9712	MetaboAnalyst	1	34571936	1041
9713	MAVEN	1	34571942	5996
9714	HMDB	1	34571942	6799
9715	MetaboAnalyst	3	34571942	9012
9716	El-MAVEN	2	34571983	11398
9717	MAVEN	2	34571983	11401
9718	HMDB	1	34572516	4404
9719	MetaboAnalyst	1	34572600	2162
9720	MetaboAnalyst	3	34573458	3204
9721	BinBase	1	34573464	5425
9722	MetaboAnalyst	3	34573464	7129
9723	MetaboAnalyst	2	34573467	4427
9724	MetaboAnalyst	1	34574135	7455
9725	HMDB	2	34574275	4994
9726	LDA	2	34574323	6432
9727	MetaboAnalyst	1	34575062	3028
9728	UC2	1	34575139	13828
9729	MetaboAnalyst	4	34575139	15801
9730	HMDB	1	34575139	17834
9731	XCMS	1	34575431	10512
9732	MetaboAnalyst	2	34575614	4428
9733	HMDB	1	34575614	7241
9734	AMDIS	1	34576273	2724
9735	SpectConnect	1	34576273	2779
9736	MetaboAnalyst	1	34576273	3744
9737	MZmine 2	1	34576306	13287
9738	MetaboAnalyst	2	34576306	14721
9739	GNPS	4	34576698	4230
9740	GNPS	5	34576706	17932
9741	AMDIS	1	34576740	10315
9742	HMDB	1	34576740	10938
9743	MetaboAnalyst	4	34576740	13052
9744	mixOmics	1	34576834	7174
9745	HMDB	1	34577050	2542
9746	HMDB	1	34577635	5556
9747	MetaboAnalyst	2	34577635	5607
9748	LDA	2	34578849	6113
9749	MetaboAnalyst	1	34578904	7945
9750	SMPDB	1	34578904	8163
9751	LDA	1	34578924	6064
9752	MetaboAnalyst	2	34578983	3755
9753	BioCyc	1	34579001	9014
9754	XCMS	2	34579131	2256
9755	XCMS online	1	34579131	2256
9756	MyCompoundID	1	34579131	2710
9757	HMDB	1	34579131	2833
9758	MetaboAnalyst	5	34579131	2911
9759	SMPDB	1	34579131	3944
9760	MetaboAnalyst	2	34579160	12988
9761	HMDB	1	34579160	14407
9762	MetAlign	1	34579322	3957
9763	MetaboAnalyst	1	34579580	14374
9764	MetaboAnalyst	1	34580274	9735
9765	CAMERA	1	34581684	7806
9766	HMDB	1	34585111	14022
9767	ropls	2	34585111	15124
9768	mixOmics	3	34585111	15136
9769	Skyline	1	34585527	5672
9770	MetaboLights	1	34585527	7246
9771	LIQUID	1	34588474	9455
9772	HMDB	1	34588992	9029
9773	MetaboAnalyst	1	34589420	1373
9774	XCMS	1	34593007	4025
9775	Pathview	1	34593556	11604
9776	HMDB	2	34593948	8321
9777	BioCyc	1	34593948	8350
9778	LIPID MAPS	1	34593948	8658
9779	METLIN	1	34593948	8676
9780	ChemSpider	1	34593997	14005
9781	XCMS	1	34595199	6970
9782	MetaboAnalyst	1	34595216	7598
9783	SMPDB	1	34595216	7991
9784	MetaboAnalyst	1	34601638	5779
9785	MetaboAnalyst	2	34603031	7385
9786	MetaboAnalyst	1	34603060	6184
9787	MAVEN	1	34603239	5116
9788	PiMP	2	34603243	3227
9789	HMDB	1	34603243	3480
9790	MetaboAnalyst	1	34603243	5646
9791	SMPDB	1	34603243	7013
9792	LDA	1	34603247	10359
9793	HMDB	1	34603257	8495
9794	MetaboAnalyst	1	34604232	3305
9795	DrugBank	2	34604271	2880
9796	XCMS	1	34604284	1542
9797	XCMS online	1	34604284	1542
9798	MetaboAnalystR	1	34605330	5708
9799	mixOmics	1	34607466	10485
9800	MetaboAnalyst	3	34608951	3589
9801	MetaboLights	1	34608951	11102
9802	MetaboAnalyst	1	34610054	5736
9803	Skyline	5	34610319	11592
9804	UniProtKB	1	34612663	3549
9805	MelonnPan	1	34612663	5579
9806	MetaboAnalyst	1	34612663	6823
9807	mixOmics	1	34612663	8652
9808	HMDB	1	34612668	5565
9809	MetaboAnalyst	1	34612668	5781
9810	MetaboLights	1	34612668	5981
9811	MetaboAnalyst	5	34615530	5691
9812	METLIN	2	34615530	6509
9813	SMPDB	1	34615530	7297
9814	HMDB	1	34615530	7390
9815	Skyline	2	34616026	16538
9816	MetaboLights	2	34618073	6961
9817	XCMS	1	34618855	7947
9818	MetaboAnalyst	2	34620173	5978
9819	MetaBridge	2	34620173	11245
9820	MetaboAnalyst	1	34620947	5618
9821	UniProtKB	1	34620947	6787
9822	Skyline	1	34621259	9075
9823	VMH	1	34622163	2218
9824	UniProtKB	1	34622163	7051
9825	DrugBank	1	34622163	7128
9826	ChemSpider	1	34627378	8716
9827	HMDB	1	34630079	2473
9828	XCMS	2	34630316	5383
9829	CAMERA	1	34630316	5561
9830	METLIN	1	34630316	7510
9831	HMDB	1	34630316	7586
9832	MetaboAnalyst	1	34630316	9139
9833	ChemSpider	1	34630321	3070
9834	MetaboAnalyst	2	34630321	4764
9835	ropls	1	34630385	2956
9836	LDA	2	34630385	4256
9837	XCMS	1	34630473	2808
9838	MetaboAnalystR	2	34630478	5454
9839	MetaboAnalyst	1	34630613	5944
9840	HMDB	1	34634050	4786
9841	icoshift	1	34634050	4888
9842	ropls	1	34635603	3660
9843	MetaboAnalyst	1	34636159	4784
9844	MetaboAnalyst	2	34637456	8067
9845	MetaboAnalyst	1	34638563	5392
9846	XCMS	1	34638761	8556
9847	XCMS	2	34638812	3058
9848	METLIN	1	34638812	3609
9849	MetaboAnalyst	1	34638812	3668
9850	MetaboAnalyst	1	34638943	8545
9851	HMDB	1	34638943	14156
9852	MAIT	5	34638950	15
9853	AMDIS	1	34638954	14102
9854	MET-IDEA	1	34638954	15007
9855	MetaboAnalyst	1	34638954	15849
9856	HMDB	1	34639114	6241
9857	icoshift	1	34639158	4014
9858	MetaboAnalyst	1	34640372	7884
9859	GNPS	2	34641295	5431
9860	missForest	3	34641330	2630
9861	ChemSpider	1	34641340	5729
9862	XCMS	3	34641553	5893
9863	XCMS online	2	34641553	6116
9864	MetaboAnalyst	1	34641553	7255
9865	HMDB	2	34641844	3045
9866	XCMS	1	34641951	6450
9867	GNPS	1	34641951	7604
9868	mixOmics	1	34641951	7901
9869	LDA	1	34641951	8196
9870	MetaboAnalyst	1	34641951	9052
9871	Skyline	1	34642781	4711
9872	MetaboAnalyst	1	34645459	6693
9873	ChemSpider	1	34645877	7214
9874	MetaboAnalyst	1	34645877	7536
9875	METLIN	1	34646164	12571
9876	HMDB	1	34646164	12621
9877	ChemSpider	1	34646171	4432
9878	HMDB	1	34646171	4795
9879	MetaboAnalyst	1	34646171	5696
9880	Skyline	2	34646367	8127
9881	GAM	3	34648693	4363
9882	FingerID	36	34650271	1587
9883	HMDB	6	34650271	6972
9884	GNPS	2	34650271	23590
9885	MetaboAnalyst	2	34650271	23788
9886	MetFrag	2	34650271	48540
9887	MAGMa	2	34650271	48549
9888	CFM-ID	2	34650271	48557
9889	ChemSpider	4	34650271	49245
9890	HMDB	2	34650432	4580
9891	BATMAN	2	34650432	5004
9892	MetaboAnalyst	1	34650434	4447
9893	MetaboAnalyst	1	34650548	7817
9894	XCMS	2	34650578	6268
9895	CAMERA	1	34650578	6516
9896	LDA	2	34650578	8956
9897	HMDB	1	34650578	9502
9898	MS-FLO	1	34654818	11181
9899	AMDIS	1	34657143	9899
9900	XCMS	2	34658884	2464
9901	CAMERA	1	34658884	2591
9902	HMDB	1	34658884	2847
9903	METLIN	1	34658884	2875
9904	MetaboAnalyst	1	34658884	3960
9905	MetaboAnalystR	1	34659295	5876
9906	ropls	1	34659308	8381
9907	HMDB	1	34659317	1705
9908	HMDB	1	34662237	29523
9909	MetaboLights	1	34662237	29595
9910	MetaboAnalyst	1	34667167	9301
9911	SMPDB	1	34667167	9528
9912	MS-FINDER	2	34667167	12919
9913	HMDB	2	34667167	13062
9914	MoNA	1	34667167	14451
9915	UniProtKB	1	34669446	7350
9916	LDA	1	34670873	1683
9917	HMDB	1	34670873	2746
9918	MetaboAnalyst	1	34671116	9228
9919	METLIN	3	34673019	6397
9920	LIPID MAPS	3	34673019	6665
9921	MetFrag	1	34673019	13779
9922	XCMS	2	34675267	3769
9923	AMDIS	1	34675267	4210
9924	MetaboAnalyst	1	34675267	5390
9925	XCMS	1	34675385	34348
9926	PARADISe	1	34675385	39297
9927	LDA	1	34675893	5260
9928	LDA	1	34675901	5499
9929	ICT	1	34677365	657
9930	MetaboAnalyst	2	34677365	4411
9931	SMPDB	1	34677365	5080
9932	XCMS	1	34677366	3439
9933	MetaboAnalyst	2	34677366	4818
9934	MetaboAnalyst	3	34677371	7846
9935	MetaboAnalyst	2	34677373	8942
9936	HMDB	1	34677374	1806
9937	MetaboAnalyst	1	34677379	7824
9938	HMDB	2	34677396	6061
9939	GNPS	2	34677399	2972
9940	METLIN	1	34677404	5755
9941	ChemSpider	1	34677404	5820
9942	MetaboAnalyst	1	34677404	6089
9943	HMDB	1	34677417	4061
9944	MetaboAnalyst	2	34677417	6894
9945	MetAlign	1	34677423	4800
9946	MSClust	1	34677423	5132
9947	MoNA	1	34677425	3328
9948	MetaboAnalyst	1	34677426	5773
9949	ChemSpider	1	34677706	36967
9950	HMDB	1	34678984	9570
9951	MoNA	1	34678984	9615
9952	MetaboAnalyst	1	34679483	2874
9953	XCMS	1	34679637	7185
9954	CAMERA	1	34679637	8155
9955	MetaboAnalyst	2	34679637	9745
9956	MoNA	1	34679645	3595
9957	HMDB	1	34679645	3625
9958	MetaboAnalyst	2	34679645	3879
9959	Mummichog	1	34679645	4876
9960	MetaboAnalyst	1	34680207	13984
9961	METLIN	1	34680306	9929
9962	HMDB	1	34680306	9945
9963	MetaboAnalyst	1	34680696	6324
9964	SMART	1	34680700	1358
9965	MetaboAnalyst	2	34680825	3465
9966	ChemSpider	1	34681095	3657
9967	ORCA	1	34681102	8012
9968	LDA	1	34681433	8857
9969	WikiPathways	1	34681471	8517
9970	PathVisio	2	34681471	8591
9971	HMDB	1	34681485	5916
9972	MetaboAnalyst	1	34681485	6031
9973	BinBase	1	34681497	3550
9974	MetaboAnalyst	1	34681497	6377
9975	HMDB	1	34681552	9918
9976	MetaboAnalyst	1	34681552	10793
9977	HMDB	1	34681563	4232
9978	MetFrag	1	34681563	4446
9979	MetaboAnalyst	1	34681701	3326
9980	LIPID MAPS	1	34681883	2407
9981	LipidXplorer	1	34681883	5938
9982	MetaboAnalyst	3	34681883	6615
9983	CMM	1	34682210	960
9984	GAM	1	34682210	1176
9985	LDA	1	34683329	6531
9986	MetaboAnalyst	1	34684271	9386
9987	ropls	1	34684324	7395
9988	AMDIS	1	34684335	7990
9989	MetaboAnalyst	1	34684335	8549
9990	HMDB	1	34684365	6426
9991	SMPDB	1	34684365	6567
9992	XCMS	1	34684645	3296
9993	MetaboAnalyst	1	34684645	5087
9994	XCMS	1	34684759	2667
9995	MetaboAnalyst	2	34684759	3089
9996	ropls	1	34684759	4904
9997	METLIN	1	34684798	10295
9998	MetaboAnalyst	1	34685558	2105
9999	MetaboLab	4	34685569	4161
10000	icoshift	1	34685569	4282
10001	icoshift	2	34685657	14579
10002	MetaboAnalyst	2	34685801	4419
10003	MZmine 2	1	34685855	4128
10004	MetaboAnalyst	1	34685907	4430
10005	HMDB	1	34686144	3293
10006	METLIN	1	34686144	3312
10007	HMDB	1	34691101	5129
10008	METLIN	1	34691101	5221
10009	ropls	2	34691103	5955
10010	HMDB	1	34691116	4691
10011	METLIN	1	34691116	4718
10012	AMDIS	1	34691121	6545
10013	MetaboAnalystR	1	34691121	7311
10014	Pathview	1	34691140	3176
10015	HMDB	2	34692531	3124
10016	MetaboAnalyst	1	34692531	4530
10017	HMDB	1	34692558	5059
10018	ORCA	1	34693133	9182
10019	BiGG Models	1	34696732	78
10020	HMDB	1	34697380	6894
10021	METLIN	1	34697380	6906
10022	BinBase	1	34697401	5396
10023	GNPS	2	34698546	5263
10024	METLIN	1	34702323	10166
10025	DrugBank	1	34702323	13112
10026	MetaboAnalyst	2	34702323	13308
10027	mixOmics	1	34702988	14005
10028	MetaboAnalyst	1	34702988	17389
10029	SMPDB	1	34702988	17569
10030	XCMS	1	34703256	4050
10031	HMDB	1	34703256	4310
10032	METLIN	1	34703256	4321
10033	LIPID MAPS	1	34703256	4421
10034	MetaboAnalyst	1	34703256	5197
10035	HMDB	4	34707105	14450
10036	MetaboAnalyst	2	34707105	14998
10037	LDA	2	34707567	4017
10038	MS-FLO	1	34708068	12341
10039	MetaboAnalyst	1	34708068	12694
10040	METLIN	1	34708121	3017
10041	MetaboAnalyst	1	34710433	9076
10042	HMDB	1	34711543	3480
10043	XCMS	2	34711812	10522
10044	CAMERA	1	34711812	10623
10045	HMDB	1	34711812	10996
10046	El-MAVEN	1	34711973	4348
10047	MAVEN	1	34711973	4351
10048	HMDB	4	34711973	6890
10049	YMDB	2	34711973	18576
10050	XCMS	1	34712219	2978
10051	MetaboAnalyst	2	34712219	3273
10052	GNPS	2	34715914	35
10053	DrugBank	1	34716403	4237
10054	missForest	1	34718859	6491
10055	mixOmics	1	34718859	7997
10056	HMDB	1	34718859	9710
10057	LDA	2	34719692	3918
10058	MetaboAnalyst	1	34720532	5294
10059	SMART	2	34721000	2814
10060	MetaboAnalyst	1	34721000	4814
10061	LDA	1	34721011	13701
10062	HMDB	1	34721350	5510
10063	LIPID MAPS	1	34721350	5521
10064	XCMS	1	34721368	3215
10065	LDA	2	34721374	5967
10066	MoNA	1	34721374	7034
10067	HMDB	1	34721374	7081
10068	MetaboAnalyst	3	34721374	7369
10069	HMDB	1	34721396	5760
10070	MetaboAnalyst	3	34721396	5888
10071	HMDB	1	34721434	3440
10072	ropls	2	34721434	3576
10073	LDA	2	34721434	6239
10074	MZmine 2	1	34721476	4320
10075	MetaboAnalyst	1	34721476	5289
10076	GAM	2	34721619	1477
10077	MetImp	1	34721736	1858
10078	MetaboAnalyst	1	34722271	4982
10079	LDA	2	34722512	3348
10080	XCMS	1	34722512	5382
10081	MetaboAnalyst	1	34722512	6656
10082	MetaboLights	1	34722512	6891
10083	MetaboAnalystR	1	34722597	7934
10084	HMDB	1	34722683	4142
10085	MetaboAnalyst	3	34722683	5291
10086	mixOmics	1	34722695	13537
10087	XCMS	2	34724893	5057
10088	CAMERA	1	34724893	5488
10089	LIPID MAPS	1	34724893	5813
10090	GNPS	3	34725148	13138
10091	MZmine 2	1	34725148	13429
10092	GNPS	4	34725378	7678
10093	FingerID	2	34725378	10465
10094	CANOPUS	1	34725378	11068
10095	MetaboAnalyst	1	34725378	11925
10096	GNPS	6	34726483	5229
10097	FALCON	1	34730826	3293
10098	ropls	1	34730826	11716
10099	MetaboAnalyst	1	34730826	12218
10100	IIS	1	34732700	8611
10101	MetaboAnalyst	1	34732715	13086
10102	CFM-ID	1	34732779	13377
10103	MetaboAnalystR	1	34732892	1798
10104	SMART	2	34736493	3601
10105	MetaboAnalyst	1	34737295	18340
10106	MetaboAnalyst	1	34737330	3504
10107	MetaboAnalyst	2	34737748	5882
10108	MAIT	2	34737749	938
10109	HMDB	1	34737967	2708
10110	MetaboAnalyst	2	34737967	3031
10111	GNPS	3	34741130	8967
10112	Qemistree	2	34741130	9252
10113	XCMS	1	34744513	5063
10114	MetaboAnalyst	1	34744513	5335
10115	LDA	2	34744513	6699
10116	MetaboAnalyst	1	34744776	1861
10117	LDA	1	34745096	7227
10118	MetaboAnalyst	1	34745161	12565
10119	XCMS	1	34745177	2416
10120	XCMS	1	34745185	7908
10121	RAMClustR	2	34745185	8047
10122	SMART	1	34745305	2154
10123	XCMS	1	34746022	3708
10124	MetaboAnalyst	1	34746022	4739
10125	LDA	2	34746022	5094
10126	MZmine 2	1	34746230	6145
10127	GNPS	1	34746230	6982
10128	MetaboAnalyst	1	34747187	4096
10129	MetaboLights	1	34748631	862
10130	XCMS	6	34750405	9604
10131	GNPS	8	34750405	9894
10132	XCMS online	1	34750405	10127
10133	HMDB	1	34750405	13340
10134	LDA	1	34750528	13751
10135	MetaboAnalyst	1	34751131	8300
10136	SMART	3	34752455	22521
10137	XCMS	1	34753981	13159
10138	LIPID MAPS	1	34755127	23396
10139	XCMS	1	34756024	2042
10140	MetaboLights	1	34756061	4820
10141	HMDB	1	34759281	5279
10142	Skyline	1	34759281	5368
10143	MetaboAnalyst	1	34759822	5774
10144	ropls	1	34760715	5539
10145	HMDB	1	34760716	3055
10146	MelonnPan	7	34760716	3197
10147	Escher	1	34761247	10782
10148	LIPID MAPS	1	34761720	4487
10149	MetaboAnalyst	1	34761720	4781
10150	XCMS	3	34764412	4448
10151	CAMERA	4	34764412	4457
10152	HMDB	1	34764412	8055
10153	LIPID MAPS	1	34764985	820
10154	HMDB	1	34765668	3675
10155	ropls	2	34765668	4501
10156	TargetSearch	1	34765928	9118
10157	MetaboLights	1	34769107	8491
10158	PiMP	6	34769107	8677
10159	HMDB	1	34769107	9359
10160	UniProtKB	1	34769107	10348
10161	MetaboAnalyst	1	34769107	13796
10162	ChemSpider	1	34769110	11091
10163	XCMS	1	34769237	989
10164	MS-FINDER	1	34769237	1483
10165	MetaboAnalyst	1	34769237	1605
10166	HMDB	1	34769237	1928
10167	VANTED	1	34769244	15226
10168	CAMERA	1	34769491	5245
10169	AMDIS	2	34770882	2329
10170	MET-IDEA	2	34770882	2935
10171	ropls	1	34770994	4770
10172	MetaboAnalystR	1	34770994	5075
10173	GNPS	2	34771057	2995
10174	XCMS	5	34771057	5298
10175	HMDB	1	34771066	7368
10176	METLIN	1	34771066	7426
10177	MetaboAnalyst	1	34771721	3447
10178	mixOmics	1	34772452	7774
10179	METLIN	1	34776946	2131
10180	METLIN	1	34776980	723
10181	MetaboAnalyst	1	34776980	1875
10182	MetaboAnalyst	1	34777195	4440
10183	MetaboAnalyst	1	34777253	2138
10184	Skyline	1	34777268	7245
10185	MetaboAnalyst	1	34777300	7729
10186	XCMS	2	34777365	3108
10187	HMDB	1	34777408	3536
10188	LIPID MAPS	1	34777408	3556
10189	MetaboAnalyst	3	34777443	9536
10190	LDA	1	34778417	3154
10191	MetaboAnalyst	1	34778417	4564
10192	MetaboAnalyst	1	34778450	653
10193	HMDB	1	34778450	939
10194	ChemSpider	1	34780562	2099
10195	MetaboAnalyst	2	34781966	10913
10196	MetaboAnalyst	1	34781986	7435
10197	MetaboAnalyst	1	34782793	12922
10198	MAVEN	1	34783580	13190
10199	MetaboAnalyst	1	34785772	6477
10200	XCMS	1	34785942	2641
10201	MetaboAnalyst	1	34785942	4431
10202	LIPID MAPS	1	34785942	4478
10203	MetaboAnalyst	1	34789172	5602
10204	XCMS	2	34789766	3721
10205	IPO	1	34789766	3825
10206	xMSannotator	1	34789766	3978
10207	HMDB	2	34789766	4026
10208	LIPID MAPS	2	34789766	4042
10209	MetaboAnalyst	1	34789766	4880
10210	BinBase	1	34789813	6888
10211	MetaboAnalyst	1	34789813	7728
10212	MetaboAnalyst	2	34790114	2550
10213	HMDB	1	34790120	2737
10214	HMDB	1	34790674	12726
10215	GNPS	4	34792466	5650
10216	LIPID MAPS	1	34793147	7180
10217	PARADISe	1	34793277	1004
10218	GNPS	9	34793633	174
10219	DrugBank	1	34793633	6945
10220	XCMS	1	34795577	2666
10221	XCMS online	1	34795577	2666
10222	MetaboAnalyst	1	34795577	2804
10223	MetaboAnalyst	2	34796220	3653
10224	LDA	2	34797504	4968
10225	MetaboAnalyst	1	34797504	7244
10226	HMDB	1	34798822	5650
10227	LIPID MAPS	1	34798822	5685
10228	LDA	2	34799549	6748
10229	MetaboAnalyst	1	34803663	5814
10230	UniProtKB	1	34803669	12342
10231	DrugBank	1	34803705	2557
10232	MetaboAnalyst	3	34803940	7779
10233	HMDB	3	34803940	8508
10234	AMDIS	2	34804084	8716
10235	MetaboAnalyst	2	34804084	11928
10236	MetaboAnalyst	1	34804095	7474
10237	MetaboAnalyst	1	34804183	4001
10238	XCMS	1	34804604	7093
10239	MetaboLyzer	2	34804604	7380
10240	HMDB	1	34804604	8325
10241	LIPID MAPS	1	34804604	8378
10242	BioCyc	1	34804604	8400
10243	XCMS	2	34804922	2711
10244	METLIN	1	34804922	3840
10245	MetaboAnalyst	1	34804922	3983
10246	MetaboAnalyst	1	34804983	2711
10247	HMDB	1	34805802	16093
10248	LIQUID	1	34809453	5379
10249	VANTED	1	34809453	9660
10250	MetaboAnalystR	1	34809576	6436
10251	MetaboAnalyst	1	34811951	4400
10252	GNPS	1	34812649	7110
10253	METLIN	1	34812650	5979
10254	MetaboAnalyst	1	34812650	6266
10255	XCMS	1	34814909	4774
10256	mixOmics	2	34815499	3899
10257	XCMS	1	34815677	1418
10258	MetaboAnalyst	2	34815677	2272
10259	UniProtKB	1	34815808	10480
10260	HMDB	1	34819016	6594
10261	LDA	1	34819503	4104
10262	METLIN	1	34819575	13229
10263	MetaboAnalyst	1	34819936	5670
10264	METLIN	1	34819936	6201
10265	LIPID MAPS	1	34819936	6215
10266	MoNA	1	34819936	6681
10267	SigMa	3	34822375	7609
10268	XCMS	1	34822376	3275
10269	MetaboAnalyst	1	34822376	7727
10270	MetaboAnalyst	2	34822382	3442
10271	MetaboAnalyst	1	34822384	2609
10272	MetaboAnalyst	2	34822389	16709
10273	HMDB	8	34822401	642
10274	LIPID MAPS	6	34822401	655
10275	MetaboAnalystR	6	34822401	2364
10276	MetaboAnalyst	5	34822401	2390
10277	Newton	1	34822403	3171
10278	GNPS	2	34822403	5831
10279	MetaboAnalyst	1	34822403	6147
10280	AMDIS	2	34822406	4397
10281	MetaboAnalyst	2	34822406	5369
10282	IntLIM	1	34822411	906
10283	LDA	1	34822411	1034
10284	MetaboAnalyst	1	34822411	1156
10285	VOCCluster	1	34822411	1171
10286	MetaboClust	1	34822411	1276
10287	LIPID MAPS	1	34822412	8912
10288	MetaboLights	1	34822415	187
10289	XCMS	1	34822421	3802
10290	GNPS	4	34822421	5246
10291	ChemSpider	1	34822421	7003
10292	HMDB	1	34822421	7632
10293	MetaboAnalyst	2	34822421	8315
10294	MS-FLO	1	34822424	2119
10295	mixOmics	1	34822424	3082
10296	icoshift	1	34822425	2963
10297	MAVEN	1	34822429	1495
10298	XCMS	1	34822429	1601
10299	MetaboAnalyst	1	34822440	5441
10300	AMDIS	2	34822445	6967
10301	LIPID MAPS	2	34822464	7107
10302	MetaboAnalyst	1	34822464	14302
10303	AMDIS	1	34822477	3604
10304	GNPS	4	34822504	9847
10305	MZmine 2	2	34822504	9859
10306	MetaboAnalyst	3	34822504	9983
10307	METLIN	1	34822532	5670
10308	MetaboAnalyst	1	34823168	9889
10309	MetaboAnalyst	1	34825649	26544
10310	MSClust	2	34827700	6591
10311	HMDB	1	34827700	8295
10312	MAGMa	1	34827700	8331
10313	mixOmics	1	34827776	4853
10314	XCMS	1	34827776	6762
10315	MetaboAnalyst	1	34827776	7346
10316	LDA	3	34827839	3929
10317	MetaboAnalyst	1	34827839	6609
10318	MetaboAnalyst	1	34828038	6534
10319	XCMS	2	34828320	4824
10320	CAMERA	1	34828320	5026
10321	MetaboAnalystR	1	34828395	4769
10322	MetAlign	1	34828797	4590
10323	HMDB	1	34828797	5688
10324	XCMS	1	34828897	8532
10325	SMART	1	34829001	9769
10326	METLIN	1	34829107	6735
10327	MetaboAnalyst	1	34829150	10268
10328	AMDIS	1	34829254	3887
10329	MetaboAnalyst	1	34829254	5566
10330	Newton	1	34829692	1357
10331	XCMS	1	34829692	7223
10332	UniProtKB	1	34829892	667
10333	CAMERA	1	34830114	9394
10334	Skyline	1	34830406	4825
10335	MetaboAnalyst	1	34830406	6244
10336	MetaBridge	1	34831173	3133
10337	MetaboAnalystR	1	34831297	2499
10338	MetaboAnalyst	1	34831297	2924
10339	apLCMS	2	34831363	3138
10340	xMSanalyzer	1	34831363	3584
10341	xMSannotator	3	34831363	3922
10342	mixOmics	1	34831363	6019
10343	XCMS	1	34831363	8351
10344	MetFrag	2	34831363	8587
10345	Mummichog	1	34831363	9249
10346	MetaboAnalyst	1	34831456	15119
10347	AMDIS	1	34833116	7411
10348	MetaboAnalyst	1	34833116	7550
10349	HMDB	1	34833859	5263
10350	ropls	1	34834005	5153
10351	GNPS	2	34834042	4977
10352	HMDB	1	34834042	5339
10353	SMART	1	34834602	5575
10354	ropls	1	34834750	6245
10355	MetaboAnalyst	1	34834789	8140
10356	XCMS	3	34834827	3546
10357	METLIN	2	34834827	3606
10358	MetaboAnalyst	1	34834827	4448
10359	PARADISe	1	34834870	9619
10360	AMDIS	1	34834870	9879
10361	ropls	1	34834870	11860
10362	ChemSpider	1	34835313	3668
10363	LDA	2	34835341	3953
10364	MetaboAnalyst	1	34835493	3676
10365	AMDIS	1	34836157	3562
10366	MetaboAnalyst	1	34836157	4057
10367	SMPDB	1	34836157	4185
10368	XCMS	1	34836175	4499
10369	MetaboAnalyst	1	34836275	8133
10370	MetaboAnalyst	1	34836982	4269
10371	SMPDB	1	34836982	5941
10372	METLIN	1	34839410	5853
10373	MetaboAnalyst	1	34840582	5261
10374	HMDB	1	34840582	5436
10375	XCMS	8	34841446	3678
10376	METLIN	2	34841446	4375
10377	CFM-ID	2	34841446	5819
10378	MoNA	1	34841446	6651
10379	GNPS	1	34841446	6676
10380	SMART	1	34842744	651
10381	SMPDB	1	34845204	9942
10382	CAMERA	1	34845204	9960
10383	METLIN	1	34845335	7400
10384	MetaboAnalyst	1	34847184	6072
10385	XCMS	1	34848684	7496
10386	CAMERA	1	34848684	7502
10387	HMDB	1	34848684	7581
10388	FALCON	2	34849782	2311
10389	LDA	2	34851178	12844
10390	MetaboAnalyst	1	34851990	5510
10391	XCMS	2	34852827	7439
10392	XCMS	1	34852831	11949
10393	MetaboAnalyst	1	34852831	12326
10394	LDA	2	34853299	14542
10395	BATMAN	1	34855861	5654
10396	icoshift	1	34855861	7268
10397	LDA	2	34855861	8010
10398	HMDB	1	34855864	7812
10399	MetaboLights	1	34855864	8377
10400	ropls	2	34855864	10377
10401	HMDB	1	34857836	3275
10402	MetaboLights	1	34857933	8560
10403	HMDB	1	34858511	3285
10404	XCMS	1	34858883	5774
10405	CAMERA	1	34858883	6293
10406	MetaboAnalyst	1	34858883	7592
10407	HMDB	1	34858883	8041
10408	LDA	3	34858992	1470
10409	HMDB	1	34859050	2743
10410	MetaboAnalyst	2	34859050	3998
10411	AMDIS	2	34861817	11325
10412	XCMS	1	34861817	14538
10413	HMDB	1	34861817	15249
10414	Workflow4Metabolomics	1	34862403	23347
10415	CAMERA	1	34862403	23724
10416	Bayesil	1	34862433	7015
10417	SMPDB	1	34862433	16724
10418	NMRProcFlow	1	34862469	3180
10419	MetaboAnalyst	2	34862469	3695
10420	MetaboAnalyst	1	34863291	3233
10421	MetaboAnalyst	1	34864852	4121
10422	HMDB	1	34867365	3340
10423	METLIN	1	34867365	3368
10424	ChemSpider	1	34867365	3408
10425	MetaboAnalyst	1	34867365	3473
10426	DrugBank	1	34867387	587
10427	mixOmics	1	34867463	8897
10428	XCMS	2	34867796	1567
10429	CAMERA	1	34867796	1744
10430	Escher	3	34867870	1619
10431	BioCyc	3	34867870	3404
10432	HMDB	1	34868045	3092
10433	CAMERA	1	34868045	3321
10434	METLIN	1	34868120	4829
10435	HMDB	1	34868120	4837
10436	XCMS	1	34868156	3673
10437	XCMS	1	34868158	3017
10438	XCMS	1	34868176	6205
10439	HMDB	1	34868309	6016
10440	MetaboAnalyst	1	34869321	9524
10441	METLIN	1	34869538	17836
10442	HMDB	1	34873082	834
10443	SMPDB	1	34873082	881
10444	HMDB	1	34873196	7064
10445	mixOmics	1	34875085	10184
10446	XCMS	1	34876179	3599
10447	statTarget	2	34876179	3845
10448	HMDB	1	34876827	2969
10449	MetaboAnalyst	1	34876827	3292
10450	XCMS	2	34878607	15700
10451	MetaboliteDetector	1	34879068	14612
10452	HMDB	1	34880597	968
10453	METLIN	1	34880597	1039
10454	MetaboAnalyst	2	34884168	5051
10455	Mummichog	2	34884168	5075
10456	MetaboAnalyst	1	34884654	11391
10457	HMDB	1	34884677	5376
10458	METLIN	1	34884677	5422
10459	LIPID MAPS	1	34884677	5487
10460	MetaboAnalyst	1	34884677	6447
10461	HMDB	1	34884735	6716
10462	mixOmics	2	34884735	11235
10463	ChemSpider	1	34884793	3878
10464	Skyline	4	34884803	7247
10465	WikiPathways	1	34884877	7854
10466	MetaboAnalyst	2	34884945	3780
10467	GNPS	2	34885725	4859
10468	HMDB	1	34885759	2555
10469	MetaboAnalystR	1	34885857	6198
10470	GNPS	1	34885898	6983
10471	GNPS	1	34885908	7424
10472	GNPS	4	34885912	6763
10473	METLIN	1	34885934	3508
10474	HMDB	1	34886808	5183
10475	METLIN	1	34886808	5203
10476	XCMS	1	34887835	1486
10477	MetaboAnalystR	1	34887835	1859
10478	GNPS	1	34887879	5293
10479	MetaboAnalyst	1	34887879	5929
10480	MetaboAnalyst	1	34893088	7164
10481	UniProtKB	1	34895137	4499
10482	XCMS	1	34899293	5235
10483	MetaboAnalyst	2	34899293	5533
10484	METLIN	1	34899293	6201
10485	HMDB	1	34899293	6238
10486	ropls	2	34899312	3194
10487	LDA	2	34899312	4347
10488	HMDB	1	34899328	4858
10489	MetaboAnalyst	1	34899328	6041
10490	METLIN	1	34899775	4222
10491	HMDB	1	34899784	6903
10492	mixOmics	2	34901107	9039
10493	LDA	2	34901130	3844
10494	MetaboAnalyst	1	34901130	5218
10495	MAVEN	1	34903058	4549
10496	IsoCor	1	34903058	5352
10497	MetaboLights	1	34903058	16266
10498	MetaboAnalyst	1	34903180	6818
10499	ChemSpider	1	34906076	6589
10500	SMART	1	34906076	7221
10501	GNPS	7	34910917	405
10502	PAPi	1	34910917	20374
10503	NMRProcFlow	1	34914634	8690
10504	Newton	1	34914701	1040
10505	MetaboAnalyst	1	34916939	3971
10506	XCMS	1	34917427	5228
10507	MZmine 2	1	34920733	5250
10508	MetaboAnalyst	1	34920733	9141
10509	Mummichog	1	34920733	9240
10510	rNMR	1	34921302	3486
10511	HMDB	1	34921302	3640
10512	MetaboAnalyst	3	34921655	8573
10513	SMPDB	1	34921655	9727
10514	MS-FLO	1	34923590	12259
10515	MetaboAnalyst	1	34923590	12692
10516	mixOmics	1	34923903	9429
10517	LDA	3	34925250	3115
10518	MetaboAnalyst	3	34925250	6480
10519	XCMS	1	34925252	3923
10520	UniProtKB	1	34925259	6418
10521	AMDIS	1	34925415	7457
10522	XCMS	2	34925415	7721
10523	MetaboAnalyst	1	34925415	9739
10524	MetaboAnalyst	1	34926538	17999
10525	LDA	1	34927019	12196
10526	MetaboLights	1	34928464	68
10527	MetaboAnalyst	1	34928464	11572
10528	AMDIS	1	34930524	29593
10529	apLCMS	1	34931082	6645
10530	xMSanalyzer	1	34931082	6717
10531	MetaboAnalyst	1	34933458	11299
10532	MetaboAnalyst	1	34934071	16791
10533	MetaboAnalystR	2	34934079	3693
10534	MetaboAnalyst	1	34936449	16492
10535	XCMS	1	34936950	6626
10536	XCMS	3	34936958	8186
10537	METLIN	1	34936958	9199
10538	HMDB	2	34938316	4746
10539	MetaboAnalyst	3	34938316	5168
10540	MetaboAnalyst	1	34938667	7045
10541	MetaboLab	1	34938772	1535
10542	Mummichog	1	34940360	17797
10543	IsoCor	1	34940360	18200
10544	METLIN	2	34940566	4859
10545	MetaboAnalyst	1	34940566	5257
10546	METLIN	1	34940568	3938
10547	HMDB	1	34940568	4242
10548	LIPID MAPS	1	34940568	4344
10549	METLIN	1	34940574	1806
10550	GNPS	4	34940578	8638
10551	ChemSpider	1	34940578	11461
10552	MetaboAnalyst	1	34940578	12190
10553	MetaboAnalyst	3	34940582	4223
10554	MetaboAnalyst	2	34940586	7375
10555	MetaboLights	1	34940587	5964
10556	MetaboAnalyst	1	34940587	7900
10557	GAVIN	1	34940588	4341
10558	AMDIS	3	34940588	4423
10559	XCMS	1	34940588	5241
10560	MetaboAnalyst	1	34940588	6763
10561	HMDB	1	34940588	6951
10562	MetFrag	1	34940590	10554
10563	HMDB	1	34940592	7369
10564	MetFrag	1	34940593	13620
10565	HMDB	1	34940593	13781
10566	HMDB	1	34940608	4542
10567	MetaboAnalyst	1	34940609	5646
10568	Workflow4Metabolomics	1	34940620	6064
10569	MetaboAnalyst	3	34940626	9712
10570	LDA	2	34940630	4048
10571	HMDB	1	34940632	1469
10572	MetaboAnalyst	2	34940632	1519
10573	MetaboAnalyst	3	34940641	4017
10574	GNPS	3	34940687	9632
10575	MZmine 2	1	34941699	9126
10576	GNPS	1	34941699	12932
10577	MZmine 2	1	34942963	3306
10578	XCMS	1	34943959	7919
10579	CAMERA	1	34943959	8242
10580	ProbMetab	4	34943959	8424
10581	PAPi	3	34943959	9096
10582	MetaboAnalyst	3	34943959	9417
10583	METLIN	1	34944104	1599
10584	MetaboAnalyst	1	34944401	8530
10585	GNPS	3	34944436	1323
10586	MetFrag	3	34944436	2555
10587	MoNA	1	34944436	3841
10588	MetaboAnalyst	1	34944760	3032
10589	XCMS	1	34944824	5572
10590	LIPID MAPS	1	34944824	6004
10591	XCMS	2	34945571	10707
10592	MetaboAnalyst	1	34945571	10847
10593	LDA	2	34945611	4367
10594	MS-FINDER	1	34945618	6545
10595	CANOPUS	2	34945618	9397
10596	MetaboAnalyst	1	34945618	10874
10597	Skyline	1	34945728	2779
10598	VMH	4	34946095	3681
10599	HMDB	1	34946610	3692
10600	XCMS	1	34947037	10271
10601	MetaboAnalyst	1	34947073	8607
10602	ChemSpider	1	34947984	4381
10603	BioCyc	1	34947984	4393
10604	MetaboAnalyst	2	34947984	6018
10605	MetaboAnalyst	1	34948018	9208
10606	LipidXplorer	2	34948069	4045
10607	MetaboAnalyst	1	34948069	6974
10608	MetaboAnalyst	1	34948086	6828
10609	MetaboAnalyst	1	34948203	6025
10610	METLIN	1	34948233	7006
10611	MetaboAnalyst	1	34948233	7336
10612	Newton	1	34948275	6838
10613	LDA	2	34948275	11712
10614	XCMS	2	34948275	12189
10615	XCMS online	1	34948275	12360
10616	MetaboAnalyst	1	34948275	12397
10617	MetaboAnalyst	3	34948402	5090
10618	XCMS	1	34949931	12190
10619	HMDB	1	34949931	12912
10620	MetaboAnalyst	1	34949931	13028
10621	AMDIS	1	34950168	10032
10622	mixOmics	2	34950168	12034
10623	SMART	1	34950170	1237
10624	HMDB	1	34950217	3331
10625	METLIN	1	34950217	3367
10626	MetaboAnalyst	1	34950217	3492
10627	HMDB	1	34950627	5780
10628	MetaboAnalyst	1	34955838	4669
10629	MetaboAnalyst	2	34956093	5451
10630	MetaboAnalyst	2	34956382	3845
10631	METLIN	1	34956382	4170
10632	HMDB	1	34956382	4246
10633	LDA	2	34957184	7722
10634	XCMS	1	34957184	9264
10635	UniProtKB	1	34957918	5376
10636	FALCON	1	34957918	7770
10637	mixOmics	2	34959869	5527
10638	LDA	1	34959902	13199
10639	MetaboAnalyst	1	34960119	5634
10640	METLIN	1	34961163	2688
10641	GNPS	3	34961166	1970
10642	MZmine 2	1	34961166	2315
10643	ChemSpider	2	34961768	6244
10644	LDA	1	34966928	4389
10645	MZmine 2	1	34969972	9479
10646	HMDB	1	34970155	6726
10647	MetaboAnalyst	1	34970155	7344
10648	MetaboAnalyst	1	34972183	5936
10649	ChemSpider	1	34972183	6284
10650	MetExtract	2	34972198	8998
10651	HMDB	1	34975476	8097
10652	MetaboAnalyst	1	34975477	5393
10653	MetaboAnalyst	1	34975567	4128
10654	VANTED	1	34975570	2492
10655	MetAlign	1	34975775	2978
10656	LDA	1	34975793	4784
10657	XCMS	1	34975793	7875
10658	CAMERA	1	34975793	7881
10659	HMDB	1	34975793	7960
10660	XCMS	1	34975894	4499
10661	HMDB	1	34975894	5221
10662	MoNA	1	34975894	5400
10663	MetaboAnalyst	1	34975894	6593
10664	MetaboAnalyst	1	34975919	11390
10665	ropls	1	34975982	7489
10666	mixOmics	1	34975998	4494
10667	ChemSpider	1	34976172	4742
10668	HMDB	1	34976172	4845
10669	MetaboAnalyst	1	34976172	5373
10670	LDA	1	34976857	2515
10671	ChemSpider	2	34977152	12994
10672	MetaboLights	1	34977152	14747
10673	mixOmics	1	34978446	6793
10674	LDA	2	34980222	12452
10675	MetaboAnalyst	3	34984327	23965
10676	METLIN	1	34986329	22355
10677	MetaboAnalyst	2	34986857	15846
10678	NOREVA	1	34989902	2137
10679	MetaboAnalyst	3	34989902	7147
10680	METLIN	1	34992409	6092
10681	HMDB	1	34992409	6204
10682	MetaboAnalyst	1	34992409	6265
10683	MAIT	7	34992584	5446
10684	HMDB	1	34993178	2431
10685	MetFrag	2	34993178	3078
10686	HMDB	1	34993196	3638
10687	mixOmics	1	34993216	4914
10688	ORCA	1	34998404	20964
10689	METLIN	1	34999928	9708
10690	HMDB	1	34999928	9777
10691	CFM-ID	1	34999928	9864
10692	FingerID	1	34999928	9908
10693	LIPID MAPS	1	34999928	9966
10694	VANTED	1	35002254	4844
10695	METLIN	1	35002731	5332
10696	HMDB	1	35002731	5382
10697	mixOmics	1	35002971	1912
10698	MetaboAnalyst	2	35003294	4955
10699	ChemSpider	1	35003296	6233
10700	MetaboAnalyst	2	35003367	2403
10701	XCMS	1	35004290	3252
10702	LIPID MAPS	1	35004290	4044
10703	HMDB	1	35004290	4063
10704	MetaboAnalyst	1	35004344	7972
10705	XCMS	1	35004355	4216
10706	HMDB	1	35004825	2739
10707	METLIN	1	35004825	2767
10708	mzMatch	1	35004911	4714
10709	MetaboAnalyst	3	35004911	4957
10710	TargetSearch	1	35005550	19001
10711	mixOmics	1	35005552	9510
10712	MZmine 2	1	35008505	6564
10713	GNPS	2	35008505	6682
10714	HMDB	1	35008620	7893
10715	METLIN	1	35008620	7988
10716	UniProtKB	1	35008636	995
10717	Binner	1	35008662	10209
10718	COVAIN	1	35008692	3576
10719	XCMS	2	35008957	7366
10720	CAMERA	1	35008957	7589
10721	METLIN	1	35008957	7873
10722	HMDB	1	35008957	7948
10723	MetaboAnalyst	1	35009065	7622
10724	METLIN	1	35009135	3993
10725	MetaboAnalyst	1	35009135	4332
10726	METLIN	1	35010189	10192
10727	BioCyc	1	35010801	11070
10728	FingerID	1	35010903	2593
10729	CANOPUS	1	35010903	2672
10730	XCMS	1	35010992	6618
10731	HMDB	1	35010992	6715
10732	METLIN	1	35011021	4567
10733	HMDB	1	35011021	4578
10734	BinBase	1	35011090	4905
10735	MetaboAnalyst	1	35011090	7949
10736	HMDB	1	35011185	6011
10737	CMM	1	35011319	6249
10738	SMART	2	35011346	4859
10739	MetaboAnalyst	1	35011454	7586
10740	PathVisio	1	35011454	7698
10741	WikiPathways	1	35011454	7950
10742	HMDB	1	35011537	4106
10743	METLIN	1	35011537	4124
10744	MetaboAnalyst	2	35011739	8827
10745	UniProtKB	5	35012528	1830
10746	LDA	1	35013099	4692
10747	HMDB	1	35013099	6008
10748	ropls	1	35013196	16472
10749	HMDB	1	35013245	12745
10750	LDA	2	35013245	17200
10751	BioCyc	1	35013245	25256
10752	MetaboAnalyst	1	35013261	28217
10753	ropls	1	35013420	7881
10754	MetScape	1	35013420	7937
10755	HMDB	1	35013420	8066
10756	MetaboAnalyst	1	35014610	13017
10757	METLIN	1	35024036	12257
10758	HMDB	1	35024036	12265
10759	MetaboLights	1	35024088	4139
10760	XCMS	1	35024202	3705
10761	HMDB	1	35024202	3867
10762	MetaboAnalyst	1	35024202	5101
10763	XCMS	1	35025731	11239
10764	MetaboAnalyst	1	35026155	15120
10765	MetaboAnalyst	2	35028011	3578
10766	HMDB	1	35028011	3722
10767	LDA	1	35033121	9317
10768	ropls	1	35036082	3813
10769	MetaboAnalyst	1	35036401	3594
10770	HMDB	1	35036412	3158
10771	LIPID MAPS	1	35039505	9081
10772	GNPS	1	35040428	4504
10773	Skyline	1	35040428	4779
10774	LDA	4	35041143	12782
10775	ChemSpider	1	35041143	15017
10776	LIPID MAPS	1	35041143	15085
10777	METLIN	2	35041143	15417
10778	LIPID MAPS	1	35042855	11661
10779	BinBase	1	35043127	4900
10780	MS-FLO	1	35043127	5830
10781	MetaboAnalystR	1	35043180	9596
10782	MAVEN	2	35046093	5125
10783	LDA	2	35046693	1966
10784	MetaboAnalystR	1	35046693	3455
10785	mixOmics	1	35046693	3829
10786	XCMS	1	35046817	3375
10787	MetaboAnalyst	1	35046836	7910
10788	MetaboAnalyst	1	35046903	11702
10789	MetaboAnalyst	1	35046983	6913
10790	MetaboAnalyst	1	35047107	2602
10791	HMDB	1	35047580	2988
10792	MetaboAnalyst	1	35047580	3042
10793	IPO	1	35048024	3904
10794	XCMS	2	35048024	4090
10795	MetaboAnalyst	3	35048024	5039
10796	Mummichog	1	35048024	6553
10797	XCMS	1	35049827	7086
10798	CAMERA	1	35049827	7097
10799	HMDB	1	35049827	7217
10800	XCMS	1	35049836	4399
10801	HMDB	1	35049836	5170
10802	GNPS	9	35049863	8186
10803	GNPS	6	35049876	11349
10804	Qemistree	1	35049876	15232
10805	GNPS	5	35049906	4225
10806	MetaboAnalyst	1	35049971	7906
10807	MetaboAnalyst	1	35049995	8787
10808	UniProtKB	1	35050006	16721
10809	MZmine 2	1	35050096	7480
10810	XCMS	1	35050097	6405
10811	missForest	2	35050127	744
10812	VMH	1	35050132	652
10813	MetaboAnalyst	1	35050132	2740
10814	MetaboAnalyst	1	35050139	4017
10815	HMDB	1	35050140	5726
10816	MetaboAnalyst	1	35050140	6606
10817	GNPS	1	35050142	3732
10818	XCMS	1	35050142	3876
10819	HMDB	1	35050142	4272
10820	MetaboAnalyst	1	35050146	3939
10821	MetaboAnalyst	2	35050149	5265
10822	MetaboAnalyst	2	35050150	7977
10823	MetaboAnalyst	1	35050151	3247
10824	MetaboAnalyst	1	35050158	7366
10825	LIPID MAPS	1	35050170	2704
10826	HMDB	3	35050171	475
10827	METLIN	2	35050171	4492
10828	MetaboAnalyst	1	35050180	3167
10829	HMDB	1	35050199	5041
10830	MZmine 2	1	35050199	11520
10831	HMDB	1	35050202	3791
10832	MetaboAnalyst	1	35050208	10655
10833	ChemSpider	1	35052569	7445
10834	GNPS	1	35052637	5115
10835	XCMS	1	35053189	1610
10836	HMDB	1	35053189	2312
10837	HMDB	1	35053260	4578
10838	MetaboAnalyst	3	35053260	8870
10839	Skyline	3	35053890	1027
10840	XCMS	1	35053919	6887
10841	MetaboAnalyst	1	35053919	7670
10842	MetaboAnalyst	1	35053924	7392
10843	UC2	1	35054508	4495
10844	MetaboAnalyst	1	35054508	4968
10845	Skyline	1	35054892	3909
10846	mixOmics	1	35054964	6689
10847	MetaboAnalyst	1	35054964	11934
10848	ChemSpider	1	35055063	9247
10849	MetaboAnalyst	1	35055063	10850
10850	HMDB	1	35057510	10889
10851	MetaboAnalyst	1	35057570	6305
10852	MetaboAnalyst	2	35057709	5805
10853	MetaboAnalystR	1	35057709	6453
10854	MetaboAnalyst	1	35058786	9569
10855	MetaboAnalyst	1	35058969	2807
10856	MetaboAnalyst	1	35060995	7067
10857	MZmine 2	2	35064110	20788
10858	MetImp	1	35064110	21690
10859	MetaboAnalyst	2	35064110	21790
10860	HMDB	1	35068048	7635
10861	ChemSpider	1	35068048	7670
10862	MetaboAnalyst	1	35068048	8023
10863	SMART	1	35068346	904
10864	MetaboAnalyst	1	35069201	4500
10865	HMDB	1	35069466	10269
10866	METLIN	1	35069466	10276
10867	LDA	2	35069475	5358
10868	XCMS	1	35069475	8079
10869	XCMS	2	35069638	3869
10870	XCMS online	1	35069638	3869
10871	MetaboAnalystR	1	35069647	3466
10872	MetFrag	1	35069647	7365
10873	SMART	1	35069652	1205
10874	ChemSpider	1	35069661	14911
10875	MetaboAnalyst	1	35070995	4217
10876	XCMS	1	35071028	1718
10877	MetaboAnalyst	1	35071028	3537
10878	HMDB	1	35071028	3749
10879	SMPDB	1	35071028	3879
10880	HMDB	1	35071294	5952
10881	ropls	1	35071294	6610
10882	mixOmics	1	35071294	6634
10883	GAM	1	35076269	2788
10884	LDA	2	35082330	3921
10885	mixOmics	1	35082386	6430
10886	XCMS	1	35082786	5808
10887	MoNA	1	35083512	8319
10888	MetaboAnalyst	2	35083512	12174
10889	HMDB	1	35083512	13538
10890	mixOmics	1	35083969	6022
10891	LDA	1	35085328	5460
10892	Metab	1	35085362	12359
10893	MetaboAnalyst	1	35086470	2341
10894	SMPDB	1	35086470	3181
10895	LDA	5	35087073	3556
10896	XCMS	1	35087163	1006
10897	Skyline	2	35087163	1871
10898	GNPS	5	35087228	15369
10899	Qemistree	2	35087228	16616
10900	LDA	1	35087506	3524
10901	HMDB	1	35087623	9283
10902	MetaboAnalyst	1	35087623	9534
10903	HMDB	1	35087889	4956
10904	mixOmics	1	35090836	6493
10905	MetaboAnalyst	2	35090836	6812
10906	XCMS	1	35090836	10130
10907	XCMS online	1	35090836	10130
10908	METLIN	1	35090836	10312
10909	XCMS	1	35091582	5043
10910	MetaboAnalyst	2	35094641	587
10911	MZmine 2	1	35094641	3025
10912	HMDB	1	35094669	2222
10913	MetaboAnalyst	2	35094669	3258
10914	XCMS	2	35094961	14334
10915	MetaboAnalyst	2	35095533	5194
10916	LDA	2	35095912	10109
10917	MetaboAnalyst	3	35095912	13709
10918	LDA	1	35096637	2671
10919	LDA	2	35096929	4115
10920	LDA	1	35096946	496
10921	MetaboAnalystR	1	35097000	5962
10922	GNPS	4	35101918	467
10923	MetaboAnalyst	3	35102134	25984
10924	MoNA	1	35102215	6418
10925	MS-FLO	1	35102215	7207
10926	MetaboAnalyst	2	35102215	7848
10927	HMDB	1	35105883	10336
10928	LIPID MAPS	1	35105939	1068
10929	XCMS	1	35106469	15137
10930	Workflow4Metabolomics	1	35106469	16588
10931	HMDB	2	35106469	24396
10932	ropls	1	35111140	1535
10933	MetaboAnalyst	2	35111749	3229
10934	HMDB	1	35111800	4705
10935	METLIN	1	35111800	4736
10936	MetaboAnalyst	1	35113136	2223
10937	LDA	1	35115021	2945
10938	XCMS	1	35115021	4309
10939	icoshift	1	35115588	6662
10940	GAM	8	35115640	8722
10941	MetaboAnalyst	1	35115762	1625
10942	METLIN	1	35115923	8527
10943	MetaboAnalyst	1	35116008	5557
10944	HMDB	1	35116016	4790
10945	LIPID MAPS	1	35116016	4799
10946	MetaboAnalyst	1	35116504	2873
10947	HMDB	1	35116989	2690
10948	METLIN	1	35116989	2759
10949	LIPID MAPS	1	35117632	854
10950	MetaboAnalyst	2	35117632	1173
10951	XCMS	1	35118093	3611
10952	HMDB	1	35118093	4024
10953	SMPDB	1	35118093	4155
10954	MetaboAnalyst	1	35118093	5395
10955	LDA	1	35118112	10881
10956	MetaboAnalyst	2	35118112	12447
10957	LDA	1	35119359	23742
10958	LIPID MAPS	1	35119615	4854
10959	HMDB	1	35121660	8680
10960	LIPID MAPS	1	35121660	8689
10961	MetaboAnalyst	2	35121660	8839
10962	MetaboAnalyst	2	35125106	3942
10963	SMPDB	1	35125106	4085
10964	ChemSpider	1	35125876	2884
10965	HMDB	1	35125876	2952
10966	LIPID MAPS	1	35125876	3011
10967	MetaboAnalyst	1	35125876	3671
10968	XCMS	1	35126117	5901
10969	METLIN	1	35126117	6566
10970	MoNA	1	35126117	6618
10971	MetaboAnalyst	3	35126117	6801
10972	METLIN	1	35126436	7210
10973	ropls	1	35126479	4099
10974	MetaboAnalyst	1	35126479	4909
10975	MetaboAnalyst	1	35126813	5253
10976	XCMS	1	35127469	1796
10977	MetaboAnalyst	1	35127469	2921
10978	MetaboLights	1	35128501	681
10979	XCMS	3	35128501	9351
10980	nPYc-Toolbox	1	35128501	9618
10981	METLIN	1	35128501	11856
10982	LDA	2	35130930	7926
10983	MetaboAnalyst	1	35131692	20357
10984	SMART	1	35132306	1474
10985	MetaboAnalyst	1	35132306	4038
10986	MetaboAnalyst	2	35133451	11643
10987	MetaboAnalyst	1	35133975	8600
10988	ropls	1	35135573	9827
10989	MetaboAnalyst	1	35135573	10897
10990	MetaboLights	2	35138146	6055
10991	MetaboAnalyst	1	35138146	7298
10992	MetaboAnalyst	1	35139174	4644
10993	HMDB	1	35141305	5006
10994	AMDIS	2	35142905	8419
10995	XCMS	3	35145075	7558
10996	MetaboAnalyst	1	35145150	18546
10997	HMDB	2	35145183	5024
10998	apLCMS	1	35145393	4584
10999	xMSanalyzer	1	35145393	4613
11000	Mummichog	4	35145393	9977
11001	MetaboAnalyst	1	35145419	10447
11002	MetaboAnalyst	1	35145434	7653
11003	LDA	1	35145919	1264
11004	XCMS	1	35145922	5813
11005	LDA	2	35153770	4460
11006	HMDB	2	35153793	4501
11007	MetaboAnalyst	1	35153799	4795
11008	XCMS	1	35154012	3816
11009	MetaboAnalyst	1	35154012	5190
11010	METLIN	1	35154385	3540
11011	HMDB	1	35154385	3573
11012	MetaboAnalyst	1	35154385	4379
11013	HMDB	1	35155234	3972
11014	ropls	1	35155234	5094
11015	ChemSpider	1	35155525	4745
11016	HMDB	1	35155525	5193
11017	MetaboAnalyst	1	35155525	5905
11018	METLIN	1	35155540	4845
11019	MetaboAnalyst	1	35155540	5293
11020	LDA	2	35155569	2137
11021	XCMS	1	35155569	2358
11022	MetaboAnalyst	1	35156921	10405
11023	MetaboAnalyst	1	35158113	10832
11024	MetaboAnalyst	1	35158655	3681
11025	NMRProcFlow	2	35159141	1229
11026	MetaboAnalyst	1	35159141	2449
11027	MetaboAnalyst	1	35159203	4520
11028	BinBase	1	35159313	4175
11029	MetaboAnalyst	3	35159313	4537
11030	mixOmics	1	35159336	4698
11031	MetaboAnalystR	1	35159491	5634
11032	LDA	1	35159523	3714
11033	METLIN	1	35159623	3929
11034	HMDB	1	35159623	4039
11035	HMDB	1	35159627	4551
11036	HMDB	1	35159633	6405
11037	MetaboAnalyst	1	35159633	6412
11038	MetaboAnalyst	1	35160173	3742
11039	ropls	2	35161246	2524
11040	XCMS	2	35161271	4391
11041	XCMS online	1	35161271	4391
11042	ChemSpider	1	35161329	8510
11043	GNPS	1	35161417	9477
11044	NOREVA	1	35162994	10429
11045	MetaboAnalyst	2	35162994	10530
11046	MetaboAnalyst	1	35163046	17147
11047	HMDB	1	35163182	6632
11048	METLIN	1	35163182	6745
11049	MetaboAnalyst	2	35163182	6814
11050	Newton	1	35163517	1239
11051	PiMP	4	35163517	5488
11052	HMDB	1	35163517	6335
11053	MetaboAnalyst	2	35163517	12491
11054	SMPDB	1	35163517	14260
11055	HMDB	1	35163617	7412
11056	METLIN	1	35163617	7576
11057	COVAIN	1	35163626	3593
11058	MetaboAnalyst	1	35163642	6785
11059	Pathview	1	35163724	8845
11060	MAVEN	1	35163837	1393
11061	MetaboAnalyst	2	35163837	2039
11062	HMDB	2	35163837	2764
11063	IIS	1	35163842	311
11064	MetaboAnalystR	1	35164035	6437
11065	DrugBank	1	35164183	349
11066	GNPS	1	35164382	4789
11067	XCMS	1	35164712	4456
11068	MetaboAnalyst	1	35164712	4832
11069	ropls	1	35164712	5622
11070	LIMSA	1	35165416	18019
11071	IsoCor	1	35165416	36535
11072	Binner	1	35166340	4511
11073	HMDB	2	35166340	5010
11074	Lilikoi	2	35166340	7903
11075	LDA	1	35166340	8339
11076	XCMS	1	35168605	5891
11077	XCMS	1	35168606	5605
11078	MetaboAnalyst	1	35168606	6648
11079	MetaboAnalyst	1	35169141	21675
11080	MetaboAnalyst	1	35169190	8544
11081	HMDB	1	35169190	8683
11082	XCMS	1	35169201	7951
11083	Skyline	2	35169201	10294
11084	MetaboAnalyst	1	35169224	7251
11085	apLCMS	1	35169669	5811
11086	xMSanalyzer	1	35169669	5830
11087	Mummichog	4	35169669	8549
11088	mixOmics	1	35171682	7143
11089	MetaboAnalyst	2	35171682	7812
11090	HMDB	1	35171916	11146
11091	CAMERA	3	35172886	2933
11092	RAMClustR	3	35172886	2944
11093	IPO	1	35172886	3808
11094	GNPS	1	35172886	4493
11095	MetaboAnalyst	1	35174118	6124
11096	CAMERA	1	35174345	2111
11097	XCMS	1	35174345	2122
11098	HMDB	1	35174345	2284
11099	XCMS	1	35176705	4868
11100	MetaboAnalyst	1	35176705	5705
11101	HMDB	1	35177764	2883
11102	METLIN	1	35177764	2892
11103	ropls	2	35177860	20828
11104	HMDB	1	35177987	6454
11105	LIPID MAPS	1	35177987	6483
11106	MetaboAnalyst	1	35177987	9339
11107	XCMS	1	35178026	3942
11108	SMART	1	35179309	2898
11109	LIPID MAPS	1	35180835	6144
11110	Workflow4Metabolomics	1	35183130	11387
11111	MetaboAnalyst	1	35183130	11514
11112	METLIN	1	35183130	13084
11113	HMDB	1	35183130	13178
11114	LDA	2	35183234	5665
11115	BioCyc	1	35184756	5256
11116	METLIN	1	35185568	7207
11117	HMDB	1	35185568	7244
11118	MetaboAnalyst	1	35185568	7324
11119	LIPID MAPS	1	35185578	3437
11120	XCMS	1	35185616	3478
11121	MetaboAnalyst	2	35185616	4018
11122	ropls	1	35185866	4290
11123	MetaboAnalyst	1	35185866	5681
11124	LDA	2	35185872	12702
11125	Newton	1	35185934	3530
11126	LDA	1	35185934	4737
11127	MetaboAnalyst	1	35185934	5888
11128	MetaboAnalyst	1	35185956	8092
11129	ChemSpider	1	35185956	10047
11130	METLIN	1	35186461	2079
11131	MetaboAnalyst	2	35186493	3724
11132	speaq	3	35186675	4495
11133	HMDB	1	35186675	8452
11134	ChemoSpec	1	35186675	9313
11135	HMDB	1	35186998	2096
11136	MetaboAnalyst	1	35186998	2788
11137	MetaboAnalyst	2	35187366	4086
11138	MetaboAnalyst	2	35189961	2189
11139	mixOmics	1	35189961	2365
11140	MetaboAnalystR	1	35189961	2622
11141	MetaboAnalyst	1	35191396	5100
11142	HMDB	1	35192550	8715
11143	UniProtKB	2	35193703	11571
11144	LDA	3	35194021	4867
11145	UniProtKB	1	35196097	9422
11146	BioCyc	1	35196097	12643
11147	XCMS	2	35197864	6085
11148	HMDB	1	35197864	6705
11149	MAVEN	1	35197866	7627
11150	MetaboAnalyst	1	35197866	7844
11151	LDA	1	35197978	2655
11152	XCMS	1	35197978	3924
11153	XCMS	1	35198059	5975
11154	HMDB	1	35198059	5994
11155	MetaboAnalyst	1	35198450	3299
11156	XCMS	2	35198601	4698
11157	CAMERA	1	35198601	4970
11158	HMDB	1	35198601	5618
11159	MetaboAnalyst	1	35198601	7447
11160	MetaboAnalyst	2	35198866	8070
11161	UniProtKB	3	35200643	4105
11162	XCMS	1	35200643	5092
11163	CAMERA	1	35200643	5266
11164	HMDB	1	35200758	4852
11165	MetaboAnalyst	1	35200758	6855
11166	AMDIS	1	35201538	10377
11167	HMDB	1	35202168	3405
11168	METLIN	1	35202168	3464
11169	XCMS	1	35203121	3654
11170	HMDB	1	35203121	4553
11171	AMDIS	1	35203128	2841
11172	MetaboAnalyst	3	35204160	4894
11173	Skyline	1	35204804	7188
11174	MetaboAnalyst	2	35204826	6254
11175	SMART	1	35205328	2906
11176	XCMS	2	35205328	7031
11177	CAMERA	1	35205328	7214
11178	LipidXplorer	1	35205741	5788
11179	MetaboAnalyst	2	35205741	8250
11180	MetaboAnalyst	1	35206008	4188
11181	MetaboAnalystR	1	35206008	5456
11182	MetaboAnalyst	1	35206569	7989
11183	MetaboAnalystR	1	35206766	3755
11184	MetaboAnalyst	1	35206766	3890
11185	MetaboAnalyst	2	35207596	3082
11186	ChemSpider	1	35208172	4779
11187	HMDB	1	35208173	6854
11188	MetaboAnalyst	1	35208175	1260
11189	HMDB	1	35208175	2310
11190	HMDB	1	35208181	6540
11191	XCMS	2	35208184	2132
11192	METLIN	1	35208184	4359
11193	GAVIN	1	35208191	4578
11194	AMDIS	1	35208191	4726
11195	HMDB	1	35208194	1592
11196	GNPS	1	35208194	3848
11197	GNPS	2	35208196	1801
11198	ChemSpider	1	35208196	2711
11199	AMDIS	2	35208197	3106
11200	MetaboAnalyst	2	35208202	4168
11201	XCMS	5	35208212	593
11202	ropls	1	35208212	735
11203	MetaboAnalyst	1	35208216	7591
11204	MetaboAnalyst	1	35208222	7674
11205	ropls	1	35208223	4305
11206	METLIN	1	35208234	4562
11207	MetaboAnalyst	1	35208235	3164
11208	XCMS	2	35208239	9250
11209	MS-FLO	1	35208239	10032
11210	MetaboAnalyst	1	35208239	10300
11211	MetaboAnalyst	1	35208245	8693
11212	Mummichog	1	35208245	9206
11213	MetaboAnalyst	1	35208248	6747
11214	batchCorr	1	35208249	4950
11215	MetaboAnalyst	2	35208249	6880
11216	MZmine 2	1	35208268	3643
11217	xMSanalyzer	1	35208268	3807
11218	MetMSLine	1	35208268	4252
11219	mixOmics	3	35208268	4985
11220	MetaboAnalystR	1	35208730	5257
11221	GNPS	2	35208983	12837
11222	AMDIS	1	35209098	5208
11223	MetaboAnalyst	1	35209098	7240
11224	HMDB	1	35209182	5442
11225	METLIN	1	35209182	5448
11226	LIPID MAPS	1	35209182	5460
11227	ropls	1	35211018	3790
11228	METLIN	1	35211018	4318
11229	MetaboAnalyst	1	35211018	4654
11230	HMDB	1	35211022	2768
11231	MetaboAnalyst	1	35211023	4893
11232	MZmine 2	1	35211029	2546
11233	MetaboAnalyst	1	35211029	4730
11234	MetaboAnalyst	2	35211530	1859
11235	GNPS	1	35213299	7641
11236	MetScape	1	35213972	6815
11237	Skyline	1	35213972	18379
11238	HMDB	1	35213994	4053
11239	MetaboAnalyst	1	35214818	5878
11240	HMDB	1	35214832	5645
11241	METLIN	1	35214832	5678
11242	MetaboAnalyst	2	35214843	8171
11243	HMDB	1	35215385	3371
11244	ChemSpider	1	35215385	3439
11245	MetaboAnalyst	1	35215385	4428
11246	MetaboAnalyst	1	35215418	6106
11247	MetaboAnalyst	1	35215423	8775
11248	LDA	2	35215464	4321
11249	MetaboAnalyst	1	35215464	6884
11250	ChemSpider	1	35215464	7191
11251	Mummichog	1	35215464	7574
11252	AMDIS	1	35215494	5444
11253	Skyline	1	35216100	4444
11254	MetaboAnalyst	5	35216260	3332
11255	MetaboAnalyst	1	35220033	10478
11256	HMDB	1	35220945	5141
11257	MetaboAnalyst	1	35222043	4725
11258	HMDB	1	35222262	21283
11259	mixOmics	2	35222262	23715
11260	XCMS	1	35222333	8981
11261	MetaboAnalyst	1	35222431	2931
11262	XCMS	1	35222522	1366
11263	HMDB	1	35222522	3792
11264	MetaboAnalystR	2	35222522	4859
11265	SMART	1	35223208	1788
11266	MetaboAnalyst	4	35223868	2614
11267	HMDB	1	35223868	3748
11268	XCMS	1	35225020	6770
11269	MetaboAnalyst	1	35225020	7558
11270	GNPS	2	35225656	6744
11271	MetFrag	1	35225656	7065
11272	MetaboAnalyst	1	35228511	7352
11273	ropls	1	35228616	4820
11274	Skyline	1	35229219	6226
11275	MetaboAnalyst	1	35229219	6469
11276	GNPS	1	35235054	4604
11277	MetaboAnalyst	1	35235538	3245
11278	HMDB	1	35235538	3385
11279	specmine	1	35235538	8249
11280	XCMS	1	35236293	9850
11281	MetaboAnalyst	1	35236817	4357
11282	XCMS	1	35236817	4612
11283	MetaboAnalyst	1	35236903	14528
11284	XCMS	1	35237165	3135
11285	HMDB	1	35237165	4090
11286	METLIN	1	35237165	4118
11287	LDA	1	35237248	2593
11288	LDA	1	35237276	5131
11289	MoNA	1	35237591	6862
11290	MetaboAnalyst	1	35237591	7386
11291	LipidXplorer	1	35239643	4249
11292	CAMERA	1	35241402	9020
11293	msPurity	1	35241402	9162
11294	HMDB	1	35241402	9485
11295	GNPS	1	35241402	9510
11296	Workflow4Metabolomics	1	35241402	10628
11297	LDA	1	35241650	4961
11298	CAMERA	1	35241650	9028
11299	XCMS	2	35241650	9100
11300	MetaboAnalyst	1	35242495	9805
11301	HMDB	1	35243467	3389
11302	CMM	1	35246557	10410
11303	XCMS	1	35247098	11318
11304	CAMERA	1	35247098	11615
11305	LipidXplorer	1	35247455	12317
11306	mixOmics	1	35248077	10651
11307	ropls	1	35250923	5918
11308	METLIN	1	35251004	4927
11309	ropls	2	35251004	5080
11310	HMDB	1	35251087	4461
11311	METLIN	1	35251087	4481
11312	UniProtKB	1	35251091	4332
11313	LDA	1	35252022	5844
11314	Skyline	4	35252194	9055
11315	MetaboAnalyst	1	35252194	13727
11316	MetaboAnalystR	1	35252312	1122
11317	HMDB	1	35252418	6947
11318	XCMS	1	35252839	4589
11319	MetaboAnalyst	1	35252839	5654
11320	ChemSpider	1	35252948	4909
11321	HMDB	1	35252948	4921
11322	MetaboAnalyst	2	35253576	1872
11323	HMDB	2	35254863	7194
11324	iPath	1	35254863	12174
11325	MetScape	1	35254863	12216
11326	UniProtKB	1	35256050	12868
11327	LDA	1	35256637	4400
11328	ropls	1	35256871	6287
11329	IPO	2	35258726	9970
11330	MetaboAnalystR	2	35264111	2225
11331	LDA	1	35264111	5499
11332	XCMS	1	35264202	7816
11333	MetaboAnalyst	1	35264202	9578
11334	PARADISe	1	35264656	13224
11335	HMDB	2	35264984	1281
11336	HMDB	1	35265099	1238
11337	MetScape	3	35265105	7646
11338	LIPID MAPS	1	35265228	9066
11339	MetaboAnalyst	2	35266810	4365
11340	Skyline	1	35266810	11988
11341	El-MAVEN	1	35267619	2491
11342	MAVEN	1	35267619	2494
11343	MetaboAnalyst	5	35267619	2656
11344	HMDB	1	35267619	3690
11345	MetaboAnalyst	1	35268381	2594
11346	BioCyc	1	35268621	4176
11347	CFM-ID	1	35268621	4319
11348	MetaboAnalyst	1	35268621	4846
11349	Skyline	1	35268621	7438
11350	MetaboLights	1	35268621	7777
11351	MetaboAnalyst	1	35268665	5689
11352	HMDB	1	35269449	6309
11353	MS-FINDER	1	35269530	5807
11354	MetaboAnalyst	3	35269616	6264
11355	BioCyc	1	35269616	8658
11356	HMDB	1	35269702	7408
11357	mixOmics	2	35269702	11750
11358	XCMS	2	35269790	2537
11359	METLIN	1	35269790	3272
11360	XCMS	1	35269848	6391
11361	Workflow4Metabolomics	1	35269848	6407
11362	MAVEN	1	35269866	2251
11363	MetaboAnalyst	1	35269866	3963
11364	MetaboAnalyst	1	35269869	9348
11365	MetaboAnalyst	1	35269890	7732
11366	MetaboAnalyst	1	35270070	4462
11367	ChemSpider	1	35270073	1946
11368	HMDB	1	35270073	2343
11369	XCMS	1	35270147	5760
11370	CAMERA	1	35270147	5769
11371	HMDB	1	35270147	6733
11372	METLIN	1	35270147	6816
11373	MetaboAnalyst	3	35270170	3771
11374	MetScape	1	35270170	5226
11375	MetAlign	1	35270465	10003
11376	HMDB	1	35271582	5837
11377	METLIN	1	35271582	5907
11378	MZmine 2	1	35271607	6532
11379	HMDB	1	35271628	5320
11380	METLIN	1	35271628	5358
11381	MetaboAnalyst	2	35273249	4692
11382	HMDB	1	35273504	3219
11383	MetaboAnalystR	1	35273623	9445
11384	MAVEN	1	35276824	3738
11385	MetaboAnalyst	1	35276824	5722
11386	MAVEN	1	35276896	6457
11387	MetaboAnalyst	2	35276896	7594
11388	Skyline	1	35277503	11314
11389	MetAlign	1	35279073	3499
11390	SMART	1	35279697	1558
11391	LIPID MAPS	1	35279697	8190
11392	MetaboAnalyst	1	35281911	9588
11393	MetaboAnalyst	1	35281932	4161
11394	XCMS	1	35282369	6322
11395	HMDB	1	35283772	4241
11396	MetaboAnalystR	1	35283889	4144
11397	SMART	1	35283890	6901
11398	MetaboAnalyst	1	35283891	4689
11399	ICT	1	35283897	3823
11400	SMART	1	35283915	2924
11401	MetaboNetworks	1	35285859	11652
11402	Skyline	1	35287348	2621
11403	MetaboAnalyst	1	35287348	2917
11404	GNPS	3	35287577	6885
11405	XCMS	1	35287674	4668
11406	MetaboAnalyst	3	35287674	5846
11407	mixOmics	1	35287674	7289
11408	XCMS	1	35287685	4464
11409	HMDB	1	35288537	12883
11410	InCroMAP	2	35288557	42428
11411	UniProtKB	1	35288557	42945
11412	HMDB	1	35288652	14818
11413	XCMS	1	35291274	7306
11414	Skyline	1	35292629	8730
11415	XCMS	1	35293806	5646
11416	ropls	1	35293806	6219
11417	MetaboAnalyst	1	35293806	6629
11418	CMM	17	35293898	709
11419	MetAlign	1	35293898	10693
11420	HMDB	1	35294444	4523
11421	METLIN	1	35294444	4566
11422	ORCA	1	35294578	3040
11423	statTarget	1	35295336	6942
11424	MetaboAnalyst	1	35295336	8371
11425	MAVEN	2	35295860	4494
11426	MetaboAnalyst	1	35295860	6465
11427	MetaboAnalystR	1	35295916	4055
11428	MetaboAnalyst	1	35296015	3812
11429	XCMS	1	35296075	2914
11430	MetaboAnalyst	3	35296075	4091
11431	HMDB	1	35299741	4531
11432	XCMS	1	35299761	1943
11433	MetaboAnalyst	2	35299761	2717
11434	MetaboAnalyst	1	35300427	1760
11435	HMDB	1	35300427	1850
11436	MetaboAnalyst	1	35306337	11236
11437	MetaboAnalyst	1	35306726	13312
11438	METLIN	1	35308272	6883
11439	MetaboAnalyst	1	35308272	8306
11440	MetaboAnalyst	1	35308347	3809
11441	iPath	1	35308347	3874
11442	MS-FINDER	3	35308402	6832
11443	HMDB	1	35308402	7981
11444	MetaboAnalyst	1	35308402	8956
11445	XCMS	1	35308536	2687
11446	XCMS	1	35309369	147
11447	HMDB	1	35309369	224
11448	MetaboAnalyst	1	35309369	552
11449	XCMS	1	35309511	1000
11450	HMDB	1	35309511	1379
11451	METLIN	1	35309511	1386
11452	MetaboAnalyst	1	35309511	3201
11453	HMDB	1	35310032	4966
11454	MetaboAnalyst	1	35310032	5094
11455	MetaboAnalyst	1	35310395	5002
11456	MetaboAnalyst	1	35310560	4988
11457	MetaboAnalystR	1	35310646	4364
11458	Skyline	1	35313899	4802
11459	HMDB	1	35313919	6723
11460	MetaboLights	1	35314705	7115
11461	AMDIS	1	35314754	10200
11462	MetaboAnalyst	1	35314754	10309
11463	GAM	1	35316277	2257
11464	MetaboAnalystR	1	35317751	3832
11465	HMDB	1	35318439	8004
11466	MetaboAnalyst	1	35318439	8295
11467	XCMS	1	35321330	1348
11468	MetaboAnalyst	1	35321330	2359
11469	METLIN	2	35321499	5894
11470	HMDB	1	35321499	6204
11471	ChemSpider	1	35321499	6346
11472	GNPS	3	35323485	9887
11473	HMDB	1	35323500	6290
11474	MetaboAnalyst	1	35323500	6429
11475	GNPS	4	35323509	6533
11476	BinBase	1	35323643	2340
11477	MetaboAnalyst	1	35323643	3237
11478	HMDB	2	35323648	10767
11479	MetaboAnalyst	1	35323650	2864
11480	MetaboLights	1	35323655	2158
11481	MetaboAnalyst	1	35323662	7148
11482	MetaboAnalyst	1	35323664	3010
11483	HMDB	2	35323670	1522
11484	rDolphin	5	35323670	2385
11485	BATMAN	5	35323670	2398
11486	METLIN	1	35323678	7581
11487	CROP	1	35323684	7054
11488	XCMS	1	35323685	5287
11489	XCMS online	1	35323685	5287
11490	GNPS	5	35323688	2100
11491	ChemSpider	1	35323691	9901
11492	MetaboAnalyst	3	35323691	10284
11493	DrugBank	2	35323692	482
11494	MetaboAnalyst	1	35323698	5199
11495	CFM-ID	2	35323700	1709
11496	XCMS	1	35323710	2086
11497	XCMS online	1	35323710	2086
11498	Skyline	1	35323715	1802
11499	Newton	3	35324890	5062
11500	XCMS	3	35326111	3172
11501	HMDB	1	35326111	3265
11502	METLIN	1	35326111	4032
11503	MetaboAnalyst	1	35326111	4129
11504	HMDB	2	35326124	3765
11505	METLIN	1	35326124	3822
11506	LDA	1	35326133	9013
11507	MetaboAnalyst	2	35326233	4262
11508	MetaboAnalyst	2	35326656	5224
11509	HMDB	1	35326656	5541
11510	HMDB	1	35326714	3327
11511	MetaboAnalyst	1	35326714	3899
11512	MetaboAnalystR	1	35327272	5658
11513	MetaboAnalyst	1	35327340	6538
11514	MetAlign	1	35327358	4886
11515	HMDB	1	35327358	6055
11516	HMDB	2	35327409	2756
11517	MoNA	1	35327409	3109
11518	MetaboAnalyst	1	35327409	3439
11519	MetaboAnalyst	2	35327505	1539
11520	SMART	1	35327963	734
11521	SMART	1	35328393	1317
11522	mixOmics	1	35328410	6438
11523	MetaboAnalyst	1	35328410	6815
11524	XCMS	1	35328583	6206
11525	METLIN	1	35328583	6608
11526	IsoCor	1	35328583	6925
11527	MetaboAnalyst	3	35328608	14833
11528	Newton	1	35328720	646
11529	MetaboAnalyst	3	35328720	5258
11530	HMDB	1	35328788	3471
11531	LIPID MAPS	1	35328788	3537
11532	XCMS	1	35328810	7008
11533	MetaboAnalyst	1	35328810	7676
11534	MetaboAnalyst	1	35329007	6370
11535	ChemSpider	2	35330290	6944
11536	XCMS	1	35330317	2223
11537	MetaboAnalyst	1	35331175	4663
11538	MetaboAnalystR	1	35332430	3536
11539	MetaboAnalyst	1	35332430	3902
11540	HMDB	1	35334818	5931
11541	MetaboAnalyst	1	35334818	8428
11542	SMART	1	35334834	1719
11543	METLIN	1	35335153	8789
11544	HMDB	1	35335153	8895
11545	MetaboAnalyst	1	35335153	9041
11546	XCMS	1	35335269	5140
11547	MetaboAnalyst	1	35335269	5418
11548	LDA	1	35335269	7190
11549	IntLIM	2	35335269	7705
11550	ropls	1	35335269	8905
11551	HMDB	1	35335342	3321
11552	ropls	2	35335342	3511
11553	AMDIS	1	35336633	2901
11554	Skyline	1	35337146	2871
11555	HMDB	1	35337319	8227
11556	MetaboAnalyst	1	35337319	8665
11557	XCMS	1	35338186	4928
11558	MetaboAnalyst	2	35338186	7049
11559	MetaboAnalyst	1	35339899	7105
11560	HMDB	1	35342309	230
11561	MetaboAnalyst	1	35342309	451
11562	MetaboAnalyst	1	35343759	3652
11563	UniProtKB	1	35343796	3519
11564	MelonnPan	1	35343796	4597
11565	mixOmics	1	35346378	10222
11566	HMDB	4	35347128	10814
11567	HMDB	1	35347173	5645
11568	HMDB	1	35349673	3209
11569	ropls	1	35350989	5078
11570	LIPID MAPS	1	35351138	9256
11571	LIPID MAPS	1	35351864	4901
11572	MetaboAnalyst	1	35351920	6360
11573	AMDIS	2	35352020	4011
11574	Metab	2	35352020	4703
11575	MAVEN	1	35353006	17384
11576	UniProtKB	1	35356516	5989
11577	DrugBank	1	35356765	2383
11578	WikiPathways	1	35359411	3597
11579	MetaboAnalyst	1	35359829	4019
11580	MetaboAnalyst	1	35359846	2133
11581	MetaboAnalystR	1	35360097	4494
11582	MetaboAnalyst	1	35360173	6143
11583	XCMS	1	35360317	2941
11584	HMDB	1	35360317	3817
11585	XCMS	1	35360659	4071
11586	XCMS online	1	35360659	4071
11587	ropls	1	35360659	4459
11588	MetaboliteDetector	3	35360693	5069
11589	MetaboAnalystR	1	35361125	5532
11590	MetImp	1	35361774	5365
11591	GSimp	1	35361774	5637
11592	HMDB	1	35361825	11002
11593	MetaboliteDetector	1	35365607	20481
11594	HMDB	1	35365762	5184
11595	MetaboAnalyst	1	35365762	12974
11596	XCMS	1	35369429	4166
11597	CAMERA	1	35369429	4176
11598	MetaboAnalyst	1	35369429	4979
11599	LDA	1	35369462	1104
11600	XCMS	2	35369462	5026
11601	MAVEN	1	35369463	9685
11602	MetaboLights	1	35369463	10514
11603	XCMS	1	35369869	13234
11604	XCMS	1	35370704	5067
11605	BioCyc	1	35370823	6313
11606	MetaboAnalyst	1	35370823	7196
11607	MetaboAnalyst	1	35371012	7466
11608	HMDB	1	35371117	5671
11609	METLIN	1	35371117	5683
11610	XCMS	1	35371330	2206
11611	HMDB	2	35371330	3026
11612	METLIN	1	35371330	3035
11613	XCMS	1	35372095	13852
11614	HMDB	1	35372095	14757
11615	XCMS	1	35372122	4237
11616	LDA	1	35372122	5649
11617	MetaboAnalyst	1	35372500	4266
11618	iPath	1	35372500	5017
11619	missForest	1	35378331	13030
11620	GNPS	1	35378331	28865
11621	LDA	1	35379849	11277
11622	MIDAS	2	35382436	2699
11623	XCMS	1	35383199	10897
11624	MetaboAnalyst	1	35383199	11364
11625	MetaboAnalyst	1	35386989	3625
11626	MetaboAnalyst	1	35387258	3051
11627	HMDB	1	35387258	3693
11628	METLIN	1	35387258	3724
11629	LDA	1	35387330	5713
11630	HMDB	1	35387559	6478
11631	HMDB	4	35387615	25
11632	MetaboAnalystR	2	35388219	3202
11633	MetaboAnalyst	1	35388219	5762
11634	MAVEN	1	35388289	3877
11635	MetaboAnalyst	1	35388289	4136
11636	MIDAS	2	35390015	579
11637	MetaboAnalyst	1	35391557	10919
11638	HMDB	1	35391562	4324
11639	MetaboAnalyst	2	35391562	4470
11640	MetaboAnalyst	2	35392172	4942
11641	GAM	3	35392557	765
11642	MetaboAnalyst	1	35392557	2944
11643	HMDB	1	35392557	3355
11644	HMDB	1	35397165	3248
11645	COVAIN	1	35399158	14137
11646	LDA	2	35399511	5305
11647	LDA	1	35399683	5048
11648	HMDB	1	35399949	6666
11649	MetaboAnalystR	1	35399949	6856
11650	LDA	2	35399950	7091
11651	HMDB	1	35401159	4797
11652	METLIN	1	35401159	4824
11653	MetScape	1	35401159	5132
11654	XCMS	1	35401162	2520
11655	MetaboAnalyst	1	35401162	5281
11656	HMDB	2	35401189	11288
11657	MetaboAnalyst	1	35401189	11886
11658	XCMS	1	35401192	5089
11659	LIPID MAPS	1	35401429	5264
11660	HMDB	1	35401441	3958
11661	ropls	1	35401451	5662
11662	XCMS	1	35401452	10526
11663	MetaboAnalyst	1	35401452	11680
11664	LDA	2	35401452	11865
11665	GNPS	1	35401454	7186
11666	XCMS	1	35401485	3893
11667	CAMERA	1	35401485	3920
11668	HMDB	1	35401485	4116
11669	MoNA	1	35401596	4966
11670	MetaboAnalyst	1	35401596	6792
11671	ChemSpider	1	35401649	5506
11672	MetaboAnalyst	2	35401649	6091
11673	HMDB	1	35401701	4506
11674	MetaboAnalyst	1	35401701	4689
11675	MetaboAnalystR	1	35401816	5085
11676	HMDB	1	35402287	2884
11677	LIPID MAPS	1	35402287	2915
11678	MetaboAnalyst	1	35402287	3364
11679	HMDB	1	35402299	8202
11680	MetaboAnalyst	1	35402299	8244
11681	XCMS	1	35402462	5117
11682	MetaboAnalyst	1	35402462	6756
11683	LDA	2	35402538	6934
11684	LDA	2	35402559	852
11685	XCMS	1	35402863	9725
11686	mzAccess	1	35404953	11457
11687	MetaboAnalyst	1	35405879	4758
11688	MZmine 2	1	35406139	7819
11689	HMDB	1	35406537	4582
11690	MetaboLights	1	35406548	2461
11691	MetaboAnalyst	2	35406548	2673
11692	HMDB	1	35406630	3077
11693	MoNA	1	35406823	6665
11694	MS-FINDER	1	35406823	7416
11695	METLIN	1	35406947	6932
11696	ChemSpider	1	35406958	6113
11697	MetaboAnalyst	1	35406958	12515
11698	XCMS	1	35407029	6696
11699	MetaboAnalyst	2	35407036	7811
11700	XCMS	1	35407096	3877
11701	METLIN	1	35407096	5252
11702	MoNA	1	35407096	5320
11703	LDA	3	35408523	297
11704	HMDB	2	35408523	8476
11705	UniProtKB	1	35408572	7423
11706	COVAIN	1	35408702	4549
11707	METLIN	1	35408732	4381
11708	MetaboLights	1	35408732	5658
11709	HMDB	1	35408745	5572
11710	SMART	1	35408870	876
11711	Newton	1	35408870	6973
11712	MAVEN	1	35409014	6054
11713	XCMS	1	35409014	6449
11714	CAMERA	1	35409014	6458
11715	MetaboLights	1	35409014	6776
11716	MetaboAnalyst	2	35409014	7219
11717	MetaboAnalyst	1	35409026	9415
11718	XCMS	1	35409083	3606
11719	HMDB	1	35409083	3796
11720	LDA	1	35409192	4546
11721	HMDB	1	35409192	5579
11722	HMDB	1	35410114	10299
11723	LIPID MAPS	1	35410114	10306
11724	eRah	1	35411052	5938
11725	HMDB	1	35411052	27592
11726	MetaboAnalyst	1	35411952	24782
11727	MetaboAnalyst	3	35414043	11685
11728	HMDB	1	35414794	1337
11729	XCMS	1	35418203	13436
11730	muma	1	35418203	13704
11731	MetaboLights	1	35418203	13955
11732	MetaboAnalyst	1	35418244	3837
11733	HMDB	1	35418244	4244
11734	mixOmics	1	35418244	8856
11735	LDA	1	35418758	2937
11736	UniProtKB	1	35421091	512
11737	LDA	1	35422668	2735
11738	LDA	2	35422782	6806
11739	HMDB	1	35422836	4938
11740	METLIN	1	35422836	4948
11741	DrugBank	1	35424258	10176
11742	MZmine 2	1	35425721	8295
11743	GNPS	4	35425721	11676
11744	HMDB	1	35425721	12460
11745	MetaboAnalyst	4	35425789	11735
11746	GNPS	1	35425789	16234
11747	MZmine 2	1	35425793	4499
11748	MetaboAnalyst	1	35427356	8750
11749	HMDB	1	35428752	8805
11750	HMDB	1	35431771	6492
11751	BATMAN	1	35431901	4177
11752	MetaboAnalyst	2	35431901	6366
11753	XCMS	2	35431939	7390
11754	HMDB	1	35431939	8022
11755	METLIN	1	35431939	8028
11756	XCMS online	1	35431939	8148
11757	ChemSpider	2	35431942	8041
11758	HMDB	1	35431942	8129
11759	MetaboAnalyst	1	35431942	8417
11760	MetaboAnalyst	1	35431980	5947
11761	LDA	2	35432278	8149
11762	LDA	2	35432311	4295
11763	HMDB	1	35432311	5635
11764	MetaboAnalyst	1	35432311	6107
11765	mixOmics	2	35432452	3405
11766	XCMS	1	35432583	6686
11767	XCMS online	1	35432583	6686
11768	MIDAS	2	35440638	13725
11769	MetaboAnalyst	1	35440638	23865
11770	GNPS	2	35440708	7572
11771	SMART	1	35443619	4948
11772	MS-FINDER	1	35444259	17792
11773	MetaboAnalyst	1	35444259	18594
11774	HMDB	1	35444465	4690
11775	MetaboAnalyst	1	35444465	4856
11776	XCMS	1	35444956	4753
11777	LDA	1	35444956	5607
11778	XCMS	2	35445104	6049
11779	HMDB	2	35445104	7544
11780	MetaboAnalyst	1	35445104	7885
11781	LDA	1	35446112	6705
11782	HMDB	1	35446112	7350
11783	METLIN	1	35446112	7379
11784	MetaboNetworks	1	35446234	6993
11785	XCMS	1	35447798	8182
11786	LDA	2	35447815	4960
11787	GNPS	2	35447942	6555
11788	MetaboAnalyst	1	35448272	3679
11789	HMDB	6	35448463	2611
11790	GNPS	1	35448466	8773
11791	ChemSpider	1	35448466	9989
11792	METLIN	1	35448468	5382
11793	rDolphin	5	35448470	1659
11794	AMDIS	1	35448473	6392
11795	HMDB	1	35448473	7263
11796	MetaboAnalyst	2	35448473	11107
11797	MetaboAnalyst	1	35448475	3370
11798	METLIN	1	35448488	5101
11799	MetaboAnalyst	1	35448488	5975
11800	mixOmics	1	35448491	9235
11801	XCMS	1	35448494	6326
11802	GNPS	1	35448494	7674
11803	MetFrag	1	35448494	8013
11804	XCMS	1	35448496	3344
11805	MetaboAnalyst	2	35448496	3644
11806	METLIN	1	35448496	4886
11807	ChemSpider	1	35448497	8174
11808	MetaboAnalyst	1	35448497	8981
11809	Escher	1	35448501	3908
11810	MetaboAnalyst	2	35448504	4319
11811	GNPS	1	35448513	1968
11812	MetaboAnalyst	1	35448526	5299
11813	MetaboAnalyst	2	35448533	6597
11814	MetaboAnalyst	3	35448540	7843
11815	HMDB	1	35448540	10666
11816	HMDB	1	35448548	4344
11817	MetaboAnalyst	1	35448548	10856
11818	SMART	1	35448571	2007
11819	SMART	1	35448587	1583
11820	IIS	1	35448592	6283
11821	LDA	1	35448667	8167
11822	SMART	1	35449157	13355
11823	HMDB	1	35449540	2789
11824	METLIN	1	35449540	2808
11825	HMDB	1	35449541	2713
11826	LDA	1	35450049	3936
11827	MetaboAnalyst	2	35450870	7104
11828	GNPS	2	35453264	5266
11829	mixOmics	1	35453356	7569
11830	MetaboAnalyst	1	35453461	8870
11831	eRah	2	35453629	2280
11832	HMDB	1	35454761	807
11833	METLIN	1	35454761	836
11834	METLIN	1	35455081	4048
11835	GNPS	2	35455463	7041
11836	UniProtKB	1	35455936	5301
11837	MetaboAnalyst	1	35455936	8608
11838	SMPDB	1	35455936	9330
11839	MetaboAnalyst	1	35456270	10105
11840	SMART	1	35456464	690
11841	UniProtKB	1	35456791	2074
11842	mixOmics	1	35456812	8506
11843	SMART	2	35457040	341
11844	AMDIS	1	35457046	9142
11845	MetaboAnalyst	1	35457066	7228
11846	HMDB	1	35457694	10842
11847	MoNA	1	35457694	10860
11848	MetaboAnalyst	1	35457694	11683
11849	MetaboAnalyst	1	35457790	5431
11850	IPO	1	35458780	6586
11851	XCMS	3	35458780	6638
11852	MetaboAnalyst	1	35458780	7522
11853	MIDAS	2	35463427	8027
11854	HMDB	1	35463434	2988
11855	METLIN	1	35463434	3008
11856	SMART	1	35463452	414
11857	ropls	1	35463645	6998
11858	XCMS	1	35463946	2118
11859	XCMS online	1	35463946	2118
11860	MetaboAnalyst	2	35463946	2833
11861	HMDB	1	35463946	3781
11862	MoNA	1	35463946	3809
11863	METLIN	1	35463946	3887
11864	XCMS	1	35463951	715
11865	HMDB	2	35463954	5757
11866	ChemSpider	1	35463954	5763
11867	METLIN	1	35463954	5787
11868	MetaboAnalyst	1	35463954	7743
11869	MZmine 2	1	35464220	7104
11870	MetaboAnalyst	2	35464220	7478
11871	HMDB	1	35464220	8467
11872	XCMS	2	35464408	5614
11873	XCMS	1	35464990	3886
11874	ropls	1	35465261	4085
11875	MetaboAnalyst	1	35467388	5963
11876	LIPID MAPS	1	35468151	5234
11877	HMDB	1	35468151	5581
11878	LDA	1	35468731	8911
11879	HMDB	1	35468731	9900
11880	METLIN	1	35468731	9929
11881	XCMS	1	35473956	5911
11882	mixOmics	1	35474093	12589
11883	MetaboAnalyst	1	35474093	13144
11884	mixOmics	1	35476806	16494
11885	mixOmics	2	35477313	7523
11886	LDA	2	35479637	8863
11887	HMDB	1	35479637	10992
11888	ropls	1	35479637	11120
11889	XCMS	1	35481140	5220
11890	MetaboAnalyst	1	35481147	6014
11891	HMDB	1	35481548	5502
11892	MetaboAnalyst	1	35481834	7268
11893	LDA	1	35482778	3931
11894	MetaboAnalyst	1	35484407	11390
11895	HMDB	1	35484490	4673
11896	METLIN	1	35484490	4692
11897	ropls	1	35490556	9805
11898	HMDB	1	35491843	5750
11899	apLCMS	1	35493444	4680
11900	HMDB	1	35494569	3126
11901	MetaboAnalyst	1	35494569	3752
11902	MetAlign	1	35495658	5532
11903	MRMPROBS	1	35495658	6218
11904	LDA	1	35495661	3243
11905	MetaboAnalyst	1	35495661	8732
11906	HMDB	1	35495670	4729
11907	METLIN	1	35495720	5562
11908	LDA	1	35495722	4233
11909	HMDB	1	35496313	5895
11910	HMDB	3	35497496	4642
11911	SMART	1	35497911	3516
11912	GNPS	6	35497911	5850
11913	FingerID	1	35497911	6696
11914	XCMS	1	35497913	5206
11915	MoNA	1	35497913	6308
11916	MetaboAnalyst	1	35497913	6463
11917	HMDB	1	35497913	6540
11918	GNPS	3	35498703	1694
11919	Qemistree	3	35498703	5732
11920	FingerID	1	35498703	6430
11921	CANOPUS	2	35498703	6540
11922	ropls	1	35498711	7117
11923	XCMS	1	35498747	4119
11924	LDA	3	35503862	5028
11925	MetaboAnalyst	2	35506256	7671
11926	HMDB	1	35506364	2172
11927	MetaboAnalyst	3	35508141	13688
11928	HMDB	1	35508141	14760
11929	MetaboAnalyst	1	35508571	6221
11930	MetaboLights	1	35508980	3684
11931	HMDB	1	35509094	3728
11932	MetaboAnalyst	1	35509094	5924
11933	LDA	1	35509309	1167
11934	HMDB	1	35509309	6882
11935	DrugBank	1	35509362	607
11936	XCMS	2	35510071	11973
11937	MetaboAnalyst	1	35510071	13109
11938	MetaboAnalyst	1	35513538	9115
11939	HMDB	2	35513538	9188
11940	METLIN	1	35513691	6269
11941	MetaboAnalyst	1	35513691	7220
11942	LDA	4	35513691	9229
11943	UniProtKB	1	35513812	1066
11944	SMART	1	35513812	1262
11945	HMDB	1	35514958	3387
11946	MetaboAnalyst	2	35514958	5051
11947	MetaboAnalyst	1	35514981	7588
11948	LIPID MAPS	1	35515291	4752
11949	HMDB	1	35515291	4764
11950	ChemSpider	1	35515291	4780
11951	METLIN	1	35515291	4804
11952	MetaboAnalyst	1	35515291	4834
11953	HMDB	1	35515565	6063
11954	MetaboAnalyst	1	35515607	5204
11955	LDA	1	35516431	1807
11956	AMDIS	1	35517002	4451
11957	MetaboAnalyst	1	35517678	3200
11958	BATMAN	1	35517983	2282
11959	HMDB	1	35517983	4696
11960	MetaboAnalyst	1	35519589	8066
11961	SMPDB	1	35521635	3938
11962	GNPS	3	35522651	7054
11963	GNPS	5	35523965	582
11964	MetScape	1	35523980	8611
11965	HMDB	1	35526154	2424
11966	MetaboAnalyst	1	35526154	2585
11967	MetaboAnalyst	1	35527278	7296
11968	HMDB	1	35528207	6454
11969	MetaboAnalyst	2	35528207	7664
11970	VANTED	2	35529220	7845
11971	XCMS	1	35529437	1847
11972	ropls	1	35529437	2389
11973	MoNA	1	35529437	2897
11974	XCMS	1	35529487	1772
11975	HMDB	1	35529487	1970
11976	MetaboAnalyst	1	35530249	5780
11977	XCMS	1	35531479	6518
11978	Mummichog	1	35531479	6719
11979	MetaboAnalyst	1	35531479	6854
11980	MetaboAnalyst	2	35532069	8152
11981	ORCA	1	35534496	17344
11982	SMART	1	35534720	6821
11983	METLIN	1	35536645	17207
11984	HMDB	1	35536645	17318
11985	LIPID MAPS	1	35536645	17490
11986	XCMS	3	35536659	2651
11987	Skyline	1	35536659	6295
11988	Skyline	2	35538051	13338
11989	MZmine 2	1	35538079	11162
11990	HMDB	2	35539642	6904
11991	MetaboAnalyst	1	35539642	7673
11992	MetaboAnalyst	2	35540008	12396
11993	METLIN	1	35540831	5609
11994	HMDB	1	35540831	5645
11995	HMDB	1	35541663	4548
11996	HMDB	1	35542200	4644
11997	MetaboAnalyst	1	35542200	5350
11998	MAVEN	1	35543104	10425
11999	MetaboAnalyst	3	35543104	12814
12000	MetaboAnalyst	1	35546810	3748
12001	MetaboAnalyst	1	35547113	6275
12002	MetaboAnalyst	1	35547657	5728
12003	HMDB	1	35547747	10959
12004	METLIN	1	35547747	10965
12005	XCMS	1	35548284	6743
12006	MetaboAnalyst	1	35548284	7512
12007	HMDB	1	35548353	3381
12008	MetaboAnalyst	1	35548353	3412
12009	HMDB	1	35548355	2277
12010	HMDB	1	35548575	14257
12011	METLIN	1	35548575	14333
12012	MetaboAnalyst	1	35548688	2475
12013	PARADISe	1	35549618	9502
12014	PAPi	1	35549618	10199
12015	MetaboAnalyst	1	35550508	30761
12016	MAIT	15	35551522	982
12017	MetaboAnalyst	1	35557902	6634
12018	HMDB	1	35558553	5511
12019	MetaboAnalyst	1	35558750	4602
12020	GNPS	2	35559152	4964
12021	LDA	1	35559259	9017
12022	HMDB	1	35559259	9502
12023	METLIN	1	35559259	9568
12024	MetaboAnalyst	1	35559259	9696
12025	Skyline	1	35562924	1982
12026	BioCyc	2	35562924	3085
12027	MetaboAnalystR	1	35563095	9236
12028	HMDB	1	35563169	2394
12029	METLIN	1	35563169	2417
12030	HMDB	1	35563234	5750
12031	MetaboAnalystR	2	35563234	6083
12032	HMDB	1	35563341	7464
12033	METLIN	1	35563341	7525
12034	missForest	2	35563473	11659
12035	MetaboAnalyst	1	35563473	17830
12036	SMPDB	1	35563473	18555
12037	MetaboAnalyst	1	35563842	7333
12038	MetaboAnalyst	1	35563933	2167
12039	AMDIS	1	35564062	3867
12040	MET-IDEA	1	35564062	4041
12041	MetaboAnalyst	1	35565573	1771
12042	MetaboAnalyst	2	35565664	4075
12043	MetaboAnalyst	1	35565666	4964
12044	MetaboAnalyst	1	35565848	7851
12045	MetaboAnalyst	1	35565990	8046
12046	AMDIS	1	35566284	2657
12047	MET-IDEA	1	35566284	2768
12048	XCMS	3	35566316	707
12049	XCMS online	1	35566316	707
12050	MetaboAnalyst	1	35566329	15906
12051	ropls	1	35566345	3993
12052	MetaboAnalyst	1	35568757	8118
12053	mixOmics	1	35568757	10005
12054	LDA	1	35568757	10974
12055	ICT	5	35571086	203
12056	LDA	1	35571917	3870
12057	LDA	1	35571952	7584
12058	MetaboAnalyst	3	35571952	10132
12059	MetaboAnalyst	2	35572129	7028
12060	MAVEN	1	35572647	23726
12061	MetaboLights	1	35572647	23845
12062	GNPS	3	35572656	4275
12063	MetaboAnalyst	3	35572676	3700
12064	METLIN	1	35572676	3766
12065	HMDB	2	35572676	4584
12066	LDA	1	35572677	6230
12067	mixOmics	1	35572677	6713
12068	XCMS	2	35572684	4949
12069	CAMERA	1	35572684	5054
12070	HMDB	2	35572705	7902
12071	MetScape	1	35572705	10119
12072	MetaboAnalyst	1	35572705	10214
12073	HMDB	3	35572970	5317
12074	METLIN	1	35572970	5323
12075	SMPDB	1	35572970	6063
12076	MetaboAnalystR	1	35572970	6087
12077	ropls	1	35572970	6359
12078	HMDB	1	35573051	12428
12079	METLIN	1	35573051	12457
12080	MetaboAnalyst	1	35573781	1923
12081	XCMS	1	35574395	854
12082	MetaboAnalyst	4	35574395	1356
12083	METLIN	1	35574395	1763
12084	XCMS	2	35574439	8595
12085	HMDB	1	35574681	11247
12086	METLIN	1	35574681	11280
12087	HMDB	1	35577875	7371
12088	MoNA	1	35577875	7399
12089	Mummichog	2	35577875	10033
12090	MetaboAnalyst	2	35577875	10073
12091	missForest	1	35578165	27695
12092	XCMS	1	35578728	5912
12093	HMDB	1	35578728	6526
12094	METLIN	1	35578728	6532
12095	MetaboAnalyst	1	35579969	2390
12096	LDA	1	35579969	17399
12097	XCMS	1	35583333	5058
12098	MetaboAnalyst	1	35584084	7691
12099	HMDB	1	35584084	9505
12100	HMDB	1	35585165	3622
12101	MetaboAnalyst	1	35585165	5337
12102	ropls	2	35585490	8353
12103	HMDB	1	35586045	4082
12104	GNPS	3	35586120	4833
12105	MZmine 2	1	35586120	5099
12106	LDA	1	35586694	3961
12107	GNPS	3	35586858	12785
12108	Qemistree	1	35586858	14467
12109	METLIN	1	35587487	15279
12110	AMDIS	1	35588422	5853
12111	XCMS	2	35588422	6273
12112	METLIN	1	35588422	7058
12113	HMDB	1	35588422	7066
12114	MetaboAnalyst	1	35588422	7168
12115	MetaboAnalyst	4	35590091	4435
12116	XCMS	2	35590378	4905
12117	CAMERA	1	35590378	5082
12118	MetaboAnalyst	1	35590378	6180
12119	SMART	1	35591992	4349
12120	XCMS	1	35592677	2906
12121	HMDB	1	35592677	3079
12122	MetaboAnalyst	3	35592677	3310
12123	LDA	1	35592677	3742
12124	XCMS	1	35596146	12336
12125	METLIN	1	35596146	12480
12126	LDA	1	35600308	2436
12127	MetaboAnalyst	1	35600819	2405
12128	LDA	2	35600873	3973
12129	HMDB	1	35600884	4050
12130	METLIN	1	35600884	4078
12131	MetaboAnalyst	1	35600966	3243
12132	MetaboAnalystR	1	35601148	2477
12133	MetaboAnalystR	1	35601343	6653
12134	KIMBLE	1	35601460	656
12135	HMDB	1	35601834	3547
12136	METLIN	1	35601834	3579
12137	MetaboAnalystR	1	35602217	11758
12138	MetaboLights	1	35604083	425
12139	AMDIS	1	35604913	1623
12140	GNPS	1	35606844	8885
12141	MoNA	1	35608707	9845
12142	MetaboAnalyst	2	35608941	7657
12143	SMART	1	35608941	18151
12144	MelonnPan	1	35610236	9605
12145	SigMa	2	35610257	12288
12146	icoshift	1	35610257	12451
12147	HMDB	1	35613310	3832
12148	METLIN	1	35613310	3860
12149	Newton	1	35614105	2229
12150	HMDB	1	35615140	5424
12151	UniProtKB	1	35615451	1779
12152	Newton	1	35615451	2808
12153	MetaboAnalyst	2	35615451	3906
12154	XCMS	2	35619714	3347
12155	CAMERA	1	35619714	3451
12156	HMDB	1	35619714	3873
12157	LDA	2	35619714	6708
12158	MetaboAnalyst	1	35619960	9455
12159	XCMS	2	35620391	7296
12160	ropls	1	35620391	7674
12161	HMDB	1	35620391	8613
12162	XCMS	2	35620409	4292
12163	Newton	1	35620447	5662
12164	SMART	1	35621756	2624
12165	GNPS	1	35621985	7104
12166	MetaboAnalyst	2	35622894	6975
12167	MetaboAnalyst	1	35624184	5174
12168	Skyline	1	35624723	12729
12169	ORCA	1	35624814	7131
12170	MetaboLights	1	35624832	4620
12171	SMART	1	35625360	4416
12172	LIPID MAPS	1	35625636	3312
12173	MetaboAnalyst	1	35625656	5478
12174	METLIN	1	35626705	2539
12175	MetaboAnalyst	2	35626705	2873
12176	MetaboAnalyst	1	35626711	2031
12177	XCMS	1	35627008	2532
12178	MetaboAnalyst	1	35627073	5458
12179	ropls	2	35627078	3743
12180	MetaboAnalystR	1	35627204	4040
12181	GAM	1	35628110	1323
12182	MetaboAnalyst	1	35628110	4924
12183	HMDB	1	35628266	5433
12184	HMDB	1	35628277	6095
12185	MetaboAnalystR	1	35628591	4747
12186	XCMS	1	35628676	6520
12187	MetaboLights	1	35628712	3700
12188	METLIN	1	35628712	4252
12189	MetaboAnalyst	1	35628712	5988
12190	MetaboAnalyst	1	35628740	2751
12191	METLIN	2	35628740	3327
12192	METLIN	1	35628774	8253
12193	MetaboAnalyst	2	35628774	8487
12194	MetaboAnalyst	2	35629283	10663
12195	MetaboAnalyst	2	35629872	4522
12196	MetaboAnalyst	2	35629876	3480
12197	HMDB	1	35629882	6541
12198	LIPID MAPS	1	35629883	9405
12199	ChemSpider	1	35629883	9435
12200	MetaboAnalyst	5	35629883	9639
12201	MZmine 2	1	35629895	12334
12202	XCMS	2	35629897	3238
12203	MetaboAnalystR	1	35629909	4897
12204	XCMS	1	35629910	2670
12205	Rhea	1	35629921	3521
12206	HMDB	1	35629923	5150
12207	MetaboAnalyst	1	35629923	7403
12208	XCMS	2	35629925	6278
12209	Workflow4Metabolomics	1	35629925	7386
12210	MS-FINDER	2	35629930	5699
12211	MS-FINDER	1	35629931	5035
12212	GNPS	1	35629931	5138
12213	IsoMS	1	35629931	6089
12214	MetaboAnalyst	2	35629931	7356
12215	GNPS	1	35629933	3957
12216	MoNA	1	35629933	3970
12217	METLIN	1	35629938	2695
12218	LIPID MAPS	1	35629938	5606
12219	MoNA	1	35629939	3753
12220	MetaboAnalyst	2	35629945	6200
12221	HMDB	1	35629948	6674
12222	MetaboAnalyst	2	35629950	2018
12223	HMDB	1	35629950	3049
12224	MetScape	1	35629950	3363
12225	SMPDB	1	35629950	3620
12226	XCMS	2	35629952	3141
12227	CAMERA	2	35629952	3361
12228	MetaboAnalyst	1	35629952	3913
12229	MetaboAnalyst	2	35629955	10958
12230	METLIN	1	35629960	4585
12231	HMDB	1	35629960	4614
12232	MetaboAnalyst	1	35629960	5277
12233	XCMS	1	35629970	4195
12234	LDA	2	35630287	3916
12235	AMDIS	1	35630392	5056
12236	GNPS	6	35630432	6863
12237	HMDB	1	35630484	8179
12238	LDA	1	35630624	2309
12239	Workflow4Metabolomics	1	35630628	5352
12240	XCMS	1	35630628	5690
12241	MZmine 2	2	35630628	8131
12242	GNPS	4	35630628	8503
12243	MetaboAnalystR	1	35630739	5454
12244	HMDB	1	35630781	19434
12245	MetaboAnalyst	2	35631761	4648
12246	MAVEN	1	35633719	7367
12247	MetaboAnalyst	1	35633719	8030
12248	LDA	1	35634144	3095
12249	LDA	2	35634334	3321
12250	GAM	2	35634412	2985
12251	HMDB	1	35634726	3394
12252	LIPID MAPS	1	35635592	5602
12253	SMART	1	35637443	1578
12254	METLIN	1	35637516	9243
12255	FingerID	1	35637516	10216
12256	MetaboAnalyst	2	35637516	10365
12257	MetaboAnalyst	1	35641484	8559
12258	HMDB	1	35641596	2666
12259	MetaboAnalyst	1	35641596	10023
12260	LDA	1	35643532	9886
12261	HMDB	1	35646021	3548
12262	METLIN	1	35646021	3577
12263	MetaboAnalyst	1	35646266	3116
12264	GAM	1	35646853	873
12265	MetaboLights	1	35647058	2075
12266	MetaboAnalystR	1	35647058	2569
12267	XCMS	1	35647087	5969
12268	CAMERA	1	35647087	6188
12269	MetaboAnalyst	1	35647087	7943
12270	VANTED	1	35650294	4403
12271	XCMS	1	35651872	196
12272	MetaboAnalyst	2	35653354	5079
12273	Mummichog	2	35653354	5192
12274	HMDB	1	35653354	5841
12275	MetaboAnalyst	1	35655026	24826
12276	HMDB	1	35655785	7540
12277	LDA	2	35656031	2372
12278	MetaboAnalyst	1	35662725	2680
12279	BATMAN	1	35662946	107
12280	HMDB	1	35662946	3692
12281	ORCA	1	35663400	9888
12282	MetaboAnalyst	1	35663851	9581
12283	LDA	2	35663882	5982
12284	MetaboAnalyst	1	35663953	2832
12285	MetaboAnalyst	1	35663956	3396
12286	LDA	2	35664067	2273
12287	XCMS	1	35664067	6591
12288	MetaboAnalyst	1	35664067	9669
12289	MetScape	1	35664067	11450
12290	HMDB	1	35664672	899
12291	METLIN	1	35665157	3861
12292	MZmine 2	1	35665175	7224
12293	MetaboAnalyst	3	35665175	8028
12294	XCMS	1	35665181	5815
12295	MetaboAnalyst	1	35665181	6201
12296	MAIT	4	35667686	15
12297	GNPS	4	35668110	3184
12298	MZmine 2	1	35668110	3737
12299	GNPS	5	35668112	28449
12300	MetaboAnalyst	1	35668764	12996
12301	LDA	2	35668764	13506
12302	MetaboAnalyst	1	35668948	3111
12303	HMDB	1	35668948	3324
12304	MetaboAnalyst	1	35669116	2943
12305	MetFrag	1	35669266	7127
12306	HMDB	1	35669576	2782
12307	METLIN	1	35669576	2819
12308	XCMS	2	35669701	4865
12309	muma	1	35670764	13634
12310	HMDB	3	35672784	55856
12311	SMART	1	35672841	15788
12312	Bayesil	1	35673543	6830
12313	MetaboAnalyst	2	35674498	3032
12314	MetaboAnalyst	2	35675394	8399
12315	HMDB	1	35675394	35639
12316	XCMS	1	35676264	7345
12317	CAMERA	1	35676264	7354
12318	HMDB	2	35676264	7775
12319	XCMS	1	35677092	4581
12320	MoNA	1	35677092	5220
12321	ChemSpider	2	35677380	4744
12322	LDA	2	35677380	6003
12323	MetaboAnalyst	1	35680868	4773
12324	DrugBank	1	35681322	2360
12325	XCMS	1	35681322	4409
12326	MetaboAnalyst	1	35681401	7974
12327	SMPDB	1	35681748	7743
12328	MAVEN	1	35681802	4615
12329	HMDB	1	35681802	5075
12330	MetaboLights	1	35681802	7707
12331	MetaboAnalyst	1	35681802	8500
12332	ChemSpider	1	35682198	6505
12333	HMDB	1	35682198	6879
12334	MetaboAnalystR	1	35682623	2911
12335	HMDB	1	35682688	9529
12336	MetaboAnalyst	2	35682688	10574
12337	HMDB	1	35682698	6001
12338	ChemSpider	1	35682698	6052
12339	MetaboAnalyst	1	35682698	7238
12340	XCMS	1	35682742	8428
12341	MetaboAnalyst	2	35682742	11372
12342	HMDB	1	35682888	2803
12343	MetaboAnalyst	1	35682941	3892
12344	XCMS	1	35684010	7863
12345	XCMS online	1	35684010	7863
12346	METLIN	1	35684010	7925
12347	HMDB	1	35684049	4445
12348	MetaboAnalyst	1	35684049	6723
12349	MetaboAnalyst	2	35684267	7468
12350	ChemSpider	1	35684459	4889
12351	XCMS	2	35684485	2247
12352	MetaboAnalyst	1	35684496	7030
12353	MoNA	1	35684496	7716
12354	CFM-ID	1	35684496	8894
12355	MetaboAnalyst	1	35684580	3334
12356	ChemSpider	1	35684580	3516
12357	HMDB	1	35684580	3584
12358	HMDB	1	35685882	3785
12359	Skyline	1	35685882	3857
12360	MetaboAnalyst	1	35686233	6542
12361	Skyline	1	35690868	37702
12362	MetaboAnalyst	1	35692040	10764
12363	MIDAS	1	35692387	2633
12364	CFM-ID	1	35692690	9509
12365	MetaboAnalyst	2	35693010	7139
12366	ropls	1	35693782	6815
12367	MetaboAnalyst	1	35693822	6660
12368	MetaboAnalyst	1	35693845	1089
12369	MetaboAnalyst	1	35694160	6494
12370	ropls	1	35694253	2908
12371	HMDB	1	35694273	2655
12372	MetaboAnalyst	1	35694273	2714
12373	MZmine 2	1	35694313	8110
12374	GNPS	3	35694313	8123
12375	LDA	1	35694542	1987
12376	XCMS	1	35694542	2814
12377	TargetSearch	1	35696216	6070
12378	HMDB	1	35696443	3542
12379	MetaboAnalyst	1	35697869	3941
12380	XCMS	1	35701423	4894
12381	HMDB	1	35701423	5458
12382	MoNA	1	35701423	5525
12383	MetaboAnalyst	1	35701529	6559
12384	SMPDB	1	35701529	6812
12385	MetaboAnalystR	1	35701735	15629
12386	BinBase	3	35703366	5677
12387	MS-FLO	2	35703366	6977
12388	HMDB	1	35706052	4593
12389	MetaboAnalyst	2	35706818	4967
12390	apLCMS	1	35707205	13082
12391	xMSanalyzer	1	35707205	13098
12392	MetaboAnalyst	3	35707205	15115
12393	HMDB	1	35707402	2034
12394	MoNA	1	35707402	2063
12395	METLIN	1	35707402	2109
12396	MetaboAnalyst	1	35707402	3273
12397	MetaboAnalyst	1	35707720	20848
12398	HMDB	1	35707722	9562
12399	HMDB	1	35710396	5761
12400	Newton	1	35710532	6956
12401	DrugBank	1	35712079	8771
12402	MetaboAnalyst	1	35712719	6658
12403	GNPS	3	35714148	5351
12404	icoshift	1	35715864	3850
12405	HMDB	1	35715864	4407
12406	SMPDB	1	35715864	6619
12407	XCMS	1	35718784	3159
12408	LDA	1	35719348	5213
12409	LIPID MAPS	1	35720307	4227
12410	XCMS	1	35720415	3242
12411	HMDB	1	35720415	3422
12412	MetaboAnalyst	1	35720415	5239
12413	MetaboLights	1	35720601	9779
12414	LDA	1	35721198	5060
12415	HMDB	1	35721691	4634
12416	MetaboAnalyst	2	35721691	7611
12417	MetaboAnalyst	1	35721748	6011
12418	LDA	2	35721808	6162
12419	SistematX	1	35723407	521
12420	BATMAN	2	35725485	9849
12421	MS-FINDER	1	35725845	10477
12422	XCMS	5	35725845	12141
12423	X13CMS	1	35725845	21590
12424	El-MAVEN	5	35725845	22038
12425	MAVEN	5	35725845	22041
12426	Skyline	5	35725845	22680
12427	Bayesil	1	35725997	9233
12428	MetaboAnalyst	3	35725997	10026
12429	MetaboAnalyst	4	35729338	8226
12430	XCMS	1	35729658	5232
12431	CAMERA	1	35729658	5441
12432	LDA	1	35729658	6390
12433	MetaboAnalyst	1	35729681	11716
12434	UniProtKB	1	35730646	3486
12435	IIS	2	35731870	10664
12436	HMDB	1	35733791	10037
12437	METLIN	1	35733791	10067
12438	SMART	1	35733792	8137
12439	HMDB	1	35733792	12276
12440	METLIN	1	35733792	12294
12441	LDA	2	35733792	13690
12442	LDA	1	35733970	7702
12443	XCMS	1	35734371	6065
12444	MS-FINDER	1	35734371	6501
12445	MetaboAnalyst	1	35734601	3832
12446	SMART	1	35736065	1355
12447	MetaboAnalyst	1	35736102	4754
12448	MAIT	3	35736127	335
12449	HMDB	1	35736405	7837
12450	AlpsNMR	1	35736406	7145
12451	HMDB	1	35736406	7895
12452	MetaboAnalyst	1	35736406	8243
12453	XCMS	1	35736412	3407
12454	XCMS online	1	35736412	3407
12455	HMDB	1	35736416	4633
12456	GNPS	1	35736416	4757
12457	MetaboAnalyst	1	35736418	6085
12458	GNPS	8	35736420	7238
12459	MetFrag	1	35736420	8749
12460	MetaboAnalyst	1	35736420	11531
12461	HMDB	1	35736423	4127
12462	MetaboAnalyst	2	35736423	4916
12463	muma	1	35736423	5124
12464	ADAP-GC	1	35736424	195
12465	BioCyc	1	35736426	5103
12466	CFM-ID	1	35736426	5217
12467	MetaboAnalyst	1	35736426	5686
12468	Skyline	1	35736426	8192
12469	MetaboLights	2	35736426	9775
12470	HMDB	1	35736427	638
12471	mQTL	1	35736429	4370
12472	HMDB	2	35736430	3506
12473	MetaboAnalyst	1	35736430	5808
12474	MetaboAnalyst	4	35736433	6884
12475	HMDB	1	35736441	3952
12476	MetaboAnalyst	4	35736441	4044
12477	AMDIS	1	35736446	3258
12478	metabomxtr	1	35736446	3735
12479	HMDB	1	35736453	6102
12480	METLIN	1	35736453	6121
12481	MetaboLyzer	1	35736453	6514
12482	MetaboAnalyst	2	35736453	7643
12483	HMDB	2	35736459	1117
12484	HMDB	1	35736462	5143
12485	LDA	1	35736462	9245
12486	MetaboAnalyst	1	35736464	6878
12487	Skyline	2	35736465	5226
12488	MelonnPan	1	35736470	7024
12489	MetaboAnalyst	4	35736470	9118
12490	MIDAS	2	35736476	3592
12491	BinBase	2	35736480	2626
12492	MetaboAnalyst	1	35736480	5885
12493	MetaboAnalyst	1	35736481	3751
12494	GNPS	1	35736484	6493
12495	HMDB	2	35736488	3947
12496	XCMS	1	35736490	3924
12497	MetaboAnalyst	1	35736497	5181
12498	HMDB	1	35736499	4150
12499	MetaboAnalyst	2	35736721	4941
12500	XCMS	1	35736910	3149
12501	MetaboAnalyst	2	35736910	3524
12502	Skyline	1	35737056	8848
12503	LDA	2	35738801	4269
12504	MetaboAnalyst	1	35739152	13214
12505	SMART	1	35739834	680
12506	LDA	1	35739834	2276
12507	HMDB	1	35739834	5235
12508	XCMS	1	35739855	7422
12509	AMDIS	1	35739855	7696
12510	MetaboAnalyst	2	35739855	8335
12511	MetaboAnalystR	1	35739855	8509
12512	MetaboAnalyst	1	35739871	2748
12513	XCMS	4	35740022	1573
12514	METLIN	2	35740022	2894
12515	NMRProcFlow	1	35740075	2601
12516	MetaboAnalyst	1	35740075	4606
12517	MetaboAnalyst	2	35740189	5838
12518	MetaboAnalystR	1	35740189	5860
12519	SMPDB	1	35740417	16740
12520	MetaboAnalyst	1	35740417	16775
12521	mixOmics	1	35740417	16958
12522	METLIN	1	35740505	1921
12523	SMART	1	35740525	406
12524	MetaboAnalyst	1	35740569	7858
12525	MetaboAnalyst	5	35741360	5155
12526	XCMS	1	35741667	8029
12527	XCMS online	1	35741667	8029
12528	MetaboAnalyst	1	35741667	8109
12529	METLIN	1	35741667	8313
12530	HMDB	1	35741667	8420
12531	LIPID MAPS	1	35741667	8436
12532	MetaboAnalyst	1	35741895	4576
12533	MetaboAnalyst	1	35741928	6032
12534	XCMS	1	35741945	6343
12535	PARADISe	1	35741978	6408
12536	HMDB	1	35742832	2425
12537	SMART	1	35742843	818
12538	HMDB	1	35743000	5788
12539	XCMS	1	35743098	1698
12540	XCMS online	1	35743098	1698
12541	Newton	1	35743127	5888
12542	ChemSpider	1	35743127	6735
12543	mixOmics	1	35743153	5368
12544	MetaboAnalyst	1	35743153	7174
12545	SMPDB	1	35743153	7216
12546	METLIN	1	35743262	8298
12547	MetaboAnalyst	1	35743262	8557
12548	WikiPathways	1	35743803	2028
12549	MetaboAnalyst	1	35743839	4039
12550	MetaboLights	1	35743946	2185
12551	MetaboAnalystR	1	35743952	4933
12552	Newton	1	35744767	2777
12553	HMDB	1	35744776	7643
12554	MetaboAnalyst	1	35744917	6223
12555	LDA	5	35745107	2916
12556	HMDB	1	35745269	4264
12557	METLIN	1	35745841	1334
12558	MetaboAnalyst	4	35745841	5821
12559	GNPS	1	35745841	5961
12560	MetaboAnalyst	1	35746630	5484
12561	MetaboAnalyst	1	35746785	5631
12562	MetaboAnalyst	1	35747443	3665
12563	LDA	2	35751084	3523
12564	LDA	1	35754027	4728
12565	XCMS	1	35754027	5975
12566	ropls	1	35754027	6091
12567	MetaboAnalyst	2	35754480	8986
12568	HMDB	2	35755250	5050
12569	GNPS	1	35755250	12517
12570	AMDIS	1	35755645	7276
12571	MS-FINDER	1	35755672	2324
12572	HMDB	1	35755672	2515
12573	HMDB	1	35755691	9198
12574	GNPS	11	35755693	5028
12575	HMDB	1	35755693	6671
12576	SMART	1	35755695	3654
12577	HMDB	1	35755823	3246
12578	LDA	2	35756051	3986
12579	MetaboAnalyst	1	35756524	9819
12580	HMDB	1	35756524	9891
12581	MS-FINDER	3	35756625	6820
12582	HMDB	2	35756625	7231
12583	MetaboAnalyst	1	35756625	7560
12584	ropls	1	35757166	4448
12585	MZmine 2	1	35757167	9362
12586	GNPS	4	35757167	10594
12587	MetaboAnalyst	1	35757167	12051
12588	icoshift	1	35761396	4643
12589	MetaboAnalyst	3	35762019	3289
12590	MetFrag	1	35762019	5581
12591	HMDB	1	35763153	12298
12592	MetaboAnalyst	1	35763153	12513
12593	mixOmics	1	35763459	5134
12594	ropls	2	35763459	5190
12595	HMDB	1	35768426	16567
12596	MetaboAnalyst	1	35768426	16888
12597	HMDB	1	35769305	4305
12598	MetaboAnalyst	1	35769305	5063
12599	XCMS	1	35769613	2884
12600	MetaboAnalyst	1	35770013	1339
12601	HMDB	1	35770096	3748
12602	METLIN	1	35770096	3776
12603	Skyline	1	35770156	11618
12604	MetaboLights	1	35770156	11770
12605	MetaboAnalyst	1	35770156	12665
12606	MAVEN	2	35772749	10281
12607	HMDB	1	35774407	6625
12608	MetaboAnalyst	1	35774604	7182
12609	MetaboAnalystR	1	35774604	7900
12610	ChemSpider	1	35776737	4155
12611	mixOmics	1	35777909	9060
12612	HMDB	1	35777909	10977
12613	UniProtKB	1	35778403	18273
12614	METLIN	1	35778427	7772
12615	MoNA	1	35779152	8814
12616	HMDB	1	35782075	4139
12617	ropls	2	35782075	4349
12618	XCMS	2	35782123	4304
12619	CAMERA	1	35782123	4338
12620	MetaboAnalyst	1	35782123	4402
12621	LDA	1	35782130	1688
12622	LDA	1	35782134	5365
12623	VANTED	1	35782956	1895
12624	MetaboAnalyst	1	35782956	1971
12625	MetaboAnalystR	1	35783922	4909
12626	MetaboAnalyst	1	35783968	8208
12627	SMPDB	1	35784533	2288
12628	XCMS	1	35784552	2402
12629	CAMERA	1	35784552	2442
12630	HMDB	1	35784552	2870
12631	UniProtKB	1	35784695	3418
12632	HMDB	1	35784698	7205
12633	MetaboAnalyst	1	35784698	7841
12634	HMDB	1	35784718	4608
12635	icoshift	1	35785197	3299
12636	HMDB	2	35788821	8222
12637	XCMS	1	35789656	6033
12638	SMART	2	35790870	3311
12639	Mummichog	1	35794176	8514
12640	MetaboAnalyst	1	35794176	8539
12641	MetaboAnalyst	1	35794650	9129
12642	ropls	2	35794909	7278
12643	SMART	1	35795387	289
12644	HMDB	1	35795562	8501
12645	MetaboAnalyst	1	35795562	8605
12646	HMDB	1	35795688	9293
12647	ropls	1	35795688	11204
12648	MetaboAnalyst	2	35800160	10545
12649	MetaboAnalyst	1	35800349	9368
12650	XCMS	2	35800606	7250
12651	MetaboAnalyst	1	35800606	8068
12652	MetaboAnalystR	1	35800608	4500
12653	XCMS	2	35800611	6978
12654	XCMS online	1	35800611	7296
12655	MetaboAnalyst	3	35800611	7691
12656	LDA	2	35801115	3505
12657	ChemSpider	1	35802586	7679
12658	BioCyc	1	35802586	7691
12659	MetaboAnalystR	1	35802662	4153
12660	METLIN	1	35802662	7574
12661	XCMS	1	35804560	11025
12662	MetaboAnalyst	1	35806145	9790
12663	HMDB	1	35806406	5082
12664	MZmine 2	1	35807242	4580
12665	MetaboAnalyst	1	35807242	5700
12666	AMDIS	1	35807317	5237
12667	IIS	1	35807361	2364
12668	SMART	1	35807600	10123
12669	MetaboAnalyst	1	35807670	3005
12670	XCMS	1	35807707	8852
12671	MetaboAnalyst	1	35807707	9542
12672	MetaboLights	1	35811841	0
12673	HMDB	1	35811940	15053
12674	LIPID MAPS	1	35811940	15064
12675	MetaboAnalyst	1	35811960	3908
12676	HMDB	1	35812339	4922
12677	METLIN	1	35812339	4954
12678	MetaboAnalyst	1	35812339	5082
12679	MetaboAnalyst	1	35812349	2216
12680	XCMS	1	35812438	12491
12681	MetaboAnalyst	1	35812438	13879
12682	xMSannotator	2	35812438	14499
12683	HMDB	1	35812438	14564
12684	LIPID MAPS	1	35812438	14598
12685	XCMS	1	35812865	6355
12686	MetaboAnalyst	1	35812865	6668
12687	HMDB	1	35812868	2131
12688	ropls	2	35812868	2270
12689	XCMS	1	35812872	5902
12690	MetaboAnalyst	3	35812872	6656
12691	XCMS	2	35812876	4101
12692	CAMERA	1	35812876	4227
12693	HMDB	1	35812876	4701
12694	HMDB	1	35812889	5835
12695	MetaboAnalyst	1	35812889	6099
12696	MetaboAnalystR	1	35812920	6198
12697	Pathview	2	35812937	3906
12698	XCMS	2	35812937	10440
12699	HMDB	1	35812937	11410
12700	METLIN	1	35812937	11416
12701	PARADISe	1	35813244	4115
12702	XCMS	1	35813812	5054
12703	METLIN	1	35813820	4429
12704	HMDB	1	35813820	4551
12705	XCMS	1	35814644	3102
12706	CAMERA	1	35814644	3108
12707	XCMS	1	35814697	6986
12708	MetaboAnalyst	1	35814697	7788
12709	MetaboAnalyst	1	35815273	8281
12710	Pathview	1	35815273	8382
12711	MetaboAnalyst	1	35815276	7076
12712	MetaboAnalyst	1	35815941	9259
12713	MetaboAnalyst	2	35817773	5728
12714	XCMS	1	35820806	6936
12715	MetaboAnalyst	1	35820806	7034
12716	XCMS	2	35820909	6337
12717	CAMERA	1	35820909	6519
12718	XCMS	1	35821237	6970
12719	ChemSpider	1	35821237	7078
12720	MetaboAnalyst	1	35821246	5480
12721	MetaboAnalystR	2	35821263	11569
12722	UniProtKB	1	35822617	10910
12723	AMDIS	1	35829802	2678
12724	XCMS	1	35829802	2810
12725	MetaboAnalyst	1	35829802	3359
12726	MZmine 2	1	35831335	4236
12727	MetaboAnalyst	1	35831335	5366
12728	HMDB	1	35831427	945
12729	MetaboAnalyst	1	35831456	8265
12730	MetaboAnalyst	1	35831878	9692
12731	MetaboAnalyst	1	35833033	4155
12732	MetaboAnalyst	3	35834517	3363
12733	HMDB	1	35834517	6144
12734	XCMS	1	35835746	3698
12735	XCMS online	1	35835746	3698
12736	mzR	1	35835746	13578
12737	METLIN	1	35835746	14445
12738	AMDIS	1	35835786	5203
12739	XCMS	6	35835939	8608
12740	GNPS	3	35835939	8930
12741	XCMS online	2	35835939	9242
12742	HMDB	1	35835939	14957
12743	METLIN	1	35836291	4300
12744	XCMS	1	35836421	2891
12745	MetaboAnalyst	1	35836421	8034
12746	XCMS	1	35837285	4463
12747	MoNA	1	35837285	4811
12748	MetaboAnalyst	1	35837823	4715
12749	MIDAS	2	35840907	1117
12750	XCMS	1	35841106	4964
12751	MetaboAnalyst	1	35841106	5462
12752	HMDB	1	35842456	4027
12753	LDA	1	35844229	9123
12754	BATMAN	1	35845577	398
12755	MetaboAnalyst	1	35845638	6335
12756	MetaboAnalyst	1	35845655	10900
12757	UniProtKB	1	35846007	6086
12758	MetaboAnalyst	4	35846007	7570
12759	HMDB	1	35846007	9141
12760	NMRProcFlow	2	35846007	9492
12761	MetaboAnalyst	3	35846359	12275
12762	HMDB	1	35846359	12823
12763	MetaboAnalyst	1	35847012	5087
12764	LDA	2	35847062	3591
12765	MetaboAnalyst	3	35847062	5047
12766	LDA	2	35847083	3870
12767	XCMS	1	35847083	4819
12768	HMDB	1	35847641	3046
12769	MetaboAnalyst	1	35847641	3167
12770	UniProtKB	1	35847793	17909
12771	DrugBank	1	35847793	18366
12772	HMDB	1	35847971	7643
12773	MetaboAnalyst	1	35847971	7821
12774	MAVEN	1	35847976	3579
12775	MetaboAnalyst	1	35847976	3768
12776	MetaboAnalyst	1	35850889	9947
12777	MetaboAnalyst	4	35851093	8888
12778	HMDB	2	35854233	4304
12779	XCMS	1	35855861	3003
12780	DrugBank	1	35855861	5026
12781	eRah	1	35857216	5851
12782	MetaboAnalyst	4	35859746	7759
12783	ropls	1	35860242	4543
12784	ChemSpider	1	35860432	4823
12785	WikiPathways	1	35860432	11035
12786	MetaboAnalyst	1	35860432	11077
12787	Metab	1	35864195	2032
12788	METLIN	1	35864195	4802
12789	ropls	1	35864448	4334
12790	Skyline	1	35865003	8748
12791	HMDB	2	35865003	9915
12792	MetaboAnalyst	1	35865003	10642
12793	XCMS	1	35866150	3770
12794	MetaboAnalyst	3	35866150	3835
12795	mixOmics	1	35867817	8533
12796	XCMS	1	35867817	25317
12797	MetaboAnalyst	1	35869172	6029
12798	icoshift	1	35869176	11673
12799	MetaboAnalyst	1	35869468	3613
12800	MetaboAnalyst	2	35871072	4385
12801	XCMS	1	35873647	604
12802	LDA	1	35873770	2094
12803	HMDB	1	35873784	4569
12804	MetaboAnalyst	2	35873784	4662
12805	MetaboAnalyst	1	35873980	5803
12806	MetaboAnalyst	1	35873984	5716
12807	MetaboAnalyst	1	35874019	4493
12808	MetaboAnalyst	1	35874738	4251
12809	MetaboAnalyst	2	35874959	5087
12810	MoNA	1	35874959	6202
12811	METLIN	1	35874959	6247
12812	MetaboAnalyst	1	35875567	8338
12813	LDA	2	35875568	4058
12814	LDA	2	35875583	5165
12815	GNPS	4	35877716	15255
12816	MS-FINDER	2	35877716	16989
12817	MZmine 2	1	35877717	21349
12818	GNPS	1	35877724	7394
12819	SMART	1	35877735	2209
12820	HMDB	1	35878224	1562
12821	MetaboAnalyst	1	35878224	1829
12822	MetaboAnalyst	2	35879786	3860
12823	LIPID MAPS	1	35883090	7628
12824	HMDB	1	35883090	7701
12825	HMDB	1	35883325	5266
12826	MetaboAnalyst	1	35883325	5331
12827	UniProtKB	1	35883427	5452
12828	HMDB	1	35883474	11621
12829	METLIN	1	35883474	11645
12830	HMDB	2	35883517	4335
12831	MetaboAnalyst	1	35883517	6757
12832	SMART	1	35883518	913
12833	LDA	3	35883701	6362
12834	MetaboAnalyst	2	35883787	10448
12835	MZmine 2	1	35883868	5312
12836	Skyline	2	35884951	9678
12837	MetaboAnalyst	2	35885023	4173
12838	MassTRIX	1	35885023	5304
12839	HMDB	1	35885023	5511
12840	LDA	2	35885045	825
12841	MAVEN	1	35887101	2879
12842	MetaboAnalyst	2	35887101	3528
12843	HMDB	2	35887101	4232
12844	mixOmics	1	35887145	4646
12845	BinBase	1	35887145	5375
12846	CAMERA	1	35887145	6171
12847	XCMS	1	35887145	6183
12848	MetaboAnalyst	1	35887145	6494
12849	MetaboAnalyst	1	35887177	2400
12850	MetaboAnalyst	1	35887227	4740
12851	BinBase	1	35887252	4167
12852	MS-FLO	1	35887252	5097
12853	LDA	1	35887270	5190
12854	HMDB	1	35887316	13448
12855	HMDB	1	35887324	2999
12856	MetaboAnalyst	1	35887324	3129
12857	MetaboAnalystR	1	35887438	2261
12858	LDA	1	35887438	4248
12859	ropls	1	35887452	2310
12860	MetaboAnalyst	2	35887478	4860
12861	MetaboAnalyst	1	35888178	6872
12862	XCMS	1	35888694	5145
12863	HMDB	1	35888705	4305
12864	Escher	1	35888717	5965
12865	ChemSpider	1	35888724	3316
12866	HMDB	2	35888724	3328
12867	HMDB	1	35888728	8600
12868	HMDB	1	35888729	4439
12869	LIPID MAPS	1	35888729	4458
12870	MoNA	3	35888729	7448
12871	MetaboAnalyst	1	35888734	5276
12872	AMDIS	1	35888738	4866
12873	MetaboAnalyst	2	35888738	6267
12874	apLCMS	1	35888748	2942
12875	xMSanalyzer	1	35888748	2953
12876	MetaboAnalyst	2	35888764	5078
12877	XCMS	1	35888769	5424
12878	METLIN	1	35888769	6284
12879	MetaboLights	1	35888774	4223
12880	mixOmics	1	35888774	8428
12881	MetaboAnalyst	1	35888777	3903
12882	MetaboAnalyst	2	35888781	5659
12883	MetaboAnalystR	2	35888781	5778
12884	HMDB	1	35888782	4509
12885	MetaboAnalyst	2	35888782	7084
12886	MetaboAnalyst	1	35888784	4353
12887	MZmine 2	1	35888789	4368
12888	missForest	1	35888795	6865
12889	MetaboAnalyst	2	35889255	2819
12890	ropls	1	35889255	4948
12891	METLIN	1	35889316	8708
12892	MetFrag	1	35889316	8764
12893	GNPS	4	35889335	5007
12894	SMART	1	35889347	4028
12895	MetaboAnalystR	1	35889387	2359
12896	MoNA	1	35889392	12841
12897	METLIN	1	35889392	12848
12898	HMDB	1	35889392	12887
12899	GNPS	2	35889415	3610
12900	MZmine 2	1	35889415	4196
12901	XCMS	1	35889514	2987
12902	mixOmics	1	35889514	3252
12903	HMDB	1	35889770	7309
12904	MetaboAnalyst	1	35889916	8086
12905	PathVisio	1	35890009	4425
12906	HMDB	1	35890029	11314
12907	Skyline	2	35890199	4288
12908	BATMAN	1	35891565	475
12909	MetaboAnalyst	1	35892645	4319
12910	LIQUID	1	35892959	4616
12911	XCMS	1	35892959	6370
12912	HMDB	1	35892959	6569
12913	XCMS	1	35893250	1617
12914	XCMS	1	35893252	2256
12915	HMDB	1	35893254	6311
12916	METLIN	1	35893254	6321
12917	MetaboAnalyst	1	35893260	4311
12918	MetaboAnalyst	1	35893874	5786
12919	XCMS	3	35893876	3932
12920	XCMS online	3	35893876	3932
12921	Mummichog	1	35893876	4852
12922	METLIN	1	35893876	5341
12923	MoNA	1	35893876	5379
12924	MetaboAnalyst	6	35896750	22360
12925	HMDB	1	35896750	31333
12926	Newton	5	35897871	5703
12927	HMDB	1	35897871	8891
12928	XCMS	1	35897876	2221
12929	Workflow4Metabolomics	1	35897888	5393
12930	MetaboAnalyst	1	35898456	2174
12931	MetaboAnalyst	1	35898548	4171
12932	XCMS	1	35898907	7519
12933	MetaboAnalystR	1	35899115	3089
12934	HMDB	1	35899122	5661
12935	METLIN	1	35899122	5667
12936	missForest	1	35902589	16855
12937	HMDB	1	35902973	8455
12938	LDA	2	35903106	6301
12939	XCMS	2	35903344	5416
12940	HMDB	1	35903344	7391
12941	MetaboAnalyst	1	35903344	7685
12942	DrugBank	1	35903344	8601
12943	MetScape	2	35903344	9540
12944	MetaboAnalyst	1	35906399	7617
12945	MetScape	1	35906399	7929
12946	ChemSpider	1	35906695	4413
12947	MetaboAnalyst	1	35906695	5442
12948	MetaboLights	1	35906695	5545
12949	LDA	1	35907917	8304
12950	HMDB	2	35909528	5663
12951	MyCompoundID	1	35909528	5677
12952	MetaboAnalystR	1	35909726	3070
12953	MetaboAnalyst	1	35909773	10884
12954	ropls	1	35909785	2837
12955	MetaboAnalystR	1	35909785	6625
12956	XCMS	1	35909955	5162
12957	MetaboAnalyst	1	35910130	11175
12958	ropls	2	35910368	4068
12959	MetaboAnalyst	2	35910368	4369
12960	XCMS	1	35910368	6649
12961	MetaboAnalyst	2	35910560	2761
12962	LDA	1	35910597	5866
12963	MetaboAnalyst	1	35910597	7814
12964	PAPi	1	35910599	5856
12965	LDA	3	35910622	11676
12966	LDA	1	35910661	3806
12967	HMDB	1	35910841	4897
12968	MetaboAnalyst	2	35910841	5488
12969	HMDB	1	35911097	3863
12970	ropls	2	35911097	4316
12971	ropls	1	35911107	9281
12972	XCMS	2	35911235	2016
12973	XCMS online	1	35911235	2016
12974	MetaboAnalyst	1	35911235	2374
12975	HMDB	1	35911761	3592
12976	LDA	2	35911767	9227
12977	MetaboAnalyst	1	35911767	9614
12978	HMDB	1	35912104	12870
12979	METLIN	1	35912104	12901
12980	MZedDB	1	35917032	9840
12981	HMDB	1	35918343	11699
12982	PARADISe	1	35918401	3102
12983	BinBase	5	35922450	7878
12984	MetaboAnalyst	1	35922847	7808
12985	Skyline	1	35922847	9700
12986	MetaboAnalystR	1	35923198	4806
12987	XCMS	1	35923396	6659
12988	MetaboAnalyst	1	35923396	7893
12989	LDA	1	35923406	7711
12990	METLIN	1	35928228	6321
12991	ChemSpider	1	35928228	6356
12992	MetaboAnalyst	2	35928228	8128
12993	HMDB	1	35928255	5417
12994	MetaboAnalyst	1	35928255	5822
12995	MetaboAnalyst	1	35928270	3965
12996	LDA	3	35928270	5211
12997	METLIN	1	35928271	3404
12998	MetaboAnalyst	1	35928271	5887
12999	MetaboAnalyst	1	35928403	4218
13000	Mummichog	1	35931769	10000
13001	MetaboAnalyst	3	35933481	4811
13002	HMDB	1	35933481	7865
13003	MetaboAnalystR	2	35934696	4937
13004	HMDB	1	35935130	12125
13005	MAVEN	3	35935218	6064
13006	El-MAVEN	2	35935218	6071
13007	MetaboAnalyst	1	35935232	11994
13008	HMDB	1	35935818	6875
13009	MetaboAnalyst	1	35935818	7046
13010	MetFrag	2	35936100	4644
13011	MetaboAnalyst	1	35936679	1918
13012	HMDB	1	35936789	2015
13013	YMDB	1	35936789	2066
13014	MetaNetter	2	35936789	6092
13015	METLIN	1	35937565	3020
13016	HMDB	1	35937565	3059
13017	HMDB	1	35937683	7086
13018	MetaboAnalyst	1	35937683	7105
13019	GNPS	1	35941113	5362
13020	UC2	1	35941134	2919
13021	ropls	1	35941193	11120
13022	XCMS	1	35941589	5657
13023	MetaboAnalyst	1	35941589	6401
13024	HMDB	1	35941611	3776
13025	MetaboAnalyst	1	35941611	3817
13026	XCMS	1	35941695	9874
13027	ropls	1	35941695	10473
13028	LDA	2	35942316	11070
13029	HMDB	2	35945752	2062
13030	METLIN	1	35945752	2200
13031	MetaboAnalyst	1	35945752	2523
13032	MetaboAnalystR	1	35957691	2006
13033	MetaboAnalyst	1	35957829	4840
13034	GNPS	4	35958129	12128
13035	XCMS	1	35958261	6008
13036	HMDB	2	35958261	6655
13037	MetaboAnalyst	1	35958261	7999
13038	MetaboAnalyst	2	35958335	5905
13039	SMPDB	1	35958335	6354
13040	HMDB	1	35958907	4148
13041	HMDB	1	35959293	2373
13042	METLIN	1	35959293	2456
13043	ChemSpider	1	35959419	5746
13044	HMDB	1	35959433	7925
13045	LIPID MAPS	1	35959433	7967
13046	MetaboAnalyst	1	35959433	8056
13047	MetaboAnalyst	2	6481057	3384
13048	HMDB	1	6481057	3703
13049	LIPID MAPS	1	9311960	7841
13050	MetaboAnalyst	3	9312993	4199
\.


--
-- Data for Name: os_options; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.os_options (tool_name, os, rowid) FROM stdin;
MetMatch	Windows	1
SoyKB	Windows	2
SoyKB	Mac	3
SoyKB	Linux	4
BioCyc	Mac	5
BioCyc	Linux	6
BioCyc	Windows	7
Normalyzer	Linux	8
Normalyzer	Mac	9
Normalyzer	Windows	10
PhenoMeNal	Linux	11
FragPred	Windows	12
ChemoSpec	Windows	13
ChemoSpec	Linux	14
MassTRIX	Windows	15
MassTRIX	Linux	16
LiverWiki	Mac	17
MetaboGroup S	Windows	18
polyPK	Windows	19
MetNorm	Linux	20
metaMS	Windows	21
metaMS	Mac	22
swathTUNER	Windows	23
IIS	Linux	24
SpectConnect	Windows	25
SpectConnect	Mac	26
SpectConnect	Linux	27
MetaboQuant	Windows	28
TOXcms	Linux	29
BinBase	Windows	30
eMZed	Linux	31
ICT	Windows	32
ICT	Linux	33
mineXpert	Windows	34
mineXpert	Mac	35
mineXpert	Linux	36
BioNetApp	Windows	37
xMSannotator	Linux	38
MeltDB	Mac	39
MeltDB	Linux	40
Meta P-server	Windows	41
ChromA	Windows	42
SpectraClassifier	Windows	43
SpectraClassifier	Mac	44
SpectraClassifier	Linux	45
IntLIM	Linux	46
proFIA	Windows	47
proFIA	Mac	48
proFIA	Linux	49
MetaBox	Mac	50
MetaBox	Linux	51
HAMMER	Windows	52
batchCorr	Mac	53
MoDentify	Mac	54
MoDentify	Windows	55
MetCCS Predictor	Windows	56
MetCCS Predictor	Mac	57
MetCCS Predictor	Linux	58
TargetSearch	Windows	59
TargetSearch	Mac	60
TargetSearch	Linux	61
PAPi	Windows	62
PAPi	Mac	63
PAPi	Linux	64
MAVEN	Linux	65
RDFIO	Mac	66
PhenoMeNal	Windows	67
MetSign	Mac	68
MetSign	Linux	69
XCMS	Windows	70
decoMS2	Mac	71
Bayesil	Linux	72
MSPrep	Windows	73
MetaboDiff	Mac	74
MetaboLights	Windows	75
GSimp	Windows	76
GSimp	Mac	77
MetCirc	Windows	78
MetCirc	Mac	79
MetCirc	Linux	80
IsoMS	Windows	81
IsoMS	Mac	82
IsoMS	Linux	83
IntLIM	Windows	84
InterpretMSSpectrum	Windows	85
mzAccess	Mac	86
mzAccess	Linux	87
credential	Mac	88
metabomxtr	Linux	89
RAMClustR	Mac	90
RAMClustR	Linux	91
MetaboLyzer	Linux	92
MetaboLyzer	Mac	93
CSI:FingerrID	Linux	94
crmn	Mac	95
TracMass2	Windows	96
ChemDistiller	Linux	97
UniProtKB	Windows	98
UniProtKB	Mac	99
GNPS-MassIVE	Windows	100
TriDAMP	Linux	101
eRah	Mac	102
DrugBank	Windows	103
DrugBank	Mac	104
DrugBank	Linux	105
FALCON	Windows	106
FALCON	Mac	107
FALCON	Linux	108
El-MAVEN	Mac	109
MMSAT	Mac	110
mzMatch	Mac	111
mzMatch	Linux	112
MetaboLab	Windows	113
MetaboLab	Linux	114
MetaboLab	Mac	115
SAMMI	Windows	116
eMZed	Windows	117
MathDAMP	Mac	118
MetShot	Linux	119
MetaQuant	Windows	120
ChemoSpec	Mac	121
MetaboQuant	Linux	122
apLCMS	Windows	123
MetTailor	Windows	124
compMS2Miner	Mac	125
Warpgroup	Mac	126
Warpgroup	Linux	127
Pathview	Windows	128
Pathview	Linux	129
Pathview	Mac	130
MassTRIX	Mac	131
MetaBox	Windows	132
mz.unity	Windows	133
mz.unity	Linux	134
libChEBI	Mac	135
Ideom	Mac	136
Ideom	Windows	137
IsoCor	Mac	138
TNO-DECO	Linux	139
missForest	Windows	140
PathVisio	Windows	141
PathVisio	Mac	142
PathVisio	Linux	143
missForest	Mac	144
MZedDB	Linux	145
MI-PACK	Mac	146
PepsNMR	Mac	147
LDA	Windows	148
E-Dragon	Windows	149
E-Dragon	Mac	150
E-Dragon	Linux	151
DiffCorr	Mac	152
DiffCorr	Linux	153
CSI:FingerrID	Windows	154
CSI:FingerrID	Mac	155
MetFusion	Windows	156
MAVEN	Mac	157
MetExplore	Windows	158
MetExplore	Mac	159
MetExplore	Linux	160
MathDAMP	Windows	161
intCor	Mac	162
intCor	Windows	163
intCor	Linux	164
Mummichog	Linux	165
FingerID	Windows	166
nmrglue	Windows	167
nmrglue	Mac	168
nmrglue	Linux	169
alsace	Mac	170
alsace	Linux	171
MathDAMP	Linux	172
mzR	Windows	173
mzR	Linux	174
MI-PACK	Windows	175
MI-PACK	Linux	176
MZedDB	Mac	177
MetDisease	Windows	178
MetDisease	Linux	179
MetDisease	Mac	180
HCOR	Windows	181
HCOR	Linux	182
Workflow4Metabolomics	Linux	183
MeRy-B	Mac	184
MeRy-B	Linux	185
PeroxisomeDB	Windows	186
ORCA	Mac	187
MET-IDEA	Windows	188
metabomxtr	Windows	189
MetaboSearch	Linux	190
MET-COFEI	Windows	191
MESSI	Windows	192
MESSI	Mac	193
MetFrag	Windows	194
Metab	Linux	195
RMassBank	Linux	196
BioNetApp	Mac	197
BioNetApp	Linux	198
MetiTree	Linux	199
MetaDB	Windows	200
MetaDB	Mac	201
MetaDB	Linux	202
Maui-VIA	Windows	203
OPTIMAS-DW	Windows	204
OPTIMAS-DW	Linux	205
MetaboClust	Windows	206
Ideom	Linux	207
Pathos	Windows	208
FragPred	Mac	209
WikiPathways	Windows	210
WikiPathways	Mac	211
WikiPathways	Linux	212
MetShot	Mac	213
Lilikoi	Linux	214
MetFlow	Windows	215
MetFlow	Mac	216
MetFlow	Linux	217
MetFamily	Mac	218
ProbMetab	Windows	219
VSClust	Windows	220
Escher	Mac	221
Escher	Linux	222
MetFrag	Mac	223
BRAIN	Windows	224
BRAIN	Mac	225
MetMSLine	Mac	226
BRAIN	Linux	227
ropls	Linux	228
COVAIN	Windows	229
MRMPROBS	Windows	230
BiGG Models	Mac	231
MetFamily	Windows	232
NMRPro	Linux	233
MetaboLights	Mac	234
crmn	Windows	235
crmn	Linux	236
GSim	Mac	237
MetaMapR	Mac	238
Curatr	Mac	239
Curatr	Linux	240
PiMP	Windows	241
PiMP	Mac	242
Rhea	Windows	243
COLMAR	Windows	244
KeggArray	Mac	245
KeggArray	Linux	246
MetaboNetworks	Windows	247
MetaboNetworks	Mac	248
MetaboNetworks	Linux	249
CFM-ID	Linux	250
CFM-ID	Windows	251
CFM-ID	Mac	252
polyPK	Linux	253
iPath	Windows	254
iPath	Mac	255
iPath	Linux	256
Lipid-Pro	Windows	257
MSPrep	Mac	258
MSPrep	Linux	259
CGBayesNets	Windows	260
CGBayesNets	Mac	261
CGBayesNets	Linux	262
mQTL	Windows	263
mQTL	Mac	264
mQTL	Linux	265
MetFamily	Linux	266
MWASTools	Windows	267
MetaQuant	Mac	268
MetaQuant	Linux	269
MET-COFEA	Windows	270
MetaboGroup S	Mac	271
MetaboGroup S	Linux	272
msPurity	Linux	273
msPurity	Mac	274
batchCorr	Linux	275
Pathos	Linux	276
GSim	Linux	277
ADAP/MZmine	Linux	278
ADAP/MZmine	Mac	279
polyPK	Mac	280
SysteMHC	Mac	281
SysteMHC	Linux	282
CAMERA	Linux	283
MAVEN	Windows	284
NP-StructurePredictor	Linux	285
KPIC	Mac	286
KPIC	Linux	287
MetScape	Windows	288
eRah	Linux	289
MetSign	Windows	290
TNO-DECO	Windows	291
TNO-DECO	Mac	292
AMDORAP	Mac	293
AMDORAP	Linux	294
AMDORAP	Windows	295
MetabNet	Mac	296
MetabNet	Linux	297
SMPDB	Linux	298
XCMS	Linux	299
Ionwinze	Windows	300
specmine	Mac	301
specmine	Linux	302
SAMMI	Mac	303
SAMMI	Linux	304
Workflow4Metabolomics	Windows	305
Workflow4Metabolomics	Mac	306
LiverWiki	Windows	307
MetMask	Windows	308
MetMask	Mac	309
MetMask	Linux	310
ChemSpider	Linux	311
MultiAlign	Windows	312
Lilikoi	Windows	313
Lilikoi	Mac	314
VMH	Linux	315
SmileMS	Windows	316
RDFIO	Windows	317
decoMS2	Linux	318
MetaMapR	Windows	319
muma	Windows	320
PathoLogic	Windows	321
PathoLogic	Mac	322
PathoLogic	Linux	323
ChemSpider	Windows	324
ChemSpider	Mac	325
pyQms	Windows	326
MINEs	Linux	327
UC2	Windows	328
UC2	Linux	329
mzAccess	Windows	330
Rhea	Linux	331
MetaboQuant	Mac	332
ChromA	Linux	333
MyCompoundID	Windows	334
ADAP/MZmine	Windows	335
msPurity	Windows	336
QCScreen	Windows	337
nPYc-Toolbox	Windows	338
MIDAS	Linux	339
MetabNet	Windows	340
PepsNMR	Windows	341
matNMR	Mac	342
matNMR	Linux	343
MAGMa	Windows	344
MAGMa	Linux	345
MetaMapR	Linux	346
Curatr	Windows	347
VANTED	Mac	348
ORCA	Linux	349
alsace	Windows	350
GabiPD	Linux	351
BatMass	Windows	352
BatMass	Mac	353
BatMass	Linux	354
ChemDistiller	Mac	355
ConsensusPathDB	Windows	356
ConsensusPathDB	Mac	357
XCMS online	Linux	358
XCMS online	Windows	359
XCMS online	Mac	360
Newton	Mac	361
GNPS-MassIVE	Mac	362
GNPS-MassIVE	Linux	363
LIPID MAPS	Mac	364
ALLocater	Windows	365
HMDB	Mac	366
MAIT	Linux	367
TOXcms	Mac	368
MetaboLights	Linux	369
swathTUNER	Mac	370
eMZed	Mac	371
QPMASS	Windows	372
TracMass2	Mac	373
IsoMS-Quant	Windows	374
IsoMS-Quant	Mac	375
MetFusion	Linux	376
flagme	Mac	377
flagme	Windows	378
icoshift	Mac	379
MyCompoundID	Mac	380
MyCompoundID	Linux	381
MetTailor	Mac	382
METLIN	Windows	383
METLIN	Mac	384
METLIN	Linux	385
IsoMS-Quant	Linux	386
swathTUNER	Linux	387
TOXcms	Windows	388
MZedDB	Windows	389
MoDentify	Linux	390
MetaboDiff	Windows	391
Metab	Windows	392
MetaboMiner	Windows	393
MetaboMiner	Linux	394
iMet-Q	Windows	395
MoNA	Mac	396
MoNA	Linux	397
PathWhiz	Windows	398
MetTailor	Linux	399
MetExtract	Windows	400
ropls	Windows	401
ropls	Mac	402
X13CMS	Windows	403
VSClust	Linux	404
X13CMS	Linux	405
X13CMS	Mac	406
CAMERA	Mac	407
MetScape	Mac	408
rNMR	Windows	409
rNMR	Linux	410
rNMR	Mac	411
BATMAN	Mac	412
MS2Analyzer	Windows	413
MS2Analyzer	Mac	414
PAMBD	Windows	415
Pathos	Mac	416
muma	Mac	417
muma	Linux	418
IPO	Linux	419
SMART	Windows	420
SMART	Mac	421
SMART	Linux	422
COLMAR	Mac	423
PathBank	Mac	424
GAVIN	Linux	425
MS-LAMP	Windows	426
MS-LAMP	Linux	427
IIS	Windows	428
BiNCHE	Linux	429
PiMP	Linux	430
BiNCHE	Windows	431
BiNCHE	Mac	432
Skyline	Windows	433
MetFrag	Linux	434
PepsNMR	Linux	435
mzMatch	Windows	436
MAIT	Mac	437
KMDA	Windows	438
KMDA	Mac	439
KMDA	Linux	440
CluMSID	Windows	441
CluMSID	Mac	442
CluMSID	Linux	443
El-MAVEN	Windows	444
KIMBLE	Windows	445
KIMBLE	Mac	446
KIMBLE	Linux	447
Rhea	Mac	448
InterpretMSSpectrum	Mac	449
Binner	Mac	450
CCPN Metabolomics	Mac	451
MetaboHunter	Linux	452
MetaboSearch	Windows	453
MetaboSearch	Mac	454
eRah	Windows	455
MetaboDiff	Linux	456
BATMAN	Linux	457
SistematX	Windows	458
SistematX	Mac	459
SistematX	Linux	460
RaMP	Windows	461
RaMP	Mac	462
RaMP	Linux	463
PARADISe	Windows	464
MetaboMiner	Mac	465
HiCoNet	Linux	466
LipidXplorer	Linux	467
decoMS2	Windows	468
MWASTools	Mac	469
MWASTools	Linux	470
IPO	Windows	471
VMH	Mac	472
Automics	Windows	473
MetabolAnalyze	Windows	474
MetabolAnalyze	Linux	475
Pachyderm	Windows	476
Pachyderm	Mac	477
Pachyderm	Linux	478
MetaboHunter	Mac	479
PaintOmics	Windows	480
PaintOmics	Linux	481
MetMSLine	Linux	482
ALEX123	Windows	483
IPO	Mac	484
specmine	Windows	485
GabiPD	Windows	486
GabiPD	Mac	487
MetaboAnalystR	Linux	488
MetaboAnalystR	Windows	489
xMSanalyzer	Windows	490
xMSanalyzer	Mac	491
MESSI	Linux	492
QCScreen	Linux	493
PlantMAT	Mac	494
GSim	Windows	495
SysteMHC	Windows	496
PlantMAT	Windows	497
PathBank	Windows	498
Warpgroup	Windows	499
BiGG Models	Windows	500
missForest	Linux	501
UniProtKB	Linux	502
ConsensusPathDB	Linux	503
compMS2Miner	Windows	504
MetaboHunter	Windows	505
VSClust	Mac	506
icoshift	Linux	507
MetNorm	Mac	508
Greazy	Windows	509
cosmiq	Mac	510
NMRPro	Windows	511
NMRPro	Mac	512
NOMAD	Windows	513
NOMAD	Mac	514
NOMAD	Linux	515
MetaboAnalystR	Mac	516
SMPDB	Mac	517
Galaxy-M	Windows	518
Galaxy-M	Linux	519
MetaboLyzer	Windows	520
ChemDistiller	Windows	521
MaConDa	Windows	522
MaConDa	Mac	523
MaConDa	Linux	524
CliqueMS	Mac	525
CliqueMS	Linux	526
CliqueMS	Windows	527
mzOS	Windows	528
LiverWiki	Linux	529
Binner	Windows	530
iontree	Mac	531
iontree	Windows	532
MetaComp	Linux	533
MetaComp	Windows	534
pyQms	Mac	535
pyQms	Linux	536
PhenoMeNal	Mac	537
xMSannotator	Windows	538
xMSannotator	Mac	539
RDFIO	Linux	540
XCMS	Mac	541
credential	Linux	542
PAMBD	Mac	543
VANTED	Linux	544
MeRy-B	Windows	545
credential	Windows	546
LIMSA	Windows	547
InterpretMSSpectrum	Linux	548
SMPDB	Windows	549
PeroxisomeDB	Mac	550
PeroxisomeDB	Linux	551
batchCorr	Windows	552
Escher	Windows	553
ProbMetab	Mac	554
ProbMetab	Linux	555
MS2Analyzer	Linux	556
GNPS	Linux	557
GNPS	Mac	558
GAVIN	Windows	559
xMSanalyzer	Linux	560
SDAMS	Windows	561
SDAMS	Mac	562
SDAMS	Linux	563
MINEs	Windows	564
MINEs	Mac	565
MetImp	Windows	566
MetImp	Mac	567
MMSAT	Windows	568
DeepRiPP	Linux	569
TracMass2	Linux	570
metaMS	Linux	571
Metabolomics-Filtering	Windows	572
ALLocater	Mac	573
AMDIS	Windows	574
MIDAS	Mac	575
RAMClustR	Windows	576
MAIT	Windows	577
libChEBI	Linux	578
cosmiq	Linux	579
Galaxy-M	Mac	580
HMDB	Linux	581
VMH	Windows	582
LIPID MAPS	Windows	583
LIPID MAPS	Linux	584
icoshift	Windows	585
ChromA	Mac	586
PaintOmics	Mac	587
LipidXplorer	Windows	588
RankProd	Linux	589
RankProd	Windows	590
RankProd	Mac	591
MetabolAnalyze	Mac	592
cosmiq	Windows	593
QCScreen	Mac	594
libChEBI	Windows	595
FingerID	Mac	596
FingerID	Linux	597
BiGG Models	Linux	598
KeggArray	Windows	599
Metabolomics-Filtering	Mac	600
ALLocater	Linux	601
MET-XAlign	Windows	602
MINMA	Windows	603
MINMA	Mac	604
KPIC	Windows	605
GNPS	Windows	606
COLMAR	Linux	607
Newton	Linux	608
MetiTree	Windows	609
MetiTree	Mac	610
iontree	Linux	611
PAMBD	Linux	612
nPYc-Toolbox	Mac	613
nPYc-Toolbox	Linux	614
LIMSA	Linux	615
MeltDB	Windows	616
ORCA	Windows	617
BATMAN	Windows	618
MoNA	Windows	619
vaLID	Mac	620
vaLID	Linux	621
GSimp	Linux	622
MetaNetter	Windows	623
MetaNetter	Mac	624
MetaNetter	Linux	625
COVAIN	Mac	626
metabomxtr	Mac	627
COVAIN	Linux	628
Metab	Mac	629
MSClust	Windows	630
matNMR	Windows	631
Bayesil	Windows	632
Bayesil	Mac	633
apLCMS	Mac	634
apLCMS	Linux	635
compMS2Miner	Linux	636
Mummichog	Windows	637
Mummichog	Mac	638
LICRE	Mac	639
LICRE	Linux	640
VANTED	Windows	641
BinBase	Linux	642
SIMAT	Mac	643
SIMAT	Linux	644
jQMM	Mac	645
jQMM	Linux	646
jQMM	Windows	647
mzOS	Mac	648
MIDAS	Windows	649
OPTIMAS-DW	Mac	650
MMSAT	Linux	651
GAM	Windows	652
GAM	Linux	653
Zero-fill	Windows	654
Zero-fill	Mac	655
Zero-fill	Linux	656
HCOR	Mac	657
metabnorm	Windows	658
metabnorm	Mac	659
metabnorm	Linux	660
PathWhiz	Mac	661
MetNorm	Windows	662
mQTL.NMR	Windows	663
mQTL.NMR	Linux	664
mQTL.NMR	Mac	665
HMDB	Windows	666
MetShot	Windows	667
IsoCor	Linux	668
IsoCor	Windows	669
MAGMa	Mac	670
DiffCorr	Windows	671
IntLIM	Mac	672
MZmine 2	Windows	673
MZmine 2	Linux	674
MZmine 2	Mac	675
MetaboliteDetector	Linux	676
Meta P-server	Mac	677
Meta P-server	Linux	678
CROP	Mac	679
CROP	Linux	680
CROP	Windows	681
Qemistree	Mac	682
Qemistree	Windows	683
Qemistree	Linux	684
Notame	Mac	685
Notame	Linux	686
Notame	Windows	687
MetaboShiny	Mac	688
MetaboShiny	Windows	689
MetaboShiny	Linux	690
Viime	Mac	691
Viime	Windows	692
Viime	Linux	693
NOREVA	Mac	694
NOREVA	Windows	695
NOREVA	Linux	696
JS-MS	Mac	697
JS-MS	Windows	698
JS-MS	Linux	699
pyMolNetEnhancer	Mac	700
pyMolNetEnhancer	Windows	701
pyMolNetEnhancer	Linux	702
RMolNetEnhancer	Mac	703
RMolNetEnhancer	Windows	704
RMolNetEnhancer	Linux	705
ZEAMAP	Mac	706
ZEAMAP	Windows	707
ZEAMAP	Linux	708
MassComp	Linux	709
MassComp	Windows	710
iMet	Mac	711
iMet	Windows	712
iMet	Linux	713
MetNormalizer	Mac	714
MetNormalizer	Windows	715
MetNormalizer	Linux	716
CAMERA	Windows	717
FragPred	Linux	718
MINMA	Linux	719
MVAPACK	Linux	720
MVAPACK	Mac	721
flagme	Linux	722
MetImp	Linux	723
MetAlign	Windows	724
MetaboAnalyst	Mac	725
MetaboAnalyst	Linux	726
MetaboAnalyst	Windows	727
InCroMAP	Windows	728
YMDB	Windows	729
YMDB	Mac	730
LDA	Linux	731
DeepRiPP	Mac	732
vaLID	Windows	733
MetaBridge	Windows	734
MetaBridge	Mac	735
MetaBridge	Linux	736
mz.unity	Mac	737
HiCoNet	Mac	738
HiCoNet	Windows	739
SmileMS	Mac	740
InCroMAP	Mac	741
InCroMAP	Linux	742
SIMAT	Windows	743
MetFusion	Mac	744
MetaboQC	Windows	745
MetaboQC	Linux	746
LIQUID	Windows	747
NMRfilter	Mac	748
NMRfilter	Windows	749
NMRfilter	Linux	750
SmileMS	Linux	751
MetScape	Linux	752
MarVis-Suite	Windows	753
MarVis-Suite	Linux	754
CCPN Metabolomics	Windows	755
CCPN Metabolomics	Linux	756
MetMSLine	Windows	757
MS-FINDER	Windows	758
Newton	Windows	759
YMDB	Linux	760
PathWhiz	Linux	761
LICRE	Windows	762
CANOPUS	Linux	763
marr	Mac	764
marr	Windows	765
marr	Linux	766
GISSMO	Windows	767
sampleDrift	Mac	768
sampleDrift	Windows	769
sampleDrift	Linux	770
VOCCluster	Mac	771
VOCCluster	Windows	772
VOCCluster	Linux	773
WiPP	Linux	774
Peakonly	Mac	775
Peakonly	Windows	776
Peakonly	Linux	777
HastaLaVista	Mac	778
HastaLaVista	Windows	779
HastaLaVista	Linux	780
DecoMetDIA	Mac	781
DecoMetDIA	Windows	782
DecoMetDIA	Linux	783
rDolphin	Mac	784
rDolphin	Windows	785
rDolphin	Linux	786
RawTools	Mac	787
RawTools	Windows	788
RawTools	Linux	789
misaR	Mac	790
misaR	Windows	791
misaR	Linux	792
statTarget	Mac	793
statTarget	Windows	794
statTarget	Linux	795
ICT	Mac	796
LDA	Mac	797
GAM	Mac	798
IIS	Mac	799
mzR	Mac	800
HappyTools	Windows	801
HappyTools	Mac	802
HappyTools	Linux	803
mzOS	Linux	804
RMassBank	Windows	805
RMassBank	Mac	806
BinBase	Mac	807
Metabolomics-Filtering	Linux	808
PathBank	Linux	809
GAVIN	Mac	810
DeepRiPP	Windows	811
UC2	Mac	812
tmod	Mac	813
tmod	Windows	814
tmod	Linux	815
PYQUAN	Mac	816
PYQUAN	Windows	817
PYQUAN	Linux	818
MAIMS	Mac	819
MAIMS	Windows	820
MAIMS	Linux	821
POMAShiny	Mac	822
POMAShiny	Windows	823
POMAShiny	Linux	824
MetaboloDerivatizer	Windows	825
AlpsNMR	Mac	826
AlpsNMR	Windows	827
AlpsNMR	Linux	828
ADAP-GC	Mac	829
ADAP-GC	Windows	830
ADAP-GC	Linux	831
MIDcor	Mac	832
MIDcor	Windows	833
MIDcor	Linux	834
MS-FLO	Mac	835
MS-FLO	Windows	836
MS-FLO	Linux	837
MetaboQC	Mac	838
AntDAS	Linux	839
pymass	Windows	840
pymass	Linux	841
pySM	Mac	842
pySM	Linux	843
massPix	Mac	844
massPix	Windows	845
massPix	Linux	846
NMRProcFlow	Windows	847
NMRProcFlow	Mac	848
NMRProcFlow	Linux	849
speaq	Mac	850
speaq	Windows	851
speaq	Linux	852
SigMa	Windows	853
MSHub	Mac	854
MSHub	Windows	855
MSHub	Linux	856
RGCxGC	Mac	857
RGCxGC	Windows	858
RGCxGC	Linux	859
ncGTW	Mac	860
ncGTW	Windows	861
ncGTW	Linux	862
TidyMS	Mac	863
TidyMS	Windows	864
TidyMS	Linux	865
AntDAS	Windows	866
AntDAS	Mac	867
Autotuner	Mac	868
Autotuner	Windows	869
Autotuner	Linux	870
hRUV	Mac	871
hRUV	Linux	872
MetumpX	Windows	873
MetumpX	Linux	874
fobitools	Mac	875
fobitools	Windows	876
fobitools	Linux	877
fobitoolsGUI	Mac	878
fobitoolsGUI	Windows	879
fobitoolsGUI	Linux	880
BioDendro	Mac	881
BioDendro	Windows	882
BioDendro	Linux	883
QSSR Automator	Windows	884
MetIDfyR	Mac	885
MetIDfyR	Windows	886
MetIDfyR	Linux	887
CANOPUS	Mac	888
CANOPUS	Windows	889
mwTab	Mac	890
mwTab	Windows	891
mwTab	Linux	892
MetaboSignal	Mac	893
MetaboSignal	Windows	894
MetaboSignal	Linux	895
Risa	Windows	896
Risa	Linux	897
Risa	Mac	898
SLAW	Windows	899
SLAW	Mac	900
SLAW	Linux	901
MS2Query	Mac	902
MS2Query	Windows	903
MS2Query	Linux	904
\.


--
-- Data for Name: output_variables; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.output_variables (tool_name, output, rowid) FROM stdin;
MetaboAnalyst	Tables	1
MetaboAnalyst	Interactive graphics	2
MetaboAnalyst	Images	3
MetaboAnalyst	Analysis Report	4
ALEX123	adduct	5
ALEX123	lipid category	6
ALEX123	structure information	7
ALEX123	lipid species	8
ALEX123	molecular lipid species	9
ALEX123	target m/z	10
ALEX123	fragment name	11
ALEX123	ms3 activation	12
ALEX123	lipid class	13
ALEX123	ms2 precursor m/z	14
ALEX123	ms2 activation	15
ALEX123	ms3 precursor m/z	16
ALEX123	conflicts	17
ALEX123	charge	18
ALEX123	polarity	19
ALEX123	ms dimension	20
iontree	MS2/MS3 ion trees	21
iontree	iontree database	22
AMDORAP	m/z estimates	23
AMDORAP	ion chromatograms	24
apLCMS	intensities table	25
ChemoSpec	frequency	26
ChemoSpec	intensity	27
ChemoSpec	names	28
ChemoSpec	groups	29
ChemoSpec	colors	30
ChemoSpec	symbols	31
ChemoSpec	alternate symbols	32
MetaboAnalystR	Tables	33
MetaboAnalystR	Interactive graphics	34
MetaboAnalystR	Images	35
MetaboAnalystR	Analysis Report	36
ChemoSpec	units	37
ChemoSpec	description	38
Bayesil	HMDB ID	39
Bayesil	compound name	40
Bayesil	concentration	41
Bayesil	confidence score	42
batchCorr	drift corrected data	43
rNMR	peak list	44
rNMR	ROI table	45
alsace	peak intensities 	46
ALLocater	pseudo spectra	47
CAMERA	annotated peak table	48
BATMAN	couple code	49
BATMAN	J constant	50
BATMAN	relative intensity	51
BATMAN	overwrite pos	52
BATMAN	overwrite truncation	53
BATMAN	include multiplet	54
BATMAN	metabolite	55
BATMAN	pos in ppm	56
\.


--
-- Data for Name: programming_language; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.programming_language (tool_name, language, rowid) FROM stdin;
Metab	R	1
GSim	C++	2
muma	R	3
BinBase	Linux	4
BinBase	Java	5
proFIA	R	6
pyQms	Python	7
SmileMS	Java	8
CFM-ID	Ruby	9
Galaxy-M	R	10
Galaxy-M	Python	11
MarVis-Suite	MATLAB	12
TracMass2	MATLAB	13
MetaNetter	Java	14
eRah	R	15
Curatr	Python	16
UC2	Java	17
PiMP	R	18
El-MAVEN	C	19
MeltDB	R	20
NOMAD	R	21
XCMS online	Python	22
LIPID MAPS	PHP	23
MetAlign	C++	24
IsoCor	Python	25
IIS	JavaScript	26
XCMS	R	27
MAIT	R	28
MultiAlign	C++	29
ORCA	MATLAB	30
DiffCorr	R	31
IsoMS-Quant	R	32
SIMAT	R	33
ICT	Perl	34
MetExtract	C++	35
Escher	Python	36
RaMP	Python	37
MultiAlign	C#	38
apLCMS	R	39
MetiTree	R	40
MetShot	R	41
MetaboliteDetector	C++	42
KMDA	R	43
PhenoMeNal	JavaScript	44
VSClust	R	45
mz.unity	R	46
MSClust	C++	47
MS2Analyzer	Java	48
Newton	Java	49
mixOmics	R	50
IPO	R	51
NP-StructurePredictor	Java	52
MetaComp	C#	53
Warpgroup	R	54
Ideom	R	55
SoyKB	PHP	56
MeRy-B	Java	57
MeRy-B	JavaScript	58
iMet-Q	C#	59
AMDORAP	R	60
Mummichog	Python	61
SpectConnect	C	62
Pathos	Java	63
XCMS online	Java	64
ConsensusPathDB	JavaScript	65
matNMR	MATLAB	66
MetMatch	R	67
SpectraClassifier	Java	68
compMS2Miner	R	69
metabomxtr	R	70
MetaboSearch	Java	71
CCPN Metabolomics	Java	72
MetaboSearch	C	73
MetaboLyzer	Python	74
MetaboLyzer	R	75
cosmiq	R	76
Meta P-server	R	77
Meta P-server	Perl	78
MetaComp	R	79
MAGMa	Python	80
IIS	PHP	81
MZedDB	PHP	82
MZedDB	R	83
RaMP	R	84
NMRPro	Python	85
MetCirc	R	86
MetaboQuant	MATLAB	87
MetaBridge	R	88
MetFrag	Java	89
libChEBI	Java	90
libChEBI	MATLAB	91
eMZed	Python	92
HAMMER	Python	93
MAVEN	C++	94
MetMask	Python	95
MetaboClust	C#	96
mQTL.NMR	R	97
crmn	R	98
rNMR	R	99
MS-LAMP	Perl	100
mzR	R	101
batchCorr	R	102
nPYc-Toolbox	Python	103
PathVisio	Java	104
MetaboSearch	MATLAB	105
mineXpert	JavaScript	106
mineXpert	C++	107
KPIC	R	108
MI-PACK	Python	109
LIMSA	C++	110
MetaboNetworks	MATLAB	111
Automics	Visual C++	112
MetaboAnalyst	R	113
MetaboAnalyst	Java	114
XCMS online	JavaScript	115
XCMS online	BASH	116
XCMS online	Perl	117
XCMS online	PHP	118
XCMS online	R	119
RAMClustR	R	120
mzMatch	R	121
jQMM	Python	122
Skyline	C#	123
PlantMAT	Visual Basic	124
InCroMAP	Java	125
BATMAN	R	126
polyPK	R	127
VANTED	Java	128
MESSI	R	129
MetaboHunter	Perl	130
CMM	Java	131
mzAccess	C#	132
RDFIO	PHP	133
HappyTools	Python	134
RMassBank	R	135
PAPi	R	136
KeggArray	Java	137
E-Dragon	Java	138
WikiPathways	Java	139
LiverWiki	Java	140
X13CMS	R	141
PaintOmics	Perl	142
ALLocater	Java	143
ALLocater	R	144
MeltDB	Perl	145
MeltDB	Java	146
alsace	R	147
SMART	R	148
CROP	R	149
Qemistree	Python	150
Notame	R	151
MetaboShiny	R	152
Viime	Javascript	153
Viime	Python	154
Viime	R	155
NOREVA	R	156
JS-MS	Java	157
JS-MS	Javascript	158
AntDAS	MATLAB	159
pymass	C	160
pymass	C++	161
pymass	Python	162
pySM	Python	163
massPix	R	164
NMRProcFlow	PHP	165
NMRProcFlow	R	166
NMRProcFlow	Javascript	167
NMRProcFlow	C++	168
speaq	R	169
SigMa	MATLAB	170
MSHub	Python	171
RGCxGC	R	172
ncGTW	R	173
ncGTW	C++	174
TidyMS	Python	175
Autotuner	R	176
hRUV	R	177
MetumpX	R	178
fobitools	R	179
fobitoolsGUI	R	180
BioDendro	Python	181
QSSR Automator	Python	182
MetIDfyR	R	183
CANOPUS	Java	184
CANOPUS	Python	185
marr	R	186
marr	C++	187
Plasmodesma	Python	188
GISSMO	MATLAB	189
sampleDrift	R	190
VOCCluster	Python	191
WiPP	Python	192
WiPP	R	193
Peakonly	Python	194
HastaLaVista	Javascript	195
HastaLaVista	R	196
MelonnPan	R	197
PaintOmics	Python	198
MoNA	Scala	199
MoNA	Java	200
Pachyderm	Go	201
MI-PACK	R	202
SpectConnect	Python	203
DeepRiPP	R	204
DeepRiPP	Python	205
Workflow4Metabolomics	Python	206
MetSign	MATLAB	207
GAVIN	MATLAB	208
MetDisease	Java	209
MVAPACK	Octave	210
specmine	R	211
Ionwinze	C++	212
CluMSID	R	213
Lilikoi	R	214
FingerID	MATLAB	215
QCScreen	R	216
TargetSearch	C	217
MetaBox	R	218
icoshift	MATLAB	219
MetaMapR	R	220
MetaboHunter	JavaScript	221
VANTED	Ruby	222
MetaboMiner	Java	223
credential	R	224
CAMERA	R	225
SAMMI	JavaScript	226
METLIN	PHP	227
METLIN	JavaScript	228
Rdisop	R	229
MMSAT	C++	230
MMSAT	Python	231
MetNorm	R	232
mzOS	Python	233
nmrglue	Python	234
MetiTree	Java	235
MAIMS	Python	236
POMAShiny	R	237
MetaboloDerivatizer	Java	238
AlpsNMR	R	239
ADAP-GC	R	240
MIDcor	R	241
MS-FLO	Python	242
MS-FLO	Javascript	243
MetaboQC	R	244
LIQUID	C#	245
TOXcms	R	246
MetFusion	Java	247
MeltDB	JavaScript	248
xMSanalyzer	R	249
MIDAS	Python	250
ALEX123	Java	251
ALEX123	Python	252
MetaboAnalystR	R	253
BiNCHE	JavaScript	254
DrugBank	JavaScript	255
BioNetApp	Java	256
LipidXplorer	Python	257
ALEX123	C++	258
ALEX123	Lisp	259
WikiPathways	JavaScript	260
MET-COFEI	C++	261
IsoMS	R	262
GSimp	R	263
KIMBLE	R	264
KIMBLE	Python	265
Galaxy-M	MATLAB	266
SistematX	JavaScript	267
KIMBLE	JavaScript	268
RankProd	R	269
MetaDB	Grails	270
MetaQuant	Java	271
FragPred	R	272
MetabNet	R	273
PathWhiz	JavaScript	274
InterpretMSSpectrum	R	275
ChemSpider	JavaScript	276
HiCoNet	Python	277
ChemDistiller	Python	278
MyCompoundID	JavaScript	279
MetaboGroup S	R	280
MoDentify	R	281
MetaboDiff	R	282
SDAMS	R	283
Zero-fill	R	284
Escher	JavaScript	285
libChEBI	Python	286
MetFrag	JavaScript	287
Maui-VIA	Java	288
MET-COFEA	C++	289
MINMA	R	290
msPurity	R	291
ADAP/MZmine	Java	292
ProbMetab	R	293
QCScreen	Python	294
ropls	R	295
MetaboHunter	PHP	296
BiGG Models	Python	297
BiGG Models	JavaScript	298
BiNCHE	Java	299
FingerID	Python	300
MoNA	JavaScript	301
LDA	Java	302
Pathview	R	303
Pathview	PHP	304
MetaMapR	JavaScript	305
ChromA	JavaScript	306
NMRfilter	Python	307
NMRfilter	Java	308
MET-XAlign	C++	309
MZmine 2	Java	310
MZmine 2	R	311
ChromA	Java	312
CGBayesNets	MATLAB	313
Metabolomics-Filtering	R	314
SistematX	Java	315
flagme	R	316
LICRE	MATLAB	317
PARADISe	C++	318
metaMS	R	319
MRMPROBS	C#	320
MetImp	R	321
ChemoSpec	R	322
TargetSearch	R	323
metabnorm	R	324
PAMBD	C++	325
PiMP	Python	326
MassTRIX	Perl	327
CliqueMS	R	328
MZedDB	Perl	329
xMSannotator	R	330
MetCCS Predictor	R	331
MetCCS Predictor	Python	332
QPMASS	C++	333
FALCON	MATLAB	334
FALCON	C	335
TNO-DECO	MATLAB	336
vaLID	Java	337
MetFamily	R	338
BRAIN	R	339
PathoLogic	Lisp	340
PathoLogic	Java	341
PathoLogic	Perl	342
GAM	R	343
MESSI	PHP	344
PepsNMR	R	345
mzMatch	Java	346
MetScape	Java	347
El-MAVEN	C++	348
MS-FINDER	C++	349
mQTL	R	350
MetMSLine	R	351
COVAIN	MATLAB	352
COVAIN	C	353
iPath	JavaScript	354
WikiPathways	PHP	355
pyMolNetEnhancer	Python	356
RMolNetEnhancer	R	357
RMolNetEnhancer	Python	358
ZEAMAP	Javascript	359
ZEAMAP	PHP	360
MassComp	C++	361
iMet	R	362
MetNormalizer	R	363
tmod	R	364
PYQUAN	Python	365
R2DGC	R	366
swathTUNER	R	367
missForest	R	368
MetiTree	JavaScript	369
intCor	R	370
MetaboLab	MATLAB	371
MetabolAnalyze	R	372
MathDAMP	Mathematica	373
FALCON	Python	374
MetMatch	Java	375
iontree	R	376
MSPrep	R	377
Binner	Java	378
MetaboClust	R	379
DecoMetDIA	R	380
DecoMetDIA	C++	381
rDolphin	R	382
RawTools	C#	383
misaR	R	384
misaR	C++	385
statTarget	R	386
mwTab	Python	387
MetaboSignal	R	388
Risa	R	389
SLAW	R	390
SLAW	Python	391
SLAW	C++	392
MS2Query	Python	393
Normalyzer	R	394
HCOR	R	395
BatMass	Java	396
TriDAMP	Mathematica	397
MyCompoundID	Java	398
decoMS2	R	399
MetTailor	R	400
IntLIM	R	401
MWASTools	R	402
\.


--
-- Data for Name: publications; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.publications (rowid, tool_name, pub_doi) FROM stdin;
2710	NMRfilter	https://doi.org/10.1039/C8FD00227D
2711	NMRfilter	https://doi.org/10.1007/s11306-020-01748-1
2712	alsace	
2717	mzOS	
2811	flagme	
2713	MSClust	https://dx.doi.org/10.1007%2Fs11306-011-0368-2
2714	MS-FINDER	https://doi.org/10.1021/acs.analchem.6b00770
2715	muma	https://doi.org/10.2174/2213235X11301020005
2716	MyCompoundID	https://doi.org/10.1021/ac400099b
2718	mzR	https://doi.org/10.1038/nbt1031
2719	Newton	
2720	nmrglue	https://doi.org/10.1007/s10858-013-9718-x
2721	QCScreen	https://doi.org/10.1186/s12859-015-0783-x
2722	Rdisop	
2723	Escher	https://doi.org/10.1371/journal.pcbi.1004321
2724	RMassBank	https://doi.org/10.1002/jms.3131
2725	VMH	https://doi.org/10.1093/nar/gky992
2901	mQTL	
2726	ropls	https://doi.org/10.1021/acs.jproteome.5b00354
2727	SIMAT	https://doi.org/10.1186/s12859-015-0681-2
2728	QPMASS	https://doi.org/10.1016/j.chroma.2020.460999
2729	SpectConnect	https://doi.org/10.1021/ac0614846
2730	TargetSearch	https://doi.org/10.1186/1471-2105-10-428
2731	vaLID	https://doi.org/10.1093/bioinformatics/bts662
2732	Workflow4Metabolomics	https://doi.org/10.1093/bioinformatics/btu813
2733	xMSanalyzer	https://doi.org/10.1186/1471-2105-14-15
2734	MetaBox	https://doi.org/10.1186/s12859-014-0374-2
2735	MetaMapR	https://doi.org/10.1093/bioinformatics/btv194
2736	MetSign	https://doi.org/10.1021/ac3016856
2737	MIDAS	https://doi.org/10.1021/ac5014783
2738	ChromA	https://doi.org/10.1093/bioinformatics/btp343
2739	GAVIN	https://doi.org/10.1016/j.ab.2011.04.009
2740	HAMMER	https://doi.org/10.1093/bioinformatics/btt711
2741	HiCoNet	
2742	icoshift	https://doi.org/10.1016/j.chroma.2011.08.086
2743	InCroMAP	https://doi.org/10.1016/j.jchromb.2014.04.030
2744	IIS	https://doi.org/10.1371/journal.pone.0100385
2745	LICRE	https://doi.org/10.1093/bioinformatics/btu381
2746	MathDAMP	https://doi.org/10.1186/1471-2105-7-530
2747	matNMR	
2748	MetabNet	https://doi.org/10.3389/fbioe.2015.00087
2749	MetaboQuant	https://doi.org/10.2144/000114026
2750	PAPi	https://doi.org/10.1093/bioinformatics/btq567
2751	TNO-DECO	https://doi.org/10.1016/j.chemolab.2010.07.007
2752	BiNCHE	https://doi.org/10.1186/s12859-015-0486-3
2753	swathTUNER	https://doi.org/10.1021/acs.jproteome.5b00543
2754	IsoMS	https://doi.org/10.1021/acs.analchem.5b01434
2755	polyPK	https://doi.org/10.1093/bioinformatics/btx834
2756	nPYc-Toolbox	https://doi.org/10.1093/bioinformatics/btz566
2757	MetaboNetworks	https://doi.org/10.1093/bioinformatics/btt612
2758	MultiAlign	https://doi.org/10.1186/1471-2105-14-49
2759	OPTIMAS-DW	https://doi.org/10.1186/1471-2229-12-245
2760	DiffCorr	https://doi.org/10.1016/j.gene.2012.11.028
2761	MassTRIX	https://doi.org/10.1093/nar/gkn194
2762	MeRy-B	https://doi.org/10.1007/978-1-62703-661-0_1
2763	DrugBank	https://doi.org/10.1093/nar/gkx1037
2764	CliqueMS	https://doi.org/10.1093/bioinformatics/btz207
2765	SMPDB	https://doi.org/10.1093/nar/gkp1002
2766	KeggArray	https://doi.org/10.1039/b902356a
2767	PepsNMR	
2768	MetaboDiff	https://doi.org/10.1093/bioinformatics/bty344
2769	Pachyderm	https://dx.doi.org/10.1093%2Fbioinformatics%2Fbty699
2770	MarVis-Suite	https://doi.org/10.1186/1471-2105-10-92
2771	MetaboAnalystR	https://doi.org/10.1093/bioinformatics/bty528
2772	MetaboAnalyst	https://doi.org/10.1093/nar/gkp356
2773	Automics	https://doi.org/10.1186/1471-2105-10-83
2774	BATMAN	https://doi.org/10.1093/bioinformatics/bts308
2775	CAMERA	https://doi.org/10.1021/ac202450g
2776	HMDB	https://doi.org/10.1093/nar/gkl923
2777	SAMMI	https://doi.org/10.1093/bioinformatics/btz927
2778	credential	https://doi.org/10.1021/ac503092d
2779	decoMS2	https://doi.org/10.1021/ac400751j
2780	FragPred	http://dx.doi.org/10.1093/bioinformatics/btv085
2781	MetaboMiner	https://doi.org/10.1186/1471-2105-9-507
2782	XCMS	https://doi.org/10.1021/ac051437y
2783	batchCorr	https://dx.doi.org/10.1007%2Fs11306-016-1124-4
2784	ADAP/MZmine	
2785	cosmiq	
2786	crmn	https://doi.org/10.1021/ac901143w
2787	CSI:FingerrID	https://doi.org/10.1073/pnas.1509788112
2788	eMZed	https://doi.org/10.1093/bioinformatics/btt080
2789	eRah	https://doi.org/10.1021/acs.analchem.6b02927
2790	FingerID	https://doi.org/10.1093/bioinformatics/bts437
2791	MetaBridge	https://doi.org/10.1002/cpbi.98
2792	VSClust	https://doi.org/10.1093/bioinformatics/bty224
2793	MINEs	https://doi.org/10.1186/s13321-015-0087-1
2794	CMM	https://doi.org/10.1016/j.jpba.2018.02.046
2795	IntLIM	https://doi.org/10.1186/s12859-018-2085-6
2796	GSimp	https://doi.org/10.1371/journal.pcbi.1005973
2797	SistematX	https://doi.org/10.3390/molecules23010103
2798	MINMA	https://doi.org/10.1093/bioinformatics/btx816
2799	Curatr	https://doi.org/10.1093/bioinformatics/btx786
2800	BinBase	https://doi.org/10.1186/1471-2105-12-321
2801	WikiPathways	https://doi.org/10.3389/fgene.2018.00661
2802	NP-StructurePredictor	https://doi.org/10.1021/acs.jcim.7b00565
2803	PAMBD	https://doi.org/10.1093/nar/gkx1061
2804	mixOmics	https://doi.org/10.1371/journal.pcbi.1005752
2805	UC2	https://dx.doi.org/10.1093%2Fbioinformatics%2Fbtx649
2806	proFIA	https://doi.org/10.1093/bioinformatics/btx458
2807	MetaNetter	https://doi.org/10.1093/bioinformatics/btm536
2808	LiverWiki	https://doi.org/10.1186/s12859-017-1852-0
2809	SysteMHC	https://doi.org/10.1093/nar/gkx664
2810	MetaComp	https://doi.org/10.1186/s12859-017-1849-8
2812	MetScape	https://doi.org/10.1093/bioinformatics/btr661
2813	Binner	https://doi.org/10.1093/bioinformatics/btz798
2814	RAMClustR	https://doi.org/10.1021/ac501530d
2815	MSPrep	https://doi.org/10.1093/bioinformatics/btt589
2816	rNMR	https://doi.org/10.1002/mrc.2526
2817	ALLocater	https://doi.org/10.1371/journal.pone.0113909
2818	AMDIS	https://doi.org/10.1006/cbmr.1993.1036
2819	Metabolomics-Filtering	https://doi.org/10.1186/s12859-019-2871-9
2820	missForest	https://doi.org/10.1093/bioinformatics/btr597
2821	MetImp	https://doi.org/10.1038/s41598-017-19120-0
2822	ChemDistiller	https://doi.org/10.1093/bioinformatics/bty080
2823	Pathos	https://doi.org/10.1002/rcm.5245
2824	PaintOmics	https://doi.org/10.1093/bioinformatics/btq594
2825	VANTED	https://doi.org/10.1584/jpestics.31.289
2826	Ideom	https://doi.org/10.1093/bioinformatics/bts069
2827	mz.unity	http://dx.doi.org/10.1021/acs.analchem.6b01702
2828	Warpgroup	http://dx.doi.org/10.1093/bioinformatics/btv564
2829	KMDA	https://doi.org/10.1186/s12859-015-0506-3
2830	CFM-ID	https://doi.org/10.1093/nar/gku436
2831	MET-COFEI	
2832	TriDAMP	https://doi.org/10.1186/1471-2105-8-72
2833	Meta P-server	https://dx.doi.org/10.1155%2F2011%2F839862
2834	MZmine 2	https://doi.org/10.1186/1471-2105-11-395
2835	Bayesil	https://doi.org/10.1371/journal.pone.0124219
2836	MAIT	https://doi.org/10.1093/bioinformatics/btu136
2837	TOXcms	https://doi.org/10.1021/acs.analchem.9b03811
2838	MetDisease	https://doi.org/10.1093/bioinformatics/btu179
2839	ChemoSpec	
2840	Rhea	https://doi.org/10.1093/nar/gky876
2841	UniProtKB	https://doi.org/10.1093/bioinformatics/btz817
2842	MetaboLights	https://doi.org/10.1093/nar/gkz1019
2843	MetaboGroup S	https://doi.org/10.1021/acs.analchem.8b03065
2844	E-Dragon	https://doi.org/10.1007/s10822-005-8694-y
2845	PhenoMeNal	https://dx.doi.org/10.1093%2Fgigascience%2Fgiy149
2846	MoDentify	https://dx.doi.org/10.1093%2Fbioinformatics%2Fbty650
2847	HappyTools	https://doi.org/10.1371/journal.pone.0200280
2848	PiMP	https://doi.org/10.1093/bioinformatics/btx499
2849	MWASTools	https://doi.org/10.1093/bioinformatics/btx477
2850	RDFIO	https://doi.org/10.1186/s13326-017-0136-y
2851	pyQms	https://doi.org/10.1074/mcp.m117.068007
2852	KPIC	https://doi.org/10.1021/acs.analchem.7b01547
2853	mzAccess	https://doi.org/10.1021/acs.analchem.7b00890
2854	MoNA	
2855	SmileMS	https://doi.org/10.1021/ac900954d
2856	SDAMS	https://doi.org/10.1186/s12859-019-3067-z
2857	MetCirc	https://doi.org/10.1093/bioinformatics/btx159
2858	PathBank	https://doi.org/10.1093/nar/gkz861
2859	IsoMS-Quant	https://doi.org/10.1021/acs.analchem.5b01434
2860	MetCCS Predictor	https://doi.org/10.1093/bioinformatics/btx140
2861	compMS2Miner	https://doi.org/10.1021/acs.analchem.6b02394
2862	PathWhiz	https://doi.org/10.3791/54869
2863	SpectraClassifier	https://doi.org/10.1186/s12868-016-0328-x
2864	SoyKB	https://doi.org/10.1093/nar/gkt905
2865	xMSannotator	https://doi.org/10.1021/acs.analchem.6b01214
2866	ChemSpider	https://doi.org/10.1021/ed100697w
2867	LIPID MAPS	https://doi.org/10.1007/978-94-007-7864-1_11-1
2868	PlantMAT	https://doi.org/10.1021/acs.analchem.6b00906
2869	YMDB	https://doi.org/10.1093/nar/gkw1058
2870	MetMatch	https://doi.org/10.3390/metabo6040039
2871	PathoLogic	https://doi.org/10.4056/sigs.1794338
2872	ConsensusPathDB	https://doi.org/10.1093/nar/gks1055
2873	InterpretMSSpectrum	https://doi.org/10.1021/acs.analchem.6b02743
2874	BatMass	https://doi.org/10.1021/acs.jproteome.6b00021
2875	SMART	https://doi.org/10.1021/acs.analchem.6b00603
2876	Greazy	https://dx.doi.org/10.1021%2Facs.analchem.6b00021
2877	NMRPro	https://doi.org/10.1093/bioinformatics/btw102
2878	PARADISe	https://doi.org/10.1016/j.chroma.2017.04.052
2879	GAM	https://doi.org/10.1093/nar/gkw266
2880	Skyline	https://dx.doi.org/10.1021%2Facs.jproteome.9b00640
2881	ORCA	https://doi.org/10.1093/bioinformatics/btt723
2882	libChEBI	https://doi.org/10.1093/nar/gkv1031
2883	El-MAVEN	https://doi.org/10.1007/978-1-4939-9236-2_19
2884	MaConDa	https://doi.org/10.1093/bioinformatics/bts527
2885	GNPS-MassIVE	https://doi.org/10.1038/nbt.3597
2886	BiGG Models	https://doi.org/10.1093/nar/gkv1049
2887	mineXpert	https://doi.org/10.1021/acs.jproteome.9b00099
2888	MetMask	https://doi.org/10.1186/1471-2105-11-214
2889	PeroxisomeDB	https://doi.org/10.1093/nar/gkp935
2890	GabiPD	https://doi.org/10.1093/nar/gkn611
2891	BioNetApp	https://doi.org/10.1371/journal.pone.0211277
2892	RaMP	https://doi.org/10.3390/metabo8010016
2893	CROP	https://doi.org/10.1093/bioinformatics/btaa012
2894	Qemistree	https://doi.org/10.1038/s41589-020-00677-3
2895	Notame	https://doi.org/10.3390/metabo10040135
2896	MetaboShiny	https://doi.org/10.1007/s11306-020-01717-8
2897	Viime	https://dx.doi.org/10.21105%2Fjoss.02410
2898	NOREVA	https://doi.org/10.1093/nar/gkaa258
2899	JS-MS	https://doi.org/10.1186/s12859-017-1883-6
2900	JS-MS	https://dx.doi.org/10.1186%2Fs12859-020-03752-7
2902	Galaxy-M	https://doi.org/10.1186/s13742-016-0115-8
2903	pyMolNetEnhancer	https://doi.org/10.3390/metabo9070144
2904	RMolNetEnhancer	https://doi.org/10.3390/metabo9070144
2905	ZEAMAP	https://doi.org/10.1016/j.isci.2020.101241
2906	MassComp	https://doi.org/10.1186/s12859-019-2962-7
2907	iMet	https://doi.org/10.1021/acs.analchem.6b04512
2908	MetNormalizer	https://doi.org/10.1007/s11306-016-1026-5
2909	tmod	https://doi.org/10.7287/peerj.preprints.2420v1
2910	PYQUAN	https://doi.org/10.1016/j.jaap.2016.01.006
2911	MAIMS	https://doi.org/10.1007/s11306-017-1250-7
2912	POMAShiny	https://doi.org/10.1371/journal.pcbi.1009148
2913	MetaboloDerivatizer	https://doi.org/10.1021/acs.analchem.7b01010
2914	R2DGC	https://doi.org/10.1093/bioinformatics/btx825
2915	AlpsNMR	https://doi.org/10.1093/bioinformatics/btaa022
2916	ADAP-GC	https://doi.org/10.1021/acs.analchem.6b02222
2917	ADAP-GC	https://doi.org/10.1021/ac300898h
2918	RGCxGC	https://doi.org/10.1016/j.microc.2020.104830
2919	ncGTW	https://dx.doi.org/10.1093%2Fbioinformatics%2Fbtaa037
2920	TidyMS	https://dx.doi.org/10.3390%2Fmetabo10100416
2921	Autotuner	https://doi.org/10.1021/acs.analchem.9b04804
2922	hRUV	https://doi.org/10.1038/s41467-021-25210-5
2923	MetumpX	https://doi.org/10.1093/bioinformatics/btz765
2924	fobitools	https://doi.org/10.1093/databa/baaa033
2925	fobitoolsGUI	https://doi.org/10.1093/databa/baaa033
2926	BioDendro	https://doi.org/10.1038/s41598-020-63036-1
2927	QSSR Automator	https://doi.org/10.3390/metabo10060237
2928	MetIDfyR	https://doi.org/10.1021/acs.analchem.0c02281
2929	CANOPUS	ttps://doi.org/10.1038/s41587-020-0740-8
2930	marr	https://dx.doi.org/10.1186%2Fs12859-021-04336-9
2931	Plasmodesma	https://doi.org/10.1002/mrc.4683
2932	Plasmodesma	https://doi.org/10.1039/C8FD00242H
2933	GISSMO	https://doi.org/10.1021/acs.analchem.8b02660
2934	GISSMO	https://doi.org/10.1021/acs.analchem.7b02884
2935	sampleDrift	https://dx.doi.org/10.1093%2Fbioinformatics%2Fbtx442
2936	VOCCluster	https://doi.org/10.1021/acs.analchem.9b03084
2937	WiPP	https://doi.org/10.3390/metabo9090171
2938	Peakonly	https://doi.org/10.1021/acs.analchem.9b04811
2939	HastaLaVista	https://doi.org/10.1186/s13321-019-0399-7
2940	MelonnPan	https://doi.org/10.1038/s41467-019-10927-1
2941	DecoMetDIA	https://doi.org/10.1021/acs.analchem.9b02655
2942	CluMSID	https://doi.org/10.1093/bioinformatics/btz005
2943	Lilikoi	https://doi.org/10.1093/gigascience/giy136
2944	MetaboClust	https://doi.org/10.1371/journal.pone.0205968
2945	METLIN	https://doi.org/10.1097/01.ftd.0000179845.53213.39
2946	apLCMS	https://doi.org/10.1093/bioinformatics/btp291
2947	TracMass2	https://doi.org/10.1021/ac403905h
2948	X13CMS	https://doi.org/10.1021/ac403384n
2949	ProbMetab	https://doi.org/10.1093/bioinformatics/btu019
2950	Mummichog	https://doi.org/10.1371/journal.pcbi.1003123
2951	DeepRiPP	https://doi.org/10.1073/pnas.1901493116
2952	AMDORAP	https://doi.org/10.1186/1471-2105-12-259
2953	iMet-Q	https://doi.org/10.1371/journal.pone.0146112
2954	intCor	https://doi.org/10.1093/bioinformatics/btu423
2955	Ionwinze	https://doi.org/10.1021/ac401545b
2956	specmine	https://doi.org/10.1016/j.cmpb.2016.01.008
2957	msPurity	https://doi.org/10.1021/acs.analchem.6b04358
2958	Normalyzer	https://doi.org/10.1021/pr401264n
2959	MVAPACK	https://doi.org/10.1021/cb4008937
2960	mQTL.NMR	https://doi.org/10.1021/acs.analchem.5b00145
2961	MetFlow	https://doi.org/10.1093/bioinformatics/bty1066
2962	Zero-fill	https://doi.org/10.1021/acs.analchem.5b01434
2963	XCMS online	https://doi.org/10.1021/ac300698c
2964	BRAIN	https://doi.org/10.1021/ac303439m
2965	MetFamily	https://doi.org/10.1021/acs.analchem.6b01569
2966	COVAIN	https://doi.org/10.1093/bioinformatics/btt612
2967	MetMSLine	https://doi.org/10.1093/bioinformatics/btu705
2968	CCPN Metabolomics	https://doi.org/10.1186/1471-2105-16-s5-s11
2969	Lipid-Pro	https://doi.org/10.1093/bioinformatics/btu796
2970	iPath	https://dx.doi.org/10.1093%2Fnar%2Fgky299
2971	metabnorm	https://doi.org/10.1093/bioinformatics/btu175
2972	COLMAR	https://doi.org/10.1002/mrc.2486
2973	iontree	https://doi.org/10.3390/metabo3041036
2974	ALEX123	https://doi.org/10.1371/journal.pone.0079736
2975	Pathview	https://doi.org/10.1093/nar/gkx372
2976	LDA	https://doi.org/10.1093/bioinformatics/btq699
2977	KIMBLE	https://doi.org/10.1016/j.aca.2018.07.070
2978	BioCyc	https://doi.org/10.3390/metabo5020291
2979	jQMM	https://doi.org/10.1186/s12859-017-1615-y
2980	RankProd	https://doi.org/10.1093/bioinformatics/btx292
2981	GNPS	https://dx.doi.org/10.1038%2Fnbt.3597
2982	MetaboLab	https://doi.org/10.1186/1471-2105-12-366
2983	NOMAD	https://doi.org/10.1016/j.jbior.2017.11.005
2984	GSim	
2985	HCOR	
2986	ICT	https://doi.org/10.1093/bioinformatics/btv514
2987	FALCON	https://doi.org/10.1016/j.compbiolchem.2015.08.002
2988	IsoCor	https://doi.org/10.1093/bioinformatics/bts127
2989	IPO	https://doi.org/10.1186/s12859-015-0562-8
2990	MESSI	https://doi.org/10.1093/database/bav076
2991	LIMSA	https://doi.org/10.1021/ac061390w
2992	LipidXplorer	https://doi.org/10.1371/journal.pone.0029851
2993	MAGMa	https://doi.org/10.1021/ac400861a
2994	Maui-VIA	https://doi.org/10.3389/fbioe.2014.00084
2995	MAVEN	https://doi.org/10.1021/ac1021166
2996	MeltDB	https://doi.org/10.1093/bioinformatics/btt414
2997	MET-COFEA	https://doi.org/10.1021/ac501162k
2998	PathVisio	https://dx.doi.org/10.1371%2Fjournal.pcbi.1004085
2999	MET-IDEA	https://doi.org/10.1021/ac0521596
3000	MET-XAlign	https://doi.org/10.1021/acs.analchem.5b01324
3001	Metab	https://doi.org/10.1093/bioinformatics/btr379
3002	MetaboHunter	https://doi.org/10.1186/1471-2105-12-400
3003	MetabolAnalyze	https://doi.org/10.1186/1471-2105-11-571
3004	MetaboliteDetector	https://doi.org/10.1021/ac802689c
3005	MetaboLyzer	https://dx.doi.org/10.1021%2Fac402477z
3006	metabomxtr	https://doi.org/10.1093/bioinformatics/btu509
3007	CGBayesNets	https://doi.org/10.1371/journal.pcbi.1003676
3008	MetaboSearch	https://doi.org/10.1371/journal.pone.0040096
3009	MetaDB	https://doi.org/10.3389/fbioe.2014.00072
3010	MetAlign	https://doi.org/10.1007/s11306-011-0369-1
3011	metaMS	https://doi.org/10.1016/j.jchromb.2014.02.051
3012	MetaQuant	https://doi.org/10.1093/bioinformatics/btl526
3013	MetExtract	https://doi.org/10.1093/bioinformatics/bts012
3014	MetFrag	https://dx.doi.org/10.1186%2Fs13321-016-0115-9
3015	MetFusion	https://doi.org/10.1002/jms.3123
3016	MetiTree	https://doi.org/10.1093/bioinformatics/bts486
3017	MetNorm	https://doi.org/10.1021/ac502439y
3018	MetShot	https://doi.org/10.1007/s11306-012-0401-0
3019	MetTailor	https://doi.org/10.1093/bioinformatics/btv434
3020	MI-PACK	https://doi.org/10.1016/j.chemolab.2010.04.010
3021	MMSAT	https://doi.org/10.1021/ac2026578
3022	MRMPROBS	https://doi.org/10.1021/ac400515s
3023	MZedDB	https://doi.org/10.1186/1471-2105-10-227
3024	mzMatch	https://doi.org/10.1021/ac2000994
3025	MS-LAMP	https://doi.org/10.1002/jms.3163
3026	MS2Analyzer	https://doi.org/10.1021/ac502818e
3027	MetExplore	https://doi.org/10.1093/nar/gky301
3028	ADAP-GC	https://doi.org/10.1021/pr1007703
3029	MIDcor	https://doi.org/10.1186/s12859-017-1513-3
3030	MS-FLO	https://doi.org/10.1021/acs.analchem.6b04372
3031	MetaboQC	https://doi.org/10.1016/j.talanta.2017.05.076
3032	LIQUID	https://dx.doi.org/10.1093%2Fbioinformatics%2Fbtx046
3033	AntDAS	https://doi.org/10.1021/acs.analchem.7b03160
3034	pymass	https://doi.org/10.1016/j.chemolab.2017.10.001
3035	pySM	https://doi.org/10.1038/nmeth.4072
3036	massPix	https://doi.org/10.1007/s11306-017-1252-5
3037	NMRProcFlow	https://doi.org/10.1007/s11306-017-1178-y
3038	speaq	https://doi.org/10.1186/1471-2105-12-405
3039	speaq	https://doi.org/10.1371/journal.pcbi.1006018
3040	SigMa	https://doi.org/10.1016/j.aca.2020.02.025
3041	MSHub	https://dx.doi.org/10.1038%2Fs41587-020-0700-3
3042	rDolphin	https://doi.org/10.1007/s11306-018-1319-y
3043	RawTools	https://doi.org/10.1021/acs.jproteome.8b00721
3044	misaR	https://doi.org/10.1021/acs.analchem.8b03126
3045	statTarget	https://doi.org/10.1016/j.aca.2018.08.002
3046	mwTab	https://doi.org/10.3390/metabo11030163
3047	mwTab	https://doi.org/10.1007/s11306-018-1356-6
3048	MetaboSignal	https://doi.org/10.1002/cpbi.41
3049	Risa	https://doi.org/10.1186%2F1471-2105-15-S1-S11
3050	SLAW	https://doi.org/10.1021/acs.analchem.1c02687
3051	MS2Query	https://doi.org/10.1101/2022.07.22.501125
\.


--
-- Data for Name: software_license; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.software_license (tool_name, license, rowid) FROM stdin;
ConsensusPathDB	Academic Only	1
IsoMS	Academic only	2
SAMMI	GPL-3	3
CROP	GPL-3	4
Qemistree	BSD	5
Notame	MIT License	6
MetaboShiny	Apache 2.0	7
Viime	Apache 2.0	8
JS-MS	MIT License	9
pyMolNetEnhancer	MIT License	10
RMolNetEnhancer	MIT License	11
MetNormalizer	GPL-3	12
tmod	GPL >= 2	13
PYQUAN	GPL-2	14
MAIMS	BSD	15
POMAShiny	GPL-3	16
AlpsNMR	MIT License	17
MS-FLO	LGPL-3	18
MetaboQC	GPL-2	19
LIQUID	Apache 2.0	20
pySM	Apache 2.0	21
massPix	GPL-3	22
speaq	Apache 2.0	23
MSHub	Apache 2.0	24
RGCxGC	MIT License	25
ncGTW	GPL-2	26
TidyMS	BSD	27
Autotuner	MIT License	28
hRUV	GPL-3	29
MetumpX	GPL-3	30
fobitools	GPL-3	31
fobitoolsGUI	GPL-3	32
BioDendro	Apache 2.0	33
MetIDfyR	GPL-3	34
CANOPUS	GPL-3	35
marr	GPL >= 3	36
mzMatch	LGPL-2	37
msPurity	GPL >= 2	38
mixOmics	GPL >= 2	39
MetabolAnalyze	GPL-2	40
metabomxtr	GPL-2	41
nmrglue	BSD	42
polyPK	GPL-2	43
PathVisio	Apache 2.0	44
NOMAD	GPL >= 2	45
MZmine 2	GPL-2	46
Meta P-server	Academic only	47
TriDAMP	Academic only	48
KMDA	GPL-2	49
KMDA	GPL-3	50
Zero-fill	Academic only	51
MetFrag	GPL >= 2	52
MetaboDiff	MIT License	53
eMZed	GPL-2	54
Galaxy-M	GPL-3	55
BiGG Models	Academic only	56
CliqueMS	GPL >= 2	57
RaMP	GPL-2	58
intCor	GPL-2	59
PaintOmics	GPL >= 3	60
Automics	GPL-2	61
PiMP	GPL	62
VSClust	GPL >= 2	63
MAIT	GPL-2	64
mQTL.NMR	Artistic 2.0	65
Normalyzer	Creative Commons	66
Normalyzer	GPL	67
specmine	GPL-2	68
specmine	GPL-3	69
Ionwinze	GPL-3	70
MoNA	GPL-3	71
CAMERA	GPL >= 2	72
Lilikoi	GPL-3	73
jQMM	Unique	74
RankProd	Unique	75
Lipid-Pro	Academic only	76
MetMSLine	GPL-3	77
eRah	GPL-3	78
flagme	GPL >= 2	79
MetaboClust	GPL-3	80
GNPS	Unique	81
BioCyc	Unique	82
LDA	Academic Only	83
ICT	Artistic 2.0	84
Warpgroup	GPL-3	85
mzR	Artistic 2.0	86
Rdisop	GPL-2	87
RMassBank	Artistic 2.0	88
proFIA	CeCILL	89
BATMAN	GPL-2	90
KeggArray	Academic only	91
ORCA	Academic only	92
MultiAlign	Apache 2.0	93
TracMass2	GPL >= 3	94
mQTL	GPL-2	95
mQTL	GPL-3	96
MetaDB	CPAL License	97
MetaDB	MIT License	98
GSimp	Creative Commons	99
MS-FINDER	LGPL-3	100
apLCMS	GPL >= 2	101
KIMBLE	BSD	102
xMSannotator	GPL-2	103
ProbMetab	GPL	104
cosmiq	GPL-3	105
MetaboLights	Apache 2.0	106
LIMSA	GPL	107
MetaboAnalystR	GPL-3	108
SIMAT	GPL-2	109
MetaboNetworks	Unique	110
DrugBank	Creative Commons	111
MetMask	GPL-3	112
CluMSID	MIT License	113
HCOR	GPL >= 3	114
TNO-DECO	GPL-2	115
BioNetApp	GPL	116
mineXpert	GPL-3	117
MVAPACK	GPL-3	118
MRMPROBS	LGPL-3	119
KPIC	GPL >= 2	120
matNMR	GPL-2	121
ChemSpider	Creative Commons	122
compMS2Miner	GPL >= 3	123
MetExplore	MIT License	124
LipidXplorer	GPL-2	125
CGBayesNets	MIT License	126
ChemDistiller	BSD	127
metabnorm	GPL-2	128
mzOS	MIT License	129
IntLIM	GPL-2	130
QCScreen	GPL-2	131
Curatr	Apache 2.0	132
MetiTree	GPL-3	133
MetNorm	GPL-2	134
MetNorm	GPL-3	135
InterpretMSSpectrum	GPL-3	136
SDAMS	GPL	137
MetaboLab	Unique	138
MetMatch	GPL >= 2	139
MWASTools	CC BY-NC-ND	140
iMet-Q	LGPL-2	141
AMDORAP	GPL-2	142
Mummichog	BSD	143
decoMS2	GPL-2	144
XCMS	GPL >= 2	145
NMRfilter	GPL-3	146
MSPrep	GPL-3	147
iontree	GPL-2	148
Pachyderm	Unique	149
missForest	GPL >= 2	150
MS2Analyzer	MIT License	151
MetaboAnalyst	GPL	152
pyQms	MIT License	153
ChemoSpec	GPL-3	154
DeepRiPP	GPL-3	155
Metab	GPL >= 2	156
DiffCorr	GPL >= 3	157
RDFIO	GPL-2	158
HappyTools	Apache 2.0	159
metaMS	GPL >= 2	160
BatMass	Apache 2.0	161
FALCON	GPL-3	162
HiCoNet	BSD	163
MI-PACK	GPL-3	164
MetaQuant	GPL	165
PathBank	Open Database License	166
MetaboliteDetector	GPL	167
WikiPathways	GPL-2	168
IPO	GPL >= 2	169
swathTUNER	GPL-3	170
CSI:FingerrID	GPL-3	171
MetSign	GPL-2	172
ChromA	LGPL-3	173
InCroMAP	LGPL-3	174
rNMR	GPL-3	175
BiNCHE	GPL-3	176
Skyline	Apache 2.0	177
PlantMAT	Creative Commons	178
mz.unity	GPL-3	179
Ideom	LGPL-2	180
MET-IDEA	Academic only	181
MetaBridge	GPL-3	182
GSim	GPL-2	183
alsace	GPL >= 2	184
MetaboMiner	GPL	185
MetFamily	GPL-3	186
BRAIN	GPL-2	187
PAMBD	Academic only	188
IsoMS-Quant	Academic only	189
Rhea	Creative Commons	190
MetImp	Creative Commons	191
GAM	MIT License	192
mzAccess	Apache 2.0	193
TargetSearch	GPL >= 2	194
xMSanalyzer	GPL-2	195
MetaMapR	GPL-3	196
MAGMa	Apache 2.0	197
El-MAVEN	GPL-2	198
MetabNet	GPL-2	199
PAPi	GPL >= 2	200
MetFusion	GPL-3	201
Escher	MIT License	202
libChEBI	MIT License	203
crmn	GPL >= 3	204
MetExtract	MIT License	205
FingerID	Apache 2.0	206
muma	GPL-2	207
Workflow4Metabolomics	GPL-3	208
MetaBox	GPL-3	209
MetTailor	GPL >= 2	210
ALEX123	GPL-3	211
ropls	CeCILL	212
PepsNMR	GPL-2	213
MetCirc	GPL-2	214
BinBase	LGPL-2	215
RAMClustR	GPL >= 2	216
IsoCor	GPL-3	217
Plasmodesma	CeCILL	218
sampleDrift	GPL-2	219
WiPP	MIT License	220
Peakonly	MIT License	221
HastaLaVista	MIT License	222
MelonnPan	MIT License	223
DecoMetDIA	GPL >= 2	224
rDolphin	GPL-2	225
RawTools	Apache 2.0	226
misaR	MIT License	227
statTarget	LGPL-3	228
mwTab	BSD	229
MetaboSignal	GPL-3	230
Risa	LGPL	231
SLAW	GPL2	232
MS2Query	Apache 2.0	233
MMSAT	Academic only	234
MetShot	GPL >= 2	235
\.


--
-- Data for Name: ui_options; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ui_options (tool_name, ui, rowid) FROM stdin;
mzR	CLI	1
MetaboliteDetector	GUI	2
crmn	CLI	3
MetImp	web	4
VANTED	GUI	5
MetaboGroup S	web	6
PARADISe	GUI	7
FALCON	CLI	8
YMDB	web	9
DiffCorr	CLI	10
CCPN Metabolomics	CLI	11
ChromA	web	12
icoshift	CLI	13
PepsNMR	CLI	14
MZedDB	web	15
MassTRIX	web	16
WikiPathways	web	17
KPIC	CLI	18
MRMPROBS	GUI	19
ChemDistiller	CLI	20
CGBayesNets	CLI	21
NOMAD	CLI	22
alsace	CLI	23
decoMS2	CLI	24
TargetSearch	CLI	25
MetaMapR	web	26
ORCA	CLI	27
muma	CLI	28
BinBase	CLI	29
LIPID MAPS	web	30
Warpgroup	CLI	31
MetaboHunter	web	32
RankProd	CLI	33
MoNA	web	34
UniProtKB	web	35
MoDentify	CLI	36
IntLIM	web	37
Curatr	web	38
SAMMI	web	39
QCScreen	GUI	40
MSClust	CLI	41
MeltDB	web	42
TOXcms	CLI	43
VSClust	web	44
MetaboDiff	CLI	45
eMZed	CLI	46
Bayesil	web	47
IsoMS	CLI	48
cosmiq	CLI	49
ChemoSpec	CLI	50
HCOR	CLI	51
VMH	web	52
Pathos	web	53
MathDAMP	CLI	54
SpectConnect	web	55
UC2	GUI	56
METLIN	web	57
LiverWiki	web	58
RDFIO	CLI	59
GAM	web	60
jQMM	CLI	61
SmileMS	web	62
MetaboNetworks	CLI	63
E-Dragon	web	64
MET-COFEI	GUI	65
mzMatch	CLI	66
MS2Analyzer	CLI	67
MS2Analyzer	GUI	68
MyCompoundID	web	69
Newton	GUI	70
IsoCor	GUI	71
PlantMAT	GUI	72
XCMS online	web	73
MetaDB	GUI	74
AMDORAP	CLI	75
HiCoNet	web	76
compMS2Miner	web	77
MetFamily	web	78
BRAIN	CLI	79
SMART	CLI	80
BATMAN	CLI	81
MetiTree	web	82
MetaboLights	web	83
HappyTools	CLI	84
GSim	CLI	85
DeepRiPP	web	86
Mummichog	CLI	87
Mummichog	web	88
MetExtract	GUI	89
TNO-DECO	GUI	90
Metabolomics-Filtering	CLI	91
TracMass2	GUI	92
ADAP/MZmine	GUI	93
MAGMa	web	94
PathBank	web	95
MVAPACK	CLI	96
MET-IDEA	CLI	97
MetabolAnalyze	CLI	98
MZmine 2	GUI	99
CAMERA	CLI	100
MSPrep	CLI	101
ALLocater	web	102
ConsensusPathDB	web	103
NMRPro	CLI	104
RAMClustR	CLI	105
Workflow4Metabolomics	web	106
X13CMS	CLI	107
vaLID	web	108
CSI:FingerrID	GUI	109
CliqueMS	CLI	110
GabiPD	web	111
Lilikoi	web	112
GAVIN	CLI	113
GSimp	CLI	114
RaMP	web	115
RaMP	CLI	116
Ideom	GUI	117
apLCMS	CLI	118
xMSanalyzer	CLI	119
MetSign	CLI	120
InterpretMSSpectrum	CLI	121
SpectraClassifier	GUI	122
MetaboLyzer	CLI	123
CSI:FingerrID	web	124
PaintOmics	web	125
LICRE	CLI	126
matNMR	CLI	127
batchCorr	CLI	128
MetMatch	GUI	129
BatMass	GUI	130
SoyKB	web	131
polyPK	CLI	132
MESSI	web	133
BiNCHE	web	134
Skyline	GUI	135
PiMP	web	136
MetaboLab	CLI	137
LIMSA	CLI	138
Rdisop	CLI	139
MultiAlign	GUI	140
MAIT	CLI	141
Meta P-server	web	142
credential	CLI	143
AMDIS	GUI	144
MetaBox	CLI	145
MIDAS	web	146
mz.unity	CLI	147
PeroxisomeDB	web	148
CluMSID	CLI	149
MetCirc	CLI	150
MINMA	CLI	151
PAMBD	web	152
SysteMHC	web	153
NMRfilter	CLI	154
NMRfilter	GUI	155
SMART	GUI	156
MET-XAlign	GUI	157
MetaboliteDetector	CLI	158
MetShot	CLI	159
pyQms	CLI	160
Qemistree	CLI	161
Notame	CLI	162
MetaboShiny	GUI	163
MetaboShiny	web	164
Viime	GUI	165
Viime	web	166
NOREVA	web	167
JS-MS	GUI	168
JS-MS	web	169
pyMolNetEnhancer	CLI	170
pyMolNetEnhancer	GUI	171
RMolNetEnhancer	CLI	172
RMolNetEnhancer	GUI	173
ZEAMAP	web	174
MassComp	CLI	175
iMet	web	176
MetNormalizer	CLI	177
tmod	CLI	178
PYQUAN	CLI	179
PYQUAN	GUI	180
MAIMS	CLI	181
POMAShiny	CLI	182
POMAShiny	web	183
POMAShiny	GUI	184
R2DGC	CLI	185
AlpsNMR	CLI	186
AlpsNMR	GUI	187
ADAP-GC	CLI	188
ADAP-GC	GUI	189
MIDcor	CLI	190
MS-FLO	CLI	191
MS-FLO	web	192
MetaboQC	CLI	193
LIQUID	GUI	194
AntDAS	GUI	195
pymass	CLI	196
pySM	CLI	197
massPix	CLI	198
NMRProcFlow	GUI	199
speaq	CLI	200
SigMa	GUI	201
MSHub	GUI	202
MSHub	web	203
RGCxGC	CLI	204
ncGTW	CLI	205
TidyMS	CLI	206
TidyMS	GUI	207
Autotuner	CLI	208
hRUV	GUI	209
hRUV	web	210
MetumpX	CLI	211
MetumpX	GUI	212
fobitools	CLI	213
fobitoolsGUI	GUI	214
fobitoolsGUI	web	215
BioDendro	CLI	216
BioDendro	GUI	217
QSSR Automator	GUI	218
MetIDfyR	CLI	219
MetIDfyR	GUI	220
CANOPUS	GUI	221
marr	CLI	222
marr	GUI	223
Plasmodesma	CLI	224
GISSMO	GUI	225
sampleDrift	CLI	226
VOCCluster	CLI	227
WiPP	CLI	228
Peakonly	CLI	229
HastaLaVista	CLI	230
HastaLaVista	GUI	231
MelonnPan	CLI	232
rDolphin	GUI	233
RawTools	GUI	234
misaR	CLI	235
statTarget	CLI	236
statTarget	GUI	237
mwTab	CLI	238
MetaboSignal	GUI	239
Risa	CLI	240
SLAW	CLI	241
MS2Query	CLI	242
eRah	CLI	243
xMSannotator	CLI	244
HAMMER	CLI	245
ICT	CLI	246
rNMR	CLI	247
Ionwinze	CLI	248
iMet-Q	GUI	249
MetaBridge	web	250
MetDisease	GUI	251
Binner	GUI	252
MeRy-B	web	253
mineXpert	GUI	254
MetMask	CLI	255
BioNetApp	GUI	256
IIS	web	257
HiCoNet	CLI	258
mzOS	CLI	259
CMM	web	260
MI-PACK	CLI	261
MetaboSearch	web	262
InCroMAP	GUI	263
ropls	CLI	264
MS-LAMP	GUI	265
KIMBLE	GUI	266
DrugBank	web	267
MetaboClust	GUI	268
BiGG Models	web	269
KeggArray	GUI	270
ProbMetab	CLI	271
MetAlign	CLI	272
Rhea	web	273
PhenoMeNal	web	274
LDA	GUI	275
MetaQuant	GUI	276
MetFrag	web	277
GNPS-MassIVE	web	278
SMPDB	web	279
PathWhiz	web	280
SDAMS	CLI	281
MINEs	web	282
IsoMS-Quant	CLI	283
Escher	web	284
libChEBI	CLI	285
MetaboQuant	CLI	286
PAPi	CLI	287
missForest	CLI	288
MetExplore	web	289
MetaboAnalystR	CLI	290
TriDAMP	CLI	291
CFM-ID	web	292
MS-FINDER	GUI	293
Maui-VIA	GUI	294
IPO	CLI	295
nmrglue	CLI	296
FragPred	CLI	297
mQTL	CLI	298
MetTailor	CLI	299
Normalyzer	web	300
specmine	CLI	301
rNMR	GUI	302
MetaComp	GUI	303
QPMASS	GUI	304
flagme	CLI	305
Pachyderm	CLI	306
MetMSLine	GUI	307
CROP	CLI	308
OPTIMAS-DW	web	309
Galaxy-M	GUI	310
metaMS	GUI	311
mzAccess	web	312
Automics	GUI	313
MetaboAnalyst	web	314
MarVis-Suite	GUI	315
GNPS	web	316
BioCyc	web	317
Pathview	web	318
ALEX123	GUI	319
ALEX123	CLI	320
iontree	CLI	321
COLMAR	web	322
iPath	web	323
Lipid-Pro	GUI	324
MetScape	GUI	325
MaConDa	web	326
MetabNet	CLI	327
RMassBank	CLI	328
MMSAT	web	329
MetNorm	CLI	330
MetFusion	web	331
MAVEN	GUI	332
metabnorm	CLI	333
XCMS	CLI	334
CSI:FingerrID	CLI	335
FingerID	web	336
PathoLogic	web	337
Greazy	GUI	338
ChemSpider	web	339
MetCCS Predictor	web	340
SistematX	web	341
MetaNetter	GUI	342
MWASTools	CLI	343
nPYc-Toolbox	CLI	344
El-MAVEN	GUI	345
PathVisio	GUI	346
intCor	CLI	347
swathTUNER	web	348
MetaboMiner	GUI	349
MultiAlign	CLI	350
msPurity	CLI	351
proFIA	CLI	352
LipidXplorer	CLI	353
Zero-fill	CLI	354
MetFlow	web	355
mQTL.NMR	CLI	356
LIMSA	GUI	357
MET-COFEA	GUI	358
COVAIN	CLI	359
metabomxtr	CLI	360
\.


--
-- Name: approach_domain_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.approach_domain_rowid_seq', 20, true);


--
-- Name: approach_moltype_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.approach_moltype_rowid_seq', 462, true);


--
-- Name: approach_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.approach_rowid_seq', 88, true);


--
-- Name: citations_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.citations_rowid_seq', 270, true);


--
-- Name: containers_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.containers_rowid_seq', 5, true);


--
-- Name: file_formats_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.file_formats_rowid_seq', 296, true);


--
-- Name: formats_output_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.formats_output_rowid_seq', 56, true);


--
-- Name: func_annotation_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.func_annotation_rowid_seq', 168, true);


--
-- Name: func_interpretation_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.func_interpretation_rowid_seq', 1, false);


--
-- Name: func_postprocessing_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.func_postprocessing_rowid_seq', 204, true);


--
-- Name: func_preprocessing_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.func_preprocessing_rowid_seq', 222, true);


--
-- Name: func_processing_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.func_processing_rowid_seq', 426, true);


--
-- Name: func_statistics_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.func_statistics_rowid_seq', 303, true);


--
-- Name: functionality_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.functionality_rowid_seq', 535, true);


--
-- Name: input_variables_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.input_variables_rowid_seq', 24, true);


--
-- Name: instrument_data_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.instrument_data_rowid_seq', 279, true);


--
-- Name: os_options_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.os_options_rowid_seq', 904, true);


--
-- Name: programming_language_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.programming_language_rowid_seq', 402, true);


--
-- Name: publications_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.publications_rowid_seq', 1, false);


--
-- Name: software_license_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.software_license_rowid_seq', 235, true);


--
-- Name: ui_options_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ui_options_rowid_seq', 360, true);


--
-- Name: application approach_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application
    ADD CONSTRAINT approach_domain_pkey PRIMARY KEY (rowid);


--
-- Name: molecule_type approach_moltype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.molecule_type
    ADD CONSTRAINT approach_moltype_pkey PRIMARY KEY (rowid);


--
-- Name: approach approach_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.approach
    ADD CONSTRAINT approach_pkey PRIMARY KEY (rowid);


--
-- Name: containers containers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.containers
    ADD CONSTRAINT containers_pkey PRIMARY KEY (rowid);


--
-- Name: output_variables formats_output_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.output_variables
    ADD CONSTRAINT formats_output_pkey PRIMARY KEY (rowid);


--
-- Name: func_annotation func_annotation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_annotation
    ADD CONSTRAINT func_annotation_pkey PRIMARY KEY (rowid);


--
-- Name: func_interpretation func_interpretation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_interpretation
    ADD CONSTRAINT func_interpretation_pkey PRIMARY KEY (rowid);


--
-- Name: func_postprocessing func_postprocessing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_postprocessing
    ADD CONSTRAINT func_postprocessing_pkey PRIMARY KEY (rowid);


--
-- Name: func_processing func_processing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_processing
    ADD CONSTRAINT func_processing_pkey PRIMARY KEY (rowid);


--
-- Name: func_statistics func_statistics_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_statistics
    ADD CONSTRAINT func_statistics_pkey PRIMARY KEY (rowid);


--
-- Name: functionality functionality_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.functionality
    ADD CONSTRAINT functionality_pkey PRIMARY KEY (rowid);


--
-- Name: input_variables input_variables_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.input_variables
    ADD CONSTRAINT input_variables_pkey PRIMARY KEY (rowid);


--
-- Name: instrument_data instrument_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_data
    ADD CONSTRAINT instrument_data_pkey PRIMARY KEY (rowid);


--
-- Name: os_options os_options_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.os_options
    ADD CONSTRAINT os_options_pkey PRIMARY KEY (rowid);


--
-- Name: programming_language programming_language_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programming_language
    ADD CONSTRAINT programming_language_pkey PRIMARY KEY (rowid);


--
-- Name: publications publications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.publications
    ADD CONSTRAINT publications_pkey PRIMARY KEY (rowid);


--
-- Name: file_formats rowid; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_formats
    ADD CONSTRAINT rowid PRIMARY KEY (rowid);


--
-- Name: software_license software_license_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.software_license
    ADD CONSTRAINT software_license_pkey PRIMARY KEY (rowid);


--
-- Name: main tool_name; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main
    ADD CONSTRAINT tool_name PRIMARY KEY (tool_name);


--
-- Name: ui_options ui_options_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ui_options
    ADD CONSTRAINT ui_options_pkey PRIMARY KEY (rowid);


--
-- Name: ix_network_visualization_data_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_network_visualization_data_index ON public.network_visualization_data USING btree (index);


--
-- Name: citations citations_tool_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.citations
    ADD CONSTRAINT citations_tool_name_fkey FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: publications fk_tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.publications
    ADD CONSTRAINT fk_tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: func_preprocessing func_preprocessing_tool_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_preprocessing
    ADD CONSTRAINT func_preprocessing_tool_name_fkey FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ui_options tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ui_options
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON ui_options; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.ui_options IS 'references main';


--
-- Name: programming_language tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programming_language
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON programming_language; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.programming_language IS 'references main';


--
-- Name: software_license tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.software_license
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON software_license; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.software_license IS 'references main';


--
-- Name: input_variables tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.input_variables
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON input_variables; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.input_variables IS 'references main';


--
-- Name: output_variables tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.output_variables
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON output_variables; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.output_variables IS 'references main';


--
-- Name: containers tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.containers
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON containers; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.containers IS 'references main';


--
-- Name: molecule_type tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.molecule_type
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON molecule_type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.molecule_type IS 'references main';


--
-- Name: application tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON application; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.application IS 'references main';


--
-- Name: approach tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.approach
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON approach; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.approach IS 'references main';


--
-- Name: functionality tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.functionality
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON functionality; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.functionality IS 'references main';


--
-- Name: func_annotation tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_annotation
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON func_annotation; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.func_annotation IS 'references main';


--
-- Name: func_interpretation tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_interpretation
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON func_interpretation; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.func_interpretation IS 'references main';


--
-- Name: func_postprocessing tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_postprocessing
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON func_postprocessing; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.func_postprocessing IS 'references main';


--
-- Name: instrument_data tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_data
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON instrument_data; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.instrument_data IS 'references main';


--
-- Name: func_statistics tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.func_statistics
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON func_statistics; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.func_statistics IS 'Refers to main';


--
-- Name: file_formats tool_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_formats
    ADD CONSTRAINT tool_name FOREIGN KEY (tool_name) REFERENCES public.main(tool_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT tool_name ON file_formats; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT tool_name ON public.file_formats IS 'Refers to main';


--
-- PostgreSQL database dump complete
--

