import pandas as pd
import numpy as np
import itertools

import igraph as ig
import plotly.graph_objs as go

# Make graph data


def make_graph_data(df):

    nodes = []
    temp_nodes = [{"name": name, "identifier": id}
                  for name, id in zip(df.tool_name, df.iloc[:, 2])]
    # Generate nodes
    for dic in temp_nodes:
        if dic not in nodes:
            nodes.append(dic)
    # Generate links
    unique_nodes = []
    for d in nodes:
        if d['name'] not in unique_nodes:
            unique_nodes.append(d['name'])
    nodes_names_dict = {name: val for val, name in enumerate(unique_nodes)}

    # Generate edgeWeights
    coOc_df = df[["tool_name", "PMID"]].copy()
    coOc_df["value"] = 1
    coOc_df = coOc_df.pivot_table(index="PMID", columns="tool_name")
    coOc_df.fillna(0, inplace=True)
    coOc_df = coOc_df.T.dot(coOc_df)
    np.fill_diagonal(coOc_df.values, 0)
    coOc_df.reset_index(level=0, drop=True, inplace=True)
    coOc_df.rename(nodes_names_dict, inplace=True)
    coOc_df.rename(nodes_names_dict, axis=1, inplace=True)

    tempdf = df.copy()
    tempdf['source'] = [nodes_names_dict[key] for key in tempdf.tool_name]
    tempdf.set_index('source', inplace=True)
    groupedDF = tempdf.groupby('PMID')['tool_name']
    connections = [value.values.tolist()
                   for (key, value) in groupedDF.groups.items()]
    tempEdges = [list(itertools.combinations(x, 2)) for x in connections]
    tempEdges = [li for li in tempEdges if li != []]
    edges = list(itertools.chain(*tempEdges))

    values = []
    for (i, j) in edges:
        values.append(coOc_df['value', j].loc[i])

    links = [{"source": edge[0], "target":edge[1], "value":value}
             for edge, value in zip(edges, values)]

    return nodes, links

# Make network visual


def make_network_plot_data(nodes, links):

    edges = [(dic['source'], dic['target']) for dic in links]
    labels = []
    for dic in nodes:
        if dic['name'] not in labels:
            labels.append(dic['name'])

    ids = []
    color = []
    i = 0
    for id in nodes:
        if id['identifier'] not in ids:
            ids.append(id['identifier'])
            i += 1
        color.append(i)

    value = []
    templist = []
    for node in [d['name'] for d in nodes]:
        if node not in templist:
            templist.append(node)
            value.append(6*[d['name'] for d in nodes].count(node))
    del(templist)

    edgeWeights = [dic['value'] for dic in links]
    G = ig.Graph(edges)
    G.vs['name'] = [labels[vid.index] for vid in G.vs]
    G.vs['value'] = value
    G.es['weight'] = edgeWeights
    return G


def filter_network(G, selected_nodes=[], gt_edge_weight=-1):
    G_filt = G.copy()
    if selected_nodes != []:
        # Filter nodes by name
        G_filt.delete_vertices(G_filt.vs.select(name_in=selected_nodes))

    if gt_edge_weight == 0:
        # Remove isolate nodes
        return G_filt
    elif gt_edge_weight >= 0:
        # Filter edges less than some cutoff
        G_filt.delete_edges(G_filt.es.select(weight_lt=gt_edge_weight))
        # Remove isolate nodes
        G_filt.delete_vertices(G_filt.vs.select(_degree=0))
    return G_filt


def include_network(G, selected_nodes=None):
    inds = []
    nbrs = []
    # Select Node neighbors
    # Find vertex id of selected Node
    for node in selected_nodes:
        idx = G.vs['name'].index(node)
        # Find nodes connected to selected node
        nbr = G.neighbors(G.vs[idx])
        # append to lists
        inds.append(idx)
        nbrs.append(nbr)
    # Extract selected nodes
    nodes = inds+nbrs
    nodes = np.hstack(nodes)
    # Get node names
    node_names = G.vs.select(nodes)['name']
    # Nodes to exclude
    nodes_remove = [x for x in G.vs['name'] if x not in node_names]
    # Exclude function
    G_filt = filter_network(G, nodes_remove, gt_edge_weight=-1)
    return G_filt


def make_network_plot(G, selected_nodes=None):

    labels = G.vs['name']
    edges = [(i.source, i.target) for i in G.es]
    #edges = [t for t in (set(tuple(i) for i in edges))]
    edgeWeights = G.es['weight']
    norm_edgeWeights = [(x - min(edgeWeights)) /
                        (max(edgeWeights - min(edgeWeights))) for x in edgeWeights]
    norm_edgeWeights = [x * (10 - 1) + 1 for x in norm_edgeWeights]
    value = G.vs['value']
    norm_value = [(x - min(value))/(max(value)-min(value)) for x in value]
    norm_value = [x * (35 - 25) + 25 for x in norm_value]
    textDF = pd.DataFrame({"label": labels, "value": value})
    if selected_nodes:
        textDF['color'] = textDF.label.apply(
            lambda x: 'red' if x in selected_nodes else 'blue')
    else:
        textDF['color'] = 'blue'

    layt = G.layout("kk3d")

    Xn = [layt[k][0] for k in range(len(labels))]  # x-coordinates of nodes
    Yn = [layt[k][1] for k in range(len(labels))]  # y-coordinates
    Zn = [layt[k][2] for k in range(len(labels))]  # z-coordinates
    Xe = []
    Ye = []
    Ze = []
    for e in edges:
        # x-coordinates of edge ends
        Xe += [layt[e[0]][0], layt[e[1]][0], None]
        Ye += [layt[e[0]][1], layt[e[1]][1], None]
        Ze += [layt[e[0]][2], layt[e[1]][2], None]

    Xe = [Xe[x:x+3] for x in range(0, len(Xe), 3)]
    Ye = [Ye[x:x+3] for x in range(0, len(Ye), 3)]
    Ze = [Ze[x:x+3] for x in range(0, len(Ze), 3)]

    traces = {}
    for i in range(0, len(Xe)):
        traces['trace_' + str(i)] = go.Scatter3d(x=Xe[i],
                                                 y=Ye[i],
                                                 z=Ze[i],
                                                 mode='lines',
                                                 line=dict(
                                                     color='rgb(125,125,125)', width=norm_edgeWeights[i]),
                                                 text=[int(edgeWeights[i])]*2,
                                                 hovertemplate="<b>Co-occurence:</b> %{text}<br><extra></extra>"
                                                 )

    traces['trace_' + str(len(Xe)+1)] = go.Scatter3d(x=Xn,
                                                     y=Yn,
                                                     z=Zn,
                                                     mode='markers',
                                                     name='tools',
                                                     marker=dict(symbol='circle',
                                                                 size=norm_value,
                                                                 color=textDF['color'],
                                                                 line=dict(
                                                                     color='rgb(50,50,50)', width=0.5)
                                                                 ),
                                                     text="<b>Tool Name:</b> " +
                                                     textDF['label']+"<br><b>Articles found:</b> " +
                                                     textDF['value'].map(
                                                         '{:,d}'.format).astype(str),
                                                     hovertemplate="%{text}<br><extra></extra>",
                                                     textposition="top center"
                                                     )

    axis = dict(showbackground=False,
                showline=False,
                zeroline=False,
                showgrid=False,
                showticklabels=False,
                showspikes=False,
                title=''
                )

    layout = go.Layout(
        title="<b>Metabolomics software co-occurence network</b>",
        height=800,
        showlegend=False,
        scene=dict(
            xaxis=dict(axis),
            yaxis=dict(axis),
            zaxis=dict(axis),
        ),
        margin=dict(
            t=100
        ),
        hovermode='closest',
    )
    data = list(traces.values())
    fig = go.Figure(data=data, layout=layout)
    return fig
