def process_data(database_params="dbname=metabolomics_database user=jonathan"):
    """
    Function that pulls and processes data from the database.
    Inputs: database_params a string of the database parameters and credentials
    returns: df_processing processing table from database
             df_statisticalanalysis statistical analysis table from database
             df_annotation anootation table form database
             df_instrument instrument type table from database
             df_moltype molecule type table from database
             df_ui user interface table from database
             Counted a dataframe with unique methods counted, used for sunburst visualization
             Fig an interactive figure depicting number of tools per year and cumulative number of tools per year
             user_df a dataframe with the database data reformatted in a compact format for the user
             today this is todays date, used to color the last_updated column in user_df
             formatted_two_Yago the date two years ago from today, used to color the last_updated column in user_df
             formatted_five_Yago the date five years ago from today, used to color the last_updated column in user_df
             formatted_ten_Yago the date ten years ago from today, used to color the last_updated column in user_df

    """
    import psycopg2
    import pandas as pd
    from functools import reduce
    import numpy as np
    import plotly.graph_objects as go
    from datetime import datetime, date
    # Connect to a database that exists
    conn = psycopg2.connect(database_params)
    # Open a cursor to perform database operations
    cur = conn.cursor()
    # Execute SQL commands
    cur.execute("SELECT * FROM main;")
    df_main = pd.DataFrame(cur.fetchall(), columns=[
                           col[0] for col in cur.description]).drop(['notes'], axis=1)
    cur.execute("SELECT * FROM os_options;")
    df_os = pd.DataFrame(cur.fetchall(), columns=[
                         col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM instrument_data;")
    df_instrument = pd.DataFrame(cur.fetchall(), columns=[
                                 col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM func_processing;")
    df_processing = pd.DataFrame(cur.fetchall(), columns=[
        col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM func_statistics;")
    df_statisticalanalysis = pd.DataFrame(cur.fetchall(), columns=[
                                          col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM func_annotation;")
    df_annotation = pd.DataFrame(cur.fetchall(), columns=[
                                 col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM ui_options;")
    df_ui = pd.DataFrame(cur.fetchall(), columns=[
                         col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM software_license;")
    df_license = pd.DataFrame(cur.fetchall(), columns=[
                              col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM file_formats;")
    df_fileformat = pd.DataFrame(cur.fetchall(), columns=[
                                 col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM application;")
    df_application = pd.DataFrame(cur.fetchall(), columns=[
                                  col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM containers;")
    df_containers = pd.DataFrame(cur.fetchall(), columns=[
                                 col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM functionality;")
    df_functionality = pd.DataFrame(cur.fetchall(), columns=[
                                    col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM input_variables;")
    df_inputs = pd.DataFrame(cur.fetchall(), columns=[
                             col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM output_variables;")
    df_outputs = pd.DataFrame(cur.fetchall(), columns=[
                              col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM molecule_type;")
    df_moltype = pd.DataFrame(cur.fetchall(), columns=[
                              col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM approach;")
    df_approaches = pd.DataFrame(cur.fetchall(), columns=[
                                 col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM programming_language;")
    df_language = pd.DataFrame(cur.fetchall(), columns=[
                               col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM citations;")
    df_citations = pd.DataFrame(cur.fetchall(), columns=[
                                col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM publications;")
    df_publications = pd.DataFrame(cur.fetchall(), columns=[
                                   col[0] for col in cur.description]).drop(['rowid'], axis=1)
    cur.execute("SELECT * FROM network_visualization_data;")
    df_network = pd.DataFrame(cur.fetchall(), columns=[
                                   col[0] for col in cur.description]).drop(['index'], axis=1)
    conn.close()

    # Sunburst data gen.
    frames = [df_processing,
              df_statisticalanalysis, df_annotation]
    merged_funcs = reduce(lambda left, right: pd.concat(
        [left, right], sort=True), frames)
    long_format = merged_funcs.melt(
        id_vars=['tool_name'], var_name='Function', value_name='Method')
    long_format.replace('', np.nan, inplace=True)
    long_format.dropna(inplace=True)
    counted = long_format.groupby(['Function', 'Method']).agg(
        Counts=('Method', 'count')).reset_index()

    # line/bar data visual
    df_yreleased = df_main[['tool_name', 'year_released']]
    df_time = df_yreleased.groupby('year_released').agg(
        Counts=('year_released', 'count')).reset_index()
    df_time['CumSum'] = df_time.Counts.cumsum()
    #visual####
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df_time['year_released'], y=df_time['CumSum'],
                             mode='lines+markers',
                             name='Cumulative Sum'))
    fig.add_trace(go.Bar(x=df_time['year_released'], y=df_time['Counts'],
                         opacity=0.7,
                         name='Counts'))
    fig.update_layout(
        title_text="Number of tools by year and across time",
        xaxis_domain=[0.05, 1.0]
    )
    fig.update_xaxes(title="Year")
    fig.update_yaxes(title="Number of software tools")
    ###########

    # Binarise the variables
    bin_language = pd.get_dummies(df_language, columns=[
                                  'language'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_os = pd.get_dummies(df_os, columns=['os'], prefix="", prefix_sep="").groupby(
        ['tool_name'], as_index=False).sum()
    bin_instrument = pd.get_dummies(df_instrument, columns=[
                                    'instrument'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_processing = pd.get_dummies(df_processing, columns=[
        'processing'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_statisticalanalysis = pd.get_dummies(df_statisticalanalysis, columns=[
                                             'stats_method'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_annotation = pd.get_dummies(df_annotation, columns=[
                                    'annotation'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_ui = pd.get_dummies(df_ui, columns=['ui'], prefix="", prefix_sep="").groupby(
        ['tool_name'], as_index=False).sum()
    bin_license = pd.get_dummies(df_license, columns=[
                                 'license'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_fileformat = pd.get_dummies(df_fileformat, columns=[
                                    'format'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_application = pd.get_dummies(df_application, columns=[
                                     'domain'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_containers = pd.get_dummies(df_containers, columns=[
                                    'container_url'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_functionality = pd.get_dummies(df_functionality, columns=[
                                       'function'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_inputs = pd.get_dummies(df_inputs, columns=[
                                'input'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_outputs = pd.get_dummies(df_outputs, columns=[
                                 'output'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_moltype = pd.get_dummies(df_moltype, columns=[
                                 'moltype'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_approaches = pd.get_dummies(df_approaches, columns=[
                                    'approach'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_citations = pd.get_dummies(df_citations, columns=[
                                   'citations'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()
    bin_publications = pd.get_dummies(df_publications, columns=[
                                      'pub_doi'], prefix="", prefix_sep="").groupby(['tool_name'], as_index=False).sum()

    # Make sure all dataframes have all the tools
    tools = df_main['tool_name'].values
    bin_dfs = [bin_os, bin_functionality, bin_ui, bin_language, bin_license, bin_approaches, bin_processing,
               bin_statisticalanalysis, bin_annotation, bin_instrument, bin_fileformat, bin_inputs, bin_outputs, bin_containers,
               bin_application, bin_moltype, bin_citations, bin_publications]
    clean_dfs = []
    for df in bin_dfs:
        for tool in tools:
            if tool not in df['tool_name'].values:
                df = df.append({"tool_name": tool}, ignore_index=True)
        clean_dfs.append(df)

    dataframes = [df_main] + clean_dfs

    # Merge all dataframes into one.
    #merged_df = reduce(lambda left, right: pd.merge(left, right, on=["tool_name"], how="right"), dataframes)
    #merged_df.fillna(value = 0, inplace = True)

    # Re-structure the data into a compact form for the user
    dfs_os = df_os.groupby("tool_name").agg(lambda x: ", ".join(x))
    dfs_moltype = df_moltype.groupby("tool_name").agg(lambda x: ", ".join(x))
    dfs_ui = df_ui.groupby("tool_name").agg(lambda x: ", ".join(x))
    dfs_language = df_language.groupby("tool_name").agg(lambda x: ", ".join(x))
    dfs_license = df_license.groupby("tool_name").agg(lambda x: ", ".join(x))
    dfs_approaches = df_approaches.groupby(
        "tool_name").agg(lambda x: ", ".join(x))
    dfs_processing = df_processing.groupby(
        "tool_name").agg(lambda x: ", ".join(x))
    dfs_statisticalanalysis = df_statisticalanalysis.groupby(
        "tool_name").agg(lambda x: ", ".join(x))
    dfs_annotation = df_annotation.groupby(
        "tool_name").agg(lambda x: ", ".join(x))
    dfs_instrument = df_instrument.groupby(
        "tool_name").agg(lambda x: ", ".join(x))
    dfs_fileformat = df_fileformat.groupby(
        "tool_name").agg(lambda x: ", ".join(x))
    dfs_functionality = df_functionality.groupby(
        "tool_name").agg(lambda x: ", ".join(x))
    dfs_inputs = df_inputs.groupby("tool_name").agg(lambda x: ", ".join(x))
    dfs_outputs = df_outputs.groupby("tool_name").agg(lambda x: ", ".join(x))
    dfs_containers = df_containers.groupby(
        "tool_name").agg(lambda x: ", ".join(x))
    dfs_application = df_application.groupby(
        "tool_name").agg(lambda x: ", ".join(x))
    dfs_citations = df_citations.groupby("tool_name").agg(lambda x: sum(x))
    dfs_publications = df_publications.groupby(
        "tool_name").agg(lambda x: ", ".join(x))

    dfs_os.reset_index(level=0, inplace=True)
    dfs_moltype.reset_index(level=0, inplace=True)
    dfs_ui.reset_index(level=0, inplace=True)
    dfs_language.reset_index(level=0, inplace=True)
    dfs_license.reset_index(level=0, inplace=True)
    dfs_approaches.reset_index(level=0, inplace=True)
    dfs_processing.reset_index(level=0, inplace=True)
    dfs_statisticalanalysis.reset_index(level=0, inplace=True)
    dfs_annotation.reset_index(level=0, inplace=True)
    dfs_instrument.reset_index(level=0, inplace=True)
    dfs_fileformat.reset_index(level=0, inplace=True)
    dfs_functionality.reset_index(level=0, inplace=True)
    dfs_inputs.reset_index(level=0, inplace=True)
    dfs_outputs.reset_index(level=0, inplace=True)
    dfs_containers.reset_index(level=0, inplace=True)
    dfs_application.reset_index(level=0, inplace=True)
    dfs_citations.reset_index(level=0, inplace=True)
    dfs_publications.reset_index(level=0, inplace=True)

    tools = df_main['tool_name'].values
    dfs = [dfs_os, dfs_moltype, dfs_ui, dfs_language, dfs_license, dfs_approaches, dfs_processing,
           dfs_statisticalanalysis, dfs_annotation, dfs_instrument, dfs_fileformat, dfs_functionality, dfs_inputs, dfs_outputs,
           dfs_containers, dfs_application, dfs_citations, dfs_publications]
    frames = []
    for df in dfs:
        for tool in tools:
            if tool not in df['tool_name'].values:
                df = df.append({"tool_name": tool}, ignore_index=True)
        frames.append(df)
    # Add links to table
    frames = [df_main] + frames
    user_df = reduce(lambda left, right: pd.merge(
        left, right, on=["tool_name"], how="right"), frames)
    user_df = user_df.fillna(value="")
    user_df.drop(['date_modified'], axis=1, inplace=True)
    sites = user_df.website
    markdown_links = []
    for site in sites:
        if site != "":
            markdown_links.append("[Link](" + site + ")")
        else:
            markdown_links.append("")
    user_df.website = markdown_links
    sites = user_df.pub_doi
    markdown_links = []
    for site in sites:
        if site != "":
            if len(site.split(", ")) > 1:
                markdown_links.append(
                    [" [Link](" + x + ")" for x in site.split(", ")])
            else:
                markdown_links.append("[Link](" + site + ")")
        else:
            markdown_links.append("")
    user_df.pub_doi = markdown_links
    # Rename columns
    user_df.columns = ["Tool Name", "Year Published", "Website", "Last Updated", "OS", "Molecule Type", "UI", "Programming Language", "License", "Approach", "Processing",
                       "Statistical Analysis", "Annotation", "Instrument Data", "File Formats", "Functionality", "Input Variables", "Output Variables", "Containers", "Application", "Citations", "Publications"]
    # Cast date to datetime format
    user_df["Last Updated"] = pd.to_datetime(
        user_df["Last Updated"], format=('%Y-%m-%d')).dt.date
    # Sort by date
    user_df.sort_values(by=['Last Updated'],
                        ascending=False, inplace=True, ignore_index=True)

    # date
    today = datetime.today()

    two_Yago = datetime(today.year-2, today.month, today.day)
    formatted_two_Yago = two_Yago.strftime('%Y-%m-%d')

    five_Yago = datetime(today.year-5, today.month, today.day)
    formatted_five_Yago = five_Yago.strftime('%Y-%m-%d')

    ten_Yago = datetime(today.year-10, today.month, today.day)
    formatted_ten_Yago = ten_Yago.strftime('%Y-%m-%d')
    return(df_processing, df_statisticalanalysis, df_annotation, df_instrument, df_moltype, df_ui, df_language, df_license, df_os, counted, fig, user_df, today, formatted_two_Yago, formatted_five_Yago, formatted_ten_Yago, df_network)
