#!/bin/bash
while sleep 0.1; do
# Minimum delay between two script executions, in seconds. 
seconds=$((60*60*24*28))

if test "$(($(date "+%s")-$(date -r "$0" "+%s")))" -gt "$seconds" ; then
    # Store the current date as modification time stamp of this script file
    touch -m -- "$0"
    echo running
    # Run the following scripts
    rm -f noNewAbstracts.csv
    R --no-save --quiet < getCitations.R
    R --no-save --quiet < getFulltext.R
    python3 getNetworkData.py
    R --no-save --quiet < getAbstracts.R
    python3 getPreds.py 
    python3 ./GitHub_ext/scripts/update_mscat.py
    
fi


done