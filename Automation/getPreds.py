import pandas as pd
import nltk
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
from nltk.corpus import stopwords
import sparknlp
from pyspark.ml import PipelineModel, Pipeline
from sparknlp.annotator import *
from sparknlp.base import *
from sparknlp.common import *
import pyspark.sql.functions as F
import io
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
import enchant
from sqlalchemy import create_engine

# Connect to a database that exists
## Database connection settings (should be moved to envir variables)
engine = create_engine('postgresql+psycopg2://postgres:postgres@pgsql/postgres')

# Function that detects camel case words
def camel(word):
    return word != word.lower() and word != word.upper() and word.istitle() != True and "_" not in word

# Function to export csv to email
def export_csv(df):
    with io.StringIO() as buffer:
        df.to_csv(buffer)
        return buffer.getvalue()

try:
    noNewAbstracts = pd.read_csv("/app/NoNewAbstracts.csv")
except:
    print("New abstracts are available.")

if 'noNewAbstracts' in globals():
    print("No new abstracts found aborting!")
    sys.exit(0)
PubMedAbstracts = pd.read_csv("/app/PubMedAbstracts.csv", usecols=['pmid', 'title_abstract'])
text = PubMedAbstracts

# Initialize spark session
spark = sparknlp.start(gpu=False)

X = spark.createDataFrame(text)

# Define annotators
document = (
    DocumentAssembler()
    .setInputCol('title_abstract')
    .setOutputCol('document')
)

sentence = (
    SentenceDetector()
    .setInputCols(['document'])
    .setOutputCol('sentence')
)

token = (
    Tokenizer()
    .setInputCols(['sentence'])
    .setOutputCol('token')
)

# Define pipeline with annotators
annotationPipeline = Pipeline(
    stages=[
        document,
        sentence,
        token
    ]
)

# Apply annotations
X = annotationPipeline.fit(X).transform(X)

# Load the saved model
ner_model = PipelineModel.load("/app/NER_model")

# Apply the model onto the test data
predictions = ner_model.transform(X)

predictions = (
    predictions
    .select('pmid', F.explode(F.arrays_zip('normalized.result', 'ner.result', 'ner.metadata')).alias('cols'))
    .select('pmid',
            F.col('cols.0').alias('word'),
            F.col('cols.1').alias('Prediction'),
            F.col('cols.2.confidence').alias('Confidence T'))
    .select('pmid', 'word','Prediction','Confidence T')
    .dropna()
    .dropDuplicates(['word', 'Prediction'])
)


predictions = predictions.toPandas()
potentialTools = predictions[predictions.Prediction == 'T']

stop = set(stopwords.words('english'))
potentialTools = potentialTools[~potentialTools.word.str.lower().isin(stop)]

potentialTools['camelCase'] = [camel(x) for x in potentialTools.word]
potentialTools['NumCapital'] = [sum(map(str.isupper, x)) for x in potentialTools.word]
potentialTools.sort_values(['camelCase', 'NumCapital'], ascending=False, inplace = True)
# Remove numeric
potentialTools = potentialTools[~potentialTools.word.str.match(r'[0-9]')]

# Remove less than 3 chars 
potentialTools = potentialTools[~potentialTools.word.str.match(r'^\w{1,2}$')]

# Remove all lowercase words
potentialTools = potentialTools[potentialTools.NumCapital != 0]

# initiallize english dictionary
d = enchant.Dict('en_US')
# Break dataframe into 2 parts
sub2 = potentialTools[(potentialTools.camelCase==False)&(potentialTools.NumCapital==1)]

sub1 = potentialTools[~potentialTools.index.isin(sub2.index)]

# Remove dictionary words from subset
sub2 = sub2[[not d.check(x) for x in sub2.word]]
# Re-merge subsets
potentialTools = pd.concat([sub1, sub2])



# Check if tools are already in database or if they are already deemed as false positives
previousTools = None
try:
    previousTools = pd.read_csv("/app/previousTools.csv")
except:
    print("No file found")

databaseTools = pd.read_sql('SELECT tool_name FROM main;', engine)
if previousTools is not None:
    previousTools = pd.concat([previousTools.squeeze(), databaseTools.squeeze()], ignore_index=True).drop_duplicates()
    potentialTools = potentialTools[~potentialTools.word.isin(previousTools)]
else:
    potentialTools = potentialTools[~potentialTools.word.isin(databaseTools)]

if previousTools is not None:
    previousTools = pd.concat([previousTools, potentialTools.word], ignore_index = True)
    previousTools.to_csv("/app/previousTools.csv", encoding='utf-8', index=False)
else:
    potentialTools.word.to_csv("/app/previousTools.csv", encoding="utf-8", index=False)

# Send an email with the passed predictions
mail_content = '''Hello,
This is an automated email.
In this email we are sending potential metabolomics software tools.
'''
sender_address = 'MetaboGuru@gmail.com'
sender_pass = 'NLPmetabolomics'
receiver_address = 'incoming+metabolomics-tools-database-database-container-24259151-issue-@incoming.gitlab.com'
message = MIMEMultipart()
message['From'] = sender_address
message['To'] = receiver_address
message['Subject'] = 'Potential Metabolomics Software Tools.'

message.attach(MIMEText(mail_content, 'plain'))
attachment = MIMEApplication(export_csv(potentialTools))
attachment['Content-Disposition'] = 'attachment; filename="{}"'.format('PotentialTools.csv')
message.attach(attachment)

session = smtplib.SMTP('smtp.gmail.com', 587) 
session.starttls() 
session.login(sender_address, sender_pass) 
text = message.as_string()
session.sendmail(sender_address, receiver_address, text)
session.quit()
print('eMail Sent.')