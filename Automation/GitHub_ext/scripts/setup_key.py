# -----------------------------------------------------------
# setup_key.py script sets up the github api key by
# storing it in a .env file
# -----------------------------------------------------------

import os
import argparse

from github_ext.mscat_utils.github_ext_paths import GithubExtPaths


def create_dotenv_file(api_key, env_path):
    """Generated a dotenv file"""
    with open(env_path, "w") as env_file:
        content = f"GITHUB_KEY={api_key}"
        env_file.write(content)


if __name__ == "__main__":

    # CLI arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--token", type=str, required=True, help="GitHub token")

    parser.add_argument(
        "-o",
        "--overwrite",
        action="store_true",
        required=False,
        default=False,
        help="Overwrite exists github token",
    )
    args = parser.parse_args()

    gh_paths = GithubExtPaths()

    check = os.path.exists(gh_paths._env_file)
    if check is False:
        # creating a dot env file if not found
        print("WARNING: dotenv file not found, creating one ...")
        create_dotenv_file(args.token, gh_paths._automation_env_file)

        # double checking if the .env file is created
        print("MESSAGE: Searching for dotenv file")
        check2 = os.path.exists(gh_paths._automation_env_file)
        if check2 is False:
            raise RuntimeError("Failed to create dotenv file")
    else:
        # overwriting .env file
        if args.overwrite is True:
            print("WARNING: file exists, overwriting with new token")
            create_dotenv_file(args.token, gh_paths._automation_env_file)
        else:
            # occurs if user does not provide the overwrite parameter
            msg = "To overwrite current .env file, please add the '-o' option"
            raise RuntimeError(msg)
    print("Setup complete!")
