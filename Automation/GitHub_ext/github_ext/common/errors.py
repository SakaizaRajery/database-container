class InvalidLinkError(Exception):
    """Occurs when invalid links are found"""


class GithubRequestFailed(Exception):
    """Occurs when sending a requests has failed"""


class UserDoesNotExists(Exception):
    """Raised when the github user does not exsits"""


class RepoDoesNotExists(Exception):
    """Raised when the github repository does not exists"""


class MissingGitHubParams(Exception):
    """Occurs when a required parameter is missing"""


class MissingToken(Exception):
    """Raises when github token is not found when required"""


class InvalidFormatError(Exception):
    """Raised when the format of specific input or parameter is invalid"""
