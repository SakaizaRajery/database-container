# -------------------------------
# checks.py
# Responsible for checking data types
# -------------------------------
import json
from github_ext.mscat_utils.github_ext_paths import GithubExtPaths
from github_ext.common.errors import GithubRequestFailed
from github_ext.mscat_utils.github_ext_paths import GithubExtPaths


def check_request(status_code):
    if status_code == 200:
        return True
    elif status_code != 200:
        if status_code == 422:
            err_msg = "Unable to processes request code error {}".format(status_code)
            raise GithubRequestFailed(err_msg)
        else:
            err_msg = "URL proivded is invalid. Code: {}".format(status_code)
            raise GithubRequestFailed(err_msg)


def check_mscat_data_format(column_names):
    """Checks of the added file is in the mscat format buy
    using its columns.

    This is uselful for maintaining consistency across all
    method and function calls

    column_names : list
        list containing all column names

    Return
    ------
    bool
        True if correct format. False if incorrect format
    """
    mscat_cols = {
        "Tool Name",
        "Year Published",
        "Website",
        "Publications",
        "Citations",
        "Last Updated",
        "OS",
        "UI",
        "Molecule Type",
        "Programming Language",
        "License",
        "Approach",
        "Functionality",
        "Pre-Processing",
        "Post-Processing",
        "Statistical Analysis",
        "Annotation",
        "Instrument Data",
        "File Formats",
    }

    # if all columns names are the same between sets, it should be 0
    check_counts = len(mscat_cols - set(column_names))

    # checkin conditions
    if not len(column_names) == len(mscat_cols):
        return False
    elif check_counts != 0:
        return False
    else:
        return True


def check_supported_language(prog_lang):
    """checks if the language is

    Parameters
    ----------
    prog_lang : str
        programing language to check

    Returns
    -------
    bool
        True if supported, False if not
    """
    config_path = GithubExtPaths()

    with open(config_path.unsupported_lang_config_path, "r") as infile:
        not_supported_langs = json.load(infile)
        unsupported_langs = [lang.lower() for lang in not_supported_langs["langs"]]

    if prog_lang.lower() in unsupported_langs:
        return False
    else:
        return True
