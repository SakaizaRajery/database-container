# package is mean for loading tokens / keys into the program
from environs import Env


def load_api_key():
    """Loads github api key

    Raises
    ------
    MissingToken
        Raises if there is not API key is found in the environment

    Raises
    ------
    EnvError
        Raised if GITHUB_KEY environment variables does not exists
    """

    # reading local env variables
    env = Env()
    env.read_env()

    # loading GITHUB_KEY
    oauth_key = env("GITHUB_KEY")
    return oauth_key
