# ------------------------------
# urls.py
# Contains link endpoint for specific data
# ------------------------------
import os


class EndPointUrls:
    def __init__(self, owner_name=None, repo_name=None):
        self.owner_name = owner_name
        self.repo_name = repo_name
        self.contents = "https://raw.githubusercontent.com/"
        self.repo_url = "https://api.github.com/repos/"
        self.repo_search_url = "https://api.github.com/search/repositories"
        self.commits_url = "https://api.github.com/repos/{}/{}/commits".format(
            owner_name, repo_name
        )
        self.contributors = "https://api.github.com/repos/{}/{}/contributors".format(
            owner_name, repo_name
        )


class BioconEndPoints:
    def __init__(self, name):
        self.name = name
        self.bioconductor_page = (
            "https://www.bioconductor.org/packages/release/bioc/html/{}.html".format(
                name
            )
        )
