import json
import os
import numpy as np
from collections import namedtuple
from github_ext.common.checks import check_supported_language
from github_ext.structs.repo import RepoStruct
from github_ext.structs.commits import CommitStruct


def parse_json_repo(contents):
    """Parses json contents and converts it into a Repo structure

    Parameters
    ----------
    contents : dict
        contents of json file

    Returns
    -------
    Repo (github_ext.structs.repo)
        Repo struture containing all the required information

    """
    # Converting the  contents into a dictionary by using the json
    # -- over writes the input parameters
    # contents = json.loads(contents)

    # selecting fields that seem necessary

    # -- general repo info
    user_info = []
    repo_id = contents["id"]
    name = contents["name"]
    full_name = contents["full_name"]
    homepage = contents["homepage"]
    isprivate = True if contents["private"] == "true" else False
    isfork = True if contents["fork"] == "true" else False
    user_info.append([repo_id, name, full_name, homepage, isfork, isprivate])

    # -- Owner information
    owner_info = []
    owner_name = contents["owner"]["login"]
    owner_url = contents["owner"]["html_url"]
    owner_repos = contents["owner"]["repos_url"]  # TODO: Requires a parser
    commits_url = contents["commits_url"]  # TODO: requires a parser
    owner_info.append([owner_name, owner_url, owner_repos, commits_url])

    # -- specific repo information
    repo_info = []
    branch = contents["default_branch"]
    topics = contents["topics"]
    description = contents["description"]
    issues_url = contents["issues_url"]  # TODO: requires a parser and struct
    lang_url = contents["languages_url"]  # TODO: requires a parser and struct
    pulls_url = contents["pulls_url"]  # TODO: requires a parser and struct
    git_url = contents["git_url"]
    clone_url = contents["clone_url"]  # TODO: requires a parser and struct
    repo_info.append(
        [
            branch,
            topics,
            description,
            issues_url,
            lang_url,
            pulls_url,
            git_url,
            clone_url,
        ]
    )

    # -- stats
    stats = []
    size = int(contents["size"])
    stargazers_count = int(contents["stargazers_count"])
    watchers_count = int(contents["watchers_count"])
    network_count = int(contents["network_count"])
    subs_count = int(contents["subscribers_count"])
    n_forks = int(contents["forks_count"])
    n_issues = int(contents["open_issues_count"])
    top_lang = contents["language"]
    has_issues = True if contents["has_issues"] == "true" else False
    has_project = True if contents["has_projects"] == "true" else False
    has_wiki = True if contents["has_wiki"] == "true" else False
    stats.append(
        [
            size,
            stargazers_count,
            watchers_count,
            network_count,
            subs_count,
            n_forks,
            n_issues,
            top_lang,
            has_issues,
            has_project,
            has_wiki,
        ]
    )

    # -- license
    license_data = []
    try:
        license_type = contents["license"]["key"]
        license_node_id = contents["license"]["node_id"]
        license_data.append([license_type, license_node_id])
    except:
        license_data.append([np.nan, np.nan])

    result = user_info + owner_info + repo_info + stats + license_data
    final_result = __flatten(result)
    struct = RepoStruct(*final_result)

    return struct


def parse_json_commit(contents, json_prep=True):
    """Parses JSON file containing commit data

    Parameters
    ----------
    contents : dict
        Commit contents
    json_prep : bool
        json preprocessing. If set to False, contents must
        be in dict format. If True, contents is in str format

    Returns
    -------
    CommitStruct
        Object containing commit data
    """
    date, time = __parse_date_time(contents["commit"]["committer"]["date"])

    # -- Commits data
    commits_data = []
    sha_id = contents["sha"]
    committer = contents["commit"]["committer"]["name"]
    commit_date = date
    commit_time = time
    changes_html = contents["url"]
    comments_url = contents["comments_url"]
    commits_result = [
        sha_id,
        committer,
        commit_date,
        commit_time,
        changes_html,
        comments_url,
    ]
    commits_data.append(commits_result)

    # -- stats
    stats = []
    total_change = int(contents["stats"]["total"])
    total_addition = int(contents["stats"]["additions"])
    total_deletions = int(contents["stats"]["deletions"])

    files = contents["files"]  # do not add into struct
    changed_files = []
    for file_info in files:
        filename = file_info["filename"]
        code_url = file_info["raw_url"]
        pairs = tuple([filename, code_url])
        changed_files.append(pairs)

    files_changed = changed_files
    result_stats = [total_change, total_addition, total_deletions, files_changed]
    stats.append(result_stats)

    results = commits_data + stats
    final_results = __flatten(results)
    struct = CommitStruct(*final_results)

    return struct


def parse_json_languages(contents):
    """Parses the langue data obtained from the

    Parameters
    ----------
    contents : dict
        language contents

    Returns
    -------
    List
        list of tuples containing (lang, percent_score)
    """
    # filtering non-programming languages
    filtered_langs = [
        (lang, score)
        for lang, score in contents.items()
        if check_supported_language(lang)
    ]

    # summing all scores in the filtered lang getting the total score
    total_score = sum([score for _, score in filtered_langs])

    # creating a new tuple containing (lang, percent_score)
    langs = []
    for lang, score in filtered_langs:
        p_score = round((score / total_score) * 100, 3)
        langs.append((lang, p_score))

    return langs


# NOTE: Formatting functions
def __flatten(data_ls):
    """Flattents list of list into 1D

    Parameters
    ----------
    data_ls : list
        nested list data
    Returns
    -------
    list
        flatten data list
    """
    flatten_list = []
    for ls in data_ls:
        for data in ls:
            flatten_list.append(data)
    return flatten_list


def __parse_date_time(date_time_str):
    """Parses date time string

    Parameters
    ----------
    date_time_str : str
        date time string

    Return
    ------
    str
        date and time
    """
    date, time = date_time_str.replace("Z", "").split("T")
    return (date, time)
