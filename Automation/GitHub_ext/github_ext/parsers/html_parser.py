import validators
import requests
import numpy as np
from bs4 import BeautifulSoup
from github_ext.common.errors import *
from github_ext.apis.url_patterns import UrlPatterns
from github_ext.common.checks import check_url_format


def search_github_repo_links(links):
    """Searches for github repositories links

    Parameters
    ----------
    links : list
        list of links

    """
    pattern = UrlPatterns()
    github_link_pattern = pattern.github_link

    github_links = []
    for link in links:
        # check if the link provided is a valid link
        # -- this is based on the format and not if it is request-able or not
        check = check_url_format(link)
        if check is False:
            raise InvalidLinkError("Link provided contains invalid formatting")

        # searching for
        if link.startswith(github_link_pattern):
            github_links.append(link)
        elif github_link_pattern in link:
            github_links.append(link)
        else:
            continue

    return github_links


def get_all_github_links(url):
    """Gets all github links

    Parameters
    ----------
    html_conts : str
        HTML webpage contents
    """

    # parses html contents and searches for all link elements
    pattern = UrlPatterns()
    all_links = extract_all_links(url)

    if all_links is np.nan:
        return np.nan

    # iterating through all links and getting only github links
    all_gh_links = []
    for link in all_links:
        lower_url = link.lower()
        if lower_url.startswith(pattern.github_link):

            # checking url format
            url_check = validators.url(link)
            if not url_check:
                continue

            # storing github links
            all_gh_links.append(link)

    return all_gh_links


# HTML Parser module
def extract_all_links(url):
    """Extract all links in a webpage

    Parameters
    ----------
    url : str
        Link to web page

    Return
    ------
        list of all links in webpage
    """
    all_links = []

    # connecting to website
    # -- if the link does not work, return None
    try:
        r = requests.get(url, timeout=10.0).text
    except:
        # if any connection errors occurred return nan
        print("WARNING: unable to connect to {}".format(url))
        return np.nan

    # parses html contents and searches for all link elements
    soup = BeautifulSoup(r)
    link_elms = soup.findAll("a")

    if len(link_elms) == 0:
        return np.nan

    # iterating through every link and selecting github
    for link_elm in link_elms:

        # getting link text from link element
        href = link_elm.get("href")
        if href is None:
            continue

        # link format checking
        url_check = validators.url(href)
        if href is None or not url_check:
            continue

        all_links.append(href)

    return all_links
