import json
import unittest

from github_ext.apis.git_client import GitClient

# TODO: Create test data set
# TODO: Test all methods in this module
class TestGitClient(unittest.TestCase):
    """Tests all methods"""

    GITURL = json.load(open("./test.json", "r"))

    def test_ini(self, data=GITURL):
        """Tests initialization"""
        # passing list of git uri's
        for giturl in data:
            try:
                git = GitClient(giturl, temp_path="temp")
            except:
                self.assertRaises("Cannot init Git Client {}".format(giturl))

    def test_first_commit_date(self):
        """Obtaining any dates"""

        pass

    def test_last_commit():
        pass

    def test_latest_commit_date():
        pass

    def test_n_contribs():
        pass

    def test_dev_commit(self):
        pass

    def test_lang_composition(self):
        pass

    def test_top_lang(self):
        pass

    def test_remove_cache(self):
        pass

    def test_index_slicing(self):
        pass

    def test_iteration(self):
        pass
