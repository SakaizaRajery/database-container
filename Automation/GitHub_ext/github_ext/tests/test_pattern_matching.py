import unittest
import pickle
from github_ext.parsers.html_parser import search_github_repo_links


class UrlTests(unittest.TestCase):
    def test_github_link_finder(self):
        data_io = open("./testdata/githublists.pickle", "rb")
        github_links = pickle.load(data_io)

        # -- added two random websites to see if these are also selected
        # -- -- to test if it only looks for github links
        test_links = github_links + [
            "https://www.facebook.com",
            "https://www.youtube.com",
        ]

        validated_github_links = search_github_repo_links(test_links)
        self.assertEqual(len(test_links) - 2, len(validated_github_links))
