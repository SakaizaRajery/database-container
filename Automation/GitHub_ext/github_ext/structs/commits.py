from typing import NamedTuple


class CommitStruct(NamedTuple):
    sha_id: str
    committer: str
    commit_date: str
    commit_time: str
    changes_html: str
    comments_url: str
    total_change: int
    total_addition: int
    total_deletion: int
    files_changed: list
