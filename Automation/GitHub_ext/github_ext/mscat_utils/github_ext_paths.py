from pathlib import Path


class GithubExtPaths:
    """Contains paths that the github_ext requires to access"""

    def __init__(self):

        root_path = self._get_root()
        self.root = root_path

        # config paths
        # self.automation_dir = str(Path(self.root) / "Automation")
        # self.db_entries = str(Path(self.root) / "csvDump.csv")
        # self.configs = str(Path(self.automation_dir) / "configs")
        # self._automation_env_file = str(Path(self.automation_dir) / ".env")
        # self._env_file = str(Path(self.configs) / ".env")

        self.github_ext_path = str(Path(self.root) / "GitHub_ext")
        self.github_ext_configs = str(Path(self.github_ext_path) / "configs")
        self.unsupported_lang_config_path = str(
            Path(self.github_ext_configs) / "not_supported_langs.json"
        )

    def _get_root(self):
        """Creates main root path"""

        cwd_str = str(Path("./").absolute())
        # splitter = "database-container"
        splitter = "app"
        header = cwd_str.split(splitter)[0]
        if len(header) == 0:
            raise FileNotFoundError(
                "Unable to locate root folder, are you sure you are executing within the project"
            )
        root_path = Path(header) / splitter

        # checking if the root path is correct?
        if not root_path.is_dir():
            raise RuntimeError("Unable to find root path")

        return root_path
