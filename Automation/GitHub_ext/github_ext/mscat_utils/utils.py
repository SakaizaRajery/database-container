import os
import shutil
import validators
import numpy as np

# github_ext imports
from github_ext.common.errors import *


def clean_links(link):
    """Cleans the links stored in MSCAT. If links are missing specific
    portions.

    Parameters
    ----------
    link : str
        link from MSCAT

    Returns
    -------
    str or np.nan
        clean link. np.nan object is returned if link is invalid
    """
    # retuning np.nan if nan
    if link is np.nan:
        return np.nan

    # removing HTML styling
    link = link.replace("[Link](", "").replace(")", "")

    if link.startswith("http://") or link.startswith("https://"):
        if not validators.url(link):
            return np.nan
        return link

    # multi step check
    if "www." in link:
        link = link.replace("www.", "")

    if "https://" not in link or "http://" not in link:
        link = "https://{}".format(link)

    # check the format of the link
    url_check = validators.url(str(link))
    if not url_check:
        return np.nan

    return link


def style_links(url):
    """Apply HTML hyperlink formating to url strings

    Parameters:
    ----------
    url : str
        url link

    Returns
    -------
    str
        url string with HTML syling

    Raises:
    ------
    InvalidLinkError
        Occurs when an invalid formatted links is provided
    """
    # checking
    if not validators.url(url):
        InvalidLinkError("{} is not a valid URL".format(url))
    if url is np.nan:
        return np.nan

    formatted_link = "[Link]({})".format(url)
    return formatted_link


def link_search(link, pattern):
    """Link search with given link patter.

    Parameters
    ----------
    link : str
        provided url link
    pattern : str
        Pattern applied to link

    Returns
    -------
    bool
        True if pattern found in link else False
    """
    if link is np.nan:
        return False
    elif pattern in link:
        return True
    else:
        return False


def clear_cache(cache_dir="./tmp"):
    """All caching from perceval and other modules are stored in the ./tmp directory.
    This functions removes all the contents within the ./tmp folder and the ./tmp
    folder as well.

    Parameters
    ----------
    cache_dir = str
        directory path that stores cache. Default: "./tmp"
    """
    shutil.rmtree(cache_dir)
    os.rmdir(cache_dir)
