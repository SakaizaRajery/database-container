#!/bin/sh
text=`cat database_checksum.txt`
textArray=($text)
currentChkSum=`shasum sqlDump.sql`
currentChkSumArray=($currentChkSum)
if [[ ${textArray[0]} != ${currentChkSumArray[0]} ]]
then
    echo "Updating database"
    docker volume rm database-container_db_data
    echo "$currentChkSum" > database_checksum.txt
else
    echo "No changes made to database"
fi
