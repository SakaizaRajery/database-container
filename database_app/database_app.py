import dash
#import dash_auth
import dash_table
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

import pandas as pd
import numpy as np
import re
import json
from datetime import date
import sys, os
import psycopg2
from time import sleep
from maintainerFunctions import *

#f = open(os.getcwd()+"/database_app/pass.json")
path = os.getcwd()+"/Automation/databaseTools.csv"
#VALID_USERNAME_PASSWORD_PAIRS = json.load(f)
external_stylesheet = ['styles.css']

## Application Layout ##
app = dash.Dash(__name__, external_stylesheets = external_stylesheet)
#auth = dash_auth.BasicAuth(app, VALID_USERNAME_PASSWORD_PAIRS)
app.layout = html.Div(id = "main-container", children = [
	dcc.ConfirmDialog(id = "confirm", message = "Warning, you are about to delete a tool do you wish to continue with this operation?"),
	dcc.Interval(id = "interval-component", interval = 2*1000, n_intervals = 0),
	html.Datalist(id = "suggestions"),
	html.H1(children = "Metabolomics Database Updater", style = {"textAlign":"center", "textDecoration":"underline"}),
	dcc.RadioItems(id ='Radio_items' , options = [{"label":"Add New Tool", "value":"new"}, {"label":"Update Existing Tool", "value":"old"}, {"label":"Delete Tool", "value":"delete"}], value = "new", style=dict(display = 'inline-block'), labelStyle = dict(marginRight = '7px')),
	html.Div(id = "grid-container", children = [

	html.Div(id = "item1", children = [
	html.Label("Tool Name", style = dict(display = "block")),
	dcc.Input(id="tool_name", type = "text", value = None, placeholder = "Enter the tool's name", list = "suggestions" ,style = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor='white'), autoComplete = "off")
	]),
	html.Div(id = "item2", children = [
	html.Label("Year Released", style = dict(display = "block")),
	dcc.Input(id="year_released", type = "text", value = None, placeholder = "Enter the year released", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off"),
	html.P(id ='yearP', children="Year released must only contain numeric characters, and must contain 4 digits!",style=dict(display = 'none'))
	]),
	html.Div(id = "item3", children = [
	html.Label("Website", style = dict(display = "block")),
	dcc.Input(id="website", type = "text", value = None, placeholder = "Enter the tool's website", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item4", children = [
	html.Label("pub_doi", style = dict(display = "block")),
	dcc.Input(id="pub_doi", type = "text", value = None, placeholder = "Enter the tool's link to pub_doi", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off"),
	html.P(id = 'doiP', children = "Invalid DOI format!", style = dict(display = 'none'))
	]),
	html.Div(id = "item5", children = [
	html.Label("notes", style = dict(display = "block")),
	dcc.Input(id="notes", type = "text", value = None, placeholder = "Enter the notes where the tool was found", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item6", children = [
	html.Label("Last Updated", style = dict(display = "block")),
	dcc.Input(id="last_updated", type = "text", value = None, placeholder = "(YYYY-mm-dd)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off"),
	html.P(id = 'lastP', children = "Invalid date format YYYY-mm-dd, hyphens required!", style = dict(display = 'none'))
	]),
	html.Div(id = "item7", children = [
	html.Label("Annotation", style = dict(display = "block")),
	dcc.Input(id="annotation", type = "text", value = None, placeholder = "Enter the tool's annotation method", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item8", children = [
	html.Label("Approaches", style = dict(display = "block")),
	dcc.Input(id="approaches", type = "text", value = None, placeholder = "Enter the tool's approach(es)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item9", children = [
	html.Label("Inputs", style = dict(display = "block")),
	dcc.Input(id="inputs", type = "text", value = None, placeholder = "Enter the tool's input variables", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item10", children = [
	html.Label("Outputs", style = dict(display = "block")),
	dcc.Input(id="outputs", type = "text", value = None, placeholder = "Enter the tool's ouput variables", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item11", children = [
	html.Label("Instrument Data Type", style = dict(display = "block")),
	dcc.Input(id="instrument", type = "text", value = None, placeholder = "Enter the tool's instrument data type", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item12", children = [
	html.Label("Licenses", style = dict(display = "block")),
	dcc.Input(id="licenses", type = "text", value = None, placeholder = "Enter the tool's licenses", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "on")
	]),
	html.Div(id = "item13", children = [
	html.Label("Operating System", style = dict(display = "block")),
	dcc.Input(id="os", type = "text", value = None, placeholder = "Enter compatible operating system(s)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off"),
	html.P(id = 'osP', children = "Invalid OS included!", style = dict(display = 'none'))
	]),
	html.Div(id = "item14", children = [
	html.Label("Processing", style = dict(display = "block")),
	dcc.Input(id="proc", type = "text", value = None, placeholder = "Enter the tool's pre-processing methods", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item16", children = [
	html.Label("Statistical Analysis", style = dict(display = "block")),
	dcc.Input(id="statistical_analysis", type = "text", value = None, placeholder = "Enter the tool's statistical anaylsis method(s)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item17", children = [
	html.Label("User Interface", style = dict(display = "block")),
	dcc.Input(id="ui", type = "text", value = None, placeholder = "Enter the tool's available user interface(s)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off"),
	html.P(id = 'uiP', children = "Invalid UI included!", style = dict(display = 'none'))
	]),
	html.Div(id = "item18", children = [
	html.Label("Programming Language", style = dict(display = "block")),
	dcc.Input(id="programming_language", type = "text", value = None, placeholder = "Enter the tool's available programming language(s)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item19", children = [
	html.Label("Application", style = dict(display = "block")),
	dcc.Input(id="application", type = "text", value = None, placeholder = "Enter the tool's application(s)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item20", children = [
	html.Label("Container", style = dict(display = "block")),
	dcc.Input(id="containers", type = "text", value = None, placeholder = "Enter the tool's container url(s)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item21", children = [
	html.Label("Formats", style = dict(display = "block")),
	dcc.Input(id="formats", type = "text", value = None, placeholder = "Enter the tool's available file format(s)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item22", children = [
	html.Label("Functionality", style = dict(display = "block")),
	dcc.Input(id="functionality", type = "text", value = None, placeholder = "Enter the tool's functionality(ies)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "off")
	]),
	html.Div(id = "item23", children = [
	html.Label("Molecule Type", style = dict(display = "block")),
	dcc.Input(id="molecule_type", type = "text", value = None, placeholder = "Enter the tool's molecule type(s)", style = dict(margin='5px',width = '45%', display = 'inline-block'), autoComplete = "on")
	])
	]),
	html.Button("Submit", id = 'button', style = dict(display = "block")),
	html.Div(id = 'output-container', children = '')
	])

## Application callbacks ##
@app.callback(
	Output('suggestions', 'children'),
	[Input("Radio_items", 'value'),
	 Input("button", 'n_clicks')]
	)
def prePopulateTool(Radio_items = 'value', button = 'n_clicks'):
	if Radio_items == "old" or Radio_items == "delete":
		conn = psycopg2.connect("host=pgsql dbname=postgres user=postgres password=postgres")
		cur = conn.cursor()
		cur.execute("SELECT tool_name FROM main;")
		results = cur.fetchall()
		global tools
		children = [html.Option(i[0]) for i in results]
		tools = [i[0] for i in results]
		return(children)

@app.callback(
	[
	 Output('year_released', 'disabled'),
	 Output('year_released', 'style'),
	 Output('yearP', 'style'),
	 Output('website', 'disabled'),
	 Output('website', 'style'),
	 Output('pub_doi', 'disabled'),
	 Output('pub_doi', 'style'),
	 Output('doiP', 'style'),
	 Output('notes', 'disabled'),
	 Output('notes', 'style'),
	 Output('last_updated', 'disabled'),
	 Output('last_updated', 'style'),
	 Output('lastP','style'),
	 Output('annotation', 'disabled'),
	 Output('annotation', 'style'),
	 Output('approaches', 'disabled'),
	 Output('approaches', 'style'),
	 Output('inputs', 'disabled'),
	 Output('inputs', 'style'),
	 Output('outputs', 'disabled'),
	 Output('outputs', 'style'),
	 Output('instrument', 'disabled'),
	 Output('instrument', 'style'),
	 Output('licenses', 'disabled'),
	 Output('licenses', 'style'),
	 Output('os', 'disabled'),
	 Output('os', 'style'),
	 Output('osP', 'style'),
	 Output('proc', 'disabled'),
	 Output('proc', 'style'),
	 Output('statistical_analysis', 'disabled'),
	 Output('statistical_analysis', 'style'),
	 Output('ui', 'disabled'),
	 Output('ui', 'style'),
	 Output('uiP', 'style'),
	 Output('programming_language', 'disabled'),
	 Output('programming_language', 'style'),
	 Output('application', 'disabled'),
	 Output('application', 'style'),
	 Output('containers', 'disabled'),
	 Output('containers', 'style'),
	 Output('formats', 'disabled'),
	 Output('formats', 'style'),
	 Output('functionality', 'disabled'),
	 Output('functionality', 'style'),
	 Output('molecule_type', 'disabled'),
	 Output('molecule_type', 'style'),
	 ],
	 [Input('Radio_items', 'value'),
	  Input('year_released','value'),
	  Input('pub_doi', 'value'),
	  Input('last_updated', 'value'),
	  Input('os', 'value'),
	  Input('ui', 'value')]
	)
def disableInputs(Radio_items = 'value', year_released = 'value', pub_doi = 'value', last_updated = 'value', os = 'value', ui = 'value'):
	if Radio_items == "delete":
		booleans = [True, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'), 
					dict(display='none'),
					True, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'), 
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					dict(display='none'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					dict(display='none'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					dict(display='none'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					dict(display='none'),
					True, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey'),
					True,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'grey')]
		return(booleans)
	if Radio_items != "delete":
		# Year released restrictions
		if year_released == "":
			yearStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			yearpstyle = dict(display = 'none')
		elif year_released is not None and (not all(letter.isdigit() for letter in year_released) or len(year_released)!=4):
			yearStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'red')
			yearpstyle = dict(display = 'inline-block', color='red', fontSize = "15px")
		else:
			yearStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			yearpstyle = dict(display = 'none')
		# doi restrictions
		if pub_doi =="":
			doiStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			doipstyle = dict(display = 'none')
		elif pub_doi is not None:
			doiStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			doipstyle = dict(display = 'none')
			pattern = re.compile("\d\d.\d\d\d\d")
			doi_num = re.findall(".org/(.*?)[/%]", pub_doi)
			if not doi_num or not pattern.match(doi_num[0]):
				doiStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'red')
				doipstyle = dict(display = 'inline-block', color='red', fontSize='15px')
		#Last updated restrictions
		if last_updated=="":
			lastStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			lastpstyle = dict(display = 'none')
		elif last_updated is not None:
			lastStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			lastpstyle = dict(display = 'none')
			pattern = re.compile("\d\d\d\d-\d\d-\d\d")
			if not pattern.match(last_updated):
				lastStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'red')
				lastpstyle = dict(display = 'inline-block', color='red', fontSize='15px')
		else:
			lastStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			lastpstyle = dict(display = 'none')
		# OS restrictions
		os_list = os.split(", ")
		os_list = [x.lower() for x in os_list]
		os_types = ['windows', 'mac', 'linux']
		os_diff = list(set(os_list)-set(os_types))
		if os == "":
			osStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			ospstyle = dict(display = 'none')
		elif (os is not None) and (os_diff):
			osStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'red')
			ospstyle = dict(display = 'inline-block', color='red', fontSize = "15px")
		else:
			osStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			ospstyle = dict(display = 'none')
		#UI restrictions
		ui_list = ui.split(", ")
		ui_list = [x.lower() for x in ui_list]
		ui_types = ['cli', 'gui', 'web']
		ui_diff = list(set(ui_list)-set(ui_types))
		if ui == "":
			uiStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			uipstyle = dict(display = 'none')
		elif (ui is not None) and (ui_diff):
			uiStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'red')
			uipstyle = dict(display = 'inline-block', color='red', fontSize = "15px")
		else:
			uiStyle = dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
			uipstyle = dict(display = 'none')
  

		booleans = [False, 
					yearStyle,
					yearpstyle, 
					False, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'), 
					False,
					doiStyle,
					doipstyle,
					False,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False,
					lastStyle,
					lastpstyle,
					False,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False,
					osStyle,
					ospstyle,
					False, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False,
					uiStyle,
					uipstyle,
					False,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False, 
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white'),
					False,
					dict(margin='5px',width = '45%', display = 'inline-block', backgroundColor = 'white')
					]
		return(booleans)
	


@app.callback(
	[
	 Output('year_released', 'value'),
	 Output('website', 'value'),
	 Output('pub_doi', 'value'),
	 Output('notes', 'value'),
	 Output('last_updated', 'value'),
	 Output('annotation', 'value'),
	 Output('approaches', 'value'),
	 Output('inputs', 'value'),
	 Output('outputs', 'value'),
	 Output('instrument', 'value'),
	 Output('licenses', 'value'),
	 Output('os', 'value'),
	 Output('proc', 'value'),
	 Output('statistical_analysis', 'value'),
	 Output('ui', 'value'),
	 Output('programming_language', 'value'),
	 Output('application', 'value'),
	 Output('containers', 'value'),
	 Output('formats', 'value'),
	 Output('functionality', 'value'),
	 Output('molecule_type', 'value')
	 ],
	 [Input('Radio_items', 'value'),
	  Input("tool_name", 'value')]
	)
def prePropFields(Radio_items = 'value', tool_name = 'value'):
	if Radio_items != "old" and 'tools' not in locals():
		return(None, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","", "", "", "")
		raise PreventUpdate
	if Radio_items == "old":
		conn = psycopg2.connect("host=pgsql dbname=postgres user=postgres password=postgres")
		# Open a cursor to perform database operations
		cur = conn.cursor()
		cur.execute("SELECT tool_name FROM main;")
		results = cur.fetchall()
		tools = [i[0] for i in results]
		conn.commit()
		conn.close()
	if tool_name not in tools:
		return(None, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","", "", "", "")
	try:
		conn = psycopg2.connect("host=pgsql dbname=postgres user=postgres password=postgres")
		cur = conn.cursor()
		cur.execute("SELECT year_released FROM main WHERE tool_name = '{}';".format(tool_name))
		year_released = cur.fetchall()
		year_released = [i[0] for i in year_released][0]
		if year_released is None:
			year_released = ""

		cur.execute("SELECT website FROM main WHERE tool_name = '{}';".format(tool_name))
		website = cur.fetchall()
		website = [i[0] for i in website][0]

		cur.execute("SELECT pub_doi FROM publications WHERE tool_name = '{}';".format(tool_name))
		pub_doi = cur.fetchall()
		pub_doi = [i[0] for i in pub_doi]
		pub_doi = ", ".join(pub_doi)

		cur.execute("SELECT notes FROM main WHERE tool_name = '{}';".format(tool_name))
		notes = cur.fetchall()
		notes = [i[0] for i in notes][0]

		cur.execute("SELECT last_updated FROM main WHERE tool_name = '{}';".format(tool_name))
		last_updated = cur.fetchall()
		last_updated = [i[0] for i in last_updated][0]

		cur.execute("SELECT annotation FROM func_annotation WHERE tool_name = '{}';".format(tool_name))
		annotation = cur.fetchall()
		annotation = [i[0] for i in annotation]
		annotation = ", ".join(annotation)

		cur.execute("SELECT approach FROM approach WHERE tool_name = '{}';".format(tool_name))
		approaches = cur.fetchall()
		approaches = [i[0] for i in approaches]
		approaches = ", ".join(approaches)

		cur.execute("SELECT input FROM input_variables WHERE tool_name = '{}';".format(tool_name))
		inputs = cur.fetchall()
		inputs = [i[0] for i in inputs]
		inputs = ", ".join(inputs)

		cur.execute("SELECT output FROM output_variables WHERE tool_name = '{}';".format(tool_name))
		outputs = cur.fetchall()
		outputs = [i[0] for i in outputs]
		outputs = ", ".join(outputs)

		cur.execute("SELECT instrument FROM instrument_data WHERE tool_name = '{}';".format(tool_name))
		instrument = cur.fetchall()
		instrument = [i[0] for i in instrument]
		instrument = ", ".join(instrument)

		cur.execute("SELECT license FROM software_license WHERE tool_name = '{}';".format(tool_name))
		license = cur.fetchall()
		license = [i[0] for i in license]
		license = ", ".join(license)

		cur.execute("SELECT os FROM os_options WHERE tool_name = '{}';".format(tool_name))
		os = cur.fetchall()
		os = [i[0] for i in os]
		os = ", ".join(os)

		cur.execute("SELECT processing FROM func_processing WHERE tool_name = '{}';".format(tool_name))
		proc = cur.fetchall()
		proc = [i[0] for i in proc]
		proc = ", ".join(proc)


		cur.execute("SELECT stats_method FROM func_statistics WHERE tool_name = '{}';".format(tool_name))
		statistical_analysis = cur.fetchall()
		statistical_analysis = [i[0] for i in statistical_analysis]
		statistical_analysis = ", ".join(statistical_analysis)

		cur.execute("SELECT ui FROM ui_options WHERE tool_name = '{}';".format(tool_name))
		ui = cur.fetchall()
		ui = [i[0] for i in ui]
		ui = ", ".join(ui)

		cur.execute("SELECT language FROM programming_language WHERE tool_name = '{}';".format(tool_name))
		language = cur.fetchall()
		language = [i[0] for i in language]
		language = ", ".join(language)

		cur.execute("SELECT domain FROM application WHERE tool_name = '{}';".format(tool_name))
		applications = cur.fetchall()
		applications = [i[0] for i in applications]
		applications = ", ".join(applications)

		cur.execute("SELECT container_url FROM containers WHERE tool_name = '{}';".format(tool_name))
		container = cur.fetchall()
		container = [i[0] for i in container]
		container = ", ".join(container)

		cur.execute("SELECT format FROM file_formats WHERE tool_name = '{}';".format(tool_name))
		formats = cur.fetchall()
		formats = [i[0] for i in formats]
		formats = ", ".join(formats)

		cur.execute("SELECT function FROM functionality WHERE tool_name = '{}';".format(tool_name))
		function = cur.fetchall()
		function = [i[0] for i in function]
		function = ", ".join(function)

		cur.execute("SELECT moltype FROM molecule_type WHERE tool_name = '{}';".format(tool_name))
		molecule = cur.fetchall()
		molecule = [i[0] for i in molecule]
		molecule = ", ".join(molecule)


		return(str(year_released), website, pub_doi, notes, last_updated, annotation, approaches, inputs, outputs, instrument, license, os, proc, statistical_analysis, ui, language, applications, container, formats, function, molecule)
	except IndexError as e:
		print(e)


@app.callback(
	Output('button', 'n_clicks'),
	[Input('Radio_items', 'value')]
	)
def reset(Radio_items = 'value'):
	if Radio_items != 'delete':
		return None

@app.callback(
	Output('confirm', 'displayed'),
	[Input('button', 'n_clicks'),
	 Input('Radio_items', 'value')]
	)
def display_warning(n_clicks, Radio_items='value'):
	if n_clicks and Radio_items == 'delete':
		return True
	return False


@app.callback(
	Output('output-container', 'children'),
	[Input('button', 'n_clicks'),
	 Input('confirm', 'submit_n_clicks')],
	[State('Radio_items', 'value'),
	 State('tool_name', 'value'),
	 State('year_released', 'value'),
	 State('website', 'value'),
	 State('pub_doi', 'value'),
	 State('notes', 'value'),
	 State('last_updated', 'value'),
	 State('annotation', 'value'),
	 State('approaches', 'value'),
	 State('inputs', 'value'),
	 State('outputs', 'value'),
	 State('instrument', 'value'),
	 State('licenses', 'value'),
	 State('os', 'value'),
	 State('proc', 'value'),
	 State('statistical_analysis', 'value'),
	 State('ui', 'value'),
	 State('programming_language', 'value'),
	 State('application', 'value'),
	 State('containers', 'value'),
	 State('formats', 'value'),
	 State('functionality', 'value'),
	 State('molecule_type', 'value'),]
	)
def submit(n_clicks,
		   submit_n_clicks,
		   Radio_items = 'value',
		   tool_name = 'value',
		   year_released = 'value',
		   website = 'value',
		   pub_doi = 'value',
		   notes = 'value',
		   last_updated = 'value',
		   annotation = 'value',
		   approaches = 'value',
		   inputs = 'value',
		   outputs = 'value',
		   instrument = 'value',
		   licenses = 'value',
		   os = 'value', 
		   proc = 'value',
		   statistical_analysis = 'value',
		   ui = 'value',
		   programming_language = 'value',
		   application = 'value',
		   containers = 'value',
		   formats = 'value',
		   functionality = 'value',
		   molecule_type = 'value'):
	if n_clicks is None:
		raise PreventUpdate
	if last_updated is None:
		last_updated = ''
	if year_released is None:
		year_released = ''

	if Radio_items == 'new':
		try:
			addNewTool(tool_name = tool_name,
				db_params="host=pgsql dbname=postgres user=postgres password=postgres",
			year_released = year_released,
			website = website,
			pub_doi = pub_doi,
			notes = notes,
			last_updated = last_updated,
			annotation = annotation,
			approaches = approaches,
			inputs = inputs,
			outputs = outputs,
			instrument = instrument,
			licenses = licenses,
			os = os,
			proc = proc,
			statistic = statistical_analysis,
			ui = ui,
			programming = programming_language,
			applications = application,
			containers = containers,
			formats = formats,
			functionalities = functionality,
			molecules = molecule_type)
			return(dcc.ConfirmDialog(message = "Submission successful", displayed = True))
		except Exception as e:
			return (dcc.ConfirmDialog(message ="Error: Submission not successful.\n" + str(e), displayed = True))
	if Radio_items == 'old':

		try:
			addToolFeatures(tool_name = tool_name,
				db_params="host=pgsql dbname=postgres user=postgres password=postgres",
			year_released = year_released,
			website = website,
			pub_doi = pub_doi,
			notes = notes,
			last_updated = last_updated,
			annotation = annotation,
			approaches = approaches,
			inputs = inputs,
			outputs = outputs,
			instrument = instrument,
			licenses = licenses,
			os = os,
			proc = proc,
			statistic = statistical_analysis,
			ui = ui,
			programming = programming_language,
			applications = application,
			containers = containers,
			formats = formats,
			functionalities = functionality,
			molecules = molecule_type)
			return(dcc.ConfirmDialog(message = "Submission successful", displayed = True))
		except Exception as e:
			return (dcc.ConfirmDialog(message ="Error: Submission not successful.\n" + str(e), displayed = True))
	if Radio_items == 'delete':
		if submit_n_clicks:
			try:
				deleteTool(tool_name = tool_name, db_params="host=pgsql dbname=postgres user=postgres password=postgres")
				return(dcc.ConfirmDialog(message = "Submission successful", displayed = True))
			except Exception as e:
				return (dcc.ConfirmDialog(message ="Error: Submission not successful.\n" + str(e), displayed = True))
	else:
		return(dcc.ConfirmDialog(message ="Error: Tool Name is required!", displayed=True))


if __name__ == '__main__':
	app.run_server(host='0.0.0.0', port=8080, debug=True)
