# MSCAT - How to change application code and push to container images

## Preliminaries and local running

1. Install Docker
2. On a terminal login with `docker login registry.gitlab.com`
3. Clone code repository from gitlab
  
4. Check out images by pulling with `docker pull registry.gitlab.com/path/to/registry:$tag`

5. Pull and run container images with `docker compose -f dockerCompose.yaml up -d`
    - UI app is accessed through 127.0.0.1:80
    - Maintainer app is accessed through 127.0.0.1:81
    - Database is on 127.0.0.1:5432
6. When done, shut down containers with `docker compose down`

## Changing Code or Data and then releasing new container images

1. Pull from gitlab repo, start images to run (see step 5 above)
2. Change code or,
  - Change data using maintainer app or,
  - Change data via SQL
3. Commit and push code changes to repo
4. If data changed, generate dump (`pg_dump`) and replace in repository
5. Build image with `docker compose -f docker-compose.yaml up -d`
6. Tag image with `docker tag $imagename registry.gitlab.com/path/to/registry:$tag`
7. Push image with changed code `docker push registry.gitlab.com/path/to/registry:$tag`
8. Terminate container(s) with `docker compose down`
9. Check the new image by pulling with `docker pull registry.gitlab.com/path/to/registry:$tag`



