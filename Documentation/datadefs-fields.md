# MSCAT Data Definitions: Fields

Definitions and expectations of values in MSCAT database. Note: for most (all?) of these fields, the value is filled if explicitly indicated in the tool publication or tool documentation. For example, if a software tool literature or documentation does not explicitly indicate its application to a specific type of instrument data, that field is left blank.

**Tool Name**
: Name of the software tool

: e.g. MAVEN, cosmiq

**Website**
: URL of tool website or code repository

: e.g. https://lipidmaps.org, https://github.com/du-lab/mzmine3

**Last Updated**
:

**Publications**
: URL of publication DOI

**Programming Language**
: Programming language(s) the tool is written in

: e.g. R, Python, JavaScript

**License**
: Software license(s) under which the tool is distributed

: e.g. GPL-2, BSD

**Molecule Type**
: Type of 'omic(s) the tool is used for

: e.g. metabolomics for metabolites, lipidomics for lipids, proteomics for proteins

**OS**
: Operating system(s) the tool can be installed on and run

: e.g. Linux, Mac, Windows

**UI**
: Type of user interface(s) the tool is accessible in

: e.g. GUI, CLI, web

**Functionality**
: Category of function or analysis pipeline stage(s) the tool is applied in. We are classifying pipeline stages as

Pre-Processing -> Post-Processing -> Statistical Analysis -> Annotation

: but, Annotation may also happen before some of these stages so we are not imposing an order. Also we have other categories outside of these such as Database, Data Management, Workflow. See [datadefs-functionality.md] for definitions





